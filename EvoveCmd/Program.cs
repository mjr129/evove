﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Evove;
using Evove.Blocks;

namespace EvoveCmd
{
    class Program : IProgressObserver
    {
        private Population _population;
        private bool _stepThrough;
        private bool _unattended;

        enum EMode
        {
            DisplayParameters,
            DisplaySyntax,
            Normal,
            Debug,
        }

        static void Main( string[] args )
        {
            Program prog = new Program();
            prog.Start( args );
        }

        private void Start( string[] args )
        {
            EMode mode = EMode.Normal;
            EvoveParameters parameters = new EvoveParameters();

            string key = null;

            foreach (string arg in args)
            {
                if (arg != "")
                {
                    if (arg[0] == '-')
                    {
                        key = arg.Substring( 1 );
                    }
                    else
                    {
                        switch (key)
                        {
                            case "a":
                            case "assembly":
                                Console.WriteLine( "Loading: " + key );
                                Assembly.LoadFrom( key );
                                break;

                            case "m":
                            case "mode":
                                Console.WriteLine( "Mode switch: " + arg );
                                mode = (EMode)Enum.Parse( typeof( EMode ), arg );
                                break;

                            default:
                                Console.WriteLine( "Value set: " + key + " = " + arg );
                                SetProperty( parameters, key, arg );
                                break;
                        }
                    }
                }
            }

            parameters.ProgressWatcher = this;

            _population = new Population( parameters );

            switch (mode)
            {
                case EMode.Normal:
                    _unattended = true;
                    _stepThrough = false;
                    _population.Start();
                    break;

                case EMode.Debug:
                    _unattended = false;
                    _stepThrough = true;
                    _population.Start();
                    break;

                case EMode.DisplayParameters:
                    Console.WriteLine( parameters.ToString() );
                    break;

                case EMode.DisplaySyntax:
                    Console.WriteLine( _population.SyntaxManager.ToString() );
                    break;
            }

            Console.WriteLine( "Simulation ended." );
        }

        private static bool SetProperty( EvoveParameters parameters, string key, string arg )
        {
            Type type = parameters.GetType();

            FieldInfo field= type.GetField( key );

            if (field != null)
            {
                object value = ChangeType( arg, field.GetType() );
                field.SetValue( parameters, value );
                return true;
            }
            else
            {
                PropertyInfo property = type.GetProperty( key );

                if (property != null)
                {
                    object value = ChangeType( arg, field.GetType() );
                    field.SetValue( parameters, value );
                    return true;
                }
            }

            return false;
        }

        private static object ChangeType( string arg, Type type )
        {
            return Convert.ChangeType( arg, type );
        }

        void IProgressObserver.PopulationMessage( MessageArgs args )
        {
            Console.WriteLine( "SM. " + args.Message );
        }

        void IProgressObserver.OnWaitingForStep( StageChangeArgs args )
        {
            if (!_stepThrough)
            {
                return;
            }

            Console.Write( "ST. Step through at " + args.Stage.ToString() + " : " );

            bool loop = true;

            while (loop)
            {
                loop = false;
                ConsoleKeyInfo key = Console.ReadKey( true );

                switch (key.KeyChar)
                {
                    case 'H':
                        Console.WriteLine( "H/Q/S/R" );
                        Console.Write( " --> :" );
                        break;

                    case 'Q':
                        Console.WriteLine( "Quit" );
                        args.StopSimulation = true;
                        break;

                    case 'S':
                        Console.WriteLine( "Step" );
                        break;

                    case 'U':
                        if (_unattended)
                        {
                            Console.WriteLine( "Unattended (OFF)" );
                            _unattended = false;
                            Console.Write( " --> :" );
                        }
                        else
                        {
                            Console.WriteLine( "Unattended (ON)" );
                            _unattended = true;
                            Console.Write( " --> :" );
                        }

                        loop = true;
                        break;

                    case 'R':
                        Console.WriteLine( "Run" );
                        _stepThrough = false;
                        break;

                    default:
                        loop = true;
                        break;
                }
            }
        }      

        void IProgressObserver.OnError( ErrorArgs args )
        {
            Console.Error.WriteLine( "ER. " + args.Details );

            if (_unattended)
            {
                return;
            }

            Console.Write( "ES. Step through at ERROR : " );
            bool loop = true;

            while (loop)
            {
                loop = false;
                ConsoleKeyInfo key = Console.ReadKey( true );

                switch (key.KeyChar)
                {
                    case 'H':
                        Console.WriteLine( "H/I" );
                        Console.Write( " --> :" );
                        break;

                    case 'I':
                        Console.WriteLine( "Ignored" );
                        break;                 

                    default:
                        loop = true;
                        break;
                }
            }    
        }

        void IProgressObserver.OnSpecialEvent( ESpecialEvents stop )
        {
            Console.WriteLine( "EV. Event: " + stop.ToString() );
        }
    }
}
