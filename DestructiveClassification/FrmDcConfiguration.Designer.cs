﻿namespace DestructiveClassification
{
    partial class FrmDcConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._btnResume = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._txtResumeFrom = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._numUsages = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this._numCvIndex = new System.Windows.Forms.NumericUpDown();
            this._numCvSeed = new System.Windows.Forms.NumericUpDown();
            this._numCvNumber = new System.Windows.Forms.NumericUpDown();
            this._numDataHeader = new System.Windows.Forms.NumericUpDown();
            this._numClassColumn = new System.Windows.Forms.NumericUpDown();
            this._numClass1 = new System.Windows.Forms.NumericUpDown();
            this._numClass2 = new System.Windows.Forms.NumericUpDown();
            this._txtData = new System.Windows.Forms.TextBox();
            this._txtClass = new System.Windows.Forms.TextBox();
            this._btnAddEpoch = new System.Windows.Forms.Button();
            this._btnData = new System.Windows.Forms.Button();
            this._btnClass = new System.Windows.Forms.Button();
            this._lstEpoch = new System.Windows.Forms.ListBox();
            this.label13 = new System.Windows.Forms.Label();
            this._numClassHeader = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this._numDataFileRowHeader = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this._txtTrainingIds = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._btnCancel = new System.Windows.Forms.Button();
            this._btnOk = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._numUsages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numCvIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numCvSeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numCvNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numDataHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numClassColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numClass1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numClass2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numClassHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numDataFileRowHeader)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this._btnResume, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._txtResumeFrom, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this._numUsages, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 14);
            this.tableLayoutPanel1.Controls.Add(this._numCvIndex, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this._numCvSeed, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this._numCvNumber, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this._numDataHeader, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this._numClassColumn, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this._numClass1, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this._numClass2, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this._txtData, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this._txtClass, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this._btnAddEpoch, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this._btnData, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this._btnClass, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this._lstEpoch, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this._numClassHeader, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this._numDataFileRowHeader, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this._txtTrainingIds, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 15;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(820, 744);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // _btnResume
            // 
            this._btnResume.Location = new System.Drawing.Point(684, 8);
            this._btnResume.Margin = new System.Windows.Forms.Padding(8);
            this._btnResume.Name = "_btnResume";
            this._btnResume.Size = new System.Drawing.Size(128, 29);
            this._btnResume.TabIndex = 4;
            this._btnResume.Text = "Browse";
            this._btnResume.UseVisualStyleBackColor = true;
            this._btnResume.Click += new System.EventHandler(this._btnResume_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Resume from directory";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // _txtResumeFrom
            // 
            this._txtResumeFrom.Dock = System.Windows.Forms.DockStyle.Top;
            this._txtResumeFrom.Location = new System.Drawing.Point(253, 8);
            this._txtResumeFrom.Margin = new System.Windows.Forms.Padding(8);
            this._txtResumeFrom.Name = "_txtResumeFrom";
            this._txtResumeFrom.Size = new System.Drawing.Size(415, 29);
            this._txtResumeFrom.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = "Epoch trigger(s)";
            this.label2.Click += new System.EventHandler(this.label1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 157);
            this.label3.Margin = new System.Windows.Forms.Padding(8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(229, 21);
            this.label3.TabIndex = 0;
            this.label3.Text = "Usages before variable removal";
            this.label3.Click += new System.EventHandler(this.label1_Click);
            // 
            // _numUsages
            // 
            this._numUsages.DecimalPlaces = 2;
            this._numUsages.Dock = System.Windows.Forms.DockStyle.Top;
            this._numUsages.Location = new System.Drawing.Point(253, 157);
            this._numUsages.Margin = new System.Windows.Forms.Padding(8);
            this._numUsages.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this._numUsages.Minimum = new decimal(new int[] {
            1215752192,
            23,
            0,
            -2147483648});
            this._numUsages.Name = "_numUsages";
            this._numUsages.Size = new System.Drawing.Size(415, 29);
            this._numUsages.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 247);
            this.label4.Margin = new System.Windows.Forms.Padding(8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "Cross validation index";
            this.label4.Click += new System.EventHandler(this.label1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 292);
            this.label5.Margin = new System.Windows.Forms.Padding(8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(216, 21);
            this.label5.TabIndex = 0;
            this.label5.Text = "Cross validation random seed";
            this.label5.Click += new System.EventHandler(this.label1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 337);
            this.label6.Margin = new System.Windows.Forms.Padding(8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(192, 21);
            this.label6.TabIndex = 0;
            this.label6.Text = "Cross validation segments";
            this.label6.Click += new System.EventHandler(this.label1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 382);
            this.label7.Margin = new System.Windows.Forms.Padding(8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 21);
            this.label7.TabIndex = 0;
            this.label7.Text = "Data filename";
            this.label7.Click += new System.EventHandler(this.label1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 427);
            this.label8.Margin = new System.Windows.Forms.Padding(8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 21);
            this.label8.TabIndex = 0;
            this.label8.Text = "Class filename";
            this.label8.Click += new System.EventHandler(this.label1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 472);
            this.label9.Margin = new System.Windows.Forms.Padding(8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 21);
            this.label9.TabIndex = 0;
            this.label9.Text = "Data file header";
            this.label9.Click += new System.EventHandler(this.label1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 607);
            this.label10.Margin = new System.Windows.Forms.Padding(8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(127, 21);
            this.label10.TabIndex = 0;
            this.label10.Text = "Class file column";
            this.label10.Click += new System.EventHandler(this.label1_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 652);
            this.label11.Margin = new System.Windows.Forms.Padding(8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 21);
            this.label11.TabIndex = 0;
            this.label11.Text = "Class 1";
            this.label11.Click += new System.EventHandler(this.label1_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 697);
            this.label12.Margin = new System.Windows.Forms.Padding(8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 21);
            this.label12.TabIndex = 0;
            this.label12.Text = "Class 2";
            this.label12.Click += new System.EventHandler(this.label1_Click);
            // 
            // _numCvIndex
            // 
            this._numCvIndex.DecimalPlaces = 2;
            this._numCvIndex.Dock = System.Windows.Forms.DockStyle.Top;
            this._numCvIndex.Location = new System.Drawing.Point(253, 247);
            this._numCvIndex.Margin = new System.Windows.Forms.Padding(8);
            this._numCvIndex.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this._numCvIndex.Minimum = new decimal(new int[] {
            1215752192,
            23,
            0,
            -2147483648});
            this._numCvIndex.Name = "_numCvIndex";
            this._numCvIndex.Size = new System.Drawing.Size(415, 29);
            this._numCvIndex.TabIndex = 4;
            // 
            // _numCvSeed
            // 
            this._numCvSeed.DecimalPlaces = 2;
            this._numCvSeed.Dock = System.Windows.Forms.DockStyle.Top;
            this._numCvSeed.Location = new System.Drawing.Point(253, 292);
            this._numCvSeed.Margin = new System.Windows.Forms.Padding(8);
            this._numCvSeed.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this._numCvSeed.Minimum = new decimal(new int[] {
            1215752192,
            23,
            0,
            -2147483648});
            this._numCvSeed.Name = "_numCvSeed";
            this._numCvSeed.Size = new System.Drawing.Size(415, 29);
            this._numCvSeed.TabIndex = 4;
            // 
            // _numCvNumber
            // 
            this._numCvNumber.DecimalPlaces = 2;
            this._numCvNumber.Dock = System.Windows.Forms.DockStyle.Top;
            this._numCvNumber.Location = new System.Drawing.Point(253, 337);
            this._numCvNumber.Margin = new System.Windows.Forms.Padding(8);
            this._numCvNumber.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this._numCvNumber.Minimum = new decimal(new int[] {
            1215752192,
            23,
            0,
            -2147483648});
            this._numCvNumber.Name = "_numCvNumber";
            this._numCvNumber.Size = new System.Drawing.Size(415, 29);
            this._numCvNumber.TabIndex = 4;
            // 
            // _numDataHeader
            // 
            this._numDataHeader.DecimalPlaces = 2;
            this._numDataHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this._numDataHeader.Location = new System.Drawing.Point(253, 472);
            this._numDataHeader.Margin = new System.Windows.Forms.Padding(8);
            this._numDataHeader.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this._numDataHeader.Minimum = new decimal(new int[] {
            1215752192,
            23,
            0,
            -2147483648});
            this._numDataHeader.Name = "_numDataHeader";
            this._numDataHeader.Size = new System.Drawing.Size(415, 29);
            this._numDataHeader.TabIndex = 4;
            // 
            // _numClassColumn
            // 
            this._numClassColumn.DecimalPlaces = 2;
            this._numClassColumn.Dock = System.Windows.Forms.DockStyle.Top;
            this._numClassColumn.Location = new System.Drawing.Point(253, 607);
            this._numClassColumn.Margin = new System.Windows.Forms.Padding(8);
            this._numClassColumn.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this._numClassColumn.Minimum = new decimal(new int[] {
            1215752192,
            23,
            0,
            -2147483648});
            this._numClassColumn.Name = "_numClassColumn";
            this._numClassColumn.Size = new System.Drawing.Size(415, 29);
            this._numClassColumn.TabIndex = 4;
            // 
            // _numClass1
            // 
            this._numClass1.DecimalPlaces = 2;
            this._numClass1.Dock = System.Windows.Forms.DockStyle.Top;
            this._numClass1.Location = new System.Drawing.Point(253, 652);
            this._numClass1.Margin = new System.Windows.Forms.Padding(8);
            this._numClass1.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this._numClass1.Minimum = new decimal(new int[] {
            1215752192,
            23,
            0,
            -2147483648});
            this._numClass1.Name = "_numClass1";
            this._numClass1.Size = new System.Drawing.Size(415, 29);
            this._numClass1.TabIndex = 4;
            // 
            // _numClass2
            // 
            this._numClass2.DecimalPlaces = 2;
            this._numClass2.Dock = System.Windows.Forms.DockStyle.Top;
            this._numClass2.Location = new System.Drawing.Point(253, 697);
            this._numClass2.Margin = new System.Windows.Forms.Padding(8);
            this._numClass2.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this._numClass2.Minimum = new decimal(new int[] {
            1215752192,
            23,
            0,
            -2147483648});
            this._numClass2.Name = "_numClass2";
            this._numClass2.Size = new System.Drawing.Size(415, 29);
            this._numClass2.TabIndex = 4;
            // 
            // _txtData
            // 
            this._txtData.Dock = System.Windows.Forms.DockStyle.Top;
            this._txtData.Location = new System.Drawing.Point(253, 382);
            this._txtData.Margin = new System.Windows.Forms.Padding(8);
            this._txtData.Name = "_txtData";
            this._txtData.Size = new System.Drawing.Size(415, 29);
            this._txtData.TabIndex = 1;
            // 
            // _txtClass
            // 
            this._txtClass.Dock = System.Windows.Forms.DockStyle.Top;
            this._txtClass.Location = new System.Drawing.Point(253, 427);
            this._txtClass.Margin = new System.Windows.Forms.Padding(8);
            this._txtClass.Name = "_txtClass";
            this._txtClass.Size = new System.Drawing.Size(415, 29);
            this._txtClass.TabIndex = 1;
            // 
            // _btnAddEpoch
            // 
            this._btnAddEpoch.Location = new System.Drawing.Point(684, 53);
            this._btnAddEpoch.Margin = new System.Windows.Forms.Padding(8);
            this._btnAddEpoch.Name = "_btnAddEpoch";
            this._btnAddEpoch.Size = new System.Drawing.Size(128, 29);
            this._btnAddEpoch.TabIndex = 4;
            this._btnAddEpoch.Text = "Add";
            this._btnAddEpoch.UseVisualStyleBackColor = true;
            this._btnAddEpoch.Click += new System.EventHandler(this._btnAddEpoch_Click);
            // 
            // _btnData
            // 
            this._btnData.Location = new System.Drawing.Point(684, 382);
            this._btnData.Margin = new System.Windows.Forms.Padding(8);
            this._btnData.Name = "_btnData";
            this._btnData.Size = new System.Drawing.Size(128, 29);
            this._btnData.TabIndex = 4;
            this._btnData.Text = "Browse";
            this._btnData.UseVisualStyleBackColor = true;
            this._btnData.Click += new System.EventHandler(this._btnData_Click);
            // 
            // _btnClass
            // 
            this._btnClass.Location = new System.Drawing.Point(684, 427);
            this._btnClass.Margin = new System.Windows.Forms.Padding(8);
            this._btnClass.Name = "_btnClass";
            this._btnClass.Size = new System.Drawing.Size(128, 29);
            this._btnClass.TabIndex = 4;
            this._btnClass.Text = "Browse";
            this._btnClass.UseVisualStyleBackColor = true;
            this._btnClass.Click += new System.EventHandler(this._btnClass_Click);
            // 
            // _lstEpoch
            // 
            this._lstEpoch.Dock = System.Windows.Forms.DockStyle.Top;
            this._lstEpoch.FormattingEnabled = true;
            this._lstEpoch.ItemHeight = 21;
            this._lstEpoch.Location = new System.Drawing.Point(253, 53);
            this._lstEpoch.Margin = new System.Windows.Forms.Padding(8);
            this._lstEpoch.Name = "_lstEpoch";
            this._lstEpoch.Size = new System.Drawing.Size(415, 88);
            this._lstEpoch.TabIndex = 5;
            this._lstEpoch.KeyDown += new System.Windows.Forms.KeyEventHandler(this._lstEpoch_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 517);
            this.label13.Margin = new System.Windows.Forms.Padding(8);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 21);
            this.label13.TabIndex = 0;
            this.label13.Text = "Class file header";
            this.label13.Click += new System.EventHandler(this.label1_Click);
            // 
            // _numClassHeader
            // 
            this._numClassHeader.DecimalPlaces = 2;
            this._numClassHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this._numClassHeader.Location = new System.Drawing.Point(253, 517);
            this._numClassHeader.Margin = new System.Windows.Forms.Padding(8);
            this._numClassHeader.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this._numClassHeader.Minimum = new decimal(new int[] {
            1215752192,
            23,
            0,
            -2147483648});
            this._numClassHeader.Name = "_numClassHeader";
            this._numClassHeader.Size = new System.Drawing.Size(415, 29);
            this._numClassHeader.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 562);
            this.label14.Margin = new System.Windows.Forms.Padding(8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(123, 21);
            this.label14.TabIndex = 0;
            this.label14.Text = "Class file header";
            this.label14.Click += new System.EventHandler(this.label1_Click);
            // 
            // _numDataFileRowHeader
            // 
            this._numDataFileRowHeader.DecimalPlaces = 2;
            this._numDataFileRowHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this._numDataFileRowHeader.Location = new System.Drawing.Point(253, 562);
            this._numDataFileRowHeader.Margin = new System.Windows.Forms.Padding(8);
            this._numDataFileRowHeader.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this._numDataFileRowHeader.Minimum = new decimal(new int[] {
            1215752192,
            23,
            0,
            -2147483648});
            this._numDataFileRowHeader.Name = "_numDataFileRowHeader";
            this._numDataFileRowHeader.Size = new System.Drawing.Size(415, 29);
            this._numDataFileRowHeader.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 202);
            this.label15.Margin = new System.Windows.Forms.Padding(8);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(147, 21);
            this.label15.TabIndex = 0;
            this.label15.Text = "Cross validation IDs";
            this.label15.Click += new System.EventHandler(this.label1_Click);
            // 
            // _txtTrainingIds
            // 
            this._txtTrainingIds.Dock = System.Windows.Forms.DockStyle.Top;
            this._txtTrainingIds.Location = new System.Drawing.Point(253, 202);
            this._txtTrainingIds.Margin = new System.Windows.Forms.Padding(8);
            this._txtTrainingIds.Name = "_txtTrainingIds";
            this._txtTrainingIds.Size = new System.Drawing.Size(415, 29);
            this._txtTrainingIds.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this._btnCancel, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this._btnOk, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 744);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(820, 64);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // _btnCancel
            // 
            this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btnCancel.Location = new System.Drawing.Point(684, 8);
            this._btnCancel.Margin = new System.Windows.Forms.Padding(8);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(128, 48);
            this._btnCancel.TabIndex = 4;
            this._btnCancel.Text = "Cancel";
            this._btnCancel.UseVisualStyleBackColor = true;
            // 
            // _btnOk
            // 
            this._btnOk.Location = new System.Drawing.Point(540, 8);
            this._btnOk.Margin = new System.Windows.Forms.Padding(8);
            this._btnOk.Name = "_btnOk";
            this._btnOk.Size = new System.Drawing.Size(128, 48);
            this._btnOk.TabIndex = 4;
            this._btnOk.Text = "OK";
            this._btnOk.UseVisualStyleBackColor = true;
            this._btnOk.Click += new System.EventHandler(this._btnOk_Click);
            // 
            // FrmDcConfiguration
            // 
            this.AcceptButton = this._btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btnCancel;
            this.ClientSize = new System.Drawing.Size(820, 808);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmDcConfiguration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configuration";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._numUsages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numCvIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numCvSeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numCvNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numDataHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numClassColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numClass1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numClass2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numClassHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numDataFileRowHeader)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _txtResumeFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown _numUsages;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown _numCvIndex;
        private System.Windows.Forms.NumericUpDown _numCvSeed;
        private System.Windows.Forms.NumericUpDown _numCvNumber;
        private System.Windows.Forms.NumericUpDown _numDataHeader;
        private System.Windows.Forms.NumericUpDown _numClassColumn;
        private System.Windows.Forms.NumericUpDown _numClass1;
        private System.Windows.Forms.NumericUpDown _numClass2;
        private System.Windows.Forms.TextBox _txtData;
        private System.Windows.Forms.TextBox _txtClass;
        private System.Windows.Forms.Button _btnAddEpoch;
        private System.Windows.Forms.Button _btnData;
        private System.Windows.Forms.Button _btnClass;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button _btnCancel;
        private System.Windows.Forms.Button _btnOk;
        private System.Windows.Forms.Button _btnResume;
        private System.Windows.Forms.ListBox _lstEpoch;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown _numClassHeader;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown _numDataFileRowHeader;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox _txtTrainingIds;
    }
}