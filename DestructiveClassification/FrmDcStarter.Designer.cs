﻿namespace DestructiveClassification
{
    partial class FrmDcStarter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._btnCombineResults = new System.Windows.Forms.Button();
            this._btnRun = new System.Windows.Forms.Button();
            this._btnExploreResults = new System.Windows.Forms.Button();
            this._flpMain = new System.Windows.Forms.FlowLayoutPanel();
            this._btnOpenEvove = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this._flpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // _btnCombineResults
            // 
            this._btnCombineResults.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this._btnCombineResults.Dock = System.Windows.Forms.DockStyle.Top;
            this._btnCombineResults.FlatAppearance.BorderSize = 0;
            this._btnCombineResults.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveCaption;
            this._btnCombineResults.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this._btnCombineResults.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btnCombineResults.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._btnCombineResults.Location = new System.Drawing.Point(8, 263);
            this._btnCombineResults.Margin = new System.Windows.Forms.Padding(8);
            this._btnCombineResults.Name = "_btnCombineResults";
            this._btnCombineResults.Size = new System.Drawing.Size(608, 32);
            this._btnCombineResults.TabIndex = 0;
            this._btnCombineResults.Text = "Combine results";
            this._btnCombineResults.UseVisualStyleBackColor = false;
            this._btnCombineResults.Click += new System.EventHandler(this._btnCombineResults_Click);
            // 
            // _btnRun
            // 
            this._btnRun.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this._btnRun.Dock = System.Windows.Forms.DockStyle.Top;
            this._btnRun.FlatAppearance.BorderSize = 0;
            this._btnRun.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveCaption;
            this._btnRun.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this._btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btnRun.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._btnRun.Location = new System.Drawing.Point(8, 93);
            this._btnRun.Margin = new System.Windows.Forms.Padding(8);
            this._btnRun.Name = "_btnRun";
            this._btnRun.Size = new System.Drawing.Size(608, 32);
            this._btnRun.TabIndex = 0;
            this._btnRun.Text = "Run";
            this._btnRun.UseVisualStyleBackColor = false;
            this._btnRun.Click += new System.EventHandler(this._btnRun_Click);
            // 
            // _btnExploreResults
            // 
            this._btnExploreResults.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this._btnExploreResults.Dock = System.Windows.Forms.DockStyle.Top;
            this._btnExploreResults.FlatAppearance.BorderSize = 0;
            this._btnExploreResults.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveCaption;
            this._btnExploreResults.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this._btnExploreResults.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btnExploreResults.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._btnExploreResults.Location = new System.Drawing.Point(8, 178);
            this._btnExploreResults.Margin = new System.Windows.Forms.Padding(8);
            this._btnExploreResults.Name = "_btnExploreResults";
            this._btnExploreResults.Size = new System.Drawing.Size(608, 32);
            this._btnExploreResults.TabIndex = 0;
            this._btnExploreResults.Text = "Analyse";
            this._btnExploreResults.UseVisualStyleBackColor = false;
            this._btnExploreResults.Click += new System.EventHandler(this._btnExploreResults_Click);
            // 
            // _flpMain
            // 
            this._flpMain.Controls.Add(this._btnOpenEvove);
            this._flpMain.Controls.Add(this.label1);
            this._flpMain.Controls.Add(this._btnRun);
            this._flpMain.Controls.Add(this.label2);
            this._flpMain.Controls.Add(this._btnExploreResults);
            this._flpMain.Controls.Add(this.label3);
            this._flpMain.Controls.Add(this._btnCombineResults);
            this._flpMain.Controls.Add(this.label4);
            this._flpMain.Controls.Add(this.button1);
            this._flpMain.Controls.Add(this.label6);
            this._flpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this._flpMain.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this._flpMain.Location = new System.Drawing.Point(0, 0);
            this._flpMain.Name = "_flpMain";
            this._flpMain.Size = new System.Drawing.Size(628, 552);
            this._flpMain.TabIndex = 1;
            this._flpMain.WrapContents = false;
            // 
            // _btnOpenEvove
            // 
            this._btnOpenEvove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this._btnOpenEvove.Dock = System.Windows.Forms.DockStyle.Top;
            this._btnOpenEvove.FlatAppearance.BorderSize = 0;
            this._btnOpenEvove.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveCaption;
            this._btnOpenEvove.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this._btnOpenEvove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btnOpenEvove.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._btnOpenEvove.Location = new System.Drawing.Point(8, 8);
            this._btnOpenEvove.Margin = new System.Windows.Forms.Padding(8);
            this._btnOpenEvove.Name = "_btnOpenEvove";
            this._btnOpenEvove.Size = new System.Drawing.Size(608, 32);
            this._btnOpenEvove.TabIndex = 1;
            this._btnOpenEvove.Text = "Evove";
            this._btnOpenEvove.UseVisualStyleBackColor = false;
            this._btnOpenEvove.Click += new System.EventHandler(this._btnOpenEvove_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(32, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(32, 0, 8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(330, 21);
            this.label1.TabIndex = 3;
            this.label1.Text = "Close this window and open up Evove by itself";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(32, 133);
            this.label2.Margin = new System.Windows.Forms.Padding(32, 0, 8, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(524, 21);
            this.label2.TabIndex = 3;
            this.label2.Text = "Run your dataset through Evove using the currently programmed settings.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(32, 218);
            this.label3.Margin = new System.Windows.Forms.Padding(32, 0, 8, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(538, 21);
            this.label3.TabIndex = 3;
            this.label3.Text = "Open up a destructive classifier results folder and visually explore the results";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(32, 303);
            this.label4.Margin = new System.Windows.Forms.Padding(32, 0, 8, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(584, 42);
            this.label4.TabIndex = 3;
            this.label4.Text = "Combine two destructive classifier results folders, for cases where a single run " +
    "was halted and restarted part-way through.";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(8, 369);
            this.button1.Margin = new System.Windows.Forms.Padding(8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(608, 32);
            this.button1.TabIndex = 4;
            this.button1.Text = "Explore";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Gray;
            this.label6.Location = new System.Drawing.Point(32, 409);
            this.label6.Margin = new System.Windows.Forms.Padding(32, 0, 8, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(200, 21);
            this.label6.TabIndex = 5;
            this.label6.Text = "Open up Windows Explorer";
            // 
            // FrmDcStarter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(628, 552);
            this.Controls.Add(this._flpMain);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FrmDcStarter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Classifier";
            this._flpMain.ResumeLayout(false);
            this._flpMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _btnCombineResults;
        private System.Windows.Forms.Button _btnRun;
        private System.Windows.Forms.Button _btnExploreResults;
        private System.Windows.Forms.FlowLayoutPanel _flpMain;
        private System.Windows.Forms.Button _btnOpenEvove;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
    }
}

