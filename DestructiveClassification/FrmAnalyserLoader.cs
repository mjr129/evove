﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DestructiveClassification.Analysis;
using MGui.Helpers;
using TestApplication;

namespace DestructiveClassification
{
    public partial class FrmAnalyserLoader : Form
    {
        public static void ShowForm( IWin32Window owner )
        {
            using (FrmAnalyserLoader frm = new FrmAnalyserLoader())
            {
                frm.ShowDialog( owner );
            }
        }

        public FrmAnalyserLoader()
        {
            InitializeComponent();
        }

        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );

            if (!_txtFile.Prompt())
            {
                Close();
            }
        }

        private void _btnOk_Click( object sender, EventArgs e )
        {                              
            ELoadFlags flags = default( ELoadFlags );

            if (_chkFlipPredictions.Checked)
            {
                flags |= ELoadFlags.InvertPredictions;
            }

            if (_chkLoadCache.Checked)
            {
                flags |= ELoadFlags.LoadCache;
            }

            if (_chkSaveCache.Checked)
            {
                flags |= ELoadFlags.SaveCache;
            }

            FrmDcAnalyser.ShowAsync( this, _txtFile.Text, flags );
        }

        public void Send( int round )
        {
            label2.Text = "Loading round " + round;
            Application.DoEvents();
        }

        private void button1_Click( object sender, EventArgs e )
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                fbd.SelectedPath = Path.GetDirectoryName( _txtFile.Text );

                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    ListViewHelper.Populate( listView1, DirQuery.QuerySubDirectories( fbd.SelectedPath ) );
                }
            }
        }

        private void _btnCancel_Click( object sender, EventArgs e )
        {

        }
    }
}
