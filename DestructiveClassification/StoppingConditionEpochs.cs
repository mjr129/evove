﻿using System;
using System.Linq;
using DestructiveClassification.GpClass;
using Evove.Blocks;
using Evove.Operators.Stopping;

namespace DestructiveClassification
{
    internal class StoppingConditionEpochs : IStoppingCondition
    {
        private int NumberOfEpochs;

        Population _cachedPopulation;
        IDcWorld _cachedWorld;

        public StoppingConditionEpochs( int numberOfEpochs )
        {
            this.NumberOfEpochs = numberOfEpochs;
        }

        public bool CheckStop( StoppingConditionArgs args )
        {
            if (args.Population != _cachedPopulation)
            {
                _cachedPopulation = args.Population;
                _cachedWorld = (IDcWorld)_cachedPopulation.PerPopulationObjects.First( z => z is IDcWorld );
            }

            args.Population.SimpleLog( "(StoppingCondition " + args.Population.Round + ") " + (NumberOfEpochs - _cachedWorld.Epoch) + " epochs remaining" );
            return _cachedWorld.Epoch > NumberOfEpochs;
        }

        public void Reset()
        {
            // No action
        }
    }
}