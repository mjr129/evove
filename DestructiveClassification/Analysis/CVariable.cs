﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestructiveClassification.Analysis
{
    [Serializable]
    class CVariable
    {
        public List<CRound> Usages = new List<CRound>();
        public readonly int Index;
        public double MidPpm;
        internal double[] Intensities;
        internal double AvgIntensity;
        public double LogAvgIntensity;

        public CVariable( int z )
        {
            this.Index = z;
        }

        public override string ToString()
        {
            return "Variable " + Index;
        }
    }
}
