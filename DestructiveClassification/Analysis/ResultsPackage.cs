﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DestructiveClassification.GpClass;
using Evove;
using MGui.Helpers;
using MSerialisers;

namespace DestructiveClassification.Analysis
{
    [Serializable]
    class ResultsPackage
    {
        [NonSerialized]
        public FrmAnalyserLoader Owner;

        public string Directory;
        public int EliminationCount;
        public Dictionary<string, ObsName> ObservationsById = new Dictionary<string, ObsName>();
        public Dictionary<int, ObsName> ObservationsByInternalIndex = new Dictionary<int, ObsName>();
        public CVariable[] Variables;
        public CRound[] Rounds;
        public CRound[] Epochs;
        public string ConfigText;

        public ResultsPackage(  )
        {                       
        }                                                                                              

        internal static ResultsPackage New( FrmAnalyserLoader owner, string directory, ELoadFlags flags )
        {
            string snFn = Path.Combine( directory, DcConstants.FILE_CACHE );
            ResultsPackage rp;

            if (flags.Has( ELoadFlags.LoadCache ))
            {
                rp = SerialiserHelper.DeserialiseOrDefault<ResultsPackage>( snFn );
            }
            else
            {
                rp = null;
            }
                   
            if (rp != null)
            {
                rp.Owner = owner;
            }
            else
            {
                rp = new ResultsPackage();
                rp.Owner = owner;

                DynamicArray<CRound> rounds;
                DynamicArray<CVariable> variables = new DynamicArray<CVariable>( z => new CVariable( z ) );
                bool isOldFileMode;
                EFileMode isBooleanMode;
                rp.Directory = directory;
                EFitness fitness;
                EDataSet dataSet;

                ResultsAnalyser.Read_Configuration( directory, out rp.EliminationCount, out fitness, flags, out rp.ConfigText, out dataSet );
                ResultsAnalyser.Read_Fitness( directory, out rounds, out isOldFileMode, out isBooleanMode, flags );
                ResultsAnalyser.Read_Rounds( directory, rounds, variables, isOldFileMode, rp.EliminationCount, flags );
                ResultsAnalyser.Read_Predictions( directory, rounds, isBooleanMode, flags, fitness, rp._LoadProgress, rp.ObservationsById, rp.ObservationsByInternalIndex, dataSet );
                Calculate_Epochs( rounds, isOldFileMode, out rp.Rounds, out rp.Epochs, flags );
                Calculate_Variables( variables, out rp.Variables, flags );

                if (flags.Has( ELoadFlags.SaveCache ))
                {
                    SerialiserHelper.Serialise( snFn, rp );
                }
            }

            return rp;
        }

        void _LoadProgress( int round )
        {
            if ((round % 100) == 0)
            {
                Owner.Send( round );
            }
        }

        public ObsName GetObsByIndex( int x )
        {
            try
            {
                return ObservationsByInternalIndex[x];
            }
            catch
            {
                return null;
            }
        }

        private static void Calculate_Variables( DynamicArray<CVariable> variables, out CVariable[] variablesOut, ELoadFlags flags )
        {
            variablesOut = variables.TrimToCapacity().ToArray();

            for (int n = 0; n < variablesOut.Length; n++)
            {
                if (variablesOut[n] == null)
                {
                    variablesOut[n] = new CVariable( n );
                }
                else if (variablesOut[n].Index != n)
                {
                    throw new InvalidOperationException( "Variable order invalid." );
                }
            }

        }

        /// <summary>
        /// // Calculate epochs (given we know the epochs ends)
        /// </summary>
        /// <param name="crounds"></param>
        /// <param name="oldMode"></param>
        /// <param name="roundsOut"></param>
        /// <param name="epochsOut"></param>
        private static void Calculate_Epochs( DynamicArray<CRound> crounds, bool oldMode, out CRound[] roundsOut, out CRound[] epochsOut, ELoadFlags flags )
        {
            CRound[] rounds = crounds.TrimToCapacity().OrderBy( z => z.OverallIndex ).ToArray();

            int epochIndex = -1;
            CRound lastRoundInEpoch = null;

            foreach (CRound round in rounds.Reverse())
            {
                if (round.IsEndOfEpoch)
                {
                    epochIndex = round.EpochIndex;
                    lastRoundInEpoch = round;
                }

                if (oldMode)
                {
                    round.EpochIndex = epochIndex;
                }
                else
                {
                    EvoveHelper.Check( "round.EpochIndex", epochIndex == -1 || round.EpochIndex == epochIndex, "Epochs disordered." );
                }

                round.Epoch = lastRoundInEpoch;
            }

            // Calculate within-epoch indices (given we know the overall order and the epochs)
            // Also truncate the last set of rounds (the epoch that never finished, if there is one)
            int withinEpochIndex = 0;
            int stagnation = 0;
            Int32C stagnationBand = null;
            double delta = 0;

            for (int n = 0; n < rounds.Length; n++)
            {
                if (n == 0 || rounds[n].EpochIndex != rounds[n - 1].EpochIndex)
                {
                    // First round of epoch changed
                    withinEpochIndex = 0;
                    stagnation = 0;
                    stagnationBand = new Int32C();
                    delta = 0;
                }
                else
                {
                    withinEpochIndex++;
                    delta = rounds[n].Training.AverageScore - rounds[n - 1].Training.AverageScore;

                    if (rounds[n].Training.AverageScore != rounds[n - 1].Training.AverageScore)
                    {
                        stagnation = 0;
                        stagnationBand = new Int32C();
                    }
                    else
                    {
                        stagnation++;
                        stagnationBand.Value = stagnation;
                    }
                }
                rounds[n].Delta = delta;
                rounds[n].WithinEpochIndex = withinEpochIndex;
                rounds[n].Stagnation = stagnation;
                rounds[n].StagnationBand = stagnationBand;
            }

            rounds = rounds.Where( z => z.Epoch != null ).ToArray();

            if (rounds.Length == 0)
            {
                throw new InvalidOperationException( "It doesn't look like there are any complete epochs here." );
            }

            for (int n = 0; n < rounds.Length; n++)
            {
                if (rounds[n] == null)
                {
                    throw new InvalidOperationException( $"Missing round with index {n}." );
                }

                if (rounds[n].Epoch == null)
                {
                    throw new InvalidOperationException( $"Missing epoch for round with index {n}." );
                }

                if (n != 0 && rounds[n].OverallIndex != rounds[n - 1].OverallIndex + 1)
                {
                    throw new InvalidOperationException( $"Round with index {n} is not contiguous." );
                }

                if (n != 0 && (rounds[n].WithinEpochIndex != rounds[n - 1].WithinEpochIndex + 1) && !rounds[n - 1].IsEndOfEpoch)
                {
                    throw new InvalidOperationException( $"Round with index {n} is not contiguous with epoch." );
                }
            }

            roundsOut = rounds;
            epochsOut = rounds.Where( z => z.IsEndOfEpoch ).OrderBy( z => z.OverallIndex ).ToArray();
        }
    }
}
