﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGui.Datatypes;

namespace DestructiveClassification.Analysis
{
    [Serializable]
    class ObsName : IComparable<ObsName>
    {
        public string DisplayName;
        public readonly int Index;
        public readonly string Id;
        public readonly int ActualIndex;
        public static int _internalIndexCounter;
        public int PlotPosition;
        public bool Class;
        public int Age;

        public ObsName( string name, EDataSet set )
        {
            this.Id = name;
            this.Index = _internalIndexCounter;
            _internalIndexCounter++;

            switch (set)
            {
                case EDataSet.Unknown:
                    DisplayName = "V" + ActualIndex;
                    Class = false;
                    Age = -1;
                    break;

                case EDataSet.Meat:
                    string classs = name.Substring( 0, 1 );
                    Class = classs == "A";

                    string age = name.Substring( 1, name.Length - 3 );
                    Age = int.Parse( age );

                    DisplayName = (Class ? "W" : "C") + age;
                    ActualIndex = -1;
                    break;

                case EDataSet.Honey:
                    Class = name.StartsWith( "Ma" );
                    Age = 0;

                    DisplayName = name.Substring( 0, 2 );
                    ActualIndex = -1;
                    break;

                default:
                    throw new SwitchException( set );
            } 
        }

        int IComparable<ObsName>.CompareTo( ObsName other )
        {
            if (Age != -1)
            {
                int a = Class.CompareTo( other.Class );

                if (a == 0)
                {
                    a = Age.CompareTo( other.Age );
                }

                return a;
            }

            return this.DisplayName.CompareTo( other.DisplayName );
        }

        public override string ToString()
        {
            return DisplayName + " = " + Id;
        }
    }
}
