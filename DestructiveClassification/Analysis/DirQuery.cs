﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSerialisers;

namespace DestructiveClassification.Analysis
{
    class DirQuery
    {
        private readonly string Directory;
        public readonly string Name;
        private readonly int EliminationCount;
        private readonly EFitness Fitness;
        private readonly EFileMode IsBooleanMode;
        private readonly bool IsOldFileMode;
        public readonly int NumberOfRounds;
        public readonly string Error;
        public readonly EDataSet DataSet;

        public DirQuery( string dir )
        {
            this.Directory = dir;
            Name = Path.GetFileName( dir );
                                                          
            ELoadFlags flags = ELoadFlags.None;
            DynamicArray<CRound> rounds;
            string configText;

            try
            {
                ResultsAnalyser.Read_Configuration( dir, out EliminationCount, out Fitness, flags, out configText, out DataSet );
                ResultsAnalyser.Read_Fitness( dir, out rounds, out IsOldFileMode, out IsBooleanMode, flags );
                NumberOfRounds = rounds.Count;
            }
            catch  (Exception ex)
            {
                Error = ex.Message;
            }
        }

        public static IEnumerable<DirQuery> QuerySubDirectories( string parent )
        {  
            foreach (string dir in System.IO.Directory.GetDirectories( parent ))
            {
                yield return new DirQuery( dir );   
            }
        }

    }
}
