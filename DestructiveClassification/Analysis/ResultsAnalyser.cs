﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DestructiveClassification.GpClass;
using Evove;
using MGui.Datatypes;
using MGui.Helpers;
using MSerialisers;
using TestApplication.Data.Results;
using static TestApplication.FrmDcAnalyser;

namespace DestructiveClassification.Analysis
{
    static class ResultsAnalyser
    {
        private static StreamReader OpenFile( string fileName )
        {
            string gzFileName = fileName + ".gz";

            if (File.Exists( gzFileName ))
            {
                return new StreamReader( new GZipStream( new FileStream( gzFileName, FileMode.Open ), CompressionMode.Decompress ) );
            }
            else
            {
                return new StreamReader( fileName );
            }
        }

        /// <summary>
        /// // The "2SGP Fitness.csv" file gives the fitness values
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="rounds"></param>
        /// <param name="isOldFileMode"></param>
        public static void Read_Fitness( string directory, out DynamicArray<CRound> rounds, out bool isOldFileMode, out EFileMode isBooleanMode, ELoadFlags flags )
        {
            rounds = new DynamicArray<CRound>( z => new CRound( z ) );

            SpreadsheetReader reader = new SpreadsheetReader()
            {
                HasRowNames = false,
            };

            Spreadsheet<Cell> fitnessSS;
            string fn = Path.Combine( directory, DcConstants.FILE_FITNESS );

            using (var sr = OpenFile( fn ))
            {
                fitnessSS = reader.Read<Cell>( sr, fn );
            }

            if (fitnessSS.Columns.Contains( "training.tp" ))
            {
                isBooleanMode = EFileMode.Boolean;
            }
            else if (fitnessSS.Columns.Contains( "training.tp" ))
            {
                isBooleanMode = EFileMode.CorrectIncorrect;
            }
            else if (fitnessSS.Columns.Contains( "training.averagescore" ))
            {
                isBooleanMode = EFileMode.Score;
            }
            else
            {
                throw new InvalidOperationException( "Don't understand the file type." );
            }

            int cRound = fitnessSS.FindColumn( "round" );
            int cEpoch = fitnessSS.TryFindColumn( "epoch" );

            Spreadsheet<Cell>.Column cTTP = null
                                    , cTTN = null
                                    , cTFP = null
                                    , cTFN = null
                                    , cVTP = null
                                    , cVTN = null
                                    , cVFP = null
                                    , cVFN = null;
            Spreadsheet<Cell>.Column cTC = null
                                    , cTI = null
                                    , cVC = null
                                    , cVI = null;
            Spreadsheet<Cell>.Column cTS = null
                                   , cVS = null;

            switch (isBooleanMode)
            {
                case EFileMode.Boolean:
                    cTTP = fitnessSS.Columns["training.tp"];
                    cTTN = fitnessSS.Columns["training.tn"];
                    cTFP = fitnessSS.Columns["training.fp"];
                    cTFN = fitnessSS.Columns["training.fn"];
                    cVTP = fitnessSS.Columns["validation.tp"];
                    cVTN = fitnessSS.Columns["validation.tn"];
                    cVFP = fitnessSS.Columns["validation.fp"];
                    cVFN = fitnessSS.Columns["validation.fn"];
                    break;

                case EFileMode.CorrectIncorrect:
                    cTC = fitnessSS.Columns["training.correct"];
                    cTI = fitnessSS.Columns["training.incorrect"];
                    cVC = fitnessSS.Columns["validation.correct"];
                    cVI = fitnessSS.Columns["validation.incorrect"];
                    break;

                case EFileMode.Score:
                    // "round","epoch","training.averagescore","validation.averagescore","time"
                    cTS = fitnessSS.Columns["training.averagescore"];
                    cVS = fitnessSS.Columns["validation.averagescore"];
                    break;

                default:
                    throw new InvalidOperationException( "Unknown file mode." );
            }

            int lastEpochSeen = -1;
            CRound lastRound = null;
            isOldFileMode = cEpoch == -1;

            for (int row = 0; row < fitnessSS.NumRows; row++)
            {
                int round = fitnessSS[row, cRound] - 1; // again, Evove counts from 1
                int epoch = (cEpoch != -1) ? fitnessSS[row, cEpoch] : -1;
                CRound r = rounds[round];
                double trainingScore;
                double validationScore;

                switch (isBooleanMode)
                {
                    case EFileMode.Boolean:
                        int ttp = cTTP[row];
                        int ttn = cTTN[row];
                        int tfp = cTFP[row];
                        int tfn = cTFN[row];
                        int vtp = cVTP[row];
                        int vtn = cVTN[row];
                        int vfp = cVFP[row];
                        int vfn = cVFN[row];

                        trainingScore = (ttp + ttn) / (ttp + ttn + tfp + tfn);
                        validationScore = (vtp + vtn) / (vtp + vtn + vfp + vfn);
                        break;

                    case EFileMode.CorrectIncorrect:
                        int tc = cTC[row];
                        int ti = cTI[row];
                        int vc = cVC[row];
                        int vi = cVI[row];

                        trainingScore = (tc) / (tc + ti);
                        validationScore = (vc) / (vc + vi);
                        break;

                    case EFileMode.Score:
                        trainingScore = cTS[row];
                        validationScore = cVS[row];
                        break;

                    default:
                        throw new InvalidOperationException( "Unknown file mode." );
                }

                r._dbgTraining = trainingScore;
                r._dbgValidation = validationScore;

                if (epoch != lastEpochSeen)
                {
                    if (lastRound != null)
                    {
                        lastRound.IsEndOfEpoch = true;
                    }

                    lastEpochSeen = epoch;
                }

                r.EpochIndex = lastEpochSeen;

                lastRound = r;
            }
        }

        /// <summary>
        /// // The "2SGP Rounds.csv" file says which variables were removed in each round
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="rounds"></param>
        /// <param name="variables"></param>
        /// <param name="isOldFileMode"></param>
        /// <param name="eliminationCount"></param>
        public static void Read_Rounds( string directory, DynamicArray<CRound> rounds, DynamicArray<CVariable> variables, bool isOldFileMode, int eliminationCount, ELoadFlags flags )
        {
            int oldModeEpoch = 0;

            using (StreamReader sr = OpenFile( Path.Combine( directory, DcConstants.FILE_ROUNDS ) ))
            {
                while (!sr.EndOfStream)
                {
                    string[] e = sr.ReadLine().Split( ',' );

                    int round = int.Parse( e[0].Replace( "Round ", "" ) ) - 1; // -1 because Evove counts from 1, we'll count from 0 here, it's easier

                    CRound r = rounds[round];

                    if (isOldFileMode)
                    {
                        r.IsEndOfEpoch = true;
                        r.EpochIndex = oldModeEpoch++;
                    }

                    bool removalThisRound = false;
                    int removalIndex = 0;

                    for (int n = 1; n < e.Length; n++)
                    {
                        int eq = e[n].IndexOf( '=' );
                        string varNum;
                        string varUse;

                        if (eq == -1)
                        {
                            varNum = e[n];
                            varUse = null;
                        }
                        else
                        {
                            varNum = e[n].Substring( 0, eq );
                            varUse = e[n].Substring( eq + 1 );
                        }

                        int variable = int.Parse( varNum );

                        CVariable v = variables[variable];

                        v.Usages.Add( r );
                        r.Variables.Add( v );

                        if (varUse != null)
                        {
                            int varUseI = int.Parse( varUse );

                            if (v.Usages.Count != Math.Abs( varUseI ))
                            {
                                throw new InvalidOperationException( "Unexpected usage count." );
                            }

                            if (varUseI < 0)
                            {
                                if (eliminationCount != -varUseI)
                                {
                                    throw new InvalidOperationException( "Unexpected eliminationCount." );
                                }
                            }
                        }

                        if (v.Usages.Count == eliminationCount && !removalThisRound)
                        {
                            removalIndex++;
                            r.RemovalIndex = removalIndex;
                            removalThisRound = true;
                        }
                    }
                }
            }
        }

        private static ObsName GetObsByName( Dictionary<string, ObsName> obsById, string name )
        {
            ObsName r;

            obsById.TryGetValue( name, out r );

            return r;
        }

        private static int[] NamesToIndices( EDataSet set, Dictionary<string, ObsName> obsById, Dictionary<int, ObsName> obsByIi, string[] names )
        {
            int[] results = new int[names.Length];

            for (int n = 0; n < names.Length; n++)
            {
                var x = GetObsByName( obsById, names[n] );

                if (x == null)
                {
                    x = new ObsName( names[n], set );
                    obsById.Add( x.Id, x );
                    obsByIi.Add( x.Index, x );
                }

                results[n] = x.Index;
            }

            return results;
        }

        /// <summary>
        /// Reads a "prediction" from the predictions file.
        /// </summary>
        /// <param name="fileName">Filename (for error messages)</param>
        /// <param name="stream">The stream reader</param>
        /// <param name="set">Name of the set to read (training or validation)</param>
        /// <param name="round">The round number (returned only for the training set)</param>
        /// <param name="fileFormat">The format of the file to read</param>
        /// <returns>The <see cref="CaseResult"/> describing the prediction</returns>
        private static CaseResult ReadPredictionsFileLine( CaseResultPool pool, string fileName, StreamReader stream, string set, ref int round, EFileMode fileFormat, ELoadFlags flags, EFitness fitness, Action<int> progress, Dictionary<string, ObsName> obsById, Dictionary<int, ObsName> obsByIid, EDataSet dataSet)
        {
            // The file looks like this
            // (ROUND),"TRAINING","NAMES"    ,(VALUES) (legacy name = "INDICES")
            //        ,          ,"ACTUAL"   ,(VALUES)
            //        ,          ,"PREDICTED",(VALUES)
            //        ,          ,"ACTUAL"   ,(VALUES)
            //        ,          ,"SCORE"    ,(VALUES) (legacy name = "CORRECT")               

            // NAMES                                                                       
            string[] namesLine = SpreadsheetReader.Default.ReadFields( stream.ReadLine() );

            // -- Read ROUND
            if (set == "TRAINING")
            {
                round = int.Parse( namesLine[0] ) - 1; // Evove counts from 1
                EvoveHelper.Check( fileName, round != -1, "Expected round." );

                progress( round );
            }
            else
            {
                EvoveHelper.Check( fileName, namesLine[0] == "", "Expected empty." );
            }

            // -- Read SET
            EvoveHelper.Check( fileName, namesLine[1] == set, "Expected " + set + "." );

            // -- Test for any data
            bool noData = namesLine.Length == 4 && namesLine[3] == string.Empty;

            // - Read NAMES
            string[] names;

            if (namesLine[2] == "INDICES" || namesLine[2] == "NAMES")
            {
                if (!noData)
                {
                    names = namesLine.Skip( 3 ).ToArray();
                }
                else
                {
                    names = new string[0];
                }
            }
            else
            {
                throw new InvalidOperationException( "Expected INDICES or NAMES." );
            }

            int[] indices = NamesToIndices( dataSet, obsById, obsByIid, names );

            // ACTUAL VALUES
            string[] actualLine = SpreadsheetReader.Default.ReadFields( stream.ReadLine() );
            EvoveHelper.Check( fileName, actualLine[0] == "", "Expected empty." );
            EvoveHelper.Check( fileName, actualLine[1] == "", "Expected empty." );
            EvoveHelper.Check( fileName, actualLine[2] == "ACTUAL", "Expected ACTUAL." );

            string[] actual = noData ? new string[0] : (actualLine.Skip( 3 ).ToArray());

            // PREDICTED VALUES
            string[] predictedLine = SpreadsheetReader.Default.ReadFields( stream.ReadLine() );
            EvoveHelper.Check( fileName, predictedLine[0] == "", "Expected empty." );
            EvoveHelper.Check( fileName, predictedLine[1] == "", "Expected empty." );
            EvoveHelper.Check( fileName, predictedLine[2] == "PREDICTED", "Expected PREDICTED." );

            string[] predicted = noData ? new string[0] : (predictedLine.Skip( 3 ).ToArray());

            if (flags.Has( ELoadFlags.InvertPredictions ))
            {
                string[] temp = actual;
                actual = predicted;
                predicted = temp;
            }

            // SCORES
            string[] scoresLine = SpreadsheetReader.Default.ReadFields( stream.ReadLine() );
            EvoveHelper.Check( fileName, scoresLine[0] == "", "Expected empty." );
            EvoveHelper.Check( fileName, scoresLine[1] == "", "Expected empty." );
            EvoveHelper.Check( fileName, scoresLine[2] == "CORRECT" || scoresLine[2] == "SCORE", "Expected CORRECT." );

            double[] scores = noData ? new double[0] : (scoresLine.Skip( 3 ).Select( double.Parse ).ToArray());

            object[] actualObjects = actual.Select( z => StringToClassification( z, false, fitness ) ).ToArray();
            object[] predictedObjects = predicted.Select( z => StringToClassification( z, true, fitness ) ).ToArray();

            CaseResult result = pool.New( indices, actualObjects, predictedObjects );

            for (int n = 0; n < scores.Length; n++)
            {
                double myScore = result.Scores[n];
                double fileScore = scores[n];

                if (Math.Abs( myScore - fileScore ) > 0.0001)
                {
                    throw new InvalidOperationException( $"Score in file is {fileScore} but my score is {myScore}." );
                }
            }

            return result;
        }



        private static object ParseBoolean( string arg )
        {
            switch (arg)
            {
                case "True":
                case "1":
                    return true;

                case "False":
                case "0":
                    return false;

                default:
                    throw new SwitchException( "ParseBoolean", arg );
            }
        }

        /// <summary>
        /// Converts a string to a classification.
        /// </summary>                            
        private static object StringToClassification( string arg, bool isPrediction, EFitness fitness )
        {
            switch (fitness)
            {
                case EFitness.AgeAndClass:
                case EFitness.AgeAndClassV0:
                case EFitness.AgeAndClassV2:
                case EFitness.AgeAndClassV3:
                case EFitness.ClassWithStorage:
                    return AgePrediction.Parse( arg, isPrediction );

                case EFitness.Class:
                    return ParseBoolean( arg );

                case EFitness.Invalid:
                default:
                    throw new SwitchException( "StringToClassification", fitness );
            }  
        }

        private static bool DoublesMatch( double accuracy1, double accuracy2 )
        {
            if (double.IsNaN( accuracy1 ) && double.IsNaN( accuracy2 ))
            {
                return true;
            }
            else
            {
                return Math.Abs( accuracy1 - accuracy2 ) < 0.00001;
            }
        }

        /// <summary>
        /// // The "2SGP Predictions.csv" gives the predictions
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="rounds"></param>
        public static void Read_Predictions( string directory, DynamicArray<CRound> rounds, EFileMode isBooleanMode, ELoadFlags flags, EFitness fitness, Action<int> progress, Dictionary<string, ObsName> obsById, Dictionary<int, ObsName> obsByIid, EDataSet dataSet )
        {
            CaseResultPool pool = new CaseResultPool( fitness );

            string fn = Path.Combine( directory, DcConstants.FILE_PREDICTIONS );

            using (StreamReader sr = OpenFile( fn ))
            {
                while (!sr.EndOfStream)
                {
                    int round = -1;
                    CaseResult tr = ReadPredictionsFileLine( pool, fn, sr, "TRAINING", ref round, isBooleanMode, flags, fitness, progress, obsById, obsByIid, dataSet );
                    CaseResult va = ReadPredictionsFileLine( pool, fn, sr, "VALIDATION", ref round, isBooleanMode, flags, fitness, progress, obsById, obsByIid, dataSet );
                    string line = sr.ReadLine();
                    EvoveHelper.Check( fn, string.IsNullOrEmpty( line ), "Expected empty." );

                    // Replace the CaseResult (which just contains summary at present) with the full version

                    //EvoveHelper.Check( "accuracy", DoublesMatch( rounds[round]._dbgTraining, tr.AverageScore ), "Predictions and fitness files mismatch." );
                    //EvoveHelper.Check( "accuracy", DoublesMatch( rounds[round]._dbgValidation, va.AverageScore ), "Predictions and fitness files mismatch." );

                    //EvoveHelper.Check( "accuracy", tr.AverageScore >= 0 && tr.AverageScore <= 1, "accuracy invalid." );
                    //EvoveHelper.Check( "accuracy", va.AverageScore >= 0 && va.AverageScore <= 1, "accuracy invalid." );

                    rounds[round].Training = tr;
                    rounds[round].Validation = va;
                }
            }
        }

        /// <summary>
        /// // The "2SGP Configuration.csv" file gives the elimination count, we'll need this later
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="eliminationCount"></param>
        public static void Read_Configuration( string directory, out int eliminationCount, out EFitness fitness, ELoadFlags flags, out string configText, out EDataSet dataSet )
        {
            eliminationCount = 0;

            fitness = EFitness.Invalid;

            dataSet = EDataSet.Unknown;

            using (StreamReader sr = OpenFile( Path.Combine( directory, DcConstants.FILE_CONFIGURATION ) ))
            {
                configText = sr.ReadToEnd();
            }

            using (StringReader sr = new StringReader( configText ))
            {
                string line = sr.ReadLine();

                while (line != null)
                {
                    string[] e = SpreadsheetReader.Default.ReadFields( line );
                    string key = (e.Length >= 1) ? e[0] : "";
                    string value = (e.Length >= 2) ? e[1] : "";

                    if (key == "elimination count" || key == "UsagesBeforeVariableRemoval")
                    {
                        if (value == "int.MaxValue" || value == "\"int.MaxValue\"")
                        {
                            eliminationCount = int.MaxValue;
                        }
                        else
                        {
                            eliminationCount = int.Parse( value );
                        }
                    }

                    if (key == "DataFileName")
                    {
                        if (value.ToUpper().Contains( "MEAT" )) // TODO: Dirty hack
                        {
                            dataSet = EDataSet.Meat;
                        }
                        else if (value.ToUpper().Contains( "HONEY" ))
                        {
                            dataSet = EDataSet.Honey;
                        }
                    }

                    if (key == "FitnessFunction")
                    {
                        switch (value)
                        {
                            case "DcFitnessClassAndAgeFF0(InstantiationArgs*)":
                                fitness = EFitness.AgeAndClassV0;
                                break;

                            case "DcFitnessClassAndAge(InstantiationArgs*)":
                                fitness = EFitness.AgeAndClass;
                                break;

                            case "DcFitnessClassAndAgeFF2(InstantiationArgs*)":
                                fitness = EFitness.AgeAndClassV2;
                                break;

                            case "DcFitnessClassAndAgeFF3(InstantiationArgs*)":
                                fitness = EFitness.AgeAndClassV3;
                                break;

                            case "DcFitnessClassOnlyWithRecall(InstantiationArgs*)":
                                fitness = EFitness.ClassWithStorage;
                                break;

                            case "DcFitnessClassOnly(InstantiationArgs*)":
                                fitness = EFitness.Class;
                                break;

                            default:
                                throw new InvalidOperationException( "Unknown FitnessFunction: " + value );
                        }
                    }

                    line = sr.ReadLine();
                }
            }

            if (eliminationCount == 0)
            {
                throw new InvalidOperationException( "Could not retrieve EliminationCount from configuation file." );
            }

            if (fitness == EFitness.Invalid)
            {
                throw new InvalidOperationException( "Could not determine EFitness from configuation file." );
            }

            if (dataSet == EDataSet.Unknown)
            {
                throw new InvalidOperationException( "Could not determine EDataSet from configuation file." );
            }
        }
    }
}
