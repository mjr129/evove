﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestructiveClassification.Analysis
{
    enum EFileMode
    {
        Boolean,
        CorrectIncorrect,
        Score
    }
}
