﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DestructiveClassification.GpClass;
using MGui.Datatypes;
using MGui.Helpers;
using TestApplication.Data.Results;

namespace DestructiveClassification.Analysis
{
    class CaseResultPool
    {
        private EquivalencePool<HashedEnumerable<int[], int>> _indices = new EquivalencePool<HashedEnumerable<int[], int>>();
        private EquivalencePool<HashedEnumerable<object[], object>> _classes = new EquivalencePool<HashedEnumerable<object[], object>>();   
        private Dictionary<CaseResultSource, CaseResult> _caseResults = new Dictionary< CaseResultSource, CaseResult>();
        private readonly Func<object, object, double> _scoringFunction;
        private readonly Func<object, object, double> _bScoringFunction;

        class CaseResultSource
        {
            private object[] actualObjects;
            private CaseResultPool caseResultPool;
            private int[] indices;
            private object[] predictedObjects;

            public CaseResultSource( CaseResultPool caseResultPool, int[] indices, object[] actualObjects, object[] predictedObjects )
            {
                this.caseResultPool = caseResultPool;
                this.indices = indices;
                this.actualObjects = actualObjects;
                this.predictedObjects = predictedObjects;
            }

            public override bool Equals( object obj )
            {
                return Equals( obj as CaseResultSource ); 
            }

            public bool Equals( CaseResultSource iap )
            {
                // Compare equivalence, we have already pooled the arrays
                return this.actualObjects == iap.actualObjects
                    && this.indices == iap.indices
                    && this.predictedObjects == iap.predictedObjects;
            }

            public override int GetHashCode()
            {
                return ArrayHelper.HashTogether<object>( actualObjects, indices, predictedObjects );
            }
        }                                              

        public CaseResultPool( EFitness fitness )
        {
            _scoringFunction = GetScoringFunction( fitness );
            _bScoringFunction = GetBScoringFunction( fitness );
        }

        public CaseResult New( int[] indices, object[] actualObjects, object[] predictedObjects )
        {
            indices = _indices.Find(  indices ).Array;

            //if (!_classes.Has( actualObjects ))
            //{
            //    foreach (var a in _classes)
            //    {
            //        Debug.Assert( !Enumerable.SequenceEqual( actualObjects, a.Array ) );
            //    }
            //}

            actualObjects = _classes.Find( actualObjects ).Array;
            predictedObjects = _classes.Find( predictedObjects ).Array;

            CaseResultSource iap = new CaseResultSource( this, indices, actualObjects, predictedObjects );
            CaseResult cr;

            if (!_caseResults.TryGetValue( iap, out cr ))
            {
                cr = CaseResult.New<object>( indices, actualObjects, predictedObjects, _scoringFunction, _bScoringFunction );
                _caseResults.Add( iap, cr );
            }

            return cr;
        }

        /// <summary>
        /// Gets the scoring function.
        /// </summary>                                                                                    
        private Func<object, object, double> GetScoringFunction( EFitness fitness )
        {
            switch (fitness)
            {
                case EFitness.AgeAndClass:
                    return ( a, p ) => AgePrediction.Score( (AgePrediction)a, (AgePrediction)p );

                case EFitness.AgeAndClassV0:
                    return ( a, p ) => AgePrediction.ScoreVersion0( (AgePrediction)a, (AgePrediction)p );

                case EFitness.AgeAndClassV2:
                    return ( a, p ) => AgePrediction.ScoreVersion2( (AgePrediction)a, (AgePrediction)p );

                case EFitness.AgeAndClassV3:
                    return ( a, p ) => AgePrediction.ScoreVersion3( (AgePrediction)a, (AgePrediction)p );

                case EFitness.Class:
                    return ( a, p ) => DcFitnessClassOnly.Score( (bool)a, (bool)p );

                case EFitness.ClassWithStorage:
                    return ( a, p ) => DcFitnessClassOnlyWithRecall.Score( (AgePrediction)a, (AgePrediction)p );

                default:
                    throw new SwitchException( "GetScoringFunction", fitness );
            }     
        }

        /// <summary>
        /// Gets the B scoring function.
        /// </summary>                                                                                    
        private Func<object, object, double> GetBScoringFunction( EFitness fitness )
        {
            switch (fitness)
            {
                case EFitness.AgeAndClass:
                case EFitness.AgeAndClassV0:
                case EFitness.AgeAndClassV2:
                case EFitness.AgeAndClassV3:
                case EFitness.ClassWithStorage:
                    return ( a, p ) => DcFitnessClassOnlyWithRecall.Score( (AgePrediction)a, (AgePrediction)p );

                case EFitness.Class:
                    return ( a, p ) => DcFitnessClassOnly.Score( (bool)a, (bool)p );                            

                default:
                    throw new SwitchException( "GetScoringFunction", fitness );
            }
        }
    }
}
