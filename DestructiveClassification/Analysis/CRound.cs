﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Data;
using TestApplication.Data.Results;

namespace DestructiveClassification.Analysis
{
    [Serializable]
    class CRound
    {
        public List<CVariable> Variables = new List<CVariable>();
        public CaseResult Training = null;
        public CaseResult Validation = null;
        public readonly int OverallIndex = -1;
        public bool IsEndOfEpoch = false;
        public int RemovalIndex = -1;
        public int WithinEpochIndex = -1;
        public int EpochIndex = -1;
        public CRound Epoch;
        public int Stagnation;
        public Int32C StagnationBand;
        public double Delta;
        internal double _dbgTraining;
        internal double _dbgValidation;

        public CRound( int z )
        {
            this.OverallIndex = z;
        }

        public override string ToString()
        {
            return "ROUND-" + OverallIndex + " in EPOCH-" + EpochIndex + " (" + WithinEpochIndex + " within epoch with a score of " + Training?.AverageScore + ")";
        }
    }
}
