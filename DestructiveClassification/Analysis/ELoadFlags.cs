﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestructiveClassification.Analysis
{
    [Flags]
    enum ELoadFlags
    {
        None = 0,
        InvertPredictions = 1,
        SaveCache = 2,
        LoadCache = 4,
    }
}
