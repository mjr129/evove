﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DestructiveClassification.GpClass;
using Evove;
using Evove.Operators.Breed;
using Evove.Operators.Breed.Crossover;
using Evove.Operators.Breed.Mutation;
using Evove.Operators.SelectionMethods;
using Evove.Operators.Stopping;
using Evove.Operators.Termination;
using MGui.Helpers;
using TestApplication;

namespace DestructiveClassification
{
    internal static class ParameterBox
    {
        private static DataSetConfiguration Meat()
        {
            return new DataSetConfiguration()
            {
                DataFileName = @"E:\MJR\Data\Meat\data.csv",
                ClassFileName = @"E:\MJR\Data\Meat\info.csv",
                DataFileHeader = true,
                DataFileRowNames = true,
                ClassFileHeader = true,
                ClassFileClassColumn = "type",
                ClassFileTimeColumn = "day",
                ClassTrue = 1,
                ClassFalse = 2,
            };
        }

        private static DataSetConfiguration Honey()
        {
            return new DataSetConfiguration()
            {
                DataFileName = @"E:\MJR\Data\HoneyTwo\intensities.csv",
                ClassFileName = @"E:\MJR\Data\HoneyTwo\obsinfo.csv",
                DataFileHeader = true,
                DataFileRowNames = true,
                ClassFileHeader = true,
                ClassFileClassColumn = "class",
                ClassFileTimeColumn = null,
                ClassTrue = 1,
                ClassFalse = 0,
            };
        }            

        public static EvoveParameters New(  EDataSet eDataSet, ERemoval eRemoval, EEpoccher eEpoccher, EStopping eStopping, EFitness eFitness )
        {
            StringBuilder sb = new StringBuilder();
            DataSetConfiguration dataSet;
            int removal;
            IStoppingCondition epoccher;
            IStoppingCondition stopping;
            string filePrefix = Path.Combine( Application.StartupPath, "Runs\\" + FileHelper.DateAndTimeFile() + " - " );
            Type fitness;
            ECvMode cvMode;

            switch (eDataSet)
            {
                case EDataSet.Meat:
                    sb.Append( "Meat" );
                    dataSet = Meat();
                    cvMode = ECvMode.KeepInitial;
                    break;

                case EDataSet.Honey:
                    sb.Append( "Honey" );
                    dataSet = Honey();
                    cvMode = ECvMode.KeepInitial;
                    break;       

                default:
                    return null;
            }

            switch (eFitness)
            {
                case EFitness.AgeAndClass:
                    fitness = typeof(DcFitnessClassAndAge);
                    sb.Append( ", AgeAndClass" );
                    break;

                case EFitness.AgeAndClassV0:
                    fitness = typeof( DcFitnessClassAndAgeFF0 );
                    sb.Append( ", AgeAndClassV0" );
                    break;

                case EFitness.AgeAndClassV2:
                    fitness = typeof( DcFitnessClassAndAgeFF2 );
                    sb.Append( ", AgeAndClassV2" );
                    break;

                case EFitness.AgeAndClassV3:
                    fitness = typeof( DcFitnessClassAndAgeFF3 );
                    sb.Append( ", AgeAndClassV3" );
                    break;

                case EFitness.Class:
                    fitness = typeof( DcFitnessClassOnly );
                    sb.Append( ", Class" );
                    break;

                case EFitness.ClassWithStorage:
                    fitness = typeof( DcFitnessClassOnlyWithRecall );
                    sb.Append( ", ClassWithStorage" );
                    break;

                default:
                    return null;
            }

            switch (eRemoval)
            {
                case ERemoval.None:
                    sb.Append( ", no removal" );
                    removal = int.MaxValue;
                    break;

                case ERemoval.Five:
                    sb.Append( ", removal after 5" );
                    removal = 5;
                    break;

                default:
                    return null;
            }

            switch (eEpoccher)
            {
                case EEpoccher.Fixed250:
                    sb.Append( ", epochs every 250" );
                    epoccher = new StoppingConditions.Rounds( 250 );
                    break;

                // Bugs: Old names left in to show what they did!
                //case EEpoccher.Stagnation250:
                //    sb.Append( ", epochs after stagnation 250" );
                //    epoccher = new StoppingConditions.Rounds( 250 );  // <-- Bug
                //    break;

                //case EEpoccher.Stagnation1000:
                //    sb.Append( ", epochs after stagnation 1000" );
                //    epoccher = new StoppingConditions.Rounds( 1000 ); // <-- Bug
                //    break;

                case EEpoccher.StagnationF250:
                    sb.Append( ", epochs after stagnation 250" );
                    epoccher = new StoppingConditions.Stagnation( 250 );
                    break;

                case EEpoccher.StagnationF1000:
                    sb.Append( ", epochs after stagnation 1000" );
                    epoccher = new StoppingConditions.Stagnation( 1000 );
                    break;

                case EEpoccher.Fixed2000:
                    sb.Append( ", epochs every 2000" );
                    epoccher = new StoppingConditions.Rounds( 2000 );
                    break;

                case EEpoccher.Fixed5000:
                    sb.Append( ", epochs every 5000" );
                    epoccher = new StoppingConditions.Rounds( 5000 );
                    break;

                default:
                    return null;
            }

            switch (eStopping)
            {
                case EStopping.Never:
                    sb.Append( ", manual stop" );
                    stopping = new StoppingConditions.Never();
                    break;

                case EStopping.OneDay:
                    sb.Append( ", stop after 1 day" );
                    stopping = new StoppingConditions.Time( new TimeSpan( 1, 0, 0, 0 ) );
                    break;

                case EStopping.TwoDays:
                    sb.Append( ", stop after 2 days" );
                    stopping = new StoppingConditions.Time( new TimeSpan( 1, 0, 0, 0 ) );
                    break;

                case EStopping.H28:
                    sb.Append( ", stop after 28 hours" );
                    stopping = new StoppingConditions.Time( new TimeSpan( 28, 0, 0 ) );
                    break;

                case EStopping.SixHours:
                    sb.Append( ", stop after 6 hours" );
                    stopping = new StoppingConditions.Time( new TimeSpan( 6, 0, 0 ) );
                    break;

                case EStopping.E50:
                    sb.Append( ", stop after 50 epochs" );
                    stopping = new StoppingConditionEpochs( 50 );
                    break;

                default:
                    return null;
            }

            EvoveHelper.Check( nameof( DataSetConfiguration.DataFileName ), File.Exists( dataSet.DataFileName ), "DATA_FILE file missing." );
            EvoveHelper.Check( nameof( DataSetConfiguration.ClassFileName ), File.Exists( dataSet.ClassFileName ), "INFO_FILE file missing." );

            DcConfig config = Config( dataSet, epoccher, removal, cvMode );

            sb.Append( ", V2" );
            // V1: Removal of DoubleToInt function because it was causing variable bias
            // V2: Fix of variable biasing function "Average"
            //     Addition of boolean function "And3"
            //     Addition of boolean function "Or3"
            //     Addition of boolean function "Ternary"
            //     Addition of double function "Interpolate"

            EvoveParameters paramz = X( config, filePrefix, sb.ToString(), stopping, fitness );

            return paramz;
        }

        private static DcConfig Config( DataSetConfiguration dataSet, IStoppingCondition epocher, int removal, ECvMode cvMode )
        {
            return new DcConfig()
            {
                ResumeFromDirectory = @"",
                EpochTrigger = new[] { epocher },
                UsagesBeforeVariableRemoval = removal,
                CvMode = cvMode,
                CvModeStart = ECvModeStart.TenFold,
                TrainingIds = null,
                DataSet = dataSet,
            };
        }

        private static EvoveParameters X( DcConfig config, string filePrefix, string title, IStoppingCondition stopper, Type fitness)
        {
            return new EvoveParameters()
            {
                Title = title,
                ContinueFromFile = null,
                NumberOfDemes = 1,
                NumberOfElites = 1,
                NumberOfEvaluatorThreads = 0,
                NumberOfBreederThreads = 0,
                IndividualsPerDeme = 200,
                SelectionMethod = new SelectionMethods.FitnessProportional(),
                StoppingConditions = new[] { stopper },
                FitnessFunction = fitness,
                AdditionalFunctions = new Factory<InstantiationArgs>[0],
                OutputFolder = filePrefix + title,
                MaxTreeDepth = 5,
                AllowCaching = false,
                SupportDebugging = true,
                CheckForCacheErrors = false,
                NumberOfTestCases = 0,
                TerminationOperators = new ITerminationOperator[] { new TerminationOperators.Time( 5000 ) },
                MigrationOperator = null,
                Properties = new[] { config },
                ProgressWatcher = null,
                AbandonPerEvaluationObjects = false,
                RandomSeed = 1,
                SupportMethodOverloads = false,
                BreedingMethod = new BreedOperators.CrossoverAndMutation(
                                                   crossover: new CrossoverOperators.CommonPoint(),
                                                   chanceOfCrossover: 0.3,
                                                   mutation: new MutationOperators.PerNode( chanceOfNodeMutation: 0.05,
                                                                                            chanceOfInsertion: 0.33,
                                                                                            chanceOfReordering: 0.33,
                                                                                            chanceOfReplacement: 0.33 ),
                                                   chanceOfChildMutation: 0.92,
                                                   chanceOfCloneMutation: 0.92 ),
                Set = EIs.Tree,
            };
        }
    }

    /// <summary>
    /// Fitness methods
    /// If you change this update the following:
    ///     <see cref="ParameterBox.New"/>
    ///     <see cref="FrmDcAnalyser.Read_Configuration"/>
    /// </summary>
    public enum EFitness
    {
        Invalid,
        AgeAndClass,
        AgeAndClassV0,
        Class,
        ClassWithStorage,
        AgeAndClassV3,
        AgeAndClassV2
    }

    public enum EDataSet
    {
        Unknown,
        Meat,
        Honey,
    }

    public enum ERemoval
    {
        None,
        Five
    }

    public enum EEpoccher
    {
        //Stagnation250,
        //Stagnation1000,
        StagnationF250,
        StagnationF1000,
        Fixed250,
        Fixed2000,
        Fixed5000,
    }

    public enum EStopping
    {
        Never,
        OneDay,
        TwoDays,
        H28,
        SixHours,
        ThreeHours,
        E50,
    }
}
