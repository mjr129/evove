﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Evove.Operators.Stopping;
using EvoveUi;
using MGui.Controls;
using MGui.Helpers;

namespace DestructiveClassification
{
    internal partial class FrmDcConfiguration : Form
    {
        private          DcConfig            _result;
        private readonly CtlBinder<DcConfig> _binder = new CtlBinder<DcConfig>();

        public static DcConfig Show( IWin32Window owner, DcConfig config )
        {
            using (FrmDcConfiguration frm = new FrmDcConfiguration(config))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    return frm._result;
                }

                return null;
            }
        }

        public FrmDcConfiguration( DcConfig config )
        {
            InitializeComponent();
            this.Icon = Evove.Properties.Resources.Icon;

            //_binder.Bind( _numCvIndex,           z => z.UNUSED_CrossValidationIndex         );
            //_binder.Bind( _numCvSeed,            z => z.UNUSED_CrossValidationRandomSeed    );
            _binder.Bind( _txtTrainingIds,       z => z.TrainingIds                  );
            _binder.Bind( _numClass1,            z => z.DataSet.ClassTrue               );
            _binder.Bind( _numClass2,            z => z.DataSet.ClassFalse               );
            _binder.Bind( _numClassColumn,       z => z.DataSet.ClassFileClassColumn );
            _binder.Bind( _numClassHeader,       z => z.DataSet.ClassFileHeader      );
            _binder.Bind( _txtClass,             z => z.DataSet.ClassFileName        );
            _binder.Bind( _numDataHeader,        z => z.DataSet.DataFileHeader       );
            _binder.Bind( _txtData,              z => z.DataSet.DataFileName         );
            _binder.Bind( _numDataFileRowHeader, z => z.DataSet.DataFileRowNames     );
            //_binder.Bind( _numCvNumber,          z => z.UNUSED_NumberOfCrossValidations     );
            _binder.Bind( _txtResumeFrom,        z => z.ResumeFromDirectory          );
            _binder.Bind( _numUsages,            z => z.UsagesBeforeVariableRemoval  );

            _lstEpoch.Items.AddRange( config.EpochTrigger ?? new object[0] );

            _binder.Read( config ?? new DcConfig() );
            _binder.SetTarget( new DcConfig() { DataSet = new TestApplication.DataSetConfiguration() }  );
        }

        private void _btnOk_Click( object sender, EventArgs e )
        {
            try
            {
                _result = _binder.Commit();
                _result.EpochTrigger = _lstEpoch.Items.Cast<IStoppingCondition>().ToArray();

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show( this, "Fix this problem: " + ex.Message );
            }
        }

        private void label1_Click( object sender, EventArgs e )
        {

        }

        private void _btnResume_Click( object sender, EventArgs e )
        {
            FileHelper.Browse( _txtResumeFrom, "All files (*.*)|*.*" );
        }

        private void _btnAddEpoch_Click( object sender, EventArgs e )
        {
            object result;

            if (FrmValueEditor.Show( this, FrmValueEditor.ESet.Object, new[] { typeof( IStoppingCondition ) }, out result ))
            {
                _lstEpoch.Items.Add( result );
            }
        }

        private void _btnData_Click( object sender, EventArgs e )
        {
            FileHelper.Browse( _txtData, "All files (*.*)|*.*" );
        }

        private void _btnClass_Click( object sender, EventArgs e )
        {
            FileHelper.Browse( _txtResumeFrom, "All files (*.*)|*.*" );
        }

        private void _lstEpoch_KeyDown( object sender, KeyEventArgs e )
        {
            if (e.KeyCode == Keys.Delete)
            {
                (sender as ListBox).RemoveSelectedItem();
            }
        }

       
    }
}
