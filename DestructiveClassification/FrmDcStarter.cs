﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Evove;
using Evove.Operators.Breed;
using Evove.Operators.Breed.Crossover;
using Evove.Operators.Breed.Mutation;
using Evove.Operators.SelectionMethods;
using Evove.Operators.Stopping;
using Evove.Operators.Termination;
using EvoveUi;
using MGui.Helpers;
using TestApplication;
using TestApplication.Data;
using DestructiveClassification.GpClass;
using System.Diagnostics;
using TestApplication.Data.Results;
using DestructiveClassification;

namespace DestructiveClassification
{
    public partial class FrmDcStarter : Form
    {
        public FrmDcStarter()
        {
            InitializeComponent();
            this.Icon = Evove.Properties.Resources.Icon;
        }

        private void _btnCombineResults_Click( object sender, EventArgs e )
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    using (OpenFileDialog ofd2 = new OpenFileDialog())
                    {
                        if (ofd2.ShowDialog() == DialogResult.OK)
                        {
                            string dir1 = Path.GetDirectoryName( ofd.FileName );
                            string dir2 = Path.GetDirectoryName( ofd2.FileName );
                            string dir3 = Path.Combine( Path.GetDirectoryName( dir1 ), Path.GetFileName( dir1 ) + " + " + Path.GetFileName( dir2 ) );

                            if (Directory.Exists( dir3 ))
                            {
                                MessageBox.Show( this, "Combination directory \"" + dir3 + "\" already exists." );
                                return;
                            }

                            Directory.CreateDirectory( dir3 );

                            Combine( dir1, dir2, dir3, ECombineMode.KeepBoth, DcConstants.FILE_CONFIGURATION );
                            Combine( dir1, dir2, dir3, ECombineMode.Headers, DcConstants.FILE_FITNESS );
                            Combine( dir1, dir2, dir3, ECombineMode.Concatenate, DcConstants.FILE_PREDICTIONS );
                            Combine( dir1, dir2, dir3, ECombineMode.Concatenate, DcConstants.FILE_ROUNDS );
                            Combine( dir1, dir2, dir3, ECombineMode.Concatenate, DcConstants.FILE_TREES );

                            File.WriteAllText( Path.Combine( dir3, "Source.txt" ), "Combined data from \"" + dir1 + "\" (first directory) and \"" + dir2 + "\" (second directory)." );
                        }
                    }
                }
            }
        }

        private enum ECombineMode
        {
            KeepBoth,
            Headers,
            Concatenate,
        }

        private void Combine( string dir1, string dir2, string dir3, ECombineMode mode, string v )
        {
            string file1 = Path.Combine( dir1, v );
            string file2 = Path.Combine( dir2, v );
            string file3 = Path.Combine( dir3, v );

            switch (mode)
            {
                case ECombineMode.Concatenate:
                    {
                        string first = File.ReadAllText( file1 );
                        string second = File.ReadAllText( file2 );
                        File.WriteAllText( file3, first + second );
                    }
                    break;

                case ECombineMode.Headers:
                    {
                        string first = File.ReadAllText( file1 );
                        string second = File.ReadAllText( file2 );
                        second = second.Substring( second.IndexOf( Environment.NewLine ) + Environment.NewLine.Length );
                        File.WriteAllText( file3, first + second );
                    }
                    break;

                case ECombineMode.KeepBoth:
                    {
                        File.Copy( file1, Path.Combine( dir3, file3 ) );
                        File.Copy( file1, Path.Combine( dir3, Path.GetFileNameWithoutExtension( v ) + " (2)" + Path.GetExtension( v ) ) );
                    }
                    break;
            }
        }

        private void _btnRun_Click( object sender, EventArgs eventArgs )
        {
            List<EvoveParameters> parameters = new List<EvoveParameters>();

            parameters.Add( ParameterBox.New( EDataSet.Meat, ERemoval.None, EEpoccher.Fixed5000, EStopping.E50, EFitness.AgeAndClassV3 ) );
            //parameters.Add( ParameterBox.New( EDataSet.Honey, ERemoval.None, EEpoccher.StagnationF250, EStopping.TwoDays, EFitness.Class ) );
            //parameters.Add( ParameterBox.New( EDataSet.Honey, ERemoval.Five, EEpoccher.StagnationF250, EStopping.Never, EFitness.Class ) );


            RunMode mode = RunMode.StartInUnattendedMode;

            Hide();
            parameters.Start( this, mode );
            Show();
            MessageBox.Show( this, "The queue has been emptied. All tests have been completed by the system or cancelled by the user." );
            _btnRun.Enabled = false;
        }

        private void _btnExploreResults_Click( object sender, EventArgs e )
        {
            FrmAnalyserLoader.ShowForm( this );
        }

        private void _btnOpenEvove_Click( object sender, EventArgs e )
        {
            EvoveGui.Start( this, RunMode.ConfigureParameters | RunMode.StartInDebugMode );
        }

        private void button1_Click( object sender, EventArgs e )
        {
            Process.Start( "explorer.exe", "\"" + Application.StartupPath + "\"" );
        }
    }
}
