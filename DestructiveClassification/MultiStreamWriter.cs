﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestructiveClassification
{
    class MultiStreamWriter   : TextWriter
    {                
        private StreamWriter[] _streams;

        public MultiStreamWriter( params  string[] fileNames )
        {
            TakeOwnership = true;
            _streams = fileNames.Select( z => new StreamWriter( z ) ).ToArray();
        }

        public MultiStreamWriter( params StreamWriter[] streams )
        {
            TakeOwnership = false;
            _streams = streams;
        }

        public bool TakeOwnership
        {
            get; set;
        }

        public override Encoding Encoding
        {
            get
            {
                return _streams[0].Encoding;
            }
        }

        protected override void Dispose( bool disposing )
        {
            if (disposing)
            {
                if (TakeOwnership)
                {
                    foreach (StreamWriter sw in _streams)
                    {
                        sw.Dispose();
                    }
                }

                _streams = null;
            }

            base.Dispose( disposing );
        }

        public override void Write( char value )
        {
            foreach (StreamWriter sw in _streams)
            {
                sw.Write( value );
            }
        }

        public override void Write( string value )
        {
            foreach (StreamWriter sw in _streams)
            {
                sw.Write( value );
            }
        }
    }
}
