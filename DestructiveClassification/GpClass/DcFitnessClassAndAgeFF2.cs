﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove;
using Evove.Attributes;
using Evove.Helpers.GpFunctions;

namespace DestructiveClassification.GpClass
{

    [EvoveClass]
    public class DcFitnessClassAndAgeFF2 : DcFitnessClassAndAge
    {
        public DcFitnessClassAndAgeFF2( InstantiationArgs args )
            : base( args )
        {
        }

        protected override double Scorer( AgePrediction predicted, AgePrediction actual )
        {
            return AgePrediction.ScoreVersion2( predicted, actual );
        }
    }
}
