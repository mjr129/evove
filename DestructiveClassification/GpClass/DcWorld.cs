﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Evove;
using Evove.Blocks;
using Evove.Operators.Stopping;
using MGui.Helpers;
using TestApplication.Data;
using static Evove.Operators.Stopping.StoppingConditions;

namespace DestructiveClassification.GpClass
{
    public interface IDcWorld
    {
        int Epoch { get; }
    }

    /// <summary>The population data</summary>
    public class DcWorld<TFn> : IDisposable, IDcWorld
    {
        public List<int> FF_PermittedVariables;

        /// <summary>
        /// Enables validation set monitoring and variable counting.
        /// Due to its computational expense this is only used for the "best"
        /// individual in the population.
        /// </summary>
        public bool FF_Intensive;

        public readonly DataSet FF_Data;
        public CrossValidation FF_CrossValidation;

        public Dictionary<int, RemovalInfo> _removalOrder;
        private int[] _grandCounts;
        private IStoppingCondition[] _epochTrigger;
        private readonly string _configFile;
        private readonly StreamWriter _roundFile;
        private readonly StreamWriter _treeFile;
        private readonly StreamWriter _fitnessFile;
        private readonly StreamWriter _predictionsFile;
        private readonly DcConfig _config;
        public int _round;
        public int _epoch;
        private int _startOfEpoch;
        bool _nextRoundTerminates;

        Randomiser _rnd;

        public int Epoch
        {
            get
            {
                return _epoch;
            }
        }

        public DcWorld( PopulationArgs args )
        {
            _removalOrder = new Dictionary<int, RemovalInfo>();
            _rnd = args.Population.Randomiser;

            // Get the config
            _config = args.Population.Parameters.Properties.Get<DcConfig>();
            _epochTrigger = _config.EpochTrigger;

            // Get the files
            _roundFile = args.Population.WriteOutputFile( DcConstants.FILE_ROUNDS );                  // variables used for each simulation 
            _configFile = args.Population.GetOutputFile( DcConstants.FILE_CONFIGURATION );          // the configuration
            _fitnessFile = args.Population.WriteOutputFile( DcConstants.FILE_FITNESS );               // best fitness for each round
            _predictionsFile = args.Population.WriteOutputFile( DcConstants.FILE_PREDICTIONS );       // predictions for each round
            _treeFile = args.Population.WriteOutputFile( DcConstants.FILE_TREES );                    // best trees for each round

            // Get the dataset
            FF_Data = new DataSet( _config.DataSet );

            // Create the permitted variable array
            FF_PermittedVariables = new List<int>();

            for (int v = 0; v < FF_Data.NumVariables; v++)
            {
                FF_PermittedVariables.Add( v );
            }

            _grandCounts = new int[FF_Data.NumVariables];

            // Write the config file
            CsvWriter csv = new CsvWriter();
            csv.Add( _config );
            csv.Add( args.Population.SyntaxManager );
            csv.Add( args.Population.Parameters );
            csv.Add( FF_Data );
            File.WriteAllText( _configFile, csv.GetCsv() );

            // Write the fitness headers
            AppendAllText( _fitnessFile, Fitness.WriteHeaders() );

            // Track round ends
            args.Population.RoundStart += Population_RoundStart;
            args.Population.RoundEnd += Population_RoundEnd;

            // Resume from folder
            if (!string.IsNullOrEmpty( _config.ResumeFromDirectory ))
            {
                ResumeFromFile( args, _config.ResumeFromDirectory );
            }

            // Initialise CV / epoch trigger
            StartOfEpochCv(args.Population);
            StartOfRoundCv( args.Population, true );
            ResetEpochTrigger(); 
        }

        public void Dispose()
        {
            _roundFile.Dispose();
            _treeFile.Dispose();
            _fitnessFile.Dispose();
            _predictionsFile.Dispose();
        }

        private void Population_RoundStart( object sender, RoundStartEventArgs e )
        {
            _round = e.Population.Round;
        }

        private void ResumeFromFile( PopulationArgs args, string directory )
        {
            List<int> toRemove = new List<int>();
            int round = 0;

            // Play back the removals
            using (StreamReader sr = new StreamReader( Path.Combine( directory, DcConstants.FILE_ROUNDS ) ))
            {
                while (!sr.EndOfStream)
                {
                    string[] e = sr.ReadLine().Split( ',' );

                    e[0] = e[0].Replace( "Round ", "" );
                    round = int.Parse( e[0] );

                    for (int n = 1; n < e.Length; n++)
                    {
                        int v = int.Parse( e[n] );

                        IncrementCount( toRemove, v );
                    }

                    PerformRemoval( round, new Fitness(), toRemove );
                }
            }

            // Set the round to the last round encountered
            args.StartingRound = round;

            // Ensure the CV is the same
            using (StreamReader sr = new StreamReader( Path.Combine( directory, DcConstants.FILE_PREDICTIONS ) ))
            {
                string[] e = sr.ReadLine().Split( ',' );

                if (e[1] != "\"TRAINING\"" || e[2] != "\"INDICES\"")
                {
                    throw new InvalidOperationException( "Invalid predictions file to resume from" );
                }

                int[] training = e.Skip( 3 ).Select( int.Parse ).ToArray();

                sr.ReadLine();
                sr.ReadLine();
                sr.ReadLine();

                e = sr.ReadLine().Split( ',' );

                if (e[1] != "\"VALIDATION\"" || e[2] != "\"INDICES\"")
                {
                    throw new InvalidOperationException( "Invalid predictions file to resume from" );
                }

                int[] validation = e.Skip( 3 ).Select( int.Parse ).ToArray();

                FF_CrossValidation = new CrossValidation( training, validation );
                FF_CrossValidation.Validate( this.FF_Data );
            }

            // Make a note of this
            File.WriteAllText( args.Population.GetOutputFile( "!Resume.csv" ), "RESUMED FROM DIRECTORY,\"" + directory + "\"" );
        }    

        bool AnyCheckStop( StoppingConditionArgs args )
        {
            foreach (IStoppingCondition stoppingCondition in _epochTrigger)
            {                     
                if (stoppingCondition.CheckStop( args ))
                {
                    return true;
                }
            }

            return false;
        }

        private void Population_RoundEnd( object sender, RoundEndEventArgs args )
        {
            Individual individual = args.Population.BestIndividual;

            // Repeat the last individual for the test set with "intensive" mode
            FF_Intensive = true;
            individual.Evaluate( args.Population );
            Fitness fitness = (Fitness)individual.LatestResult.Fitness;
            FF_Intensive = false;

            // Wite the fitness, prediction, tree and variables used
            WriteFitnessFile( individual, args.Population.Round, args.Population.ElapsedTime );

            if (fitness == null)
            {
                AppendAllText( _predictionsFile, "ROUND " + args.Population.Round.ToString() + " - Fitness is NULL\r\n" );
            }
            else
            {
                AppendAllText( _predictionsFile, fitness.GetPredictionsString( this.FF_Data, args.Population.Round ) );
            }

            AppendAllText( _treeFile, individual.Genome.GetRoot().ToToken( "round " + args.Population.Round.ToString() ).Write() );

            if (!_nextRoundTerminates)
            {            
                // Not the end of the epoch so continue                                         
                _nextRoundTerminates = AnyCheckStop( new StoppingConditionArgs( args.Population ) );
                StartOfRoundCv( args.Population, false );
                return;
            }

            // EPOCH EVENT!!!!!!!!!

            ResetEpochTrigger();
            _epoch++;
            _startOfEpoch = args.Population.Round;            
                             
            // When we get an extinction event count the variables used    
            List<int> toRemove = WriteVariablesUsedFile( args.Population.Round, fitness.VariablesUsed );

            // Any variables used 𝔁 or more times get removed, the fitness is recorded
            PerformRemoval( args.Population.Round, fitness, toRemove );

            // TODO: If we remove variable(s) do we need to reset the count in case some
            //       variables only make sense in the context of others?

            // Restart the population
            // (If we run out of variables terminate it)
            if (FF_PermittedVariables.Count == 0)
            {
                args.SpecialEvents |= ESpecialEvents.Stop;
            }
            else
            {
                args.SpecialEvents |= ESpecialEvents.Extinction;
            }

            _nextRoundTerminates = AnyCheckStop( new StoppingConditionArgs( args.Population ) );
            StartOfEpochCv(args.Population);
            StartOfRoundCv( args.Population, true );
        }

        private List<int> WriteVariablesUsedFile( int round, bool[] variablesUsed )
        {
            List<int> toRemove = new List<int>();
            StringBuilder sb = new StringBuilder();
            sb.Append( "Round " + round );

            foreach (int v in FF_PermittedVariables)
            {
                if (variablesUsed[v])
                {
                    // Remove any exceeding count
                    int count = IncrementCount( toRemove, v );

                    // Record variable usage
                    sb.Append( "," );

                    sb.Append( v );
                    sb.Append( "=" );
                    sb.Append( count );
                }
            }

            sb.AppendLine();

            AppendAllText( _roundFile, sb.ToString() );
            return toRemove;
        }

        private void ResetEpochTrigger()
        {
            foreach (IStoppingCondition stoppingCondition in _epochTrigger)
            {
                stoppingCondition.Reset();
            }
        }

        private void PerformRemoval( int round, Fitness fitness, List<int> toRemove )
        {
            int order = _removalOrder.Count;

            foreach (int v in toRemove)
            {
                FF_PermittedVariables.Remove( v );
                _removalOrder.Add( v, new RemovalInfo( v, fitness, round, order ) );
            }

            toRemove.Clear();
        }

        private int IncrementCount( List<int> toRemove, int v )
        {
            _grandCounts[v]++;

            if (_grandCounts[v] >= _config.UsagesBeforeVariableRemoval)
            {
                toRemove.Add( v );
                return -_grandCounts[v];
            }

            return _grandCounts[v];
        }

        private void StartOfEpochCv(Population pop  )
        {
            switch (_config.CvModeStart)
            {
                case ECvModeStart.LeaveOneOut:
                    this.FF_CrossValidation = CrossValidation.LeaveOneOut( this.FF_Data, _config.TrainingIds, _epoch );
                    break;

                case ECvModeStart.NoCv:
                    this.FF_CrossValidation = CrossValidation.All( this.FF_Data);
                    break;

                case ECvModeStart.TenFold:
                    this.FF_CrossValidation = CrossValidation.nFold( this.FF_Data, 10, 0, _epoch );
                    break;

                case ECvModeStart.PriorFailures:
                    if (_epoch % 2 == 0)
                    {
                        this.FF_CrossValidation = CrossValidation.LeaveOneOut( this.FF_Data, _config.TrainingIds, _epoch / 2 );
                    }
                    else
                    {
                        Fitness bestFitness = (Fitness)pop.BestIndividual.LatestResult.Fitness;
                        int[] incorrectIndices = bestFitness.Training.Indices.At( bestFitness.Training.CorrectPredictions.Negate() ).ToArray();
                        this.FF_CrossValidation = CrossValidation.AndTheRest( this.FF_Data, incorrectIndices );
                    }
                    break;

                default:
                    throw new InvalidOperationException( "Invalid switch: " + _config.CvModeStart );
            }                           
        }

        private void StartOfRoundCv( Population pop, bool firstRoundInEpoch )
        {
            int rwe = pop.Round - _startOfEpoch;

            Debug.Assert( (rwe == 0) == firstRoundInEpoch );

            switch (_config.CvMode)
            {
                case ECvMode.Random500:
                    {
                        StartOfEpochCv( pop );

                        int[] randoms = new int[500];
                        List<int> opts = new List<int>( this.FF_CrossValidation.Training );
                        bool intendedClass = false;

                        for (int n = 0; n < randoms.Length; n++)
                        {
                            int r;

                            do
                            {
                                r = _rnd.Next( opts.Count );
                            } while (this.FF_Data.Classes[opts[r]] != intendedClass);

                            randoms[n] = opts[r];
                            opts.RemoveAt( r );

                            intendedClass = !intendedClass;
                        }

                        this.FF_CrossValidation = CrossValidation.AndTheRest( this.FF_Data, randoms );
                    }
                    return;

                case ECvMode.RemoveOnePerRound:
                    {
                        if (firstRoundInEpoch)
                        {
                            // Keep initial
                            return;
                        }

                        if (_nextRoundTerminates || this.FF_CrossValidation.Training.Length == 1)
                        {
                            // Final round uses all data
                            StartOfEpochCv( pop );
                            return;
                        }     

                        // Move an correctly classified observation into the validation set for now
                        Fitness bestFitness = (Fitness)pop.BestIndividual.LatestResult.Fitness;
                        List<int> possibilities = new List<int>();

                        for (int n = 0; n < bestFitness.Training.CorrectPredictions.Length; n++)
                        {
                            if (bestFitness.Training.CorrectPredictions[n])
                            {
                                possibilities.Add( bestFitness.Training.Indices[n] );
                            }
                        }

                        int[] toMove = { possibilities[_rnd.Next( possibilities.Count )] };

                        this.FF_CrossValidation = new CrossValidation(
                            this.FF_CrossValidation.Training.Except( toMove ).ToArray(),
                            this.FF_CrossValidation.Validation.Concat( toMove ).ToArray() );
                    }
                    break;

                case ECvMode.KeepInitial:
                    {
                    }
                    break;

                default:
                    throw new InvalidOperationException( "Invalid switch: " + _config.CvMode );
            }
        }

        private void WriteFitnessFile( Individual individual, int round, TimeSpan elapsedTime )
        {
            Fitness fitness = (Fitness)individual.LatestResult.Fitness;

            AppendAllText( _fitnessFile, fitness != null ? fitness.ToCsv(round, _epoch, elapsedTime ) : "(null)\r\n" );
        }

        private void AppendAllText( StreamWriter file, string content )
        {
            file.Write( content );
            file.Flush();
        }
    }
}
