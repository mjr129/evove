﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove;
using Evove.Attributes;
using Evove.Helpers.GpFunctions;

namespace DestructiveClassification.GpClass
{
    [EvoveClass]
    public class DcFitnessClassOnly : DcFitnessBase<bool>
    {
        public DcFitnessClassOnly( InstantiationArgs args )
            : base( args )
        {
            // NA
        }

        protected override double Scorer( bool predicted, bool actual )
        {
            return Score( predicted, actual );
        }

        public static double Score( bool predicted, bool actual )
        {
            return (predicted == actual) ? 1 : 0;
        }

        [EvoveIgnore]
        internal override bool GetActual( int observationIndex )
        {
            // Population.Data.Classes[_observationIndex]
            return base.Population.FF_Data.Classes[observationIndex];
        }

        internal override bool GetDefault()
        {
            return false;
        }
    }
}
