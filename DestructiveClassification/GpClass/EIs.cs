﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestructiveClassification.GpClass
{
    [Flags]
    enum EIs
    {
        None = 0x0,
        Tree = 0x1,
        Complex = 0x2,
        Always = Tree | Complex,
    }
}
