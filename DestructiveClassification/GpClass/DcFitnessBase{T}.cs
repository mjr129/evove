﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove;
using Evove.Attributes;
using Evove.Helpers.GpFunctions;
using EvoveUi;
using TestApplication.Data;
using TestApplication.Data.Results;

namespace DestructiveClassification.GpClass
{
    /// <summary>
    /// My current project.
    /// </summary>
    [EvoveClass( Caching = true, Persistence = true, Threading = false )]
    [EvoveGuiExpects( typeof( DcConfig ) )]
    [EvoveExtends( typeof( GpFunctions.Double ) )]
    [EvoveExtends( typeof( GpFunctions.Comparisson<double> ) )]
    [EvoveExtends( typeof( GpFunctions.Boolean ) )]
    [EvoveExtends( typeof( GpFunctions.Boolean3 ) )]
    public abstract class DcFitnessBase<TResult>
    {
        // Variable counts
        public bool[] VariablesUsed;
        private bool[] VariablesUsedObject;

        // Population
        [EvoveField( EField.PopulationStatic )]
        public readonly DcWorld<TResult> Population;

        // State
        protected int _observationIndex;

        // Extra info
        protected readonly Randomiser _randomiser;

        /// <summary>
        /// ******************** Fitness function ********************
        /// </summary>
        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public Fitness FitnessFunction( Func<TResult> function )
        {
            OnFitnessFunction();

            // Are we counting variables?
            if (Population.FF_Intensive)
            {
                if (VariablesUsedObject == null)
                {
                    VariablesUsedObject = new bool[NumberOfVariables];
                }
                else
                {
                    Array.Clear( VariablesUsedObject, 0, VariablesUsedObject.Length );
                }

                VariablesUsed = VariablesUsedObject;
            }
            else
            {
                VariablesUsed = null;
            }

            // Get the CV data
            CrossValidation cv = Population.FF_CrossValidation;

            // Predict the training set
            CaseResult training = Predict( true, function, cv.Training );

            // Are we predicting the validation set?
            CaseResult validation;

            if (Population.FF_Intensive)
            {
                validation = Predict( false, function, cv.Validation );
            }
            else
            {
                validation = null;
            }

            // Calculate and return the fitness
            return new Fitness( VariablesUsed, training, validation, Population._round, Population._epoch );
        }

        protected virtual void OnFitnessFunction()
        {
            // NA
        }

        /// <summary>
        /// Performs the prediction
        /// </summary>
        /// <param name="train">training (or validation)</param>
        /// <param name="function">function to predict</param>
        /// <param name="indices">indices to use</param>
        /// <returns>a CaseResult describing the prediction</returns>
        private CaseResult Predict( bool train, Func<TResult> function, int[] indices )
        {
            TResult[] predicted = new TResult[indices.Length];
            TResult[] actual = new TResult[indices.Length];

            int numberCorrect = 0;

            for (int testCase = 0; testCase < indices.Length; testCase++)
            {
                _observationIndex = indices[testCase];

                predicted[testCase] = function();

                if (predicted[testCase] == null)
                {
                    predicted[testCase] = GetDefault();
                }

                actual[testCase] = GetActual( _observationIndex );

                if (predicted[testCase].Equals( actual[testCase] ))
                {
                    numberCorrect++;
                }
            }                                                         

            return CaseResult.New(  indices: indices,
                                    actual: actual,
                                    predicted: predicted, 
                                    scorer: Scorer,
                                    bscorer: null);
        }

        protected abstract double Scorer( TResult predicted, TResult actual );   

        [EvoveIgnore]
        internal abstract TResult GetActual( int observationIndex );

        [EvoveIgnore]
        internal abstract TResult GetDefault();

        /// <summary>
        /// The number of variables
        /// </summary>
        private int NumberOfVariables
        {
            get
            {
                return Population.FF_Data.NumVariables;
            }
        }

        public DcFitnessBase( InstantiationArgs args )
        {
            _randomiser = args.Population.Randomiser;
        }

        [EvoveMethod( Role = EMethod.ConstantProvider )]
        public int ConstantIndex()
        {
            return _randomiser.Next( NumberOfVariables );
        }

        /// <summary>
        /// Returns a variable.
        /// Making "index" an integer essentially makes "index" constant since we have no integer
        /// GP functions.
        /// </summary>         
        [EvoveMethod]
        public double GetVariable( int index )
        {
            int permittedIndex = EvoveHelper.Wrap( (int)index, Population.FF_PermittedVariables.Count );

            int variableIndex = Population.FF_PermittedVariables[permittedIndex];

            if (Population.FF_Intensive)
            {
                VariablesUsed[variableIndex] = true;
            }

            return Population.FF_Data.Data[_observationIndex, variableIndex];
        }          

        [EvoveMethod]
        public double Ratio( int a, int b )
        {
            return GetVariable( a ) / GetVariable( b );
        }

        [EvoveMethod]
        public double Average( int startIndex, int count )
        {
            const int MAX_COUNT = 10;
            count = count % MAX_COUNT;
            double result = 0.0d;

            for (int n = 0; n < count; n++)
            {
                result += GetVariable( n + startIndex);
            }

            result /= count;

            return result;
        }

        [EvoveMethod]
        public double ConvertBToD( bool boolean )
        {
            return boolean ? 1.0d : 0.0d;
        }

        [EvoveMethod]
        public bool ConvertDToB( double @double )
        {
            return @double != 0.0d;
        }
    }
}
