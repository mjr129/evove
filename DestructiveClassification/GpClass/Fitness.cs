﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Helpers;
using TestApplication.Data;
using TestApplication.Data.Results;

namespace DestructiveClassification.GpClass
{
    /// <summary>
    /// Fitness object
    /// </summary>
    public class Fitness : FitnessValues.DoubleWrapper
    {
        public readonly bool[] VariablesUsed;
        internal readonly CaseResult Validation;
        internal readonly CaseResult Training;
        private readonly int _round;
        private readonly int _epoch;

        public override double Value => Training.AverageScore;

        internal Fitness()
        {
            VariablesUsed = new bool[0];
            Validation = null;
            Training = null;
        }

        internal Fitness( bool[] variablesUsed, CaseResult training, CaseResult validation, int round, int epoch )
        {
            this.VariablesUsed = variablesUsed;
            this.Training = training;
            this.Validation = validation;
            _round = round;
            _epoch = epoch;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append( "Training = " + (Training.AverageScore * 100.0d).ToString( "F1" ) + "%" );

            if (Validation != null)
            {
                sb.Append( ", Validation = " + (Validation.AverageScore * 100.0d).ToString( "F1" ) + "%" );
            }
            
            sb.Append( ", Round = " + _round );
            sb.Append( ", Epoch = " + _epoch );

            return sb.ToString();
        }

        public static string WriteHeaders()
        {                             
            return
                "\"round\",\"epoch\","
                + CaseResult.WriteHeaders( "training" )
                + ","
                + CaseResult.WriteHeaders( "validation" ) 
                + ",\"time\""
                + ",\"time.elapsed\""
                + "\r\n";
        }

        internal string ToCsv( int round, int epoch, TimeSpan elapsed )
        {
            string rounds = round.ToString() + "," + epoch + ",";
            string time = "," + AsString(DateTime.Now) + "," + AsString( elapsed );

            return rounds + Training.ToCsv(  ) + "," + Validation.ToCsv( ) + time + "\r\n";
        }

        private string AsString( DateTime now )
        {
            return now.ToString( "yyyy-MM-dd hh:mm:ss.fff" );
        }

        private string AsString( TimeSpan elapsed )
        {
            return (elapsed.Hours + (elapsed.Days * 24)).ToString( "D2" ) + ":" + elapsed.ToString( "mm\\:ss\\.fff" );
        }

        internal string GetPredictionsString( DataSet dataSet,  int round )
        {
            StringBuilder sb = new StringBuilder();

            Training.WritePredictions(  round.ToString(), "\"TRAINING\"", sb , dataSet );
            Validation.WritePredictions(  "", "\"VALIDATION\"", sb , dataSet ); 
            sb.AppendLine();

            return sb.ToString();
        }

        
    }
}
