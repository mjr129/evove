﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DestructiveClassification.GpClass;
using Evove;
using Evove.Operators.Stopping;
using EvoveUi;
using TestApplication;

namespace DestructiveClassification
{
    internal class DcConfig : ISupportsCsv, IEvoveGuiEditable
    {
        public string ResumeFromDirectory;
        public IStoppingCondition[] EpochTrigger;
        public int UsagesBeforeVariableRemoval;
        public int[] TrainingIds;
        public DataSetConfiguration DataSet;
        public ECvMode CvMode;
        public ECvModeStart CvModeStart;

        public void ToCsv( CsvWriter csv )
        {
            csv.Serialise( this );
            ((ISupportsCsv)this.DataSet).ToCsv( csv );
        }                

        public override string ToString()
        {
            return this.CsvAsString();
        }

        public IEvoveGuiEditable EditInGui( IWin32Window owner )
        {
            return FrmDcConfiguration.Show( owner, this );
        }

        internal DcConfig Clone()
        {
            return (DcConfig)MemberwiseClone();
        }
    }
}
