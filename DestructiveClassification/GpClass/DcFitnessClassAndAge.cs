﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove;
using Evove.Attributes;
using Evove.Helpers.GpFunctions;

namespace DestructiveClassification.GpClass
{
    [EvoveClass]                                   
    public class DcFitnessClassAndAge : DcFitnessBase<AgePrediction>
    {
        private bool _class;
        private double _age;

        public DcFitnessClassAndAge( InstantiationArgs args )
            : base( args )
        {
            // NA
        }

        [EvoveIgnore]
        internal override AgePrediction GetActual( int observationIndex )
        {
            if (base.Population.FF_Data.Times == null)
            {
                return new AgePrediction( base.Population.FF_Data.Classes[observationIndex], 0, false );
            }
            else
            {
                return new AgePrediction( base.Population.FF_Data.Classes[observationIndex], base.Population.FF_Data.Times[observationIndex], false );
            }
        }

        internal override AgePrediction GetDefault()
        {
            return new AgePrediction( false, 0, true );
        }

        protected override double Scorer( AgePrediction predicted, AgePrediction actual )
        {
            return AgePrediction.Score( predicted, actual );
        }

        protected override void OnFitnessFunction()
        {
            _class = false;
            _age = 0;
        }

        [EvoveMethod]
        public double RetrieveAge()
        {
            return _age;
        }

        [EvoveMethod]
        public bool RetrieveClass()
        {
            return _class;
        }    

        [EvoveMethod]
        public AgePrediction PredictClassFirst( bool @class, Func<double> age )
        {
            _class = @class;
            return new AgePrediction( @class, (int)(age()), true );
        }

        [EvoveMethod]
        public AgePrediction PredictAgeFirst( double age, Func<bool> @class )
        {
            _age = age;
            return new AgePrediction( @class(), (int)(age), true );
        }

        [EvoveMethod]
        public AgePrediction Empty()
        {
            return new AgePrediction( false, 0, true );
        }
    }
}
