﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestructiveClassification.GpClass
{
    [Serializable]
    public class AgePrediction
    {
        const int MAXIMUM_DAY = 28;

        public int Age;
        public bool Class;


        public AgePrediction( bool @class, int age, bool isPrediction )
        {
            this.Class = @class;
            this.Age = age;

            if ((!isPrediction) && (age < 0 || age > MAXIMUM_DAY))
            {
                //throw new InvalidOperationException( "Age out of range." );
            }
        }

        internal static double ScoreVersion0( AgePrediction predicted, AgePrediction actual )
        {
            return (predicted.Class == actual.Class && predicted.Age == actual.Age) ? 1 : 0;
        }

        public override bool Equals( object obj )
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var x = (AgePrediction)obj;

            return x.Age == Age && x.Class == Class;
        }

        public override int GetHashCode()
        {
            return (Class ? unchecked((int)0x80000000) : (int)0x00000000) ^ Age.GetHashCode();
        }

        public override string ToString()
        {
            return (Class ? "W" : "C") + Age;
        }

        internal static AgePrediction Parse( string arg, bool isPrediction )
        {
            bool c = (arg[0] == 'W');
            int age = int.Parse( arg.Substring( 1 ) );

            return new AgePrediction( c, age, isPrediction );
        }

        internal static double Score( AgePrediction predicted, AgePrediction actual )
        {   
            if (actual.Class != predicted.Class)
            {
                return 0.0d;
            }

            double ageOut = Math.Abs( actual.Age - predicted.Age );
            double pentalty = (ageOut / MAXIMUM_DAY);
            return 1.0d - pentalty;
        }

        internal static double ScoreVersion2( AgePrediction predicted, AgePrediction actual )
        {
            if (actual.Class != predicted.Class)
            {
                return 0.0d;
            }

            double ageOut = Math.Min( Math.Abs( actual.Age - predicted.Age ), MAXIMUM_DAY );
            double pentalty = (ageOut / MAXIMUM_DAY);
            return 1.0d - pentalty;
        }

        internal static double ScoreVersion3( AgePrediction predicted, AgePrediction actual )
        {
            if (actual.Class != predicted.Class)
            {
                return 0.0d;
            }

            double ageOut = Math.Min( Math.Abs( actual.Age - predicted.Age ), MAXIMUM_DAY );
            double pentalty = (ageOut / MAXIMUM_DAY) / 2;
            return 1.0d - pentalty;
        }
    }
}
