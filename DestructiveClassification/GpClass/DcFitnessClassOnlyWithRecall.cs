﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove;
using Evove.Attributes;
using Evove.Helpers.GpFunctions;

namespace DestructiveClassification.GpClass
{

    [EvoveClass]
    public class DcFitnessClassOnlyWithRecall : DcFitnessClassAndAge
    {
        public DcFitnessClassOnlyWithRecall( InstantiationArgs args )
            : base( args )
        {
        }

        protected override double Scorer( AgePrediction predicted, AgePrediction actual )
        {
            return Score( predicted, actual );
        }

        public static double Score( AgePrediction predicted, AgePrediction actual )
        {
            return (predicted.Class == actual.Class) ? 1 : 0;
        }
    }
}
