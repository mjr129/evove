﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestructiveClassification.GpClass
{
    static class DcConstants
    {
        // Filenames
        public const string FILE_ROUNDS = "2SGP Rounds.csv";
        public const string FILE_PREDICTIONS = "2SGP Predictions.csv";
        public const string FILE_CONFIGURATION = "2SGP Configuration.csv";
        public const string FILE_FITNESS = "2SGP Fitness.csv";
        public const string FILE_TREES = "2SGP Trees.csv";

        public const string FILE_CACHE = "2SGP AnalysisCache.msnrbf.gz";
    }
}
