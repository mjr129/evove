﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestructiveClassification.GpClass
{
    enum ECvMode
    {              
        _invalid,
        RemoveOnePerRound,
        KeepInitial,
        Random500
    }

    enum ECvModeStart
    {
        _invalid,
        LeaveOneOut,
        NoCv,
        TenFold,
        PriorFailures
    }
}
