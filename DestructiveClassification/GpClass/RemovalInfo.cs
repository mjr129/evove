﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestructiveClassification.GpClass
{
    public class RemovalInfo
    {
        public readonly int VariableIndex;
        public readonly int Round;
        public readonly Fitness Fitness;
        public readonly int Order;

        public RemovalInfo( int index, Fitness fitness, int round, int order )
        {
            this.VariableIndex = index;
            this.Fitness = fitness;
            this.Round = round;
            this.Order = order;
        }
    }
}
