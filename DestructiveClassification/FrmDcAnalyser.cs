﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DestructiveClassification;
using DestructiveClassification.Analysis;
using DestructiveClassification.GpClass;
using Evove;
using EvoveUi;
using MathNet.Numerics.Statistics;
using MCharting;
using MGui.Controls;
using MGui.Datatypes;
using MGui.Helpers;
using MSerialisers;
using TestApplication.Data;
using TestApplication.Data.Results;
using TestApplication.Properties;
using TestApplication.TestCases;

namespace TestApplication
{
    internal partial class FrmDcAnalyser : Form
    {   
        private IEnumerable _source;
        private AxisFunction _yaxis;
        private AxisFunction _xaxis;
        private Converter<IEnumerable<double>, double> _yFunction;
        
        private ObsName[] _allObservations;
        
        private readonly ResultsPackage _rp;

        public FrmDcAnalyser( FrmAnalyserLoader owner, string directory, ELoadFlags flags )
        {
            // Dialogue setup
            InitializeComponent();
            this.Icon = Evove.Properties.Resources.Icon;
                           
            _chartMain.AddControls( this.menuStrip2 );
            _chartPred.AddControls( this.menuStrip4 );
            _chartMain.Style.Margin = new Padding( 48, 48, 48, 48 );
            _chartPred.Style.Margin = new Padding( 48, 48, 48, 48 );
            CreateMenuItems();

            Text = "DC Analyser - " + Path.GetFileName( directory );

            // Load data
            _rp = ResultsPackage.New( owner, directory, flags );

            _txtConfiguration.Text = _rp.ConfigText;
            UpdateObsNames();

            StringBuilder sb = new StringBuilder();

            sb.AppendLine( "Directory                           , " + directory );
            sb.AppendLine( "Number of epochs                    , " + _rp.Epochs.Length );
            sb.AppendLine( "Number of rounds                    , " + _rp.Rounds.Length );
            sb.AppendLine( "Training average score              , " + _rp.Epochs.Average( z => z.Training.AverageScore ) );
            sb.AppendLine( "Training average class prediction   , " + _rp.Epochs.Average( z => z.Training.AverageBScore ) );
            sb.AppendLine( "Validation average score            , " + _rp.Epochs.Average( z => z.Validation.AverageScore ) );
            sb.AppendLine( "Validation average class prediction , " + _rp.Epochs.Average( z => z.Validation.AverageBScore ) );
            sb.AppendLine( "Training maximum score              , " + _rp.Epochs.Max( z => z.Training.AverageScore ) );
            sb.AppendLine( "Training maximum class prediction   , " + _rp.Epochs.Max( z => z.Training.AverageBScore ) );
            sb.AppendLine( "Validation maximum score            , " + _rp.Epochs.Max( z => z.Validation.AverageScore ) );
            sb.AppendLine( "Validation maximum class prediction , " + _rp.Epochs.Max( z => z.Validation.AverageBScore ) );
            sb.AppendLine( "Training minimum score              , " + _rp.Epochs.Min( z => z.Training.AverageScore ) );
            sb.AppendLine( "Training minimum class prediction   , " + _rp.Epochs.Min( z => z.Training.AverageBScore ) );
            sb.AppendLine( "Validation minimum score            , " + _rp.Epochs.Min( z => z.Validation.AverageScore ) );
            sb.AppendLine( "Validation minimum class prediction , " + _rp.Epochs.Min( z => z.Validation.AverageBScore ) );
            sb.AppendLine();
            sb.AppendLine( "IncorrectCount,IncorrectFraction,Training_Observations" );
            GenerateSummary( z => z.Training, sb );
            sb.AppendLine();
            sb.AppendLine( "IncorrectCount,IncorrectFraction,Validation_Observations" );
            GenerateSummary(  z => z.Training, sb );
            sb.AppendLine();

            _txtSummary.Text = sb.ToString();
        }

        void GenerateSummary( Func<CRound, CaseResult> predicates, StringBuilder sb )
        {
            Counter<int> counter = new Counter<int>();

            foreach (CRound round in _rp.Epochs)
            {
                CaseResult cr = predicates( round );
                bool[] incorrect = cr.IncorrectPredictions;

                for (int n = 0; n < cr.Indices.Length; n++)
                {
                    if (incorrect[n])
                    {
                        counter.Increment( cr.Indices[n] );
                    }
                }
            }

            int min = counter.FindMinNonZero();

            for (int n = counter.FindMax(); n >= min; n--)
            {
                sb.Append( n.ToString().PadRight( 4 ) );
                sb.Append( ", " );
                sb.Append( ((double)n / _rp.Epochs.Length).ToString( "P0" ).PadRight( 8 ) );
                sb.Append( ", " );
                sb.Append( string.Join( "; ", counter.Find( n ).Select( _rp.GetObsByIndex ).OrderBy( z => z.PlotPosition ).Select( z => z.DisplayName ) ) );
                sb.AppendLine();
            }
        }    

        private void CreateMenuItems()
        {
            // Menu items
            List<AxisFunction> list1 = new List<AxisFunction>();

            // FnVariabke
            list1.Add( new AxisFunction( "index", z => z.Index ) );
            list1.Add( new AxisFunction( "PPM [" + "first load bin data".ToSansBoldItalic() + "]", z => z.MidPpm ) );
            list1.Add( new AxisFunction( "average intensity [" + "first load intensity data".ToSansBoldItalic() + "]", z => z.AvgIntensity ) );
            list1.Add( new AxisFunction( "average intensity (log10) [" + "first load intensity data".ToSansBoldItalic() + "]", z => z.LogAvgIntensity ) );
            list1.Add( new AxisFunction( "usage count", z => z.Usages.Count ) );

            // FnRound
            list1.Add( new AxisFunction( "index within epoch", z => z.WithinEpochIndex ) );
            list1.Add( new AxisFunction( "overall index", z => z.OverallIndex ) );
            list1.Add( new AxisFunction( "index of epoch", z => z.EpochIndex ) );
            list1.Add( new AxisFunction( "index of removal", z => z.RemovalIndex ) );
            list1.Add( new AxisFunction( "number of variables removed", z => z.Variables.Count ) );
            list1.Add( new AxisFunction( "total further improvement", ( CRound z ) => z.Epoch.Training.AverageScore - z.Training.AverageScore ) );
            list1.Add( new AxisFunction( "total further improvement B", ( CRound z ) => z.Epoch.Training.AverageBScore - z.Training.AverageBScore ) );
            list1.Add( new AxisFunction( "has further improvement? (-1/0/1)", ( CRound z ) => z.Epoch.Training.AverageScore.CompareTo( z.Training.AverageScore ) ) );
            list1.Add( new AxisFunction( "has further improvement B? (-1/0/1)", ( CRound z ) => z.Epoch.Training.AverageBScore.CompareTo( z.Training.AverageBScore ) ) );
            list1.Add( new AxisFunction( "is end of epoch? (0/1)", z => z.IsEndOfEpoch ? 1 : 0 ) );
            list1.Add( new AxisFunction( "number of stagnations", z => z.Stagnation ) );
            list1.Add( new AxisFunction( "stagnation band", z => z.StagnationBand.Value ) );
            list1.Add( new AxisFunction( "delta score", z => z.Delta ) );

            // FnResult
            list1.Add( new AxisFunction( "AverageScore", (CaseResult z) => z.AverageScore ) );
            list1.Add( new AxisFunction( "AverageBScore", ( CaseResult z ) => z.AverageBScore ) );
            list1.Add( new AxisFunction( "All scores[]", ( CaseResult z ) => z.Scores ) );
            list1.Add( new AxisFunction( "Predicted values[] (to double)", ( CaseResult z ) => z.Predicted?.Cast<object>().Select( zz => Convert.ToDouble(zz) ) ) );
            list1.Add( new AxisFunction( "Actual values[] (to double)", ( CaseResult z ) => z.Actual?.Cast<object>().Select( zz => Convert.ToDouble( zz ) ) ) );
            list1.Add( new AxisFunction( "All indices[] (obs)", ( CaseResult z ) => IndicesToPlot( z.Indices ) ) );
            list1.Add( new AxisFunction( "Indices where score != 0[] (obs)", ( CaseResult z ) => IndicesToPlot( z.Indices?.At( z.CorrectPredictions ) ) ));
            list1.Add( new AxisFunction( "Indices where score == 0[] (obs)", ( CaseResult z ) => IndicesToPlot( z.Indices?.At( z.IncorrectPredictions ) ) ) );

            // Y functions
            AddYFunctionMenu( new YFunctionMenuTag( "None", null ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Sum", z => z.Sum() ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Mean", z => z.Average() ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Count", z => z.Count() ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Min", z => z.Min() ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Max", z => z.Max() ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Median", Statistics.Median ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Upper quartile", Statistics.UpperQuartile ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Lower quartile", Statistics.LowerQuartile ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Standard deviation", Statistics.StandardDeviation ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Population standard deviation", Statistics.PopulationStandardDeviation ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Variance", Statistics.Variance ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Population variance", Statistics.PopulationVariance ) );
            AddYFunctionMenu( new YFunctionMenuTag( "RootMeanSquare", Statistics.RootMeanSquare ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Geometric mean", Statistics.GeometricMean ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Harmonic mean", Statistics.HarmonicMean ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Entropy", Statistics.Entropy ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Interquartile mean", z => Statistics.Mean( InterquartileRange( z ) ) ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Upper half mean", z => Statistics.Mean( UpperHalfRange( z ) ) ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Upper quartile mean", z => Statistics.Mean( UpperQuartileRange( z ) ) ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Upper half median", z => Statistics.Median( UpperHalfRange( z ) ) ) );
            AddYFunctionMenu( new YFunctionMenuTag( "Upper quartile median", z => Statistics.Median( UpperQuartileRange( z ) ) ) );


            foreach (AxisFunction ax in list1)
            {
                Image image;

                ToolStripMenuItem tsmi1 = new ToolStripMenuItem( "placeholder", null, xaxis ) { Tag = ax };
                xAxisToolStripMenuItem.DropDownItems.Add( tsmi1 );

                image = Resources.LineSeries_12630;
                ToolStripMenuItem tsmi2 = new ToolStripMenuItem( "placeholder", null, yaxis ) { Tag = ax };
                yAxisToolStripMenuItem.DropDownItems.Add( tsmi2 );
            }
        }


        private IEnumerable<double> UpperHalfRange( IEnumerable<double> valuesEnumerable )
        {
            double[] values = valuesEnumerable.ToArray();
            Array.Sort( values );

            int firstHalf = values.Length / 2;                  

            return values.Skip( firstHalf );
        }


        private IEnumerable<double> UpperQuartileRange( IEnumerable<double> valuesEnumerable )
        {
            double[] values = valuesEnumerable.ToArray();
            Array.Sort( values );

            int firstThreeQuarters = values.Length - (values.Length / 4);

            return values.Skip( firstThreeQuarters );
        }

        private IEnumerable<double> InterquartileRange( IEnumerable<double> valuesEnumerable)
        {
            double[] values = valuesEnumerable.ToArray();
            Array.Sort( values );

            int firstQuarter = values.Length / 4;
            int secondTwoQuarters = values.Length - (firstQuarter * 2);

            return values.Skip( firstQuarter ).Take( secondTwoQuarters );
        }

        private void AddYFunctionMenu( YFunctionMenuTag tag )
        {
            ToolStripMenuItem tsmi = new ToolStripMenuItem( "&" + tag.Title, null, YFunctionMenuItem_Click );
            tsmi.Tag = tag;
            yFunctionToolStripMenuItem.DropDownItems.Add( tsmi );
        }

        private void YFunctionMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            YFunctionMenuTag tag = (YFunctionMenuTag)tsmi.Tag;
            _yFunction = tag.YFunction;
            yFunctionToolStripMenuItem.Text = "Y Function = " + tag.Title;
            Replot();
        }

        private IEnumerable<double> IndicesToPlot( IEnumerable<int> indices )
        {
            return indices.Select( z => (double)_rp.GetObsByIndex( z ).PlotPosition );
        }

     

       

      
      

  

     

     

  

     

  

        private static bool ToBinary( string arg )
        {
            return int.Parse( arg ) == 1;
        }    

        private ObsName IndexToName( double index )
        {
            return IndexToName((int)index);
        }

        private ObsName IndexToName( int index )
        {
            return _rp.ObservationsByInternalIndex[index];
        } 

     

        internal static FrmDcAnalyser ShowAsync( FrmAnalyserLoader owner, string directory, ELoadFlags flags )
        {
            FrmDcAnalyser frm = new FrmDcAnalyser( owner, directory, flags );
            frm.Show( owner );
            return frm;
        }

        private MCharting.Plot NewPlot()
        {
            MCharting.Plot plot = new MCharting.Plot();

            plot.Style.ShowZeroX = false;
            plot.Style.ShowZeroY = false;
            plot.Style.GridStyle = new Pen( Color.Goldenrod );
            plot.Style.GridStyle.DashStyle = DashStyle.Dash;

            plot.SubTitle = $"{_rp.Variables.Length} variables, {_rp.Rounds.Length} rounds, {_rp.Epochs.Length} epochs";

            _chartMain.Visible = true;     

            return plot;
        }

        private void legendToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.legendToolStripMenuItem.Checked = !this.legendToolStripMenuItem.Checked;
            this._chartMain.Style.LegendDisplay = legendToolStripMenuItem.Checked ? MCharting.ELegendDisplay.Visible : MCharting.ELegendDisplay.Hidden;
            this._chartMain.Redraw();
        }

        private int Transparency
        {
            get
            {
                return transparencyToolStripMenuItem.Checked ? 128 : 255;
            }
        }

        private void transparencyToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Replot();
        }      

        private void exitToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Close();
        }

        private void saveImageToolStripMenuItem_Click( object sender, EventArgs e )
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "PNG (*.png)|*.png";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    _chartMain.DrawToBitmap().Save( sfd.FileName );
                }
            }
        }

        private void saveDataToolStripMenuItem_Click( object sender, EventArgs e )
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "CSV (*.csv)|*.csv";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText( sfd.FileName, _chartMain.CurrentPlot.ToCsv() );
                }
            }
        }

        private void _chkTraining_Click( object sender, EventArgs e )
        {
            Replot();
        }

        private void _chkValidation_Click( object sender, EventArgs e )
        {
            Replot();
        }

        private void _chkUsages_Click( object sender, EventArgs e )
        {
            Replot();
        }

        private void chart1_SelectionChanged( object sender, EventArgs e )
        {                                                  
            _lblInfo.Text = _chartMain.SelectedItem.ToString();

            if (!_chartMain.SelectedItem.HasDataPoint)
            {
                return;
            }

            if (_xaxis.Title.EndsWith( "(obs)" ))
            {
                _lblInfo.Text+= " - obs = "+ IndexToName( _chartMain.SelectedItem.X );
            }

            if (_yaxis.Title.EndsWith( "(obs)" ))
            {
                _lblInfo.Text += " - obs = " + IndexToName( _chartMain.SelectedItem.Y );
            }
        }   

        private void post_improvement()
        {
            MCharting.Plot plot = NewPlot();
            MCharting.Series series = new MCharting.Series();
            if (_chkTraining.Checked) plot.Series.Add( series );
            series.Name = "Improvement";
            series.Style.DrawVLines = new Pen( Color.FromArgb( Transparency, 0, 0, 255 ) );

            int[] counts = new int[_rp.Rounds.Length];
            int[] improves = new int[_rp.Rounds.Length];

            for (int index = 0; index < _rp.Rounds.Length; index++)
            {
                var round = _rp.Rounds[index];

                counts[round.WithinEpochIndex]++;

                if (ImprovesAfter( index ))
                {
                    improves[round.WithinEpochIndex]++;
                }
            }

            for (int index = 0; index < _rp.Rounds.Length; index++)
            {
                double percent = (100.0d * improves[index]) / counts[index];

                series.Points.Add( new MCharting.DataPoint( index, percent ) );
            }

            plot.Title = "Plot of rounds by percentage of rounds with same number in which fitness function accuracy increased in one or more following rounds";
            plot.XLabel = "Round number within epoch";
            plot.YLabel = "Percentage increased";

            _chartMain.SetPlot( plot );
        }

        private bool ImprovesAfter( int roundArrayIndex )
        {
            CRound round = _rp.Rounds[roundArrayIndex];

            for (int m = roundArrayIndex + 1; m < _rp.Rounds.Length; m++)
            {
                CRound round2 = _rp.Rounds[m];

                if (round2.IsEndOfEpoch)
                {
                    // Another epoch
                    return false;
                }

                if (round2.Training.AverageScore > round.Training.AverageScore)
                {
                    return true;
                }
            }

            return false;
        }             

        class AxisFunction
        {
            public readonly string Title;
            public readonly Func<CVariable, double> FnVariable;
            public readonly Func<CRound, double> FnRound;     
            public readonly Func<CaseResult, double> FnResult;
            public readonly Func<CaseResult, IEnumerable<double>> FnResult2;    

            public AxisFunction( string v, Func<CVariable, double> function )
            {
                this.Title = v;
                this.FnVariable = function;             
            }

            public AxisFunction( string v, Func<CRound, double> function )
            {
                this.Title = v;
                this.FnRound = function;             
            }

            public AxisFunction( string v, Func<CaseResult, double> function )
            {
                this.Title = v;
                this.FnResult = function;           
            }   

            public AxisFunction( string v, Func<CaseResult, IEnumerable<double>> function )
            {
                this.Title = v;
                this.FnResult2 = function;           
            }        

            internal double[] Get( object item, bool validation )
            {
                // Is it a CVariable?
                CVariable variable = item as CVariable;

                if (variable != null)
                {
                    if (FnVariable != null)
                    {
                        // FnVariable( variable )

                        if (validation) return null;
                        return new[] { FnVariable( variable ) };
                    }
                    else if (FnRound != null)
                    {
                        // FnRound( variable.Usages )
                        if (validation) return null;
                        return variable.Usages.Select( FnRound )?.ToArray() ?? new double[0];
                    }
                    else if (FnResult != null)
                    {
                        // FnResult( variable.Usages.Training )  
                        if (validation)
                        {
                            return variable.Usages.Select( z=> FnResult(z.Validation ) )?.ToArray() ?? new double[0];
                        }
                        else
                        {
                            return variable.Usages.Select( z => FnResult( z.Training ) )?.ToArray() ?? new double[0];
                        }           
                    }
                    else if (FnResult2 != null)
                    {
                        // FnResult2( variable.Usages.Training )  
                        if (validation)
                        {
                            return variable.Usages.SelectMany( z => FnResult2( z.Validation ) )?.ToArray() ?? new double[0];
                        }
                        else
                        {
                            return variable.Usages.SelectMany( z => FnResult2( z.Training ) )?.ToArray() ?? new double[0];
                        }
                    }
                }

                CRound round = item as CRound;

                if (round != null)
                {
                    if (FnVariable != null)
                    {
                        // FnVariable( round.Variables )
                        if (validation) return null;     
                        return round.Variables.Select( FnVariable )?.ToArray() ?? new double[0];
                    }
                    else if (FnRound != null)
                    {
                        // FnRound( round )
                        if (validation) return null;
                        return new[] { FnRound( round ) };
                    }
                    else if (FnResult != null)
                    {
                        if (validation)
                        {
                            return new[] { FnResult( round.Validation ) };
                        }
                        else
                        {
                            return new[] { FnResult( round.Training ) };
                        }
                    }
                    else if (FnResult2 != null)
                    {
                        if (validation)
                        {
                            return  FnResult2( round.Validation )?.ToArray() ?? new double[0];
                        }
                        else
                        {
                            return  FnResult2( round.Training )?.ToArray() ?? new double[0];
                        }
                    }
                }

                return null;
            }
        }

        private void Replot()
        {
            try
            {
                DoReplot();
            }
            catch (Exception ex)
            {
                var plot = new MCharting.Plot();
                plot.Title = ex.Message;
                _chartMain.SetPlot( plot );
            }
        }

        private void DoReplot()
        {
            // Check sources
            if (_source == null || _yaxis == null || _xaxis == null)
            {
                return;
            }

            // Create a plot
            MCharting.Plot plot = NewPlot();

            // Create "series 1"
            MCharting.Series series1 = new MCharting.Series();
            series1.Style.DrawPointsLine = new Pen( Color.FromArgb( Transparency, 0, 0, 255 ) );
            series1.Style.DrawPointsShape = MCharting.SeriesStyle.DrawPointsShapes.Plus;

            // Create "series 2"
            MCharting.Series series2 = new MCharting.Series();
            series2.Style.DrawPointsLine = new Pen( Color.FromArgb( Transparency, 255, 0, 0 ) );
            series2.Style.DrawPointsShape = MCharting.SeriesStyle.DrawPointsShapes.Plus;

            bool allForOne = true;

            // Iterate the "source"
            foreach (object item in _source)    // item is CVariable or CRound (round) or CRound (epoch)
            {
                // Get the point(s)
                double[] xa = _xaxis.Get( item, false );
                double[] ya = _yaxis.Get( item, false );

                AddPoints( series1, ref allForOne, item, xa, ya );

                double[] xav = _xaxis.Get( item, true );
                double[] yav = _yaxis.Get( item, true );

                if (xav != null || yav != null)
                {
                    AddPoints( series2, ref allForOne, item, xav ?? xa, yav ?? ya );
                }
            }

            // Apply Y function
            if (_yFunction != null)
            {
                ApplyYFunction( series1 );
                ApplyYFunction( series2 );
                allForOne = true;
            }

            // Name and add series
            if (series2.Points.Count != 0)
            {
                series1.Name = "Training";
                series2.Name = "Validation";

                if (_chkTraining.Checked) plot.Series.Add( series1 );
                if (_chkValidation.Checked) plot.Series.Add( series2 );
            }
            else
            {
                series1.Name = "Data";

                plot.Series.Add( series1 );
            }

            // Set title
            plot.Title = $"Plot of {GetTitle( _xaxis )} against {GetTitle( _yaxis )}";
            plot.XLabel = GetTitle( _xaxis );
            plot.YLabel = GetTitle( _yaxis );

            // Axis labels?
            if (_xaxis.Title.EndsWith( "(obs)" ))
            {
                SetXAxisLabelsToObsNames( plot );
            }

            // Render
            _chartMain.SetPlot( plot );
        }

        private void SetXAxisLabelsToObsNames( MCharting.Plot plot )
        {       
            plot.Style.AutoTickX = false;
            plot.Style.AxisFont = new Font( "Segoe UI", 7, FontStyle.Regular );
            plot.Style.XTickTextOrientation = Orientation.Vertical;

            foreach(var x in _rp.ObservationsByInternalIndex.Values)
            {
                plot.XTicks.Add( new MCharting.Tick( x.DisplayName, x.PlotPosition, 1, 0 ) );
            }
        }

        private void ApplyYFunction( MCharting.Series series )
        {
            Dictionary<double, List<double>> totals = new Dictionary<double, List<double>>();

            foreach (var p in series.Points)
            {
                foreach (double x in p.X)
                {
                    List<double> list = new List<double>();

                    if (!totals.TryGetValue( x, out list ))
                    {
                        list = new List<double>();
                        totals.Add( x, list );
                    }

                    foreach (double y in p.Y)
                    {
                        list.Add( y );
                    }
                }
            }

            series.Points.Clear();

            foreach (var kvp in totals)
            {
                series.Points.Add( new MCharting.DataPoint( kvp.Key, _yFunction( kvp.Value ) ) );
            }
        }

        private static bool AddPoints( MCharting.Series series, ref bool allForOne, object tag, double[] x, double[] y )
        {
            if (y.Length != 1)
            {
                allForOne = false;
            }

            if (x == null || y == null)
            {
                return false;
            }

            if (x.Length == y.Length)
            {
                for (int n = 0; n < x.Length; n++)
                {
                    series.Points.Add( new MCharting.DataPoint( x[n], y[n] ) { Tag = tag } );
                }
            }
            else if (x.Length == 1)
            {
                for (int n = 0; n < y.Length; n++)
                {
                    series.Points.Add( new MCharting.DataPoint( x[0], y[n] ) { Tag = tag } );
                }
            }
            else if (y.Length == 1)
            {
                for (int n = 0; n < x.Length; n++)
                {
                    series.Points.Add( new MCharting.DataPoint( x[n], y[0] ) { Tag = tag } );
                }
            }
            else if (x.Length != 0 && y.Length != 0)
            {
                throw new InvalidOperationException( "Don't know what to make of this..." );
            }

            return true;
        }

        private static void Count( Dictionary<int, int> counts, double[][] xxa, int index )
        {
            if (index >= xxa.Length)
            {
                return;
            }

            if (xxa == null)
            {
                return;
            }

            foreach (double x in xxa[index])
            {
                int xi = (int)x;

                if (counts.ContainsKey( xi ))
                {
                    counts[xi]++;
                }
                else
                {
                    counts[xi] = 1;
                }
            }
        }

        private void variablesToolStripMenuItem1_Click( object sender, EventArgs e )
        {
            Select( sender );
            _source = _rp.Variables;
            Replot();
            sourceToolStripMenuItem.Text = "&Source = Variables";
        }

        private void roundsToolStripMenuItem1_Click( object sender, EventArgs e )
        {
            Select( sender );
            _source = _rp.Rounds;
            sourceToolStripMenuItem.Text = "&Source = Rounds";
            Replot();
        }

        private void epochsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Select( sender );
            _source = _rp.Epochs;
            sourceToolStripMenuItem.Text = "&Source = Epochs";
            Replot();
        }

        private void yaxis( object sender, EventArgs e )
        {
            Select( sender );

            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;

            _yaxis = (AxisFunction)tsmi.Tag;
            yAxisToolStripMenuItem.Text = "&Y axis = " + _yaxis.Title;

            Replot();
        }

        private void xaxis( object sender, EventArgs e )
        {
            Select( sender );

            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;

            _xaxis = (AxisFunction)tsmi.Tag;
            xAxisToolStripMenuItem.Text = "&X axis = " + _xaxis.Title;

            Replot();
        }

        public static void Select( object x )
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)x;
            ToolStripMenuItem owner = (ToolStripMenuItem)tsmi.OwnerItem;

            foreach (ToolStripMenuItem t in owner.DropDownItems.Cast<object>().Where(z=> z is ToolStripMenuItem))
            {
                t.CheckState = CheckState.Unchecked;
            }

            tsmi.CheckState = CheckState.Indeterminate;
        }

        private void configurationToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Replot();
        }      

        enum EYFunction
        {
            None,
            Total,
            Average,
            Count
        }

        class YFunctionMenuTag
        {
            public Converter<IEnumerable<double>, double> YFunction;
            public string Title;

            public YFunctionMenuTag( string title, Converter<IEnumerable<double>, double> yfn )
            {
                Title = title;
                YFunction = yfn;
            }
        }  

        // stack overflow
        public static double StdDevSample( IEnumerable<double> values )
        {
            double ret = 0;

            int count = values.Count();

            if (count > 1)
            {
                //Compute the Average
                double avg = values.Average();

                //Perform the Sum of (value-avg)^2
                double sum = values.Sum( d => (d - avg) * (d - avg) );

                //Put it all together
                ret = Math.Sqrt( sum / (count - 1) );
            }

            return ret;
        }

        private void loadBinsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                string fn = FileHelper.Browse( this, "CSV Files (*.csv)|*.csv", null );

                if (fn != null)
                {
                    var csv = SpreadsheetReader.Default.Read<string>( fn );

                    int index = csv.FindColumn( "MidPpm" );

                    // Any extra variables
                    EnsureVariables( csv.NumRows );

                    for (int n = 0; n < csv.NumRows; n++)
                    {
                        _rp.Variables[n].MidPpm = double.Parse( csv[n, index] );
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show( this, ex.Message );
            }
        }

        private void EnsureVariables( int numRows )
        {
            if (numRows >= _rp.Variables.Length)
            {
                int orig = _rp.Variables.Length;
                Array.Resize( ref _rp.Variables, numRows );

                for (int n = orig; n < _rp.Variables.Length; n++)
                {
                    _rp.Variables[n] = new CVariable( n );
                }
            }
        }

        private void loadIntensitiesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                string fn = FileHelper.Browse( this, "CSV Files (*.csv)|*.csv", null );

                if (fn != null)
                {
                    var csv = SpreadsheetReader.Default.Read<double>( fn );

                    // Any extra variables
                    EnsureVariables( csv.NumCols );

                    for (int n = 0; n < csv.NumCols; n++)
                    {
                        _rp.Variables[n].Intensities = csv.CopyColumn(n);
                        _rp.Variables[n].AvgIntensity = _rp.Variables[n].Intensities.Average();
                        _rp.Variables[n].LogAvgIntensity = Math.Log10( _rp.Variables[n].AvgIntensity );
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show( this, ex.Message );
            }
        }

        private void loadObservationsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                string fn = FileHelper.Browse( this, "CSV Files (*.csv)|*.csv", null );

                if (fn != null)
                {
                    var csv = SpreadsheetReader.Default.Read<string>( fn );

                    // Any extra variables
                    for (int index = 0; index < csv.RowNames.Length; index++)
                    {
                        string name = csv.RowNames[index];

                        int closure = index;
                        ObsName obsName = _rp.ObservationsById.Values.First( z => z.ActualIndex == closure );
                        obsName.DisplayName = name;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show( this, ex.Message );
            }

            UpdateObsNames();
        }

        private void xAxisToolStripMenuItem_DropDownOpening( object senderu, EventArgs e )
        {
            ToolStripMenuItem sender = (ToolStripMenuItem)senderu;

            foreach (ToolStripMenuItem tsmi in sender.DropDownItems)
            {
                AxisFunction axF = (AxisFunction)tsmi.Tag;
                tsmi.Text = GetTitle(axF);
            }
        }

        private string GetTitle( AxisFunction axF )
        {
            string text = axF.Title;

            if (_source==_rp.Variables)
            {
                if (axF.FnVariable != null) return "Variable: " + text;
                if (axF.FnRound != null) return "Variable.RoundsUsed: " + text;
                if (axF.FnResult != null) return "Variable.RoundsUsed.Fitness: " + text;
                if (axF.FnResult2 != null) return "Variable.RoundsUsed.Fitness: " + text;

            }
            else if (_source == _rp.Rounds)
            {
                if (axF.FnVariable != null) return "Round.VariablesUsed: " + text;
                if (axF.FnRound != null) return "Round: " + text;
                if (axF.FnResult != null) return "Round.Fitness: " + text;
                if (axF.FnResult2 != null) return "Round.Fitness: " + text;
            }
            else if (_source == _rp.Epochs)
            {
                if (axF.FnVariable != null) return "Epoch.VariablesUsed: " + text;
                if (axF.FnRound != null) return "Epoch: " + text;
                if (axF.FnResult != null) return "Epoch.Fitness: " + text;
                if (axF.FnResult2 != null) return "Epoch.Fitness: " + text;
            }

            if (axF.FnVariable != null) return "(Variable): " + text;
            if (axF.FnRound != null) return "(Round): " + text;
            if (axF.FnResult != null) return "(Fitness): " + text;
            if (axF.FnResult2 != null) return "(Fitness): " + text;

            return text;
        }

        private void viewToolStripMenuItem_Click( object sender, EventArgs e )
        {

        }

        private void observationNameToolStripMenuItem_Click( object sender, EventArgs e )
        {       
            string x= InputBox.Show(this, "Observation index" );

            if (x != null)
            {
                int i;

                if (int.TryParse( x, out i ))
                {
                    MessageBox.Show( this, _rp.GetObsByIndex(i).DisplayName );
                }
            }
        }

        private void highCountsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string x = InputBox.Show( this, "Y value" );
            StringBuilder result = new StringBuilder();

            if (x != null)
            {
                double d;

                if (double.TryParse( x, out d ))
                {
                    foreach (var series in _chartMain.CurrentPlot.Series)
                    {
                        foreach (var dataPoint in series.Points)
                        {
                            if (dataPoint.X.Length != 1 || dataPoint.Y.Length != 1)
                            {
                                MessageBox.Show( this, "Datapoints must contain 1 X and 1 Y only." );
                                return;
                            }

                            if (dataPoint.Y[0] >= d)
                            {
                                if (_xaxis.Title.EndsWith( "(obs)" ))
                                {
                                    result.AppendLine( $"{dataPoint.X[0]} = {IndexToName(dataPoint.X[0])}, {dataPoint.Y[0]}" );
                                }
                                else
                                {
                                    result.AppendLine( $"{dataPoint.X[0]}, {dataPoint.Y[0]}" );
                                }
                            }
                        }
                    }
                }
            }

            InputBox.Show( this, "Results", result.ToString(), true );
        }

        private int SelectEpoch()
        {
            string sindex = InputBox.Show( this, "Enter epoch index", InputBox.EMode.ComboBox, Enumerable.Range( 0, _rp.Epochs.Length ) );
            int index;

            if (sindex == null || !int.TryParse( sindex, out index ) || index < 0 || index >= _rp.Epochs.Length)
            {
                return -1;
            }

            return index;
        }

        private void singleEpochToolStripMenuItem_Click( object sender, EventArgs e )
        {
            int index = SelectEpoch();

            if (index == -1)
            {
                return;
            }

            CRound epoch = _rp.Epochs[index];

            _source = _rp.Rounds.Where( z => z.Epoch == epoch ).ToArray();
            sourceToolStripMenuItem.Text = "&Source = EPOCH-" + index;
            Replot();
        }   

        private void specialPlotPredictionsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            
        }

        private void UpdateObsNames(  )
        {
            _allObservations = _rp.ObservationsById.Values.OrderByDescending( z=>z ).ToArray();

            for (int n = 0; n < _allObservations.Length; n++)
            {
                _allObservations[n].PlotPosition = n;
            }

            StringBuilder sb = new StringBuilder();
                                                                 
            sb.AppendLine();

            foreach (var kvp in _allObservations)
            {
                sb.Append( kvp.ToString() );
            }

            sb.AppendLine();     

            _txtVarNames.Text = sb.ToString();
        }

        private void chart1_Click( object sender, EventArgs e )
        {

        }

        private void xAxisToolStripMenuItem_Click( object sender, EventArgs e )
        {

        }

        private void yAxisToolStripMenuItem_Click( object sender, EventArgs e )
        {

        }

        private void sourceToolStripMenuItem_Click( object sender, EventArgs e )
        {

        }

        private void yFunctionToolStripMenuItem_Click( object sender, EventArgs e )
        {

        }

        private void selectRoundToolStripMenuItem_Click( object sender, EventArgs args )
        {
            string sindex = InputBox.Show( this, "Select round", InputBox.EMode.ComboBox, Enumerable.Range( 0, _rp.Rounds.Length ) );
            int index;

            if (sindex == null || !int.TryParse( sindex, out index ) || index < 0 || index >= _rp.Rounds.Length)
            {
                return;
            }

            CRound roundSel = _rp.Rounds[index];                      
            selectRoundToolStripMenuItem.Text = roundSel.ToString();

            StringBuilder sb = new StringBuilder();

            string fileName = Path.Combine( _rp.Directory, DcConstants.FILE_TREES );
            string gzFileName = fileName + ".gz";

            StreamReader sr2;

            if (File.Exists( gzFileName ))
            {
                sr2= new StreamReader( new GZipStream( new FileStream( gzFileName, FileMode.Open ), CompressionMode.Decompress ) );
            }
            else
            {
                sr2=new StreamReader( fileName );
            }

            using (StreamReader sr = sr2)
            {
                bool valid = false;

                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();

                    string[] e = line.Split( ',' );

                    if (e[0].StartsWith( "round " ))
                    {
                        int round = int.Parse( e[0].Substring( 6 ) );
                        valid = round == index;
                    }

                    if (valid)
                    {
                        sb.Append( "︳" );

                        bool prevEmpty = false;

                        foreach (string el in e)
                        {
                            if (string.IsNullOrWhiteSpace( el ))
                            {
                                sb.Append( new string( ' ', 25 ) );
                                prevEmpty = true;
                            }                 
                            else if (prevEmpty)
                            {
                                sb.Append( el.PadLeft( 25, '…' ) );
                                prevEmpty = false;
                            }
                            else
                            {
                                sb.Append( el.PadRight( 25, ' ' ) );
                            }
                            sb.Append( " ︳" );
                        }

                        sb.AppendLine();
                    }
                }
            }

            _txtTrees.Text = sb.ToString();
        }

        private void selectEpochToolStripMenuItem_Click( object sender, EventArgs e )
        {
            int index = SelectEpoch();

            if (index == -1)
            {
                return;
            }

            CRound epoch = _rp.Epochs[index];
            var src = _rp.Rounds.Where( z => z.Epoch == epoch ).ToArray();

            selectEpochToolStripMenuItem.Text = epoch.ToString();

            MCharting.Plot plot = NewPlot();

            plot.Style.BackColour = Color.Black;
            plot.Style.AxisStyle = Pens.White;
            plot.Style.TickStyle = Pens.White;
            plot.Style.AxisText = Brushes.White;

            MCharting.Series trainingCorrect = plot.Series.Add( "Correct (training)" );
            trainingCorrect.Style.DrawPoints = new SolidBrush( Color.FromArgb( 0, 192, 0 ) );
            trainingCorrect.Style.DrawPointsSize = 5;

            MCharting.Series trainingIncorrect = plot.Series.Add( "Incorrect (training)" );
            trainingIncorrect.Style.DrawPoints = new SolidBrush( Color.FromArgb( 255, 0, 0 ) );
            trainingIncorrect.Style.DrawPointsSize = 5;

            MCharting.Series validationCorrect = plot.Series.Add( "Correct (validation)" );
            validationCorrect.Style.DrawPoints = new SolidBrush( Color.FromArgb( 128, 192, 128 ) );
            validationCorrect.Style.DrawPointsSize = 5;

            MCharting.Series validationIncorrect = plot.Series.Add( "Incorrect (validation)" );
            validationIncorrect.Style.DrawPoints = new SolidBrush( Color.FromArgb( 255, 128, 128 ) );
            validationIncorrect.Style.DrawPointsSize = 5;

            foreach (var x in src)
            {
                int y = x.OverallIndex;

                if (x.Training.CorrectPredictions != null)
                {
                    for (int n = 0; n < x.Training.CorrectPredictions.Length; n++)
                    {
                        if (x.Training.CorrectPredictions[n])
                        {
                            trainingCorrect.Points.Add( new MCharting.DataPoint( IndexToName( x.Training.Indices[n]).PlotPosition, y ) );
                        }
                        else
                        {
                            trainingIncorrect.Points.Add( new MCharting.DataPoint( IndexToName( x.Training.Indices[n] ).PlotPosition, y ) );
                        }
                    }
                }

                if (x.Validation.CorrectPredictions != null)
                {
                    for (int n = 0; n < x.Validation.CorrectPredictions.Length; n++)
                    {
                        if (x.Validation.CorrectPredictions[n])
                        {
                            validationCorrect.Points.Add( new MCharting.DataPoint( IndexToName( x.Validation.Indices[n] ).PlotPosition, y ) );
                        }
                        else
                        {
                            validationIncorrect.Points.Add( new MCharting.DataPoint( IndexToName( x.Validation.Indices[n] ).PlotPosition, y ) );
                        }
                    }
                }
            }

            plot.Title = "Plot of predictions against rounds";
            plot.XLabel = "Observation";
            plot.YLabel = "Round index (overall)";

            SetXAxisLabelsToObsNames( plot );

            this._chartPred.SetPlot( plot );
        }

        private void _mnuToggleScaling_Click( object sender, EventArgs e )
        {
            _chartMain.View = new CoordSet( _chartMain.View.X1, 1, _chartMain.View.X2, 0.8 );
            _chartMain.Redraw();
        }

        private void savePredictionsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            bool validation = sender == saveValidationPredictionsToolStripMenuItem;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "CSV (*.csv)|*.csv";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    using (StreamWriter sw = new StreamWriter( sfd.FileName ))
                    {     
                        // Determine max obs index
                        int maxObsIndex = 0;

                        foreach (var round in _rp.Rounds)
                        {
                            maxObsIndex = Math.Max( maxObsIndex, round.Training.Indices.Max() );
                            maxObsIndex = Math.Max( maxObsIndex, round.Validation.Indices.Max() );
                        }

                        // Write obs
                        for (int n = 0; n <= maxObsIndex; n++)
                        {                     
                            sw.Write( "," );
                            sw.Write( "Obs" + n + "." + _rp.GetObsByIndex( n ).ToStringSafe() );
                        }

                        sw.WriteLine();

                        int maxRound = _rp.Rounds.Max( z => z.WithinEpochIndex );

                        // Iterate rounds
                        for (int roundNumber = 0; roundNumber < maxRound; roundNumber++)
                        {
                            // Row header
                            sw.Write( "Round" + roundNumber );

                            CRound[] repeats = _rp.Rounds.Where( z => z.WithinEpochIndex == roundNumber ).ToArray();

                            int[] usages = new int[maxObsIndex + 1];
                            double[] counts = new double[maxObsIndex + 1];

                            for (int n = 0; n < counts.Length; n++)
                            {
                                counts[n] = double.NaN;
                            }

                            // Sum counts
                            foreach (CRound round in repeats)
                            {
                                var cr = validation ? round.Validation : round.Training;

                                for (int n = 0; n < cr.Indices.Length; n++)
                                {
                                    int index = cr.Indices[n];
                                    double s = cr.BScores[n]; // this is 1 or 0

                                    System.Diagnostics.Debug.Assert( s == 0 || s == 1 );

                                    if (usages[index] == 0)
                                    {
                                        counts[index] = 0;
                                    }


                                    usages[index]++;
                                    counts[index] += s;
                                }
                            }

                            for (int index = 0; index < counts.Length; index++)
                            {
                                if (!double.IsNaN( counts[index] ))
                                {
                                    counts[index] /= usages[index];
                                }
                            }

                            foreach (var c in counts)
                            {
                                sw.Write( "," );

                                if (!double.IsNaN( c ))
                                {
                                    sw.Write( c );
                                }
                            }

                            sw.WriteLine();
                        }
                    }     
                }
            }
        }
    }
}
