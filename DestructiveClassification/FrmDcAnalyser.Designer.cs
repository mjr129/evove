﻿namespace TestApplication
{
    partial class FrmDcAnalyser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            MCharting.Selection selection1 = new MCharting.Selection();
            MCharting.Selection selection2 = new MCharting.Selection();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadObservationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadIntensitiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadBinsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.saveImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePredictionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.legendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transparencyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._chkTraining = new System.Windows.Forms.ToolStripMenuItem();
            this._chkValidation = new System.Windows.Forms.ToolStripMenuItem();
            this._chkUsages = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.observationNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.highCountsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._lblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.sourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.variablesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripSeparator();
            this.roundsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.epochsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singleEpochToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xAxisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yAxisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yFunctionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._mnuToggleScaling = new System.Windows.Forms.ToolStripMenuItem();
            this._chartMain = new MCharting.MChart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._txtConfiguration = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this._txtTrees = new System.Windows.Forms.TextBox();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.selectRoundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this._chartPred = new MCharting.MChart();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.selectEpochToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this._txtVarNames = new System.Windows.Forms.TextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this._txtSummary = new System.Windows.Forms.TextBox();
            this.saveValidationPredictionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.menuStrip3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.menuStrip4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.ShowItemToolTips = true;
            this.menuStrip1.Size = new System.Drawing.Size(1042, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadObservationsToolStripMenuItem,
            this.loadIntensitiesToolStripMenuItem,
            this.loadBinsToolStripMenuItem,
            this.toolStripMenuItem2,
            this.saveImageToolStripMenuItem,
            this.saveDataToolStripMenuItem,
            this.savePredictionsToolStripMenuItem,
            this.saveValidationPredictionsToolStripMenuItem,
            this.toolStripMenuItem5,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // loadObservationsToolStripMenuItem
            // 
            this.loadObservationsToolStripMenuItem.Name = "loadObservationsToolStripMenuItem";
            this.loadObservationsToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.loadObservationsToolStripMenuItem.Text = "&Load observations...";
            this.loadObservationsToolStripMenuItem.Click += new System.EventHandler(this.loadObservationsToolStripMenuItem_Click);
            // 
            // loadIntensitiesToolStripMenuItem
            // 
            this.loadIntensitiesToolStripMenuItem.Name = "loadIntensitiesToolStripMenuItem";
            this.loadIntensitiesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.loadIntensitiesToolStripMenuItem.Text = "&Load intensities...";
            this.loadIntensitiesToolStripMenuItem.Click += new System.EventHandler(this.loadIntensitiesToolStripMenuItem_Click);
            // 
            // loadBinsToolStripMenuItem
            // 
            this.loadBinsToolStripMenuItem.Name = "loadBinsToolStripMenuItem";
            this.loadBinsToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.loadBinsToolStripMenuItem.Text = "&Load bins...";
            this.loadBinsToolStripMenuItem.Click += new System.EventHandler(this.loadBinsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(221, 6);
            // 
            // saveImageToolStripMenuItem
            // 
            this.saveImageToolStripMenuItem.Name = "saveImageToolStripMenuItem";
            this.saveImageToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.saveImageToolStripMenuItem.Text = "&Save image...";
            this.saveImageToolStripMenuItem.Click += new System.EventHandler(this.saveImageToolStripMenuItem_Click);
            // 
            // saveDataToolStripMenuItem
            // 
            this.saveDataToolStripMenuItem.Name = "saveDataToolStripMenuItem";
            this.saveDataToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.saveDataToolStripMenuItem.Text = "&Save data...";
            this.saveDataToolStripMenuItem.Click += new System.EventHandler(this.saveDataToolStripMenuItem_Click);
            // 
            // savePredictionsToolStripMenuItem
            // 
            this.savePredictionsToolStripMenuItem.Name = "savePredictionsToolStripMenuItem";
            this.savePredictionsToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.savePredictionsToolStripMenuItem.Text = "&Save training predictions";
            this.savePredictionsToolStripMenuItem.Click += new System.EventHandler(this.savePredictionsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(221, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.legendToolStripMenuItem,
            this.transparencyToolStripMenuItem,
            this._chkTraining,
            this._chkValidation,
            this._chkUsages,
            this.toolStripMenuItem3,
            this.observationNameToolStripMenuItem,
            this.highCountsToolStripMenuItem});
            this.viewToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "&View";
            this.viewToolStripMenuItem.Click += new System.EventHandler(this.viewToolStripMenuItem_Click);
            // 
            // legendToolStripMenuItem
            // 
            this.legendToolStripMenuItem.Name = "legendToolStripMenuItem";
            this.legendToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.legendToolStripMenuItem.Text = "&Legend";
            this.legendToolStripMenuItem.Click += new System.EventHandler(this.legendToolStripMenuItem_Click);
            // 
            // transparencyToolStripMenuItem
            // 
            this.transparencyToolStripMenuItem.CheckOnClick = true;
            this.transparencyToolStripMenuItem.Name = "transparencyToolStripMenuItem";
            this.transparencyToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.transparencyToolStripMenuItem.Text = "&Transparency";
            this.transparencyToolStripMenuItem.Click += new System.EventHandler(this.transparencyToolStripMenuItem_Click);
            // 
            // _chkTraining
            // 
            this._chkTraining.Checked = true;
            this._chkTraining.CheckOnClick = true;
            this._chkTraining.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkTraining.Name = "_chkTraining";
            this._chkTraining.Size = new System.Drawing.Size(171, 22);
            this._chkTraining.Text = "&Training";
            this._chkTraining.Click += new System.EventHandler(this._chkTraining_Click);
            // 
            // _chkValidation
            // 
            this._chkValidation.Checked = true;
            this._chkValidation.CheckOnClick = true;
            this._chkValidation.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkValidation.Name = "_chkValidation";
            this._chkValidation.Size = new System.Drawing.Size(171, 22);
            this._chkValidation.Text = "&Validation";
            this._chkValidation.Click += new System.EventHandler(this._chkValidation_Click);
            // 
            // _chkUsages
            // 
            this._chkUsages.Checked = true;
            this._chkUsages.CheckOnClick = true;
            this._chkUsages.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkUsages.Name = "_chkUsages";
            this._chkUsages.Size = new System.Drawing.Size(171, 22);
            this._chkUsages.Text = "&Usages";
            this._chkUsages.Click += new System.EventHandler(this._chkUsages_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(168, 6);
            // 
            // observationNameToolStripMenuItem
            // 
            this.observationNameToolStripMenuItem.Name = "observationNameToolStripMenuItem";
            this.observationNameToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.observationNameToolStripMenuItem.Text = "&Observation name";
            this.observationNameToolStripMenuItem.Click += new System.EventHandler(this.observationNameToolStripMenuItem_Click);
            // 
            // highCountsToolStripMenuItem
            // 
            this.highCountsToolStripMenuItem.Name = "highCountsToolStripMenuItem";
            this.highCountsToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.highCountsToolStripMenuItem.Text = "&High counts";
            this.highCountsToolStripMenuItem.Click += new System.EventHandler(this.highCountsToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1042, 829);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.statusStrip1);
            this.tabPage1.Controls.Add(this.menuStrip2);
            this.tabPage1.Controls.Add(this._chartMain);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1034, 803);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Plot";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._lblInfo});
            this.statusStrip1.Location = new System.Drawing.Point(3, 778);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1028, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _lblInfo
            // 
            this._lblInfo.Name = "_lblInfo";
            this._lblInfo.Size = new System.Drawing.Size(98, 17);
            this._lblInfo.Text = "Select something";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sourceToolStripMenuItem,
            this.xAxisToolStripMenuItem,
            this.yAxisToolStripMenuItem,
            this.yFunctionToolStripMenuItem,
            this._mnuToggleScaling});
            this.menuStrip2.Location = new System.Drawing.Point(3, 3);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(1028, 24);
            this.menuStrip2.TabIndex = 4;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // sourceToolStripMenuItem
            // 
            this.sourceToolStripMenuItem.BackgroundImage = global::DestructiveClassification.Properties.Resources.box;
            this.sourceToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sourceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.variablesToolStripMenuItem1,
            this.toolStripMenuItem14,
            this.roundsToolStripMenuItem1,
            this.epochsToolStripMenuItem,
            this.singleEpochToolStripMenuItem});
            this.sourceToolStripMenuItem.ForeColor = System.Drawing.Color.Blue;
            this.sourceToolStripMenuItem.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.sourceToolStripMenuItem.Name = "sourceToolStripMenuItem";
            this.sourceToolStripMenuItem.Size = new System.Drawing.Size(150, 20);
            this.sourceToolStripMenuItem.Text = "&Source = (please specify)";
            this.sourceToolStripMenuItem.Click += new System.EventHandler(this.sourceToolStripMenuItem_Click);
            // 
            // variablesToolStripMenuItem1
            // 
            this.variablesToolStripMenuItem1.Name = "variablesToolStripMenuItem1";
            this.variablesToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.variablesToolStripMenuItem1.Text = "&All variables";
            this.variablesToolStripMenuItem1.Click += new System.EventHandler(this.variablesToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(219, 6);
            // 
            // roundsToolStripMenuItem1
            // 
            this.roundsToolStripMenuItem1.Name = "roundsToolStripMenuItem1";
            this.roundsToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.roundsToolStripMenuItem1.Text = "&All rounds";
            this.roundsToolStripMenuItem1.Click += new System.EventHandler(this.roundsToolStripMenuItem1_Click);
            // 
            // epochsToolStripMenuItem
            // 
            this.epochsToolStripMenuItem.Name = "epochsToolStripMenuItem";
            this.epochsToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.epochsToolStripMenuItem.Text = "&Epoch rounds";
            this.epochsToolStripMenuItem.Click += new System.EventHandler(this.epochsToolStripMenuItem_Click);
            // 
            // singleEpochToolStripMenuItem
            // 
            this.singleEpochToolStripMenuItem.Name = "singleEpochToolStripMenuItem";
            this.singleEpochToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.singleEpochToolStripMenuItem.Text = "Rounds &in specified epoch...";
            this.singleEpochToolStripMenuItem.Click += new System.EventHandler(this.singleEpochToolStripMenuItem_Click);
            // 
            // xAxisToolStripMenuItem
            // 
            this.xAxisToolStripMenuItem.BackgroundImage = global::DestructiveClassification.Properties.Resources.box;
            this.xAxisToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.xAxisToolStripMenuItem.ForeColor = System.Drawing.Color.Blue;
            this.xAxisToolStripMenuItem.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.xAxisToolStripMenuItem.Name = "xAxisToolStripMenuItem";
            this.xAxisToolStripMenuItem.Size = new System.Drawing.Size(143, 20);
            this.xAxisToolStripMenuItem.Text = "&X axis = (please specify)";
            this.xAxisToolStripMenuItem.DropDownOpening += new System.EventHandler(this.xAxisToolStripMenuItem_DropDownOpening);
            this.xAxisToolStripMenuItem.Click += new System.EventHandler(this.xAxisToolStripMenuItem_Click);
            // 
            // yAxisToolStripMenuItem
            // 
            this.yAxisToolStripMenuItem.BackgroundImage = global::DestructiveClassification.Properties.Resources.box;
            this.yAxisToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.yAxisToolStripMenuItem.ForeColor = System.Drawing.Color.Blue;
            this.yAxisToolStripMenuItem.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.yAxisToolStripMenuItem.Name = "yAxisToolStripMenuItem";
            this.yAxisToolStripMenuItem.Size = new System.Drawing.Size(143, 20);
            this.yAxisToolStripMenuItem.Text = "&Y axis = (please specify)";
            this.yAxisToolStripMenuItem.DropDownOpening += new System.EventHandler(this.xAxisToolStripMenuItem_DropDownOpening);
            this.yAxisToolStripMenuItem.Click += new System.EventHandler(this.yAxisToolStripMenuItem_Click);
            // 
            // yFunctionToolStripMenuItem
            // 
            this.yFunctionToolStripMenuItem.BackgroundImage = global::DestructiveClassification.Properties.Resources.box;
            this.yFunctionToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.yFunctionToolStripMenuItem.ForeColor = System.Drawing.Color.Blue;
            this.yFunctionToolStripMenuItem.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.yFunctionToolStripMenuItem.Name = "yFunctionToolStripMenuItem";
            this.yFunctionToolStripMenuItem.Size = new System.Drawing.Size(115, 20);
            this.yFunctionToolStripMenuItem.Text = "Y function = none";
            this.yFunctionToolStripMenuItem.Click += new System.EventHandler(this.yFunctionToolStripMenuItem_Click);
            // 
            // _mnuToggleScaling
            // 
            this._mnuToggleScaling.BackgroundImage = global::DestructiveClassification.Properties.Resources.box;
            this._mnuToggleScaling.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._mnuToggleScaling.ForeColor = System.Drawing.Color.Blue;
            this._mnuToggleScaling.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this._mnuToggleScaling.Name = "_mnuToggleScaling";
            this._mnuToggleScaling.Size = new System.Drawing.Size(96, 20);
            this._mnuToggleScaling.Text = "Toggle scaling";
            this._mnuToggleScaling.Click += new System.EventHandler(this._mnuToggleScaling_Click);
            // 
            // _chartMain
            // 
            this._chartMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this._chartMain.ForeColor = System.Drawing.Color.Blue;
            this._chartMain.Location = new System.Drawing.Point(3, 3);
            this._chartMain.Name = "_chartMain";
            this._chartMain.SelectedItem = selection1;
            this._chartMain.Size = new System.Drawing.Size(1028, 797);
            this._chartMain.TabIndex = 2;
            this._chartMain.Text = "button1";
            this._chartMain.Click += new System.EventHandler(this.chart1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._txtConfiguration);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1034, 803);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Configuration";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _txtConfiguration
            // 
            this._txtConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtConfiguration.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._txtConfiguration.Location = new System.Drawing.Point(3, 3);
            this._txtConfiguration.Multiline = true;
            this._txtConfiguration.Name = "_txtConfiguration";
            this._txtConfiguration.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._txtConfiguration.Size = new System.Drawing.Size(1028, 797);
            this._txtConfiguration.TabIndex = 0;
            this._txtConfiguration.WordWrap = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this._txtTrees);
            this.tabPage3.Controls.Add(this.menuStrip3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1034, 803);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Trees";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // _txtTrees
            // 
            this._txtTrees.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtTrees.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._txtTrees.Location = new System.Drawing.Point(3, 27);
            this._txtTrees.Multiline = true;
            this._txtTrees.Name = "_txtTrees";
            this._txtTrees.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._txtTrees.Size = new System.Drawing.Size(1028, 773);
            this._txtTrees.TabIndex = 2;
            this._txtTrees.WordWrap = false;
            // 
            // menuStrip3
            // 
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectRoundToolStripMenuItem});
            this.menuStrip3.Location = new System.Drawing.Point(3, 3);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Size = new System.Drawing.Size(1028, 24);
            this.menuStrip3.TabIndex = 1;
            this.menuStrip3.Text = "menuStrip3";
            // 
            // selectRoundToolStripMenuItem
            // 
            this.selectRoundToolStripMenuItem.BackgroundImage = global::DestructiveClassification.Properties.Resources.box;
            this.selectRoundToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.selectRoundToolStripMenuItem.Name = "selectRoundToolStripMenuItem";
            this.selectRoundToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.selectRoundToolStripMenuItem.Text = "&Select round...";
            this.selectRoundToolStripMenuItem.Click += new System.EventHandler(this.selectRoundToolStripMenuItem_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this._chartPred);
            this.tabPage4.Controls.Add(this.menuStrip4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1034, 803);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Predictions";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // _chartPred
            // 
            this._chartPred.Dock = System.Windows.Forms.DockStyle.Fill;
            this._chartPred.ForeColor = System.Drawing.Color.Blue;
            this._chartPred.Location = new System.Drawing.Point(3, 27);
            this._chartPred.Name = "_chartPred";
            this._chartPred.SelectedItem = selection2;
            this._chartPred.Size = new System.Drawing.Size(1028, 773);
            this._chartPred.TabIndex = 3;
            this._chartPred.Text = "button1";
            // 
            // menuStrip4
            // 
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectEpochToolStripMenuItem});
            this.menuStrip4.Location = new System.Drawing.Point(3, 3);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.Size = new System.Drawing.Size(1028, 24);
            this.menuStrip4.TabIndex = 4;
            this.menuStrip4.Text = "menuStrip4";
            // 
            // selectEpochToolStripMenuItem
            // 
            this.selectEpochToolStripMenuItem.BackgroundImage = global::DestructiveClassification.Properties.Resources.box;
            this.selectEpochToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.selectEpochToolStripMenuItem.Name = "selectEpochToolStripMenuItem";
            this.selectEpochToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.selectEpochToolStripMenuItem.Text = "&Select epoch...";
            this.selectEpochToolStripMenuItem.Click += new System.EventHandler(this.selectEpochToolStripMenuItem_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this._txtVarNames);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1034, 803);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Variables";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // _txtVarNames
            // 
            this._txtVarNames.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtVarNames.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._txtVarNames.Location = new System.Drawing.Point(3, 3);
            this._txtVarNames.Multiline = true;
            this._txtVarNames.Name = "_txtVarNames";
            this._txtVarNames.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._txtVarNames.Size = new System.Drawing.Size(1028, 797);
            this._txtVarNames.TabIndex = 1;
            this._txtVarNames.WordWrap = false;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this._txtSummary);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1034, 803);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Summary";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // _txtSummary
            // 
            this._txtSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtSummary.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._txtSummary.Location = new System.Drawing.Point(3, 3);
            this._txtSummary.Multiline = true;
            this._txtSummary.Name = "_txtSummary";
            this._txtSummary.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._txtSummary.Size = new System.Drawing.Size(1028, 797);
            this._txtSummary.TabIndex = 1;
            this._txtSummary.WordWrap = false;
            // 
            // saveValidationPredictionsToolStripMenuItem
            // 
            this.saveValidationPredictionsToolStripMenuItem.Name = "saveValidationPredictionsToolStripMenuItem";
            this.saveValidationPredictionsToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.saveValidationPredictionsToolStripMenuItem.Text = "&Save validation predictions...";
            this.saveValidationPredictionsToolStripMenuItem.Click += new System.EventHandler(this.savePredictionsToolStripMenuItem_Click);
            // 
            // FrmDcAnalyser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 853);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmDcAnalyser";
            this.Text = "DC Analyser";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem legendToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transparencyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _chkTraining;
        private System.Windows.Forms.ToolStripMenuItem _chkValidation;
        private System.Windows.Forms.ToolStripMenuItem _chkUsages;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadBinsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem loadIntensitiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadObservationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem observationNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem highCountsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem variablesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem roundsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem epochsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singleEpochToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xAxisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yAxisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yFunctionToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _lblInfo;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private MCharting.MChart _chartMain;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox _txtConfiguration;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem selectRoundToolStripMenuItem;
        private System.Windows.Forms.TextBox _txtTrees;
        private System.Windows.Forms.TabPage tabPage4;
        private MCharting.MChart _chartPred;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.ToolStripMenuItem selectEpochToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem14;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox _txtVarNames;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TextBox _txtSummary;
        private System.Windows.Forms.ToolStripMenuItem _mnuToggleScaling;
        private System.Windows.Forms.ToolStripMenuItem savePredictionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveValidationPredictionsToolStripMenuItem;
    }
}