﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove;
using Evove.Operators.Stopping;

namespace TestApplication
{


    [Serializable]
    public class DataSetConfiguration : ISupportsCsv
    {
        public string   DataFileName;
        public string   ClassFileName;
        public bool     DataFileHeader;
        public bool     DataFileRowNames;
        public bool     ClassFileHeader;
        public string   ClassFileClassColumn;
        public string   ClassFileTimeColumn;

        public double   ClassTrue;
        public double   ClassFalse;

        void ISupportsCsv.ToCsv( CsvWriter csv )
        {
            csv.Serialise( this );
        }

        public override string ToString()
        {
            return this.CsvAsString();
        }
    }
}
