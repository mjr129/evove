﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGui.Helpers;

namespace TestApplication.Data.Results
{      
    /// <summary>
    /// Represents a set of predicted vs. actual values, and their corresponding match scores.
    /// </summary>
    [Serializable]
    public sealed class CaseResult
    {
        /// <summary>
        /// Indices of the observations used in the analysis.
        /// </summary>
        public readonly int[] Indices;

        /// <summary>
        /// Actual values.
        /// </summary>
        public readonly Array Actual;

        /// <summary>
        /// Predicted values.
        /// </summary>
        public readonly Array Predicted;

        /// <summary>
        /// Scores of actual vs. predicted, per value.
        /// (Actual scores depend on scoring function.)
        /// </summary>
        public readonly double[] Scores;

        /// <summary>
        /// Scores of actual vs. predicted, per value.
        /// (Actual scores depend on scoring function.)
        /// </summary>
        public readonly double[] BScores;

        /// <summary>
        /// Average of <see cref="Scores"/>.
        /// </summary>
        public readonly double AverageScore;

        /// <summary>
        /// Fraction of <see cref="Scores"/> that are not equal to zero.
        /// </summary>
        public readonly double AverageBScore;   

        /// <summary>
        /// True for values where the <see cref="Scores"/> are not equal to zero.
        /// </summary>
        public bool[] CorrectPredictions => BScores.Select( z => z != 0.0d ).ToArray();

        /// <summary>
        /// True for values where the <see cref="Scores"/> are equal to zero.
        /// </summary>
        public bool[] IncorrectPredictions => BScores.Select( z => z == 0.0d ).ToArray();                  

        /// <summary>
        /// PRIVATE CONSTRUCTOR
        /// Use <see cref="CaseResult.New{T}"/>.
        /// </summary>               
        private CaseResult( int[] indices, Array actual, Array predicted, double[] scores,double[] bscores, double averageScore, double averageBScore )
        {
            this.Indices = indices;
            this.Actual = actual;
            this.Predicted = predicted;
            this.Scores = scores;
            this.BScores = bscores;
            this.AverageScore = averageScore;
            this.AverageBScore = averageBScore;
        }

        /// <summary>
        /// Creates a new case result, using the specified scoring function.
        /// </summary>
        /// <typeparam name="T">The type of object. This is only for scoring function convenience.</typeparam>
        /// <param name="indices">Identifiers for cases represented</param>
        /// <param name="actual">Actual values of cases</param>
        /// <param name="predicted">Predicted values of cases</param>
        /// <param name="scorer">How to generate the scores (per case)</param>
        /// <returns></returns>
        public static CaseResult New<T>( int[] indices, T[] actual, T[] predicted, Func<T, T, double> scorer, Func<T, T, double> bscorer )
        {                            
            double[] scores = new double[actual.Length];
            double totalScore = 0.0d;
            double totalBScore = 0.0d;

            for (int n = 0; n < actual.Length; n++)
            {
                var sA= scorer( actual[n], predicted[n] );  
                scores[n] = sA;   
                totalScore += sA;  
            }

            double averageScore = totalScore / actual.Length;
            double[] bscores = null;

            if (bscorer != null)
            {
                bscores = new double[actual.Length];

                for (int n = 0; n < actual.Length; n++)
                {
                    var sB = bscorer( actual[n], predicted[n] );        
                    bscores[n] = sB;
                    totalBScore += sB;
                }
            }

            double averageBScore = totalBScore / actual.Length;

            return new CaseResult( indices, actual, predicted, scores, bscores, averageScore, averageBScore );
        }                                                                  

        /// <summary>
        /// Writes the predictions to a text file.
        /// </summary>
        /// <param name="roundPrefix">Indicates the round</param>
        /// <param name="setPrefix">Indicates the set (training or validation)</param>
        /// <param name="sb">Where to write to</param>
        /// <param name="dataSet">The dataset, used for resolving indices to names.</param>
        public void WritePredictions( string roundPrefix, string setPrefix, StringBuilder sb, DataSet dataSet )
        {
            if (Actual != null)
            {
                sb.AppendLine( roundPrefix + "," + setPrefix + ",\"NAMES\"," + string.Join( ",", Indices.Select( z => dataSet.Data.RowNames[z] ) ) );
                sb.AppendLine( ",,\"ACTUAL\"," + AsString( Actual ) );
                sb.AppendLine( ",,\"PREDICTED\"," + AsString( Predicted ) );
                sb.AppendLine( ",,\"SCORE\"," + string.Join( ",", Scores ) );
            }
        }

        /// <summary>
        /// Like <see cref="string.Join"/>, but works on an <see cref="Array"/>.
        /// </summary>                   
        private string AsString( Array array )
        {
            StringBuilder sb = new StringBuilder();

            foreach (object x in array)
            {
                if (sb.Length != 0)
                {
                    sb.Append( "," );
                }

                sb.Append( x );
            }

            return sb.ToString();
        }

        /// <summary>
        /// Returns the average score as a string
        /// </summary>                           
        public override string ToString()
        {
            return "Average score = " + AverageScore;
        }

        /// <summary>
        /// Writes the headers, used with <see cref="ToCsv"/>.
        /// </summary>
        /// <param name="prefix">The set (training or validation)</param>
        /// <returns>The header string</returns>
        public static string WriteHeaders(string prefix)
        {
            return $"\"{prefix}.averagescore\"";
        }

        /// <summary>
        /// Returns the CSV data - the average score.
        /// </summary>           
        public string ToCsv(  )
        {
            return AverageScore.ToString(); 
        }
    }
}
