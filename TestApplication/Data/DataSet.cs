﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove;
using MGui.Datatypes;
using MGui.Helpers;

namespace TestApplication.Data
{
    public class DataSet : ISupportsCsv
    {
        public Spreadsheet<double> Data;
        public bool[] Classes;
        public int[] Times;

        public readonly double MaxValue = double.MinValue;
        public readonly double MinValue = double.MaxValue;

        void ISupportsCsv.ToCsv( CsvWriter csv )
        {
            csv.SetHeaders( CsvWriter.FIELD, CsvWriter.VALUE );
            csv.Add( "Number of variables", NumVariables.ToString() );
            csv.Add( "Number of observations", NumObservations.ToString() );
            csv.EndHeaders();
        }

        public int NumObservations
        {
            get
            {
                return Data.NumRows;
            }
        }

        public int NumVariables
        {
            get
            {
                return Data.NumCols;
            }
        }

        public DataSet( DataSetConfiguration configuation )
        {
            // DATA
            SpreadsheetReader dreader = new SpreadsheetReader()
            {
                HasColNames = configuation.DataFileHeader,
                HasRowNames = configuation.DataFileRowNames,
                TolerantConversion = true, // Honey has missing values
            };

            Spreadsheet<double> dataSS = dreader.Read<double>( configuation.DataFileName );

            // CLASS FILE
            SpreadsheetReader reader = new SpreadsheetReader()
            {
                HasColNames = configuation.ClassFileHeader,
                TolerantConversion = true,
            };

            Spreadsheet<int> spreadsheet = reader.Read<int>( configuation.ClassFileName, converter );

            int classColumn = spreadsheet.FindColumn( configuation.ClassFileClassColumn );
            int[] times;

            if (configuation.ClassFileTimeColumn != null)
            {
                int timeColumn = spreadsheet.FindColumn( configuation.ClassFileTimeColumn );
                times = spreadsheet.CopyColumn( timeColumn );
            }
            else
            {
                times = null;
            }

            int[] classes = spreadsheet.CopyColumn( classColumn );
            int[] validObservations = classes.Which( z => z == configuation.ClassTrue || z == configuation.ClassFalse ).ToArray();

            Classes = classes.At( validObservations ).Select( z => z == configuation.ClassTrue ).ToArray();
            Times = times?.Extract( validObservations );
            Data = dataSS.Subset( validObservations, null );

            Debug.Assert( Classes.Length == Data.NumRows );
        }

        private int converter( string input )
        {
            switch (input)
            {
                case "TRUE":
                    {
                        return 1;
                    }

                case "FALSE":
                    {
                        return 0;
                    }

                default:
                    {
                        int result;

                        if (int.TryParse( input, out result ))
                        {
                            return result;
                        }

                        return 0;
                    }
            }
        }
    }
}

