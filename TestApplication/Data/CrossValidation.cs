﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove;

namespace TestApplication.Data
{
    public class CrossValidation
    {
        public readonly int[] Training;
        public readonly int[] Validation;

        public CrossValidation( int[] training, int[] validation )
        {
            this.Training = training;
            this.Validation = validation;
        }

        public static CrossValidation LeaveOneOut( DataSet data, int[] potentialIds, int looIndex )
        {
            if (potentialIds == null || potentialIds.Length == 0)
            {
                potentialIds = Enumerable.Range( 0, data.NumObservations ).ToArray();
            }
            else
            {
                potentialIds = potentialIds.OrderBy( z => z ).ToArray();
            }

            looIndex = looIndex % potentialIds.Length;
            
            int[] validationIds = new[] { potentialIds[looIndex] };
            int[] trainingIds = potentialIds.Except( validationIds ).ToArray();

            return new CrossValidation( trainingIds, validationIds );
        }

        public static CrossValidation All( DataSet data )
        {
            return new CrossValidation( Enumerable.Range( 0, data.NumObservations ).ToArray(), new int[0] );
        }

        public static CrossValidation AndTheRest( DataSet data, int[] trainingIds )
        {
            if (trainingIds == null || trainingIds.Length == 0)
            {
                trainingIds = Enumerable.Range( 0, data.NumObservations ).ToArray();
            }
            else
            {
                trainingIds = trainingIds.OrderBy(z=>z).ToArray();
            }

            int[] validationIds = Enumerable.Range( 0, data.NumObservations ).Where( z=> !trainingIds.Contains( z ) ).ToArray();

            return new CrossValidation( trainingIds, validationIds );
        }

        public static CrossValidation nFold( DataSet dataSet, int number, int randomSeed, int result )
        {
            CrossValidation[] cvs = new CrossValidation[number];

            // Divide the number of observations into 𝓷 segments
            Tuple<int, int>[] divisions = Partition( dataSet.NumObservations, number );
            int[][] segments = new int[number][];

            // Shuffle the observation indices
            int[] order = Enumerable.Range( 0, dataSet.NumObservations ).ToArray();
            Shuffle( order, randomSeed );

            // Each segment gets its portion of the order
            for (int n = 0; n < number; n++)
            {
                segments[n] = order.Skip( divisions[n].Item1 ).Take( divisions[n].Item2 ).ToArray();

                Debug.Assert( segments[n].All( z => z >= 0 && z < dataSet.NumObservations ) );
            }

            // Create 𝓷 cross validations, each using segments[-i] for the training and segments[i] for the validation
            for (int n = 0; n < number; n++)
            {
                List<int> training = new List<int>();
                List<int> validation = new List<int>();

                for (int m = 0; m < number; m++)
                {
                    if (m == n)
                    {
                        validation.AddRange( segments[m] );
                    }
                    else
                    {
                        training.AddRange( segments[m] );
                    }
                }

                cvs[n] = new CrossValidation( training.ToArray(), validation.ToArray() );
                cvs[n].Validate( dataSet );
            }

            return cvs[result % cvs.Length];
        }

        public void Validate( DataSet dataSet )
        {
            EvoveHelper.Check( "CrossValidation.Validate", this.Training.Length + this.Validation.Length == dataSet.NumObservations, "" );
            EvoveHelper.Check( "CrossValidation.Validate", this.Training.Length >= this.Validation.Length, "" );
            EvoveHelper.Check( "CrossValidation.Validate", Enumerable.Range( 0, dataSet.NumObservations ).All( z => this.Training.Contains( z ) ^ this.Validation.Contains( z ) ), "" );
            EvoveHelper.Check( "CrossValidation.Validate", this.Training.All( z => z >= 0 && z < dataSet.NumObservations && !this.Validation.Contains( z ) ), "" );
            EvoveHelper.Check( "CrossValidation.Validate", this.Validation.All( z => z >= 0 && z < dataSet.NumObservations && !this.Training.Contains( z ) ), "" );
        }

        private static Tuple<int, int>[] Partition( int total, int divisions )
        {
            int count = (int)(((double)total / divisions) + 0.5d);
            int start = 0;
            Tuple<int, int>[] divisi = new Tuple<int, int>[divisions];

            for (int i = 0; i < divisions; i++)
            {
                if (i == divisions - 1)
                {
                    count = total - start;
                }

                divisi[i] = new Tuple<int, int>( start, count );
                start += count;
            }

            return divisi;
        }

        private static void Shuffle<T>( T[] array, int randomSeed )
        {
            Random rnd = new Random( randomSeed );

            int n = array.Length;

            while (n > 1)
            {
                int k = rnd.Next( n-- );
                T temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }
    }
}
