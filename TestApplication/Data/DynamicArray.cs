﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSerialisers
{
    /// <summary>
    /// This class is an array that will expand to allow insertion at an arbitrary
    /// position, e.g. in the statement myArray[n] = x.
    /// 
    /// For reference types this behaves like a faster dictionary, allowing the user to investigate 
    /// which indices are populated and retrieve values by an integer key.
    /// For value types null cannot be used to identify empty elements and therefore this class
    /// behaves more like an expanding array (since elements are never null).            
    /// </summary>                      
    public class DynamicArray<T> : IEnumerable<T>, IList<T>, IReadOnlyList<T>
    {
        private T[] _array = new T[4];
        private int _count;
        private readonly Converter<int, T> _creator;
        private readonly bool _isClass;

        /// <summary>
        /// CONSTRUCTOR           
        /// (Similar to a Dictionary(of int, of T))
        /// </summary>
        public DynamicArray()
        {
            _isClass = typeof( T ).IsClass;
        }

        /// <summary>
        /// CONSTRUCTOR
        /// Creates a DynamicArray that automatically generates new elements if they are null when 
        /// the user tries to retrieve them via the indexer. As value types cannot be null their
        /// values are initialised when they array is created or expanded.
        /// </summary>                                                                                         
        /// <param name="creator">Creator function. This takes the index as the parameter.</param>
        public DynamicArray( Converter<int, T> creator )
            : this()
        {
            _creator = creator;
            _isClass = true;
        }

        /// <summary>
        /// Ensures array size is >= needed.
        /// O(n)
        /// </summary>                  
        private void EnsureCapacity( int needed )
        {
            if (this._array.Length > needed)
            {
                return;
            }

            int newSize;

            if (this._array.Length > (int.MaxValue / 2))
            {
                newSize = int.MaxValue;
            }
            else
            {
                newSize = this._array.Length * 2;
            }

            if (newSize < needed)
            {
                newSize = needed + 1;
            }

            T[] newArray = new T[newSize];
            Array.Copy( _array, newArray, _array.Length );

            if (!_isClass && _creator != null)
            {
                for (int n = _array.Length; n < newArray.Length; n++)
                {
                    newArray[n] = _creator( n );
                }
            }

            _array = newArray;
        }

        /// <summary>
        /// Gets or adds an item to the array.
        /// 
        /// Unlike Get() this will return NULL if the item does not exist, or create a new item if
        /// the class was constructed with a creator function.
        /// Unlike Add()/Replace() this will add or replace depending on whether the item exists.
        /// </summary>                                                                           
        public T this[int index]
        {
            get
            {
                if (Has( index ))
                {
                    return Get( index );
                }
                else if (_creator != null)
                {
                    T newElement = _creator( index );
                    Insert( index, newElement );
                    return newElement;
                }
                else
                {
                    return default( T );
                }
            }
            set
            {
                if (Has( index ))
                {
                    Replace( index, value );
                }
                else
                {
                    Insert( index, value );
                }
            }
        }

        /// <summary>
        /// Adds an item (the item must not exist)
        /// O(1) or O(n) if capacity is exceeded.
        /// </summary>           
        public void Insert( int index, T value )
        {
            if (index < 0)
            {
                throw new IndexOutOfRangeException( "Index must be >= 0." );
            }

            if (value == null)
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            if (Has( index ))
            {
                throw new InvalidOperationException( $"Item at index {index} already exists." );
            }

            EnsureCapacity( index );
            _array[index] = value;
        }

        /// <summary>
        /// Gets an item.
        /// The item must exist and cannot be null.
        /// O(1)
        /// </summary>           
        public T Get( int index )
        {
            CheckBoundsAndNull( index );
            return _array[index];
        }

        /// <summary>
        /// Returns if an item is within bounds and is not null.
        /// O(1)
        /// </summary>                  
        public bool Has( int index )
        {
            if (index < 0 || index >= _array.Length || _array[index] == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Throws an error if the item at index is out of bounds or is null.
        /// O(1)
        /// </summary>                              

        private void CheckBoundsAndNull( int index )
        {
            if (index < 0 || index > _array.Length)
            {
                throw new InvalidOperationException( $"Array index {index} out of bounds." );
            }

            if (_array[index] == null)
            {
                throw new InvalidOperationException( $"Array element {index} does not exist." );
            }
        }

        /// <summary>
        /// Clears an item in the array.
        /// The existing item cannot be null or out of bounds.
        /// O(1)
        /// </summary>                  
        public void RemoveAt( int id )
        {
            CheckBoundsAndNull( id );
            _array[id] = default( T );
            --_count;
        }

        /// <summary>
        /// Replaces an item.
        /// The original item cannot be null or out of bounds.
        /// The new item cannot be null.
        /// O(1)
        /// </summary>                  
        internal void Replace( int id, T value )
        {
            if (value == null)
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            CheckBoundsAndNull( id );
            _array[id] = value;
        }

        /// <summary>
        /// Returns the length of the array
        /// O(1)
        /// </summary>
        public int Capacity => _array.Length;

        /// <summary>
        /// Returns the count of non-null items.
        /// O(1)
        /// </summary>                  
        public int Count
        {
            get
            {
                if (_isClass)
                {
                    return _count;
                }
                else
                {
                    return Capacity;
                }
            }
        }

        /// <summary>
        /// Returns if there is any item in the array.
        /// O(1)
        /// </summary>                                
        public bool Any => _count != 0;

        /// <summary>
        /// IMPLEMENTS ICollection
        /// </summary>
        bool ICollection<T>.IsReadOnly => false;

        /// <summary>
        /// Implements IEnumerable(of T).
        /// </summary>             
        public IEnumerator<T> GetEnumerator()
        {
            return new DynamicArrayEnumerator( this );
        }

        /// <summary>
        /// Wraps IEnumerable to IEnumerable(of T).
        /// </summary>                             
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Gets the index of the first matching item.
        /// O(n) at worst.
        /// </summary>    
        public int IndexOf( T item )
        {
            for (int index = 0; index < _array.Length; index++)
            {
                if (item.Equals( _array[index] ))
                {
                    return index;
                }
            }

            return -1;
        }

        /// <summary>
        /// Adds an item in the first non-null slot.
        /// O(n) at worst
        /// </summary>                              
        public int Add( T item )
        {
            for (int index = 0; index < _array.Length; index++)
            {
                if (_array[index] == null)
                {
                    Insert( index, item );
                    return index;
                }
            }

            Insert( Capacity, item );
            return Capacity;
        }

        /// <summary>
        /// Wraps Add to ICollection.
        /// O(n)
        /// </summary>          

        void ICollection<T>.Add( T item )
        {
            Add( item );
        }

        /// <summary>
        /// Clears this array.
        /// The capacity is unchanged.
        /// O(n)
        /// </summary>
        public void Clear()
        {
            for (int index = 0; index < _array.Length; index++)
            {
                _array[index] = default( T );
            }

            _count = 0;
        }

        /// <summary>
        /// Returns if this array contains an item.
        /// O(n)
        /// </summary>
        public bool Contains( T value )
        {
            if (value == null)
            {
                throw new ArgumentNullException( nameof( value ) );
            }

            return _array.Contains( value );
        }

        /// <summary>
        /// Copies the NON-NULL elements of this array to another.
        /// O(n)
        /// </summary>                       
        public void CopyTo( T[] array, int arrayIndex )
        {
            for (int index = 0; index < _array.Length; index++)
            {
                if (_array[index] != null)
                {
                    array[arrayIndex] = _array[index];
                    arrayIndex++;
                }
            }
        }

        /// <summary>
        /// Removes an item from the array.
        /// O(n)
        /// </summary>
        public bool Remove( T item )
        {
            for (int index = 0; index < _array.Length; index++)
            {
                if (item.Equals( _array[index] ))
                {
                    RemoveAt( index );
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the array to the last non-null value.
        /// </summary>         
        public IEnumerable<T> TrimToCapacity()
        {
            int lastNonNullIndex = -1;

            for (int n = Capacity - 1; n >= 0; n--)
            {
                if (_array[n] != null)
                {
                    lastNonNullIndex = n;
                    break;
                }
            }

            for (int n = 0; n <= lastNonNullIndex; n++)
            {
                yield return _array[n];
            }
        }

        /// <summary>
        /// Enumerator class.
        /// O(n)
        /// </summary>
        private class DynamicArrayEnumerator : IEnumerator<T>
        {
            private int _currentIndex = -1;
            private DynamicArray<T> dynamicArray;

            public DynamicArrayEnumerator( DynamicArray<T> dynamicArray )
            {
                this.dynamicArray = dynamicArray;
            }

            public T Current => dynamicArray[_currentIndex];
            object IEnumerator.Current => Current;

            public void Dispose()
            {
                // N/A
            }

            public bool MoveNext()
            {
                do
                {
                    ++_currentIndex;

                    if (_currentIndex >= dynamicArray._array.Length)
                    {
                        return false;
                    }
                } while (!dynamicArray.Has( _currentIndex ));

                return true;
            }

            public void Reset()
            {
                _currentIndex = -1;
            }
        }
    }
}
