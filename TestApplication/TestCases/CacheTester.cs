﻿using Evove;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Evove.Attributes;

namespace TestApplication.TestCases
{
    /// <summary>
    /// This class tries to evolve the largest number possible.
    /// 
    /// The functions used demonstrate the various attributes which Evove uses to control
    /// caching.                                                             
    /// </summary>
    [EvoveClass( Caching = true, Persistence = true, Threading = true )]                                                                                                                            
    public sealed class CacheTester
    {
        const int NUMBER_OF_TEST_CASES = 10; // must set in parameters

        [EvoveField(EField.TestCase)]
        public int TestCase;

        private double _memory;
        private Random _random = new Random(1);

        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public double Run(Func<double> function)
        {
            _memory = 0;

            double total = 0;

            for (TestCase = 0; TestCase < NUMBER_OF_TEST_CASES; ++TestCase)
            {
                _memory = 0.0d;
                total += function();
            }

            return total;
        }

        [EvoveMethod( Cache = ECache.Uncached )]
        public double Randomise()
        {
            return _random.NextDouble();
        }

        public double GetTestCase()
        {
            return TestCase;
        }

        public double Add(double a, double b)
        {
            return a + b;
        }

        public double Subtract(double a, double b)
        {
            return a - b;
        }

        [EvoveMethod( Cache = ECache.Uncached )]
        public double StoreValue(double value)
        {
            _memory = value;
            return value;
        }

        [EvoveMethod( Cache = ECache.Uncached )]
        public double RetrieveValue()
        {
            return _memory;
        }
    }
}
