﻿using Evove;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace TestApplication.TestCases
{
    [EvoveClass( Caching = true, Persistence = true, Threading = false )]
    public sealed class EvolveOnes2
    {
        private readonly Randomiser _randomiser;

        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public double FitnessFunction(
            Func<double> v1,
            Func<double> v2,
            Func<double> v3,
            Func<double> v4,
            Func<double> v5,
            Func<double> v6,
            Func<double> v7,
            Func<double> v8,
            Func<double> v9,
            Func<double> v10)
        {
            return new[] { v1, v2, v3, v4, v5, v6, v7, v8, v9, v10 }.Sum(f => f());
        }

        public EvolveOnes2( InstantiationArgs args )
        {
            _randomiser = args.Population.Randomiser;
        }

        [EvoveMethod( Role = EMethod.ConstantProvider )]
        public double Constant()
        {
            return _randomiser.NextDouble();
        }                                  
    }
}
