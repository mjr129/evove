﻿using System;
using Evove;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.ComponentModel;
using Evove.Helpers;
using Evove.Attributes;
using Evove.Helpers.GpFunctions;

namespace TestApplication
{

    [EvoveClass( Caching = true, Persistence = true, Threading = true )]
    [Description("Tries to evolve a function to determine if the input is odd using a minimal set of functions.")]
    public sealed class SimpleSet
    {
        // Here is our "test data" - the result it 0 if the number is odd, else its is 1
        static double[] _question = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        static double[] _answer = { 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 };

        // We look at each test observation in turn, this is how we keep track
        [EvoveField( EField.TestCase )]
        public int _questionIndex;                     

        public StringBuilder DebugInfo = new StringBuilder();

        // Here is our fitness function!
        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public double Run(GpFunctions.DOUBLE evolvedFunction)
        {
            DebugInfo.Clear();

            Debug.Assert(DebugInfo.Length == 0);

            Stopwatch sw = Stopwatch.StartNew();

            double correct = 0;

            for (int n = 0; n < _question.Length; n++)
            {
                _questionIndex = n;

                double youSay = evolvedFunction();
                double iSay = _answer[_questionIndex];

                DebugInfo.Append(_questionIndex.ToString() + ": " + youSay + "/" + iSay);

                if (youSay == iSay)
                {
                    DebugInfo.AppendLine(" CORRECT");
                    correct += 1.0;
                }
                else
                {
                    DebugInfo.AppendLine(" INCORRECT");
                }
            }

            correct -= sw.Elapsed.TotalSeconds;

            return correct;
        }

        // These is our data access function!
        [EvoveMethod( Cache = ECache.TestCase )]  // To enable evaluation caching we should say this value is affected by "_questionIndex"
        public double GetQuestion()
        {
            return _question[_questionIndex];
        }

        // Now just some standard functions!
        public double Add(double d1, double d2)
        {
            return d1 + d2;
        }

        public double Multiply(double d1, double d2)
        {
            return d1 * d2;
        }

        public double IEEERemainder(double d1, double d2)
        {
            return Math.IEEERemainder(d1, d2);
        }

        public double NotZero(double v)
        {
            return (v != 0) ? 0 : 1;
        }

        public double Max(double a, double b)
        {
            return Math.Max(a, b);
        }

        public double If(double c, GpFunctions.DOUBLE a, GpFunctions.DOUBLE b)
        {
            return (c != 0) ? a() : b();
        }
    }
}