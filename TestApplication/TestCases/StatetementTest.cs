﻿using Evove;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace TestApplication.TestCases
{
    /// <summary>
    /// Evolves a program (a function with no return value) to populate an existing array with 1's.
    /// </summary>
    [EvoveClass( Persistence = false, Threading = false )]
    public sealed class StatetementTest
    {
        const int ARRAY_LENGTH = 10;
        bool[] _array = new bool[ARRAY_LENGTH];
        int _forEachValue;
        private readonly Randomiser _randomiser;

        public StatetementTest( InstantiationArgs args )
        {
            _randomiser = args.Population.Randomiser;
        }

        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public int FitnessFunction(Action function)
        {
            function();

            return _array.Count(z => z);
        }

        /// <summary>
        /// Leaf nodes may sometimes be requested to prevent excessive tree growth.
        /// In this case our leaf does nothing.
        /// </summary>
        public static void DoNothing()
        {
            // NA
        }
        
        public static void DoTwoThings(Action a, Action b)
        {
            a();
            b();
        }

        public void ForEach(Action a, int min, int max)
        {
            int previous = _forEachValue;

            for (int n = min; n <= max; n++)
            {
                _forEachValue = n;
                a();
            }

            _forEachValue = previous;
        }

        public int GetForEachValue()
        {
            return _forEachValue;
        }

        [EvoveMethod( Role = EMethod.ConstantProvider )]
        public int ConstantInt()
        {
            return _randomiser.Next(ARRAY_LENGTH);
        }

        [EvoveMethod( Role = EMethod.ConstantProvider )]
        public bool ConstantBoolean()
        {
            return _randomiser.Boolean();
        }

        public void SetValue(int index, bool value)
        {
            int trueIndex = EvoveHelper.Wrap(index, ARRAY_LENGTH);

            _array[trueIndex] = value;
        }
    }
}
