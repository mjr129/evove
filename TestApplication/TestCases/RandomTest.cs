﻿using Evove;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace TestApplication.TestCases
{
    [EvoveClass( Caching = false, Persistence = true, Threading = false )]
    public sealed class RandomTest
    {
        private readonly Randomiser _randomiser;

        public RandomTest( InstantiationArgs args )
        {
            _randomiser = args.Population.Randomiser;
        }

        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public static double FitnessFunction(Func<double> func)
        {
            return func();
        }

        public static double Add(double a, double b)
        {
            return a + b;
        }

        public static double Subtract(double a, double b)
        {
            return a - b;
        }

        public double Random()
        {
            return _randomiser.NextDouble();
        }
    }
}
