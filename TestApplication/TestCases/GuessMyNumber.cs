﻿using Evove;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Helpers;
using Evove.Attributes;
using Evove.Helpers.GpFunctions;

namespace TestApplication.TestCases
{
    [EvoveClass( Persistence = true, Threading = true )]
    [EvoveExtends(typeof(GpFunctions.Boolean))]
    [EvoveExtends(typeof(GpFunctions.Integer))]
    [EvoveExtends(typeof(GpFunctions.Memory<int>))]
    public sealed class GuessMyNumber
    {
        private Random _rnd = new Random(1);
        private int theAnswer;
        private GpFunctions.INTEGER _evolvableFunction;
        private Stack<int> _evolvableFunctionParameter = new Stack<int>();

        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public double Main(GpFunctions.INTEGER result, GpFunctions.INTEGER evolvableFunction)
        {
            Debug.Assert(evolvableFunction != null);

            _evolvableFunction = evolvableFunction;
            double avgDist = 0;

            for (int n = 0; n < 100; n++)
            {
                theAnswer = _rnd.Next(0, 10);

                int res = Math.Min(9, Math.Max(0, result()));

                avgDist += Math.Abs(res - theAnswer);
            }

            avgDist /= 100;

            return -avgDist;
        }

        public int RunEvolvableFunction(int parameter)
        {
            if (_evolvableFunctionParameter.Count == 15)
            {
                return 0;
            }

            _evolvableFunctionParameter.Push(parameter);
            int result = _evolvableFunction();
            _evolvableFunctionParameter.Pop();
            return result;
        }

        public int GetEvolvableFunctionParameter()
        {
            if (_evolvableFunctionParameter.Count == 0)
            {
                return 0;
            }

            return _evolvableFunctionParameter.Peek();
        }

        public int Guess(int x)
        {
            return x.CompareTo(theAnswer);
        }
    }
}
