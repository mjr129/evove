﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove;
using Evove.Attributes;
using Evove.Helpers;
using Evove.Helpers.GpFunctions;
using EvoveUi;
using TestApplication.Data;

namespace TestApplication.TestCases
{
    /// <summary>
    /// This class demonstrates an implementation of Davis's two-stage GP.
    /// </summary>
    [EvoveExtends( typeof( GpFunctions.Double ) )]
    [EvoveExtends( typeof( GpFunctions.Boolean ) )]
    [EvoveExtends( typeof( GpFunctions.Comparisson<double> ) )]
    [EvoveClass( Caching = false, Persistence = false, Threading = false )]
    [EvoveGuiExpects(typeof( DataSetConfiguration))]
    public sealed class TwoStageClassifier
    {
        /// <summary>
        /// Round we engage stage 2 in.
        /// </summary>
        const int TWO_STAGE_ROUND = 200;

        /// <summary>
        /// Counts the variables actually used at round 199.
        /// </summary>
        public int[] Counts;

        [EvoveField( EField.PopulationStatic )]
        public PopulationData Round;

        /// <summary>
        /// The class which calculates and holds the permitted variables after round 200.
        /// </summary>
        public class PopulationData
        {
            public IReadOnlyList<int> PermittedVariables;
            internal readonly DataSet Data;
            public int RoundNumber;

            public PopulationData( PopulationArgs args )
            {
                DataSetConfiguration config = args.Population.Parameters.Properties.Get< DataSetConfiguration>();
                args.Population.RoundEnd += Population_RoundEnd;
                args.Population.RoundStart += Population_RoundStart;
                Data = new DataSet( config );
            }

            private void Population_RoundStart( object sender, RoundStartEventArgs e )
            {
                RoundNumber = e.Population.Round;
            }

            private void Population_RoundEnd( object sender, RoundEndEventArgs args )
            {
                if (args.Population.Round != TWO_STAGE_ROUND)
                {
                    return;
                }

                int[] counts = new int[Data.NumVariables];

                foreach (Fitness i in args.Population.AllIndividuals().Select( z => z.LatestResult.Fitness ))
                {
                    for (int n = 0; n < Data.NumVariables; n++)
                    {
                        counts[n] += i.Counts[n];
                    }
                }

                int minCount = counts.OrderBy( z => z ).Take( 10 ).Last();
                List<int> permittedVariables = new List<int>();

                for (int n = 0; n < Data.NumVariables; n++)
                {
                    if (counts[n] >= minCount)
                    {
                        permittedVariables.Add( n );
                    }
                }

                PermittedVariables = permittedVariables;

                args.SpecialEvents |= ESpecialEvents.Extinction;
            }
        }

        /// <summary>
        /// Fitness object
        /// </summary>
        public class Fitness : FitnessValues.DoubleWrapper
        {
            public int[] Counts;
            public int NumberCorrect;
            private readonly int numberOfTestCases;

            public Fitness( double fitness, int[] counts, int numberCorrect, int numberOfTestCases )
                : base( fitness )
            {
                this.Counts = counts;
                this.NumberCorrect = numberCorrect;
                this.numberOfTestCases = numberOfTestCases;
            }

            public override string ToString()
            {
                return base.Value.ToString( "F1" ) + "% (" + NumberCorrect + "/" + numberOfTestCases + ")";
            }
        }

        int TestCase;

        /// <summary>
        /// Fitness function
        /// </summary>
        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public Fitness FitnessFunction( GpFunctions.BOOLEAN function )
        {
            if (Round.RoundNumber == (TWO_STAGE_ROUND - 1))
            {
                Counts = new int[NumberOfVariables];
            }

            int numberCorrect = 0;

            for (TestCase = 0; TestCase < Round.Data.NumObservations; TestCase++)
            {
                bool predicted = function();
                bool actual = Round.Data.Classes[TestCase];

                if (predicted == actual)
                {
                    numberCorrect++;
                }
            }

            double fraction = (double)numberCorrect / Round.Data.NumObservations;

            return new Fitness( fraction * 100, Counts, numberCorrect, Round.Data.NumObservations );
        }

        /// <summary>
        /// The number of variables
        /// </summary>
        private int NumberOfVariables
        {
            get
            {
                return Round.Data.NumVariables;
            }
        }

        /// <summary>
        /// Returns a variable.
        /// Making "index" an integer essentially makes "index" constant since we have no integer
        /// GP functions.
        /// </summary>         
        public double GetVariable( double index )
        {
            if (Round.PermittedVariables == null)
            {
                int actualIndex = EvoveHelper.Wrap( (int)index, NumberOfVariables );

                if (Counts != null)
                {
                    Counts[actualIndex]++;
                }

                return Round.Data.Data[TestCase,actualIndex];
            }
            else
            {
                int permittedIndex = EvoveHelper.Wrap( (int)index, Round.PermittedVariables.Count );

                return Round.Data.Data[TestCase,Round.PermittedVariables[permittedIndex]];
            }
        }
    }
}
