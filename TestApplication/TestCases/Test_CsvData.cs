﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove;
using Evove.Attributes;
using Evove.Helpers;
using Evove.Helpers.GpFunctions;
using TestApplication.Data;

namespace TestApplication.TestCases
{
    [Description( "Tries to determine the class of each observation in the StaticData set. Uses a multitude of predefined functions." )]
    [EvoveClass( Caching = true, Persistence = true, Threading = true )]
    [EvoveExtends( typeof( GpFunctions.Boolean ) )]
    [EvoveExtends( typeof( GpFunctions.Memory<double> ) )]
    [EvoveExtends( typeof( GpFunctions.Memory<int> ) )]
    [EvoveExtends( typeof( GpFunctions.Double ) )]
    [EvoveExtends( typeof( GpFunctions.Comparisson<double> ) )]
    public sealed class CsvData
    {
        internal static DataSet _staticData = null;

        [EvoveField( EField.TestCase )]
        public int TestCase;

        class ReturnInfo : IComparable
        {
            private bool[] correct;
            private int numberCorrect;
            private int current;

            public ReturnInfo( int count )
            {
                this.correct = new bool[count];
            }

            public int CompareTo( object obj )
            {
                ReturnInfo ri = obj as ReturnInfo;

                if (ri == null)
                {
                    return -1;
                }

                return numberCorrect.CompareTo( ri.numberCorrect );
            }

            public override string ToString()
            {
                return (int)((numberCorrect * 100) / correct.Length) + "%";
            }

            internal void Add( bool iscorrect )
            {
                correct[current] = iscorrect;

                if (iscorrect)
                {
                    numberCorrect++;
                }

                ++current;
            }
        }

        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public IComparable FitnessFunction( GpFunctions.DOUBLE function )
        {
            ReturnInfo result = new ReturnInfo( _staticData.Data.NumRows );

            for (TestCase = 0; TestCase < _staticData.Data.NumRows; ++TestCase)
            {
                result.Add( (int)(function() + 0.5d) == (_staticData.Classes[TestCase] ? 1.0d : 0.0d) );
            }

            return result;
        }

        [EvoveMethod( Cache = ECache.TestCase, Duplicate = 4 )]
        public double GetVariable( int index )
        {
            int col = (int)(index % _staticData.Data.NumCols);

            return _staticData.Data[TestCase,col];
        }

        [EvoveMethod( Cache = ECache.TestCase, Duplicate = 2 )]
        public double GetRange( int index, int count )
        {
            double total = 0;

            int colStart = (int)(index % _staticData.Data.NumCols);
            int colEndX = Math.Min( colStart + (int)count, _staticData.Data.NumCols );

            for (int n = colStart; n < colEndX; n++)
            {
                total += _staticData.Data[TestCase,n];
            }

            return total;
        }
    }
}
