﻿using Evove;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace TestApplication.TestCases
{
    [EvoveClass( Caching = true, Persistence = true, Threading = true )]
    public sealed class EvolveOnes
    {
        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public int FitnessFunction(
            Func<bool> v1,
            Func<bool> v2,
            Func<bool> v3,
            Func<bool> v4,
            Func<bool> v5,
            Func<bool> v6,
            Func<bool> v7,
            Func<bool> v8,
            Func<bool> v9,
            Func<bool> v10)
        {
            return new[] { v1, v2, v3, v4, v5, v6, v7, v8, v9, v10 }.Count(f => f());
        }

        public bool True()
        {
            return true;
        }

        public bool False()
        {
            return false;
        }               
    }
}
