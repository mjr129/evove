﻿using System;
using System.ComponentModel;
using Evove;
using Evove.Attributes;

namespace TestApplication
{
    [EvoveClass( Caching = true, Persistence = false, Threading = true )]
    [Description("For 10 test cases tries to determine if the two inputs add up to one. Defines everything here as an example, rather than using Evo.* helpers.")]
    public sealed class FirstAttempt
    {
        // Here is our "test data" - the result it 0 if the numbers add up to 1, else it is 1.
        // We make this static because it never changes
        // In practice this could even be in another class
        static double[] testDataA = { 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 };
        static double[] testDataB = { 1.0, 1.0, 0.8, 0.7, 0.1, 0.5, 0.1, 0.9, 0.2, 0.0, 1.0 };
        static double[] correctAnswer = { 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1 };

        // We can pass around evolved values (e.g. int) or functions
        // (e.g. Func<int>). The latter is better in cases where we don't
        // want the statement getting evaluated unless needed.
        // Let's declare the types for these now.
        public delegate void STATEMENT();
        public delegate bool BOOLEAN();
        public delegate double DOUBLE();
        public delegate int INTEGER();
        public delegate byte BYTE();

        // We also want our program to have some memory. We declare this too.
        // We'll use bytes to reference slots, only so their use remains
        // seperate from integers.
        public const byte SLOT_COUNT = 10;
        private double[] _doubleMemory = new double[SLOT_COUNT];
        private int[] _integerMemory = new int[SLOT_COUNT];
        private byte[] _slotMemory = new byte[SLOT_COUNT];

        [EvoveField(EField.TestCase)]
        public int _currentIndex;

        // Here is our fitness function!
        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public double Run(DOUBLE result)
        {
            int numberCorrect = 0;

            for (int n = 0; n < testDataA.Length; n++)
            {
                _currentIndex = n;

                if (result() == correctAnswer[n])
                {
                    numberCorrect++;
                }
            }

            return numberCorrect;
        }

        // Okay! All set let's declare the statements to choose from.
        // Remember to make these public if you want them to be used.

        [EvoveMethod( Cache = ECache.TestCase )]
        public double GetDataA()
        {
            return testDataA[_currentIndex];
        }

        [EvoveMethod( Cache = ECache.TestCase )]
        public double GetDataB()
        {
            return testDataB[_currentIndex];
        }


        // When individuals are saved they use the method name to remember which method they should call
        // Using overloads means we need to store more in the save file (and it is less easy to
        // read) so is best avoided.
        public void MultiStatement3(STATEMENT s1, STATEMENT s2, STATEMENT s3)
        {
            s1();
            s2();
            s3();
        }

        public void MultiStatement2(STATEMENT s1, STATEMENT s2)
        {
            s1();
            s2();
        }

        public void If(bool c, STATEMENT s)
        {
            if (c)
            {
                s();
            }
        }

        public void IfElse(bool c, STATEMENT s1, STATEMENT s2)
        {
            if (c)
            {
                s1();
            }
            else
            {
                s2();
            }
        }

        public double Add(double d1, double d2)
        {
            return d1 + d2;
        }

        public double Multiply(double d1, double d2)
        {
            return d1 * d2;
        }

        public double Sub(double d1, double d2)
        {
            return d1 - d2;
        }

        public double Divide(double d1, double d2)
        {
            return d1 / d2;
        }

        [EvoveMethod( Cache = ECache.Uncached )]
        public void StoreDouble(byte slot, double value)
        {
            _doubleMemory[WrapSlot(slot)] = value;
        }

        [EvoveMethod( Cache = ECache.Uncached )]
        public double RecallDouble(byte slot)
        {
            return _doubleMemory[WrapSlot(slot)];
        }

        [EvoveMethod( Cache = ECache.Uncached )]
        public void StoreInt32(byte slot, int value)
        {
            _integerMemory[WrapSlot(slot)] = value;
        }

        [EvoveMethod( Cache = ECache.Uncached )]
        public int RecallInt32(BYTE s)
        {
            return _integerMemory[WrapSlot(s())];
        }

        [EvoveMethod( Cache = ECache.Uncached )]
        public void StoreSlot(byte s, byte v)
        {
            _slotMemory[WrapSlot(s)] = WrapSlot(v);
        }

        // This is a private function, we don't want it being selected!
        private byte WrapSlot(byte v)
        {
            return (byte)(v % SLOT_COUNT);
        }

        [EvoveMethod( Cache = ECache.Uncached )]
        public byte RecallSlot(byte s)
        {
            byte slot = WrapSlot(s);
            return _slotMemory[slot];
        }

        public double Sin(double v)
        {
            return Math.Sin(v);
        }

        public double Cos(double v)
        {
            return Math.Cos(v);
        }

        public double Tan(double v)
        {
            return Math.Tan(v);
        }

        public bool NotZero(double v)
        {
            return v != 0;
        }

        public bool IsValid(double v)
        {
            return !double.IsNaN(v) && !double.IsInfinity(v);
        }

        public bool LessThan(double a, double b)
        {
            return a < b;
        }

        public bool MoreThan(double a, double b)
        {
            return a > b;
        }

        public bool NotEqualTo(double a, double b)
        {
            return a != b;
        }

        public bool EqualTo(double a, double b)
        {
            return a == b;
        }

        public bool Not(bool value)
        {
            return !value;
        }

        public bool And(bool a, BOOLEAN b)
        {
            return a && b();
        }

        public bool Or(bool a, BOOLEAN b)
        {
            return a || b();
        }

        public bool Xor(bool a, bool b)
        {
            return a ^ b;
        }

        public double TernaryOperatorD(bool c, DOUBLE a, DOUBLE b)
        {
            return c ? a() : b();
        }

        public int TernaryOperatorI(bool c, INTEGER a, INTEGER b)
        {
            return c ? a() : b();
        }

        public int ToInt(DOUBLE v)
        {
            return (int)v();
        }

        public double ToDouble(INTEGER v)
        {
            return v();
        }
    }
}