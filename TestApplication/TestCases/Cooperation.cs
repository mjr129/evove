﻿using Evove;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Helpers;
using Evove.Attributes;
using Evove.Helpers.GpFunctions;

namespace TestApplication.TestCases
{
    [EvoveExtends(typeof(GpFunctions.SimpleDouble))]
    [EvoveExtends(typeof(GpFunctions.Boolean))]
    [EvoveExtends(typeof(GpFunctions.Comparisson<double>))]
    [EvoveExtends(typeof(GpFunctions.StatementControlFlow))]
    [EvoveExtends(typeof(GpFunctions.Statement))]
    [EvoveClass( Caching = false, Persistence = true, Threading = false )]
    public sealed class Cooperation
    {
        public class World
        {
            public double Food;
            public int Poison;

            public World(PopulationArgs args)
            {
                args.Population.RoundStart += Population_RoundStart;
            }

            private void Population_RoundStart(object sender, RoundStartEventArgs e)
            {
                Food = 1.0d;
                Poison = 0;
            }
        }

        [EvoveField(EField.PopulationStatic)]
        public World Round;

        private double _energy;

        [EvoveMethod( Role = EMethod.FitnessFunction )]
        public double FitnessFunction(Action action)
        {
            action();
            return _energy;
        }

        public double GetRemainingFood()
        {
            return Round.Food;
        }

        public void Sacrifice()
        {
            Round.Poison = 0;
            _energy = -1.0d;
        }

        public void Eat()
        {
            const double EAT = 0.01;

            if (Round.Poison > 0)
            {
                _energy -= EAT;
                Round.Poison--;
            }
            else if (Round.Food < EAT)
            {
                _energy += Round.Food;
                Round.Food = 0.0d;
            }
            else
            {
                _energy += EAT;
                Round.Food -= EAT;
            }
        }

        public void Poison()
        {
            Round.Poison++;
        }
    }
}
