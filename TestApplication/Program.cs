﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Evove;
using Evove.Operators.Breed;
using Evove.Operators.Breed.Crossover;
using Evove.Operators.Breed.Mutation;
using Evove.Operators.SelectionMethods;
using Evove.Operators.Stopping;
using Evove.Operators.Termination;
using EvoveUi;

namespace TestApplication
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Thread.CurrentThread.Name = "MAIN THREAD";

            const int CLASS_AMBIENT_WARM = 1;
            const int CLASS_IDEAL_COOL = 2;

            DataSetConfiguration config = new DataSetConfiguration()
            {
                DataFileName = @"E:\MJR\Data\Meat\data.csv",
                ClassFileName = @"E:\MJR\Data\Meat\info.csv",
                DataFileHeader = true,
                DataFileRowNames = true,
                ClassFileHeader = true,
                ClassFileClassColumn = "type",
                ClassFileTimeColumn = "day",
                ClassTrue = CLASS_AMBIENT_WARM,
                ClassFalse = CLASS_IDEAL_COOL,
            };

            EvoveParameters parameters = new EvoveParameters()
            {
                ContinueFromFile = null,
                BreedingMethod = new BreedOperators.CrossoverAndMutation(
                    new CrossoverOperators.CommonPoint(), 0.25,
                    new MutationOperators.PerNode( 0.05, 0.33, 0.33, 0.33 ), 0.95, 0.25 ),
                NumberOfDemes = 1,
                NumberOfElites = 1,
                NumberOfEvaluatorThreads = 0,
                NumberOfBreederThreads = 0,
                IndividualsPerDeme = 200,
                SelectionMethod = new SelectionMethods.FitnessProportional(),
                StoppingConditions = new[] { new StoppingConditions.Never() },
                FitnessFunction = null,
                AdditionalFunctions = new Factory<InstantiationArgs>[0],
                OutputFolder = Path.Combine( Application.StartupPath, "Runs\\testdelme" ),
                MaxTreeDepth = 5,
                AllowCaching = false,
                SupportDebugging = true,
                CheckForCacheErrors = false,
                NumberOfTestCases = 0,
                TerminationOperators = new ITerminationOperator[] { new TerminationOperators.Time( 1000 ) },
                MigrationOperator = null,
                Properties = new[] { config },
                ProgressWatcher = null,
                AbandonPerEvaluationObjects = false,
                RandomSeed = 1,
                SupportMethodOverloads = false,
            };                
                             
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            EvoveGui.Start( parameters, null, RunMode.StartInDebugMode | RunMode.ConfigureParameters );
        }
    }
}
