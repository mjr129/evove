﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove
{
    public sealed class CsvWriter
    {
        public const string FIELD = "field";
        public const string VALUE = "value";
        List<string> _headers = new List<string>();
        List<int> _currentIndices = new List<int>();
        List<string[]> _data = new List<string[]>();
        Stack<string[]> _headerStack = new Stack<string[]>();

        public void SetHeaders( params string[] headers )
        {
            _headerStack.Push( headers );
            _currentIndices.Clear();

            for (int n = 0; n < headers.Length; n++)
            {
                var h = headers[n];

                if (_headers.Contains( h ))
                {
                    _currentIndices.Add( _headers.IndexOf( h ) );
                }
                else
                {
                    _currentIndices.Add( _headers.Count );
                    _headers.Add( h );
                }
            }
        }

        public void EndHeaders()
        {
            _headerStack.Pop();

            if (_headerStack.Count == 0)
            {
                SetHeaders();
            }
            else
            {
                SetHeaders( _headerStack.Peek() );
            }
        }

        public void Add( ISupportsCsv csv )
        {
            csv.ToCsv( this );
        }

        public void Add( params string[] columns )
        {
            string[] row = new string[_headers.Count];

            for (int n = 0; n < columns.Length; n++)
            {
                row[_currentIndices[n]] = columns[n];
            }

            _data.Add( row );
        }

        public static string AsString( ISupportsCsv csv )
        {
            CsvWriter w = new CsvWriter();
            w.Add( csv );
            return w.GetCsv();
        }

        public string GetCsv()
        {
            StringBuilder sb = new StringBuilder();

            // Write headers
            for (int n = 0; n < _headers.Count; n++)
            {
                if (n != 0)
                {
                    sb.Append( "," );
                }

                sb.Append( "\"" + _headers[n] + "\"" );
            }

            sb.AppendLine();

            // Write values
            for (int m = 0; m < _data.Count; m++)
            {
                var row = _data[m];

                for (int n = 0; n < row.Length; n++)
                {
                    if (n != 0)
                    {
                        sb.Append( "," );
                    }

                    var txt = row[n];

                    if (IsNumber( txt ))
                    {
                        sb.Append( txt );
                    }
                    else
                    {
                        sb.Append( "\"" + txt + "\"" );
                    }
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }

        private bool IsNumber( string txt )
        {
            decimal unused;
            return decimal.TryParse( txt, out unused );
        }       

        public void Serialise( object target )
        {
            SetHeaders( FIELD, VALUE );

            List<Tuple<string, object>> data = new List<Tuple<string, object>>();

            data.AddRange( target.GetType().GetProperties().Select( z => new Tuple<string, object>( z.Name, z.GetValue( target ) ) ) );
            data.AddRange( target.GetType().GetFields().Select( z => new Tuple<string, object>( z.Name, z.GetValue( target ) ) ) );

            foreach (Tuple<string, object> kvp in data)
            {
                string key = kvp.Item1;
                object value = kvp.Item2;
                Add( key, EvoveHelper.ToReadableString( value ) );
            }

            EndHeaders();
        }
    }

    public interface ISupportsCsv
    {
        void ToCsv( CsvWriter csv );
    }

    public static class ISupportsCsvExtensions
    {
        public static string CsvAsString( this ISupportsCsv self )
        {
            CsvWriter csv = new CsvWriter();

            self.ToCsv( csv );

            return csv.GetCsv();
        }
    }
}
