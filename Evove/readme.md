## Features

* Strong typing
    * Only allows compatible nodes to fit together, reducing the amount of crud in your GP trees
* Choose your programming language
    * Any CLI capable language is supported
* No esoteric custom syntax
    * Evove deconstructs your code to find the methods to create the GP tree elements, no learning some massive set of weird instructions
* Minimal effort required
    * Say what makes your code special, not what makes it the same
* Frontend not required
    * Contains built UI and console frontends, if you're too lazy to write your own
* Built in debugger
    * Wondering what went wrong? Use the visual log interpreter and tree debugger to examine your programs and their GP history.
* Safe
    * Debug builds contain hundreds of assertions for expected behaviour, so you can check your algorithm prior to running it in release.
* Fast:
    * Direct interface
        * Plugs directly into your untouched code - the methods run as fast as you can make them
    * Ahead-of-time
        * Constructs internal function pointer tables (V-tables) prior to creating GP trees, no interpreted-language slowdown or tenuous lookups.
    * Caches values
        * Mutations to parts of the GP trees don't necessitate reevaluating the entire tree
    * Deferred calculation
        * Unused GP branches are not evaluated
    * Threaded
        * Both single and multithreaded modes are supported

## Terms

* `Individual` - A GP tree
* `Population` - All individuals
* `Node` - A node of the GP tree
* `Deme` - An island population
* `Element` - A function or constant that can be attached to a node, has a return `ElementType` and a set of input `ElementType`s
* `ElementType` - A type, this is what makes the GP strongly typed. If you don't like strong typing there is only one of these (probably a `double`).

## Getting started

Run the following minimal example:

```
    using Evove;
    using Evove.Attributes;
    using Evove.Helpers;
    using System;

    public static class Program
    {
        [EvoveSupports]
        [EvoveExtends( typeof( GpFunctions.Double ) )]
        public class MyClass
        {
            [EvoveEntryPoint]
            public double MyFitnessFunction( Func<double> evolvedFunction )
            {
                const double whatIWantToSee = 42.0;
                return -Math.Abs( evolvedFunction() - whatIWantToSee );
            }
        }

        [STAThread]
        static void Main()
        {
            EvoveParameters.CreateBasicSet( typeof( MyClass ) ).Run();
        }
    }
```

## Basic help

### More Examples

The library comes with `TestApplication`, which contains lots of examples.

### Define your entry-point class

This is the class you'll pass to Evove. It contains the necessary functions for your program.

* Make sure the class is public
* Attribute the class with `EvoveSupports`

```
    [EvoveSupports]
    public class MyClass
    {
    }
```

### Define a fitness function

Add your *fitness function* to your entry-point class.

* Make sure the method is public.
* Attribute the method with `EvoveEntryPoint`.
* Take a delegate (function pointer) as one or more parameters (this is what Evove will evolve)
* You can choose the type of the function and the return type
    * Make sure the return type implements `IComparable`

```
    public class MyClass
    {
        [EvoveEntryPoint]
        public double MyFitnessFunction(Func<double> evolvedFunction)
        {
            const double whatIWantToSee = 42.0;
            return -Math.Abs(evolvedFunction() - whatIWantToSee);
        }
    }
```

### Find a function library

A function library contains sets of reusable functions that can act as GP nodes.
Evove contains serveral inbuilt function libraries as classes nested within the GpFunctions class.

### Add a function library

Make use of a library in your class by one of the following:

#### Method 1

Attribute your class using `EvoveExtends` and the library you will use

```
    [EvoveExtends(typeof(Functions.Double)]
    public class MyClass 
    {
        . . .
```

#### Method 2

This method is good for passing dynamic values to the constructor of your library, but won't work if your library is a static class!
Include your library as a field in your class:

* Make sure the field is public readonly
* Attribute the field with the `EvoveUse` attribute

```
    public class MyClass
    {
        [EvoveUse]
        public readonly Functions.Double _myExtensions = new Functions.Double();
        . . .
    }
```

#### Method 3

Add your library to the startup code:

```
    var p = EvoveParameters.CreateBasicSet( typeof( MyClass ) );
    
    p.AdditionalFunctions = new[] { typeof( GpFunctions.Double ) };

    p.Run();
```

### Evolve your code

The easiest way is via the UI, just run:

    EvoveParameters.CreateBasicSet( typeof( MyClass ) ).Run();

### Specify parameters

You can configure the parameters Evove uses to control the population to your liking, for example:

```
    var p = new EvoveParameters();
    
    p.IndividualsPerDeme = 200;
    
    . . .
```

Most of these parameters relate directly to the nature of GP, of which many books have been written on the subject and hence won't be covered here.
The remainderare documented in-line (either look at the code or use intellisense) and won't be discussed here.

Evove will always check your configuration for problems before it is allowed to run.

### Add your own functions

You probably don't want to just use libraries, you can include your own functions by writing them directly into your entry-point class:
* Make sure the functions are public

```
    public class MyClass
    {
        . . .
        
        public double Add(double a, double b)
        {
            return a + b;
        }
        
        public double Subtract(double a, double b)
        {
            return a - b;
        }
    }
```

Functions act as nodes, taking zero or more inputs (their parmaters) and producing one output (the return value).
Evove is strongly typed, so there will never be a case where a function with a `double` parameter ends up getting a `string`.

### Optimise your functions

Cases like Cs ternary operator (`a ? b : c`) and the logical functions (`a && b`) and (`a || b`) only evaluate the expressions when necesssary.
Evove can do the same, so unused tree branches are never called unnecessarily.
To do this just use a delegate to a type instead of the type itself.
Here are some examples for the ternary operator and the logical and:

```
    public double Ternary(bool condition, Func<double> a, Func<double> b)
    {
        return condition ? a() : b();
    }

    public bool LogicalAnd(bool a, Func<bool> b)
    {
        return a ? b();
    }
```

Note these delegates don't take any parameters, even though in reality they represent unknown functions taking an arbitrary number of parameters.
Don't worry, as far as your program tree is concerned `double` and `Func<double>` are identical.

### Support multithreading

Multithreading really makes sense for long running fitness evaluations, for fast fitness operations it is more likley to slow things down by breaking certain assumptions your processor can make about the code.
If you are really sure then you want to enable multithreading then do the following:

1. Make sure your class doesn't perform any weird cross-thread operations.
2. Modify the `EvoveSupports` attribute at the head of your class to `EvoveSupports(Threading = true)`. This tells Evove you've actually performed the previous step so you don't get any nasty surprises my accidentally turning on multithreading in the next step!
3. Set the multithreading EvoveParameters to the number of threads you'd like to use.

## Advanced topics

### More interesting fitness functions

Although `double` is good, your entry function can actually return any type implementing `IComparable`. (See the Microsoft IComparable documentation for details.)

The result can contain additional information in order to allow exploration of results in the UI, such as specific test cases. If the inbuilt UI is to be used these data should be exposed as public properties.

### Enabling caching

The `Add` method in the example earlier is deterministic - it will always give the same result for the same values.
We can make use of this by caching the results. This is done exactly the same as for multithreading.

1. Make sure you've appropriately flagged up any stochastic or uncachable functions (see below).
2. Modify the `EvoveSupports` attribute at the head of your class to `EvoveSupports(Caching = true)`. This tells Evove you've actually performed the previous step so you don't get any nasty surprises my accidentally turning on caching in the next step!
3. Set the caching EvoveParameters to allow caching.

#### Caching problems
Since determinism is the norm rather than the exception when caching is enabled all methods in the class will now cache their result.
Caches will only be cleared when nodes in the tree below them are modified.

Check for the following problems before enabling caching:

##### Account for stochastic functions

Stochastic (non-deterministic) functions cannot, of course, be cached. To prevent this use the [EvoveCacheUnpredictable] attribute - this stops nodes contaning those functions caching their results. The stochatic flag naturally propogates to the parent nodes, which become stochastic due to their unknown input.

```
    [EvoveCacheUnpredictable]
    public double Random()
    {
        return _random.NextDouble();
    }
```

##### Account for functions with side effects

Certain functions have side-effects alter the state of the program, an example is the following:

```
    [EvoveCacheUnpredictable]
    public double RememberValue(double x)
    {
        this.memory = x;
        return x;
    }

    [EvoveCacheUnpredictable]
    public double RecallValue()
    {
        return this.memory;
    }
```

Evove can't see that these functions affect each other, so mark them with [EvoveCacheUnpredictable] so they don't cache their results.
(Note: Previous versions of Evove did have a EvoveCachePredictableSideEffect attribute, this has been removed as it overly complicated things)

##### Account for test cases

Often your programs might evaluate several test cases. Rather than marking your functions as stochastic, its much faster to allow a result to be cached for each test case.
To do this mark your test index field and method with `EvoveTestCase`, as shown here:

```
    [EvoveTestCase]
    public int testIndex;               // remember it's public!
    double[] questions;
    double[] answers;
    
    [EvoveEntryPoint]
    public double CalculateFitness(Func<double> function)
    {
        double numberCorrect = 0;

        for(testIndex = 0; testIndex < numberOfTests; testIndex++)
        {
            if (function() == answers[testIndex])
            {
                numberCorrect += 1.0;
            }
        }

        return numberCorrect;
    }
    
    [EvoveTestCase]
    public double GetQuestion() // doesn't need to be marked as stochastic, since it depends only on testCase, which we've marked
    {
        return questions[testIndex];
    }
```

#### Error handling

Exceptions are assumed to indicate a design error (as opposed to an unusual GP tree having been constructed). If caught by Evove, exceptions will cause the error handler event to be raised, which in the GUI mode will cause the individual debugger to be shown.
This is intentional behaviour: Exceptions require an unwinding of the call stack and are best avoided as a means to designate particular fitness values, as they would incur a significant performance overhead. Rather than catching exceptions in your fitness function it is recommended that specific values, such as double.NaN, are instead used to convey error status. These values can then be converted to a real fitness value by the fitness evaluation function without significant performance degredation.