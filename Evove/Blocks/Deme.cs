﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    /// <summary>
    /// An island population
    /// </summary>
    public sealed class Deme : IDisposable
    {
        public readonly Population Population;
        public List<Individual> Individuals = new List<Individual>();
        public readonly int Index;
        public readonly object[] PerDemeObjects;
        public readonly Stack<object[]> PerEvaluationPool = new Stack<object[]>();

        public Deme(Population population, int index, SyntaxManager syntaxManager)
        {
            Population = population;
            Index = index;
            PerDemeObjects = syntaxManager.CreatePerDemeObjects( new DemeArgs( population, this ) );    // Do this last because the objects might use "this"
        }

        public override string ToString()
        {
            return "Deme " + Index;
        }

        public void Dispose()
        {
            foreach (object x in PerDemeObjects)
            {
                if (x is IDisposable)
                {
                    ((IDisposable)x).Dispose();
                }
            }
        }
    }
}
