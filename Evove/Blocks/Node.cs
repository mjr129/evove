﻿using Evove.Operators.Termination;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    /// <summary>
    /// A GP node.
    /// </summary>
    public sealed class Node
    {
        private static int __trackingIdCounter;

        public readonly int TrackingId;                                  // Tracking ID (initialied with the node is first created, copied nodes will have the same ID so this is not unique)
        public Node Parent;                                              // Parent of this node in the tree
        public int Index;                                                // Index of this node in its parent's collection
        public GpFunction Function;                                       // The GP function (piece of code)
        public Node[] Children;                                          // Children of this node (the parameters passed to Element)
        public object ConstantValue;                                     // Special parameter for Element (i.e. constants)
        public int Depth;                                                // Depth of this node in the tree
        public EvaluationCache Cache;
        private Delegate _cachedDelegate;                                // Cached version of the delegate for Execute
        private Type _cachedDelegateType;                                // The type of the cacled Execute delegate
        private ExecutionContext _target;

        /// <summary>
        /// Constructor
        /// </summary>
        public Node(Node parent, int index, GpFunction element, Node[] children, int depth)
        {
            this.TrackingId = ++__trackingIdCounter;
            this.Parent = parent;
            this.Index = index;
            this.Function = element;
            this.Children = children;
            this.Depth = depth;
            this.Cache = new EvaluationCache();
            this.ConstantValue = null;
        }              


        /// <summary>
        /// Copy constructor.
        /// </summary>
        private Node(Node parent, Node copyFrom)
        {
            this.TrackingId = copyFrom.TrackingId;
            this.Parent = parent;
            this.Index = copyFrom.Index;
            this.Function = copyFrom.Function;
            this.ConstantValue = copyFrom.ConstantValue;
            this.Children = new Node[copyFrom.Children.Length];
            this.Depth = copyFrom.Depth;
            this.Cache = new EvaluationCache(copyFrom.Cache);

            for (int n = 0; n < copyFrom.Children.Length; n++)
            {
                this.Children[n] = new Node(this, copyFrom.Children[n]);
            }
        }

        private EvoveParameters Parameters
        {
            get
            {
                return this.Function.SyntaxManager.Parameters;
            }
        }

        private SyntaxManager SyntaxManager
        {
            get
            {
                return this.Function.SyntaxManager;
            }
        }

        /// <summary>
        /// Deletes the cached evaluated value from this node and all its parents.
        /// If any of the nodes change state information the nodes accessing that state information
        /// are also changed.
        /// </summary>
        public void CacheMarkAsDirty(string reason)
        {
            if (!Parameters.AllowCaching)
            {
                return;
            }

            Node x = this;

            while (x != null && x.Cache.Status != EEvaluatedStatus.Flagged)
            {
                x.Cache.Clear(EEvaluatedStatus.Flagged, reason);
                x = x.Parent;
            }
        }

        /// <summary>
        /// Constructs a node from XML.
        /// </summary>
        internal Node(SyntaxManager sm, Token token)
        {
            this.Function = sm.GetElementById(token.String);

            if (token.Contains("constant"))
            {
                this.ConstantValue = Convert.ChangeType(token["constant"].String, this.Function.ReturnType.Type);
            }

            this.Children = new Node[this.Function.ParameterTypes.Length];

            for (int n = 0; n < this.Children.Length; n++)
            {
                this.Children[n] = new Node(sm, token[this.Function.ParameterTypes[n].ParameterName.ToLower()]);
            }
        }             

        /// <summary>
        /// Converts node to XML.
        /// </summary>           
        public Token ToToken(string key)
        {
            Token result = new Token(key, this.Function.Id);

            if (this.ConstantValue != null)
            {
                result.Add(new Token("constant", (string)Convert.ChangeType(this.ConstantValue, typeof(string))));
            }

            for (int n = 0; n < this.Children.Length; n++)
            {
                result.Add(this.Children[n].ToToken(this.Function.ParameterTypes[n].ParameterName));
            }

            return result;
        }

        /// <summary>
        /// Gets the next depth given the specified parent, which may be null.
        /// </summary>
        public static int GetNextDepth(Node parent)
        {
            if (parent == null)
            {
                return 0;
            }

            return parent.Depth + 1;
        }

        /// <summary>
        /// For debugging.
        /// </summary>
        public override string ToString()
        {
            return TrackingId + ". " + Function?.RelativeName( this, true );
        }        

        public class ExecutionContext
        {
            public readonly object[] PerEvaluationObjects;
            private readonly ITerminationOperator[] TerminationOperators;
            private readonly TerminationOperatorArgs[] TerminationOperatorArgs;
            private bool _terminated;

            public void TerminationIteration()
            {
                if (_terminated)
                {
                    return;
                }

                for (int n = 0; n < TerminationOperators.Length; n++)
                {
                    if (TerminationOperators[n].Terminate(TerminationOperatorArgs[n]))
                    {
                        _terminated = true;
                        return;
                    }
                }
            }

            public bool IsTerminated
            {
                get
                {
                    return _terminated;
                }
            }

            public ExecutionContext(ITerminationOperator[] terminationOperators, TerminationOperatorArgs[] terminationOperatorArgs, object[] perEvaluationObjects)
            {
                this.PerEvaluationObjects = perEvaluationObjects;
                this.TerminationOperators = terminationOperators;
                this.TerminationOperatorArgs = terminationOperatorArgs;

                for (int n = 0; n < TerminationOperators.Length; n++)
                {
                    TerminationOperators[n].Instantiate(TerminationOperatorArgs[n]);
                }
            }
        }

        /// <summary>
        /// Executes the node and returns the result.
        /// </summary>
        public object Execute(ExecutionContext exec)
        {
            Logger.Log("+Executing node: ", this);

            exec.TerminationIteration();

            if (exec.IsTerminated)
            {
                Logger.Log("-RETURNING: Out of execute chances");
                return null;
            }

            object result = Cache.Retrieve(SyntaxManager, exec.PerEvaluationObjects);

            if (result != null)
            {
                Logger.Log("-Retrieved cache: ", result);
                return result;
            }

            ECacheMode cacheMode = this.Function.CacheMode;

            if (ConstantValue != null)
            {
                Debug.Assert(Function.IsConstant);

                result = ConstantValue;
                Cache.Store(SyntaxManager, cacheMode, result, exec.PerEvaluationObjects);
                Logger.Log("-Retrieved constant: ", result);
                return result;
            }

            int numberOfParameters = this.Function.IsStatic ? this.Function.ParameterTypes.Length : (this.Function.ParameterTypes.Length + 1);

            object[] parameters = new object[numberOfParameters];

            for (int i = 0; i < this.Function.ParameterTypes.Length; i++)
            {
                ParameterType parameterType = this.Function.ParameterTypes[i];
                Node child = this.Children[i];
                int r = this.Function.IsStatic ? i : (i + 1);

                if (parameterType.DelegateType != null)
                {
                    parameters[r] = child.GetExecuteDelegate(exec, parameterType.DelegateType);
                }
                else
                {
                    parameters[r] = child.Execute(exec);
                }

                if (exec.IsTerminated)
                {
                    Logger.Log("-ABORTING: Out of execute chances");
                    return null;
                }
            }

            // *** ACTUAL METHOD INVOCATION ***   
            //result = this.Element.Method.Invoke(exec.PerEvaluationObjects[this.Element.TypeIndex], parameters);

            if (!this.Function.IsStatic)
            {
                parameters[0] = exec.PerEvaluationObjects[this.Function.TypeIndex];
            }

            result = this.Function.Delegate.DynamicInvoke(parameters);    

            // Query children (who by now must have executed or been ignored) for cache mode
            foreach (Node child in Children)
            {
                if (child.Cache.Mode > cacheMode)
                {
                    cacheMode = child.Cache.Mode;
                }
            }

            Cache.Store(SyntaxManager, cacheMode, result, exec.PerEvaluationObjects);

            // Save a constant value
            if (this.Function.IsConstant)
            {
                Debug.Assert(ConstantValue == null);
                Debug.Assert(result != null, "Result cannot be null for constant-valued functions.");
                ConstantValue = result;
            }

            Logger.Log("-Invoked: ", result);
            return result;
        }

        /// <summary>
        /// Gets the delegate for the execute method.
        /// </summary>
        public object GetExecuteDelegate(ExecutionContext exec, Type delegateType)
        {
            Debug.Assert(exec != null);

            // Tell the cache that the delegate has been requested (just for debugging)
            Cache.NotifyDelegate();

            // We're delegating the call to ourselves, so store the pertinent info for when we get the callback
            _target = exec;

            // Getting the delegate isn't exactly fast, so if we cached it don't create it again --
            if (_cachedDelegate == null || _cachedDelegateType != delegateType)
            {
                // -- Not cached, needs creating
                // -- TODO: This should be cached as well!

                // Get the "ExecuteDelegateOfT" method we'll call back to (in this class) --
                // -- It's a generic so this is a two stage process, we need to set "T" to the return type
                Type returnType = delegateType.GetMethod(nameof(Action.Invoke)).ReturnType;

                MethodInfo method;

                if (returnType == typeof(void))
                {
                    method = this.GetType().GetMethod(nameof(ExecuteDelegateOfVoid));
                }
                else
                {
                    method = this.GetType().GetMethod(nameof(ExecuteDelegateOfT));
                    method = method.MakeGenericMethod(returnType);
                }

                // From our method, create a delegate of the type the receiver is expecting
                _cachedDelegate = method.CreateDelegate(delegateType, this);

                // Cache the result
                _cachedDelegateType = delegateType;
            }

            // Return the result
            return _cachedDelegate;
        }

        /// <summary>
        /// Executes the node and returns the result.
        /// This small method is the strongly typed wrapper and hallmark of the library :)
        /// </summary>
        public T ExecuteDelegateOfT<T>()
        {
            object exec = Execute(_target);

            if (_target.IsTerminated)
            {
                return default(T);
            }

            return (T)exec;
        }

        /// <summary>
        /// Like ExecuteDelegateOfT, but for voids.
        /// </summary>
        public void ExecuteDelegateOfVoid()
        {
            Execute(_target);
        }

        public IEnumerable<Node> IterateAllNodes(bool includeRoot)
        {
            Node x = this;

            while (x.Parent != null)
            {
                x = x.Parent;
            }

            int childIndex = 0;

            if (includeRoot)
            {
                yield return x;
            }

            while (true)
            {
                // Move UP
                while (childIndex == x.Children.Length)
                {
                    childIndex = x.Index + 1;
                    x = x.Parent;

                    if (x == null)
                    {
                        yield break;
                    }

                    Debug.Assert(x.Parent == null || x.Parent.Children[x.Index] == x);
                }

                // Move DOWN
                x = x.Children[childIndex];
                childIndex = 0;
                yield return x;
            }
        }

        /// <summary>
        /// Adds this node and all its children to the list.
        /// </summary>
        public void GetAllNodes(List<Node> list)
        {
            list.Add(this);

            foreach (Node child in Children)
            {
                child.GetNodes(list);
            }
        }

        /// <summary>
        /// Adds this node and all its children to the list, providing that they are marked as [IsSafe] (i.e. excludes root nodes).
        /// </summary>
        public void GetNodes(List<Node> list)
        {
            if (IsSafe)
            {
                list.Add(this);
            }

            foreach (Node child in Children)
            {
                child.GetNodes(list);
            }
        }

        /// <summary>
        /// Is safe for mutation/crossover.
        /// 
        /// Exclude the "root" node since there is only one!
        /// </summary>
        private bool IsSafe
        {
            get { return Parent != null; }
        }

        /// <summary>
        /// Creates a random node of the specified type.
        /// </summary>
        /// <param name="parent">Parent of new node</param>
        /// <param name="index">Index of new node</param>
        /// <param name="ofType">Return type of new node</param>
        /// <param name="parameters">User options (used for max tree depth, etc.)</param>
        /// <returns>The new node</returns>
        public static Node CreateRandom(Randomiser randiomiser, Node parent, int index, GpType ofType, int maxTreeDepth)
        {
            int depth = GetNextDepth(parent);
            GpFunction element;

            Debug.Assert(depth <= maxTreeDepth, "Maximum tree depth violation");

            if (depth == maxTreeDepth)
            {
                element = randiomiser.FromList(ofType.SourceFunctions);
            }
            else
            {
                element = randiomiser.FromList(ofType.OutputFunctions);
            }

            Node[] children = new Node[element.ParameterTypes.Length];

            Node n = new Node(parent, index, element, children, depth);

            for (int i = 0; i < element.ParameterTypes.Length; i++)
            {
                children[i] = CreateRandom( randiomiser, n, i, element.ParameterTypes[i].Type, maxTreeDepth);
            }

            Debug.Assert(n.Depth < maxTreeDepth || n.Function.ParameterTypes.Length == 0);

            return n;
        }

        /// <summary>
        /// Returns if this node has been marked as destroyed.
        /// </summary>
        public bool IsDestroyed
        {
            get { return Function == null; }
        }

        /// <summary>
        /// Destroys the node - not essential but this makes sure we don't accidentally
        /// hold onto some useless node that was supposed to be got rid of.
        /// </summary>
        public void Destroy()
        {
            Parent = null;
            Function = null;
            ConstantValue = null;

            foreach (Node n in Children)
            {
                n.Destroy();
            }
        }

        /// <summary>
        /// Makes a deep copy of this node.
        /// </summary>
        public Node DeepCopy()
        {
            Debug.Assert(this.Parent == null);

            return new Node(null, this);
        }

        /// <summary>
        /// Counts all nodes from this node and its children.
        /// </summary>
        internal int CountAllNodes()
        {
            int count = 1;

            foreach (Node c in Children)
            {
                count += c.CountAllNodes();
            }

            return count;
        }

        /// <summary>
        /// Finds the maximum depth reached from this node and its children.
        /// </summary>
        public int CountDepth()
        {
            int maxDepth = this.Depth;

            foreach (Node c in Children)
            {
                int childDepth = c.CountDepth();

                if (childDepth > maxDepth)
                {
                    maxDepth = childDepth;
                }
            }

            return maxDepth;
        }

        /// <summary>
        /// Clears Flagged status from node, allowing dirtiness to propagate freely once again.
        /// </summary>                
        internal void ClearRemainingFlags() // TODO: Is this necessary? Will FLAGGED only hang around in unused branches where dirtiness isn't needed, or will tree alterations mean this is necessary? Lets keep things safe for now.
        {
            this.Cache.ClearFlags();
        }

        [Conditional("DEBUG")]
        internal void DEBUG_Validate(int maxTreeDepth)
        {
            Debug.Assert(!this.IsDestroyed, "Node marked as destroyed still in use");
            Debug.Assert(this.Function.ParameterTypes.Length == this.Children.Length, "Invalid number of children for function parameters");
            Debug.Assert(this.Depth <= maxTreeDepth, "Maximum tree depth violation");

            for (int i = 0; i < this.Children.Length; i++)
            {
                Debug.Assert(this.Children[i].Index == i, "Node record of own index is incorrect.");
            }

            if (this.Parent != null)
            {
                Debug.Assert(this.Parent.Function != null, "Node missing function");
                Debug.Assert(this.Parent.Children[this.Index] == this, "Node record of own index is incorrect.");
                Debug.Assert(this.Function.ReturnType == this.Parent.Function.ParameterTypes[this.Index].Type, "STGP type violation");
                Debug.Assert(this.Depth == this.Parent.Depth + 1, "Node record of own depth is incorrect");
            }
        }
    }
}
