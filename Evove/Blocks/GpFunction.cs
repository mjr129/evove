﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    /// <summary>
    /// The actual bit of code representing a node.
    /// 
    /// Wraps a MethodInfo up with some extra details for strong-typing.
    /// </summary>
    public sealed class GpFunction
    {
        /// <summary>
        /// Reference back to owning manager
        /// </summary>
        public readonly SyntaxManager SyntaxManager;

        /// <summary>
        /// ID of the function (for saving/loading)
        /// </summary>
        public readonly string Id;

        /// <summary>
        /// Actual method
        /// </summary>
        public readonly MethodInfo Method;

        /// <summary>
        /// Type of the result (for STGP)
        /// </summary>
        public readonly GpType ReturnType;

        /// <summary>
        /// Parameters of the method (for STGP)
        /// </summary>
        public readonly ParameterType[] ParameterTypes;

        /// <summary>
        /// If a delegate, the details of the field
        /// </summary>
        public readonly FieldInfo Field;

        /// <summary>
        /// True to indicate this method is a constant creator.
        /// </summary>
        public readonly bool IsConstant;

        /// <summary>
        /// How the result of the invokation can be cached.
        /// </summary>
        public readonly ECacheMode CacheMode;

        /// <summary>
        /// The index of the declaring object type in our manager (if the method is not static)
        /// </summary>
        public readonly int TypeIndex;

        public readonly Delegate Delegate;
        public readonly bool IsStatic;

        /// <summary>
        /// CONSTRUCTOR
        /// See fields of same names for details.
        /// </summary>
        internal GpFunction(int typeIndex, SyntaxManager syntaxManager, MethodInfo method, GpType returnType, ParameterType[] parameterTypes, FieldInfo field, bool isConstant, ECacheMode cacheMode)
        {
            this.TypeIndex = typeIndex;
            this.SyntaxManager = syntaxManager;
            this.Method = method;
            this.ReturnType = returnType;
            this.ParameterTypes = parameterTypes;
            this.Field = field;
            this.IsConstant = isConstant;
            this.CacheMode = cacheMode;
            this.Delegate = CreateDelegate(method);
            this.IsStatic = method.IsStatic;

            if (syntaxManager.Parameters.SupportMethodOverloads)
            {
                this.Id = RelativeName(null, true);
            }
            else
            {
                this.Id = ShortName;
            }
        }

        private static Delegate CreateDelegate(MethodInfo method)
        {
            Debug.Assert(!method.IsGenericMethod);

            var args = method.GetParameters().Select(z => z.ParameterType).Concat(new[] { method.ReturnType });

            if (!method.IsStatic)
            {
                // Use "this" as the first parameter to create an open delegate
                args = new[] { method.DeclaringType }.Concat(args);
            }

            var deli = Expression.GetDelegateType(args.ToArray());

            return Delegate.CreateDelegate(deli, method);

            //return method.CreateDelegate(deli);
        }

        /// <summary>
        /// OVERRIDES Object
        /// </summary>       
        public override string ToString()
        {
            return RelativeName(null, true);
        }

        /// <summary>
        /// Basic name of the function.
        /// </summary>
        public string Name
        {
            get
            {
                return RelativeName(null, false);
            }
        }

        /// <summary>
        /// Very basic name of the function, used as a simple ID.
        /// </summary>
        private string ShortName
        {
            get
            {
                if (TypeIndex == 0)
                {
                    if (Field != null)
                    {
                        return Field.Name;
                    }
                    else
                    {
                        return Method.Name;
                    }
                }
                else
                {
                    if (Field != null)
                    {
                        return EvoveHelper.NameOfType(Field.DeclaringType) + "::" + Field.Name;
                    }
                    else
                    {
                        return EvoveHelper.NameOfType(Method.DeclaringType) + "::" + Method.Name;
                    }
                }
            }
        }

        /// <summary>
        /// Creates a name from the type.
        /// </summary>                                                    
        public string RelativeName(Node gpNode, bool includeParameters, bool flagConstants = true)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.ReturnType.Name + " ");

            if (Field != null)
            {
                sb.Append(EvoveHelper.NameOfType(Field.DeclaringType) + "::" + Field.Name + ".");
            }
            else
            {
                sb.Append(EvoveHelper.NameOfType(Method.DeclaringType) + "::");
            }

            sb.Append(Method.Name);

            if (includeParameters)
            {
                sb.Append("(");

                for (int i = 0; i < this.ParameterTypes.Length; i++)
                {
                    if (i != 0)
                    {
                        sb.Append(", ");
                    }

                    var x = this.ParameterTypes[i];

                    sb.Append(x.Type.Name);

                    if (gpNode != null)
                    {
                        sb.Append(" " + x.ParameterName + " = ");
                        sb.Append(gpNode.Children[i].Function.ShortName);
                    }
                }

                sb.Append(")");
            }

            if (IsConstant && flagConstants)
            {
                if (gpNode == null)
                {
                    sb.Append(" [CONSTANT]");
                }
                else
                {
                    sb.Append(" [CONSTANT: ");

                    if (gpNode.ConstantValue == null)
                    {
                        sb.Append("(not yet known)");
                    }
                    else
                    {
                        sb.Append(gpNode.ConstantValue);
                    }

                    sb.Append("]");
                }
            }

            return sb.ToString();
        }
    }
}
