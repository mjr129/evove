﻿using Evove.Operators.Termination;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks.Data;
using Evove.Attributes;

namespace Evove.Blocks
{
    /// <summary>
    /// Creates and maintains a list of evolovable functions and types.
    /// </summary>
    public sealed class SyntaxManager : ISupportsCsv
    {
        /// <summary>
        /// Per-evaluation object types
        /// </summary>
        public  readonly PerEvaluationInfo[] PerEvaluationTypes;

        /// <summary>
        /// User parameters
        /// </summary>
        internal readonly EvoveParameters Parameters;

        /// <summary>
        /// Known number of test cases
        /// </summary>
        public int NumberOfTestCases { get; private set; }

        /// <summary>
        /// GP types
        /// </summary>
        public readonly List<GpType> GpTypes = new List<GpType>();

        /// <summary>
        /// GP functions
        /// </summary>
        private Dictionary<string, GpFunction> _gpFunctions = new Dictionary<string, GpFunction>();

        /// <summary>
        /// GP type of the root function
        /// </summary>
        private GpType _rootGpFunctionGpType;

        /// <summary>
        /// Field containing the test case.
        /// </summary>
        private FieldInfo _testCaseField;

        /// <summary>
        /// Index (in PerEvaluationTypes) of the test case object.
        /// </summary>
        private int _testCaseTypeIndex;

        /// <summary>
        /// Fields to set (in PerEvaluationTypes) to per-deme objects.
        /// </summary>
        private List<FixedFieldInfo<DemeArgs>> _perDemeFields = new List<FixedFieldInfo<DemeArgs>>();

        /// <summary>
        /// Fields to set (in PerEvaluationTypes) to per-population objects.
        /// </summary>
        private List<FixedFieldInfo<PopulationArgs>> _perPopulationFields = new List<FixedFieldInfo<PopulationArgs>>();

        /// <summary>
        /// CONSTANT
        /// </summary>
        public const BindingFlags BINDING_FLAGS = BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static;

        /// <summary>
        /// CONSTRUCTOR
        /// </summary>                       
        public SyntaxManager( EvoveParameters parameters )
        {
            // Setup the known basics
            this.Parameters = parameters;
            this.NumberOfTestCases = parameters.NumberOfTestCases;

            // Parse the target types
            List<PerEvaluationInfo> perEvaluationTypes = new List<PerEvaluationInfo>();
            if (parameters.FitnessFunction != null)
            {
                ParseType( new PerEvaluationInfo( null, parameters.FitnessFunction, perEvaluationTypes.Count, PerEvaluationInfo.EMode.Fitness ), perEvaluationTypes );
            }

            // Assert we have a root
            if (_rootGpFunctionGpType == null)
            {
                if (!parameters.AllowIncomplete)
                {
                    throw new InvalidOperationException( $"No entry point method has been found. Please mark an entry point with {nameof( EvoveMethodAttribute )}.{nameof( EvoveMethodAttribute.Role )} = {nameof( EMethod )}.{nameof( EMethod.FitnessFunction )}." );
                }
            }

            // Add other types
            foreach (var x in parameters.AdditionalFunctions)
            {
                ParseType( new PerEvaluationInfo( null, x, perEvaluationTypes.Count, PerEvaluationInfo.EMode.CustomExtension ), perEvaluationTypes );
            }

            this.PerEvaluationTypes = perEvaluationTypes.ToArray();

            // Assert functions
            var missingOutputs = this.GpTypes.Where( z => z.OutputFunctions.Count == 0    // Something must output this
                                                       && z.InputFunctions.Count != 0 );  // (assuming something requires it)

            if (missingOutputs.Count() != 0)
            {
                var fns = string.Join( ", ", missingOutputs.First().InputFunctions.Select( z => z.Name ) );
                throw new InvalidOperationException( $"Type \"{missingOutputs.First().Name}\" has no output functions. Either provide GP functions that return this type or remove this type from GP function inputs ({fns}) ." );
            }

            var missingSource = this.GpTypes.Where( z => z.SourceFunctions.Count == 0     // Something must output this
                                                      && z.InputFunctions.Count != 0 );    // (assuming something requires it)

            if (missingSource.Count() != 0 && parameters.MaxTreeDepth != int.MaxValue)
            {
                var fns = string.Join( ", ", missingSource.First().InputFunctions.Select( z => z.Name ) );

                if (!Parameters.AllowIncomplete)
                {
                    throw new InvalidOperationException( $"Type \"{missingSource.First().Name}\" has no output functions taking 0 parameters. Parameterless source functions are required as leaf nodes. Either provide GP functions that return this type and require no parameters OR remove this type from GP function inputs ({fns}) OR set the {nameof( EvoveParameters.MaxTreeDepth )} value of the parameter set to {nameof( Int32 )}.{nameof( Int32.MaxValue )} so that leaf nodes are not required." );
                }
            }

            // Assert the test case has the number of test cases if it is set
            if (_testCaseField != null && NumberOfTestCases <= 0)
            {
                throw new InvalidOperationException( $"A test case field \"{EvoveHelper.NameOfType( _testCaseField.DeclaringType )}.{_testCaseField.Name}\" has been specified but the number of test cases has not. Please specify the number of test cases in the \"{nameof( EvoveParameters )}\" setup parameters." );
            }
        }
        public void ToCsv( CsvWriter writer )
        {
            writer.SetHeaders( CsvWriter.FIELD, CsvWriter.VALUE );

            foreach (var p in this.PerEvaluationTypes)
            {
                writer.Add( "Per evaluation type", EvoveHelper.NameOfType( p.Type ) );
            }

            writer.Add( "Number of test cases", NumberOfTestCases.ToString() );

            foreach (var p in this.GpTypes)
            {
                writer.Add( "GP type", p.Name );
            }

            foreach (var p in this._gpFunctions.Values)
            {
                writer.Add( "GP type", p.RelativeName( null, true ) );
            }

            writer.Add( "Root type", _rootGpFunctionGpType.Name );

            writer.Add( "Test case field", _testCaseField != null ? (EvoveHelper.NameOfType( _testCaseField.DeclaringType ) + "::" + _testCaseField.Name) : "(none)" );

            writer.Add( "Test case type index", _testCaseTypeIndex.ToString() );

            foreach (var p in this._perDemeFields)
            {
                writer.Add( "Per deme field", p.ToString() );
            }

            foreach (var p in _perPopulationFields)
            {
                writer.Add( "Per population field", p.ToString() );
            }

            writer.EndHeaders();
        }

        /// <summary>
        /// Creates a random root node.
        /// </summary>                  
        internal Node CreateRandomRootNode( Randomiser randomiser, EvoveParameters parameters )
        {
            return Node.CreateRandom( randomiser, null, 0, _rootGpFunctionGpType, parameters.MaxTreeDepth );
        }

        /// <summary>
        /// Reflects the targetType to extact useful methods.
        /// </summary>
        /// <param name="targetType">Type to investigate</param>
        /// <param name="perEvaluationTypes">Where to put the results</param>
        private void ParseType(PerEvaluationInfo info, List<PerEvaluationInfo> perEvaluationTypes )
        {   
            Type targetType = info.Type;

            // Ignore if already seen
            if (perEvaluationTypes.Any( z => z.Type == targetType ))
            {
                return;
            }

            string parsing = EvoveHelper.NameOfType( info.Type );

            Debug.WriteLine( "*** Parsing type: " + parsing + " ***" );
            Debug.WriteLine( "Factory is: " + info.ToString() );

            // Get the primary attribute
            var attr = targetType.GetCustomAttribute<EvoveClassAttribute>( false );

            if (attr == null)
            {
                throw new InvalidOperationException( $"The type \"{EvoveHelper.NameOfType( targetType )}\" requires marking with the \"{nameof( EvoveClassAttribute )}\" if it is to provide GP functions." );
            }            

            // Add to the list (unless static)
            Debug.Assert(info.index == perEvaluationTypes.Count);
            perEvaluationTypes.Add(info);                        
            
            // Assert the type is public
            if (!targetType.IsPublic && !targetType.IsNestedPublic)
            {
                throw new InvalidOperationException( $"To avoid conflict with compiler optimisations {EvoveHelper.PROGRAM_NAME} has refused to acknowledge the non-public class \"{EvoveHelper.NameOfType( targetType )}\". Consider making the class public." );
            }

            // Assert the type allows caching if caching is enabled
            if (Parameters.AllowCaching)
            {
                if (!attr.Caching)
                {
                    throw new InvalidOperationException( $"To avoid unexpected side effects of evaluation caching {EvoveHelper.PROGRAM_NAME} has refused to acknowledge the evolvable class \"{EvoveHelper.NameOfType( targetType )}\" since it has not been marked with the \"{nameof( EvoveClassAttribute )}.{nameof( EvoveClassAttribute.Caching )}\" attribute property. Either check the methods within this class for cache issues and apply the appropriate attributes, or disable evaluation caching." );
                }
            }      

            // Add the METHODS
            MethodInfo[] methods = targetType.GetMethods( BINDING_FLAGS );

            Debug.WriteLine( $"{parsing} - Methods" );
            foreach (MethodInfo method in methods)
            {
                if (method.DeclaringType != typeof( object ))
                {
                    Debug.WriteLine( $"METHOD {method.Name} in {parsing} accepted as GP-METHOD" );
                    CreateGpFunction( targetType, info.index, method, null );
                }
            }

            // Check the fields
            // - Treat fields that are DELEGATES as METHODS
            // - Check for specially marked fields
            FieldInfo[] fields = targetType.GetFields( BINDING_FLAGS );

            Debug.WriteLine( $"{parsing} - Fields" );
            foreach (FieldInfo field in fields)
            {
                Type fieldType = field.FieldType;

                // Ignore   
                var attrField = field.GetCustomAttribute<EvoveFieldAttribute>( true );           

                if (IsDelegate(fieldType))
                {
                    if (attrField == null)
                    {
                        throw new InvalidOperationException( $"Error at {parsing}.{field.Name}. A field of delegate type should not have a {nameof(EvoveFieldAttribute)} since it is treated like a method." );
                    }

                    // Field is a delagate, include it as a method
                    Debug.WriteLine( $"FIELD {field.Name} in {parsing} accepted as GP-METHOD (DELEGATE)" );
                    CreateGpFunction(targetType, info.index, field );
                    continue;
                }

                if (field.GetCustomAttribute<EvoveMethodAttribute>( true ) != null)
                {
                    throw new InvalidOperationException( $"Error at {parsing}.{field.Name}. A field not of delegate type should not have a {nameof( EvoveMethodAttribute )} since it cannot be treated like a method." );
                }

                if (attrField == null || attrField.Use == EField.Ignored)
                {
                    // Ignore
                    continue;
                }      

                if (!IsIncludedInSet( attrField.Set ))
                {
                    Debug.WriteLine( $"FIELD {field.Name} in {parsing} ignored due to {nameof( EvoveFieldAttribute )}.{nameof( EvoveFieldAttribute.Set )}." );
                    continue;
                }

                if (attrField.Use == EField.TestCase)
                {
                    // Field is the test case, set it as the test case
                    Debug.WriteLine( $"FIELD {field.Name} in {parsing} accepted as TEST-CASE" );
                    SetTestCaseField(targetType, info.index, field, 0);
                    continue;
                }

                if (!field.IsInitOnly)
                {
                    throw new InvalidOperationException( $"The field {field.DeclaringType.NameOfType()}.{field.Name} is marked as {nameof( EField )}.{nameof( EField.Extension )}, {nameof( EField )}.{nameof( EField.DemeStatic )} or {nameof( EField )}.{nameof( EField.PopulationStatic )} but it is not marked as readonly. For optimisation, fields marked with this attribute are cached post construction and therefore cannot be changed. These fields should be marked read only to avoid changing their value, which would cause a cache inconsistancy. Please mark this field as readonly." );
                }

                if (attrField.Use == EField.Extension)
                {   
                    // Field includes other methods, parse this type also
                    Debug.WriteLine( $"FIELD {field.Name} in {parsing} accepted as EXTENSION" );
                    ParseType(new PerEvaluationInfo(info,   field, perEvaluationTypes.Count, PerEvaluationInfo.EMode.FieldExtension), perEvaluationTypes);
                    continue;
                }

                if (attrField.Use == EField.DemeStatic)
                {
                    Debug.WriteLine( $"FIELD {field.Name} in {parsing} accepted as PER-DEME" );
                    _perDemeFields.Add( new FixedFieldInfo<DemeArgs>( field, info.index, attrField.ConstructorArguments ) );
                }
                else if (attrField.Use == EField.PopulationStatic)
                {
                    Debug.WriteLine( $"FIELD {field.Name} in {parsing} accepted as PER-POPULATION" );
                    _perPopulationFields.Add( new FixedFieldInfo<PopulationArgs>( field, info.index, attrField.ConstructorArguments ) );
                }
            }

            // Finalise each type we've seen
            foreach (GpType x in this.GpTypes)
            {
                x.Complete();
            }

            // Parse any other classes we're directed to
            Debug.WriteLine( "- Extensions" );
            foreach (var extAttr in targetType.GetCustomAttributes<EvoveExtendsAttribute>())
            {
                if (IsIncludedInSet( extAttr.Set ))
                {
                    Debug.WriteLine( $"EXTENSION CLASS {extAttr.ToString()} in {parsing} accepted" );
                    ParseType( new PerEvaluationInfo( info, extAttr.CreateFactory(), perEvaluationTypes.Count, PerEvaluationInfo.EMode.Extension ), perEvaluationTypes );
                }
                else
                {
                    Debug.WriteLine( $"EXTENSION CLASS {extAttr.ToString()} in {parsing} ignored due to {nameof( EvoveExtendsAttribute )}.{nameof( EvoveExtendsAttribute.Set )}." );
                }
            }
        }

        /// <summary>
        /// Returns if the "set" is included in the current "set".
        /// </summary>                                            
        private bool IsIncludedInSet( object set )
        {
            if (this.Parameters.Set == null)
            {
                return true;
            }

            if (set == null)
            {
                return true;
            }

            Int64 required = Convert.ToInt64( this.Parameters.Set );
            Int64 has = Convert.ToInt64( set );

            return ((has & required) == required);
        }

        /// <summary>
        /// Creates the execution context for a node.
        /// IMPORTANT: The context must be returned to the pool using ReturnExecutionContext after it is no longer needed.
        /// Each thread has its own pool so we pass the pool in.
        /// </summary>           
        public Node.ExecutionContext CreateExecutionContext( Population population, Deme deme, int round )
        {
            // Input: Fitness evaluation objects
            object[] perEvaluationObjects = this.CreateTargets( population, deme );

            foreach (object x in perEvaluationObjects)
            {
                if (x is IResettable)
                {
                    ((IResettable)x).Reset();
                }
            }

            TerminationOperatorArgs[] tArgs = new TerminationOperatorArgs[this.Parameters.TerminationOperators.Length];

            for (int n = 0; n < tArgs.Length; n++)
            {
                tArgs[n] = new TerminationOperatorArgs();
            }

            return new Node.ExecutionContext( this.Parameters.TerminationOperators, tArgs, perEvaluationObjects );
        }

        /// <summary>
        /// Creates the per-evaluation objects
        /// Called from "CreateExecutionContextN". ReturnExecuteInfoD should be called to return
        /// objects to the pool.
        /// </summary>                                  
        private object[] CreateTargets( Population population, Deme deme )
        {
            object[] objects;

            Stack<object[]> pool = deme.PerEvaluationPool;

            lock(pool)
            {
                if (pool.Count != 0)
                {
                    return pool.Pop();
                }
            }
                 
            objects = new object[PerEvaluationTypes.Length];

            InstantiationArgs args = new InstantiationArgs( population, deme );

            for (int index = 0; index < objects.Length; ++index)
            {
                objects[index] = PerEvaluationTypes[index].Instantiate( args, objects);       
                SetFixedObjects( index, objects, deme.PerDemeObjects, _perDemeFields );
                SetFixedObjects( index, objects, population.PerPopulationObjects, _perPopulationFields );
            }                                                                                    

            return objects;
        }

        /// <summary>
        /// Returns an execution context to its pool.
        /// </summary>                               
        internal void ReturnExecutionContext( Deme deme, Node.ExecutionContext exec )
        {
            if (Parameters.AbandonPerEvaluationObjects)
            {
                foreach (object x in exec.PerEvaluationObjects)
                {
                    if (x is IDisposable)
                    {
                        ((IDisposable)x).Dispose();
                    }
                }

                return;
            }                     

            lock(deme.PerEvaluationPool)
            {
                deme.PerEvaluationPool.Push( exec.PerEvaluationObjects );
            }
        }

        /// <summary>
        /// Gets whether the constructor is parameterless or uses the parameter argType. Throws an error if neither is
        /// present.
        /// </summary>
        internal static bool HasTypedConstructor( Type t, Type argType, object[] constructorArguments )
        {
            Type[] untypedArguments = constructorArguments.Select(z => z.GetType()).ToArray();
            Type[] typedArguments = new Type[untypedArguments.Length + 1];

            typedArguments[0] = argType;    
            Array.Copy( untypedArguments, 0, typedArguments, 1, untypedArguments.Length );

            if (t.GetConstructor( typedArguments ) != null)
            {
                return true;
            }
            else if (t.GetConstructor( untypedArguments ) != null)
            {
                return false;
            }
            else
            {
                ConstructorInfo[] actualConstructors = t.GetConstructors( BINDING_FLAGS );

                string argsOneString = string.Join( ", ", untypedArguments.Select( z => EvoveHelper.NameOfType( z ) ) );
                string argsTwoString = string.Join( ", ", typedArguments.Select( z => EvoveHelper.NameOfType( z ) ) );
                string argsThreeString = string.Join( ", ",
                    actualConstructors.Select(
                        z => t.NameOfType()
                            + "("
                            + string.Join( ", ", z.GetParameters().Select( zz => zz.ParameterType.NameOfType() ) )
                            + ")" ) );
                throw new InvalidOperationException( $"The type \"{EvoveHelper.NameOfType( t )}\" requires requires either parameters to match the available constructors ({argsThreeString}) or a constructor to match the parameters (either {EvoveHelper.NameOfType( t )}({argsOneString}) or {EvoveHelper.NameOfType( t )}({argsTwoString})). Please provide the requisite parameters OR add one of these constructors to your class." );
            }
        }

        /// <summary>
        /// Creates a GP function from a delegate.
        /// </summary>
        private void CreateGpFunction( Type targetType, int typeIndex, FieldInfo field )
        {
            MethodInfo sig = field.FieldType.GetMethod( nameof( Action.Invoke ) );
            CreateGpFunction( targetType, typeIndex, sig, field );
        }

        /// <summary>
        /// Creates a GP function from a method or delegate.
        /// </summary>
        /// <param name="functionOwner">The owner of the function</param>
        /// <param name="functionOwnerIndex">The index of the owner of the function</param>
        /// <param name="method">The method</param>
        /// <param name="field">The field (if the method is a delegate)</param>
        private void CreateGpFunction( Type functionOwner, int functionOwnerIndex, MethodInfo method, FieldInfo field )
        {
            // Where do we get attributes?
            MemberInfo member = (MemberInfo)field ?? method;

            // Collect attributes
            EvoveMethodAttribute attr = member.GetCustomAttribute<EvoveMethodAttribute>( true ) ?? new EvoveMethodAttribute();
            EvoveIgnoreAttribute ignoreAttribute = member.GetCustomAttribute<EvoveIgnoreAttribute>( true );
            EvoveMethodAttribute methodAttribute = member.GetCustomAttribute<EvoveMethodAttribute>( true );  

            if (ignoreAttribute != null)
            {
                Debug.WriteLine( $"METHOD OR DELEGATE {member.Name} ignored due to {nameof( EvoveIgnoreAttribute )}." );
                return;
            }

            if (methodAttribute != null)
            {
                if (!IsIncludedInSet( methodAttribute.Set ))
                {
                    Debug.WriteLine( $"METHOD OR DELEGATE {member.Name} ignored due to {nameof( EvoveMethodAttribute )}.{nameof( EvoveMethodAttribute.Set )}." );
                    return;
                }
            }

            // Get the types --
            // -- Get the method return type - this is STGP so this is important!
                Type returnType = method.ReturnType;

            // -- Get the parameter types
            ParameterInfo[] paramInfos = method.GetParameters();
            ParameterType[] parameterTypes = new ParameterType[paramInfos.Length];

            for (int i = 0; i < paramInfos.Length; i++)
            {
                var x = paramInfos[i];
                var delegateType = IsDelegate( x.ParameterType ) ? x.ParameterType : null;
                parameterTypes[i] = new ParameterType( x.Name, GetOrCreateGpType( x.ParameterType ), delegateType );

                if (delegateType != null)
                {
                    MethodInfo invokeMethod = delegateType.GetMethod( nameof( Action.Invoke ) );

                    if (invokeMethod.GetParameters().Length != 0)
                    {
                        throw new InvalidOperationException( $"Parameter \"{x.Name}\" on method \"{EvoveHelper.NameOfType( method.DeclaringType )}.{method.Name}\" is a delegate with {invokeMethod.GetParameters().Length} parameters. Delegate inputs to methods must take 0 parameters since their values are sourced from the child GP nodes, of which the number of parameters in reality may vary." );
                    }
                }
            }

            // Wrap our type in GpType --
            GpType resultType;

            if (attr.Role == EMethod.FitnessFunction)
            {
                // -- also do some assertions if this function is the entry point
                if (_rootGpFunctionGpType == null)
                {
                    _rootGpFunctionGpType = new GpType( returnType );
                }
                else
                {
                    if (_rootGpFunctionGpType.Type != returnType)
                    {
                        throw new InvalidOperationException( "The method \"" + functionOwner.Name + "." + method.Name + "\" is an entry point method with a return type of \"" + EvoveHelper.NameOfType( method.ReturnType ) + "\", however an entry point method already exists with a return type of \"" + EvoveHelper.NameOfType( _rootGpFunctionGpType.Type ) + "\". To prevent issues with incomparable fitness value types " + EvoveHelper.PROGRAM_NAME + " does not allow the entry point methods to differ in their return type. If you are sure that your return types are comparable consider changing the return type of your entry point methods to a common base class or interface." );
                    }
                }

                resultType = _rootGpFunctionGpType;

                if (!typeof( IComparable ).IsAssignableFrom( returnType ))
                {
                    throw new InvalidOperationException( "The method \"" + EvoveHelper.NameOfType( method.DeclaringType ) + "." + method.Name + "\" returns an object of type \"" + EvoveHelper.NameOfType( returnType ) + "\", which does not implement IComparable. In order to compare fitness values the type returned by the entry point method(s) " + EvoveHelper.PROGRAM_NAME + " asserts that these methods return type implement the \"" + nameof( IComparable ) + "\" interface." );
                }
            }
            else
            {
                resultType = GetOrCreateGpType( returnType );
            }

            // Determine the cache mode
            ECacheMode cacheMode = ECacheMode.Single;

            if (attr.Cache == ECache.TestCase)
            {
                cacheMode = ECacheMode.Multiple;
            }

            if (attr.Role == EMethod.FitnessFunction)
            {
                if (cacheMode != ECacheMode.Single)
                {
                    throw new InvalidOperationException( "There is an ambiguity in creating the cache. The method \"" + EvoveHelper.NameOfType( method.DeclaringType ) + "." + method.Name + "\" specifies multiple cache attributes. Consider checking this method for errors." );
                }

                cacheMode = ECacheMode.Converges;
            }

            if (attr.Cache == ECache.Uncached)
            {
                if (cacheMode != ECacheMode.Single)
                {
                    throw new InvalidOperationException( "There is an ambiguity in creating the cache. The method \"" + EvoveHelper.NameOfType( method.DeclaringType ) + "." + method.Name + "\" specifies multiple cache attributes. Consider checking this method for errors." );
                }

                cacheMode = ECacheMode.Prohibited;
            }

            // Wrap the method details we collected in a GpFunction object
            // and add to the list of available functions for the GpType
            GpFunction function = new GpFunction( functionOwnerIndex, this, method, resultType, parameterTypes, field, attr.Role == EMethod.ConstantProvider, cacheMode );
            resultType.AddFunctionToOutputs( function );

            // Methods with a duplicateAttribute get added more than once --
            // -- this is a bit hacky but it will do for now!
            if (attr.Duplicate > 1)
            {
                for (int duplicates = 1; duplicates < attr.Duplicate; ++duplicates)
                {
                    resultType.AddFunctionToOutputs( function );
                }
            }

            // Also add the function as an input for its parameter types
            foreach (ParameterType p in parameterTypes)
            {
                p.Type.AddFunctionToInputs( function );
            }

            // Assert that the function ID wasn't already in use and add it to our master list
            // This list and the ID are used for saving/loading
            if (_gpFunctions.ContainsKey( function.Id ))
            {
                throw new InvalidOperationException( $"The method \"{EvoveHelper.NameOfType( method.DeclaringType )}.{method.Name}\" has been assigned the ID \"{function.Id}\", which is already in use. An ID can only be used once to allow data to be saved and loaded. Consider changing the method name to resolve the conflict or enable method overloading in the \"{nameof( EvoveParameters )}\" object." );
            }

            _gpFunctions.Add( function.Id, function );
        }

        /// <summary>
        /// Sets the field specifying the test case.
        /// </summary>               
        private void SetTestCaseField( Type targetType, int typeIndex, FieldInfo fieldName, int numberOfTestCases )
        {
            // Check for ambiguity
            if (_testCaseField != null)
            {
                throw new InvalidOperationException( $"At least two fields have been specified as containing the test case number: \"{_testCaseField.Name}\" and \"{fieldName.Name}\". To avoid exponential growth of the size of the cache {EvoveHelper.PROGRAM_NAME} only allows one field to act as the test case indicator. Consider combining your test case fields or disable caching in the methods through the use of {nameof( ECache )}.{nameof( ECache.Uncached )}." );
            }

            _testCaseField = fieldName;
            _testCaseTypeIndex = typeIndex;

            if (numberOfTestCases != 0)
            {
                // Check for ambiguity
                if (NumberOfTestCases != 0 && NumberOfTestCases != numberOfTestCases)
                {
                    throw new InvalidOperationException( $"The number of test cases has been specified more than once, as {NumberOfTestCases} and {numberOfTestCases}. Please check your class and setup for ambiguities." );
                }

                NumberOfTestCases = numberOfTestCases;
            }

            // Check type
            if (_testCaseField.FieldType != typeof( int ))
            {
                throw new InvalidOperationException( $"The field {EvoveHelper.NameOfType( _testCaseField.FieldType )}.{fieldName} has an invalid or conflicting definition. To avoid additional overhead {EvoveHelper.PROGRAM_NAME} only supports test case indicator fields of the type \"{nameof( Int32 )}\". Consider changing the type of your indicator field or preventing evaluation caching through the use of {nameof( ECache )}.{nameof(ECache.Uncached)}." );
            }
        }

        /// <summary>
        /// Returns if a type is a delagete (function pointer).
        /// </summary>                      
        public bool IsDelegate( Type type )
        {
            return typeof( MulticastDelegate ).IsAssignableFrom( type.BaseType );
        }

        /// <summary>
        /// Gets or creates a STGP type.
        /// </summary>                
        private GpType GetOrCreateGpType( Type returnType )
        {
            bool isDelegate = IsDelegate( returnType );

            if (isDelegate)
            {
                // Translate delegates into their return type 
                MethodInfo method = returnType.GetMethod( nameof( Action.Invoke ) );
                returnType = method.ReturnType;
            }

            // Try and find the existing GP-type
            foreach (GpType type in GpTypes)
            {
                if (type.Type == returnType)
                {
                    return type;
                }
            }

            // -- otherwise create a new GP-type
            GpType result = new GpType( returnType );
            GpTypes.Add( result );

            return result;
        }

        /// <summary>
        /// Gets a GP function by its function ID.
        /// </summary>                           
        internal GpFunction GetElementById( string id )
        {
            return _gpFunctions[id];
        }

        /// <summary>
        /// Creates the per-population objects.
        /// </summary>                   
        internal object[] CreatePerPopulationObjects( PopulationArgs args )
        {
            return ToObjectArray2( _perPopulationFields, args );
        }

        /// <summary>
        /// Creates the per-deme objects.
        /// </summary>                   
        internal object[] CreatePerDemeObjects( DemeArgs args )
        {
            return ToObjectArray2( _perDemeFields, args );
        }

        /// <summary>
        /// Creates a new set of objects with types specified in [fields].
        /// </summary>                                                    
        private object[] ToObjectArray2<TArgs>( List<FixedFieldInfo<TArgs>> fields, TArgs arg )
        {
            object[] result = new object[fields.Count];

            for (int n = 0; n < fields.Count; n++)
            {
                result[n] = fields[n].Factory.Instantiate( arg );
            }

            return result;
        }

        /// <summary>
        /// Sets the [fields] of the [targets] to the [objects].
        /// </summary>                                          
        private void SetFixedObjects<TArgs>( int index, object[] targets, object[] objects, List<FixedFieldInfo<TArgs>> fields )
        {
            for (int n = 0; n < fields.Count; n++)
            {
                if (fields[n].TypeIndex == index) // TODO: No! Should just have seperate lists
                {
                    fields[n].Field.SetValue( targets[index], objects[n] );
                }
            }
        }

        /// <summary>
        /// Gets the current index of the multicache.
        /// </summary>
        internal int GetMultiCacheIndex( object[] targets )
        {
            return (int)_testCaseField.GetValue( targets[_testCaseTypeIndex] );
        }

        /// <summary>
        /// Determins if caching is supported by all of the _targetTypes.
        /// </summary>                                                   
        public bool IsCachingSupported()
        {
            foreach (PerEvaluationInfo t in this.PerEvaluationTypes)
            {
                if (!EvoveClassAttribute.Get( t.Type ).Caching)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
