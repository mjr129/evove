﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    public sealed class EvaluationCache
    {
        private object _singleValue;
        private object[] _multipleValues;
        private EEvaluatedStatus _status;
        private string _clearReason;
        private ECacheMode _mode;

        public string ClearReason
        {
            get
            {
                return _clearReason;
            }
        }

        public object InspectValue()
        {
            return _singleValue;
        }

        public object[] InspectMultiValue()
        {
            return _multipleValues;
        }

        public EEvaluatedStatus Status
        {
            get
            {
                return _status;
            }
        }

        public ECacheMode Mode
        {
            get
            {
                return _mode;
            }
        }

        public EvaluationCache()
        {
            // NA
        }

        public EvaluationCache(EvaluationCache copyFrom)
        {
            _singleValue = copyFrom._singleValue;
            _multipleValues = copyFrom._multipleValues != null ? (object[])copyFrom._multipleValues.Clone() : null;
            _status = copyFrom.Status;
            _clearReason = copyFrom.ClearReason;
            _mode = copyFrom.Mode;
        }

        public void Clear(EEvaluatedStatus newStatus, string reason)
        {
            _mode = ECacheMode.Unknown;
            _singleValue = null;
            _multipleValues = null;
            _status = newStatus;
            _clearReason = reason;
        }

        public void Store(SyntaxManager syntaxManager, ECacheMode mode, object result, object[] targets)
        {
            _status = EEvaluatedStatus.Evaluated;

            if (!syntaxManager.Parameters.AllowCaching)
            {
                return;
            }

            switch (mode)
            {
                case ECacheMode.Multiple:
                    if (this._multipleValues == null)
                    {
                        this._multipleValues = new object[syntaxManager.NumberOfTestCases];
                        this._singleValue = null;
                    }

                    int multiCacheIndex = syntaxManager.GetMultiCacheIndex(targets);
                    this._multipleValues[multiCacheIndex] = result;
                    Logger.Log("Caching testcase: ", multiCacheIndex);
                    break;

                case ECacheMode.Prohibited:
                    this._multipleValues = null;
                    this._singleValue = null;
                    Logger.Log("Caching prohibited");
                    break;

                case ECacheMode.Single:
                case ECacheMode.Converges:
                    this._multipleValues = null;
                    this._singleValue = result;
                    Logger.Log("Caching");
                    break;

                default:
                    throw new InvalidOperationException("Invalid switch (mode = " + mode + ").");
            }

            this._mode = mode;
        }

        public object Retrieve(SyntaxManager syntaxManager, object[] targets)
        {
            if (!syntaxManager.Parameters.AllowCaching)
            {
                return null;
            }

            switch (this._mode)
            {
                case ECacheMode.Multiple:
                    int multiCacheIndex = syntaxManager.GetMultiCacheIndex(targets);
                    Logger.Log("Retrieving testcase: ", multiCacheIndex);
                    return _multipleValues[multiCacheIndex];

                case ECacheMode.Single:
                    Logger.Log("Retrieving single");
                    return _singleValue;

                case ECacheMode.Prohibited:
                    Logger.Log("Retrieving prohibited.");
                    return null;

                case ECacheMode.Unknown:
                    Logger.Log("Retrieving unavailable.");
                    return null;

                case ECacheMode.Converges:
                    Logger.Log("Retrieving unavailable for converger.");
                    return null;

                default:
                    throw new InvalidOperationException("Invalid switch (mode = " + this._mode + ").");
            }
        }

        internal void NotifyDelegate()
        {
            if (_status != EEvaluatedStatus.Evaluated)
            {
                _status = EEvaluatedStatus.Delegated;
            }
        }

        internal void ClearFlags()
        {
            if (this._status == EEvaluatedStatus.Flagged)
            {
                this._status = EEvaluatedStatus.None;
            }
        }
    }
}
