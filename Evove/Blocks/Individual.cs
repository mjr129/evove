﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    /// <summary>
    /// A GP individual.
    /// </summary>
    public sealed class Individual
    {
        /// <summary>
        /// Every individual gets a UID, here is where we keep track of the next UID.
        /// </summary>
        private static int __uidCounter;

        /// <summary>
        /// The genome is contained in this.
        /// </summary>
        public readonly Tree Genome;

        /// <summary>
        /// The individual's tracking ID
        /// </summary>
        public readonly int Uid;

        /// <summary>
        /// We record the history of the individual here.
        /// </summary>
        public readonly IndividualHistory History;

        /// <summary>
        /// What deme the individual is in.
        /// </summary>
        public readonly Deme Deme;

        /// <summary>
        /// We record the last fitness result here.
        /// </summary>
        private LatestResultType _latestResult;

        /// <summary>
        /// We record how long the individual took to breed here.
        /// </summary>
        public long BreedTime { get; internal set; }

        /// <summary>
        /// Constructor.
        /// New individual with random root node.
        /// </summary>                      
        internal Individual( Randomiser randomiser, SyntaxManager m, Deme deme, EvoveParameters parameters )
        {
            Uid = ++__uidCounter;
            History = new IndividualHistory( null, EHistoryEvent.New_individual, 0, Uid );

            Genome = new Tree( m.CreateRandomRootNode( randomiser, parameters ) );
            Deme = deme;
        }

        /// <summary>
        /// Constructor.               
        /// New copy of existing individual (assigns new UID).
        /// </summary>                     
        public Individual( Individual copyFrom )
            : this( copyFrom, true )
        {
            // NA
        }

        /// <summary>
        /// Constructor.
        /// New copy of existing individual (can keep UID, only for debugging purposes!).
        /// </summary>                     
        public Individual( Individual copyFrom, bool newUid )
        {
            if (newUid)
            {
                Uid = ++__uidCounter;
                History = new IndividualHistory( copyFrom.History, EHistoryEvent.Copy_of_individual, copyFrom.Uid, Uid );
            }
            else
            {
                Uid = copyFrom.Uid;
                History = new IndividualHistory( copyFrom.History, EHistoryEvent.Copy_of_individual_for_debug, copyFrom.Uid, 0 );
            }

            Genome = copyFrom.Genome.Clone();
            Deme = copyFrom.Deme;
            _latestResult = copyFrom._latestResult;
        }

        /// <summary>
        /// Constructor.
        /// New individual from XML.
        /// </summary>                 
        internal Individual( SyntaxManager sm, Deme[] demes, Token token )
        {
            this.Uid = ++__uidCounter;
            this.History = new IndividualHistory( null, EHistoryEvent.Load_from_file, 0, Uid );

            this.Genome = new Tree( new Node( sm, token["genome"] ) );
            this.Deme = demes[token["deme"].Integer];

            Genome.DEBUG_Validate( sm.Parameters );
        }

        /// <summary>
        /// Converts to XML.
        /// </summary>      
        internal Token ToToken( string key )
        {
            Token result = new Token( key );

            result.Add( this.Genome.GetRoot().ToToken( "genome" ) );
            result.Add( new Token( "deme", this.Deme.Index.ToString() ) );

            return result;
        }

        /// <summary>
        /// Quick display of this individual as a string (fitness and name).
        /// </summary>                   
        public override string ToString()
        {
            return "{ " + this.Uid + ": " + LatestResult?.ToString() + " }";
        }

        /// <summary>
        /// Gets the latest fitness value.
        /// </summary>
        public LatestResultType LatestResult
        {
            get
            {
                return _latestResult;
            }
        }

        public class LatestResultType
        {
            /// <summary>
            /// See Fitness.
            /// </summary>
            IComparable _fitness;

            /// <summary>
            /// We record how long the individual took to evaluate here.
            /// </summary>
            public readonly long EvaluationTime;

            /// <summary>
            /// We record the size of the GP tree when we run it here.
            /// Note: This is the size of the WHOLE tree, not just the evaluated parts.
            /// </summary>
            public readonly int TreeSize;

            /// <summary>
            /// If the individual was terminated
            /// </summary>
            public readonly bool WasKilled;

            /// <summary>
            /// For display
            /// </summary> 
            public override string ToString()
            {
                if (WasKilled)
                {
                    return "(TERMINATED)";
                }

                if (Fitness == null)
                {
                    return "(MISSING)";
                }

                return _fitness.ToString();
            }

            public LatestResultType( IComparable fitness, int treeSize, long evaluationTime, bool killed )
            {
                this._fitness = fitness;
                this.TreeSize = treeSize;
                this.EvaluationTime = evaluationTime;
                this.WasKilled = killed;
            }

            /// <summary>
            /// Fitness of the individual.
            /// Note that this can be NULL if the individual was killed.
            /// </summary>
            public IComparable Fitness
            {
                get
                {
                    if (_fitness is double)
                    {
                        double dFitness = (double)_fitness;

                        if (double.IsNaN( dFitness ) || double.IsInfinity( dFitness ))
                        {
                            return 0.0d;
                        }
                    }

                    return _fitness;
                }
            }
        }

        /// <summary>
        /// Performs evaluation
        /// </summary>
        public void Evaluate( Population population )
        {
            SyntaxManager manager = population.SyntaxManager;                     
            int round = population.Round;

            Logger.Log( "Evaluating individual ", this );

            // Output: Tree size
            int treeSize = Genome.CountAllNodes();

            // Time the operation
            Stopwatch stopWatch = Stopwatch.StartNew();

            Node.ExecutionContext exec = manager.CreateExecutionContext( population, this.Deme, round );

            // Run the genome!!!!!
            Logger.Log( "Executing tree" );
            IComparable fitness = Genome.Execute( exec );

            manager.ReturnExecutionContext( this.Deme, exec );

            // Output: Evaluation time
            long evaluationTime = (long)(stopWatch.Elapsed.TotalMilliseconds * 1000);
            var x = new AfterEvaluationArgs( round, fitness, treeSize, evaluationTime );
            fitness = x.Fitness;

            if (!exec.IsTerminated)
            {
                Debug.Assert( fitness != null );
            }

            _latestResult = new LatestResultType( fitness, treeSize, evaluationTime, exec.IsTerminated );

            // Assert that no cache errors occured
            if (manager.Parameters.CheckForCacheErrors)
            {
                if (exec.IsTerminated)
                {
                    throw new InvalidOperationException( "Cache check requires the programs do not run out of execute chances." );
                }

                CheckForCacheErrors( population, manager, round );
            }
        }

        /// <summary>
        /// Repeats the evaluation without caching to make sure the result is the same as for
        /// caching.
        /// </summary>
        private void CheckForCacheErrors( Population population, SyntaxManager manager, int round )
        {
            Debug.Assert( manager.Parameters.AllowCaching );

            Node.ExecutionContext exec = manager.CreateExecutionContext( population, this.Deme, round );

            foreach (bool allow in new[] { true, false })
            {
                Logger.Log( "Checking for cache errors, caching enabled is ", allow );

                Tree copy = new Tree( Genome.GetRoot().DeepCopy() );

                foreach (Node node in copy.IterateAllNodes( true ))
                {
                    node.Cache.Clear( EEvaluatedStatus.None, "Detecting cache errors" );
                }

                manager.Parameters.AllowCaching = allow;
                Logger.Log( "Executing copy of tree" );
                IComparable newResult = copy.Execute( exec );
                manager.Parameters.AllowCaching = true;

                if (exec.IsTerminated)
                {
                    throw new InvalidOperationException( "Cache check requires the programs do not run out of execute chances." );
                }

                if (!object.Equals( newResult, _latestResult ))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine( "Discrepancy between results of cached and non-cached code." );
                    sb.AppendLine( "CACHE: " + (allow ? "ENABLED" : "DISABLED") );
                    sb.AppendLine( "* UID: " + Uid );
                    sb.AppendLine( "* Cached: " + (_latestResult != null ? _latestResult.ToString() : "(null)") );
                    sb.AppendLine( "* Non-cached: " + (newResult != null ? newResult.ToString() : "(null)") );
                    sb.AppendLine( "* TreeSize: " + this.LatestResult?.TreeSize );

                    if (allow)
                    {
                        foreach (Node nodeCopy in copy.IterateAllNodes( true ))
                        {
                            Node nodeMine = this.FindEquivalentNode( nodeCopy );

                            object sv1 = nodeCopy.Cache.InspectValue();
                            object[] mv1 = nodeCopy.Cache.InspectMultiValue();
                            object sv2 = nodeMine.Cache.InspectValue();
                            object[] mv2 = nodeMine.Cache.InspectMultiValue();

                            bool oe = object.Equals( sv1, sv2 );
                            bool ae = ArrayEquals( mv1, mv2 );

                            if (!oe || !ae)
                            {
                                sb.AppendLine();
                                sb.AppendLine( " --- " + nodeMine.TrackingId + " --- " );
                                sb.AppendLine( "Depth: " + nodeMine.Depth );
                                sb.AppendLine( "Description Copy: " + nodeCopy.ToString() );
                                sb.AppendLine( "Description Orig: " + nodeMine.ToString() );

                                if (!oe)
                                {
                                    sb.AppendLine( "VALUES NOT EQUAL" );
                                }

                                if (!ae)
                                {
                                    sb.AppendLine( "ARRAYS NOT EQUAL" );
                                }

                                if (sv1 != null)
                                {
                                    sb.AppendLine( "Copy: " + sv1.ToString() );
                                }

                                if (sv2 != null)
                                {
                                    sb.AppendLine( "Orig: " + sv2.ToString() );
                                }

                                if (mv1 != null)
                                {
                                    sb.AppendLine( "Copy[]: " + string.Join( ", ", mv1 ) );
                                }

                                if (mv2 != null)
                                {
                                    sb.AppendLine( "Orig[]: " + string.Join( ", ", mv2 ) );
                                }
                            }
                        }
                    }

                    Deme.Population.CriticalHalt( new List<Individual> { this }, sb.ToString() );
                }
            }

            manager.ReturnExecutionContext( this.Deme, exec );
        }

        private bool ArrayEquals( object[] mv1, object[] mv2 )
        {
            if (mv1 == null)
            {
                return mv2 == null;
            }
            else if (mv2 == null)
            {
                return false;
            }

            if (mv1.Length != mv2.Length)
            {
                return false;
            }

            for (int n = 0; n < mv1.Length; n++)
            {
                if (!object.Equals( mv1[n], mv2[n] ))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Finds a node in a copy of a individual.
        /// </summary>
        public Node FindEquivalentNode( Node equivalent )
        {
            Node x = equivalent;
            List<int> indexes = new List<int>();

            while (x.Parent != null)
            {
                indexes.Add( x.Index );
                x = x.Parent;
            }

            indexes.Reverse();

            x = Genome.GetRoot();

            foreach (int index in indexes)
            {
                x = x.Children[index];
            }

            if (x.Function != equivalent.Function)
            {
                throw new InvalidOperationException( "Failed to find node." );
            }

            return x;
        }

        /// <summary>
        /// Clears the evaluation caches.
        /// </summary>                   
        public void ClearCache( string reason )
        {
            _latestResult = null;

            foreach (Node node in Genome.IterateAllNodes( true ))
            {
                node.Cache.Clear( EEvaluatedStatus.None, reason );
            }
        }
    }
}
