﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{        
    /// <summary>
    /// Describes the history of an individual.
    /// Used only in debugging.
    /// </summary>
    public sealed class IndividualHistory
    {
        /// <summary>
        /// Represents an event in history.
        /// </summary>
        private sealed class Record
        {
            /// <summary>
            /// Previous in timeline.
            /// </summary>
            public readonly Record Back;

            /// <summary>
            /// What happened.
            /// </summary>
            public readonly EHistoryEvent Source;

            /// <summary>
            /// UID of individual at time of event.
            /// </summary>
            public readonly int Uid;

            /// <summary>
            /// UID of individual after the event.
            /// </summary>
            public readonly int NewUid;

            public Record(Record back, EHistoryEvent source, int uid, int newUid)
            {
                Back = back;
                Source = source;
                Uid = uid;
                NewUid = newUid;
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();

                GetLoneHistory(sb);

                return sb.ToString();
            }

            public void GetCompleteHistory(StringBuilder sb)
            {
                if (Back != null)
                {
                    Back.GetCompleteHistory(sb);
                }

                GetLoneHistory(sb);
            }

            private void GetLoneHistory(StringBuilder sb)
            {
                if (NewUid != 0)
                {
                    sb.Append("[");
                    sb.Append(NewUid);
                    sb.Append("] ");
                }
                else
                {
                    sb.Append(" - ");
                }

                sb.Append(Source.ToString());

                if (Uid != 0)
                {
                    sb.Append(" (");
                    sb.Append(Uid);
                    sb.Append(")");
                }

                sb.AppendLine();
            }
        }

        private Record _history;

        public IndividualHistory(IndividualHistory back, EHistoryEvent source, int uid, int newUid)
        {
            _history = new Record(back?._history, source, uid, newUid);
        }

        public void UnsafeAdd(EHistoryEvent source, int uid, int newUid)
        {
            _history = new Record(_history, source, uid, newUid);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            _history.GetCompleteHistory(sb);

            return sb.ToString();
        }
    }

    public static class IndividualHistoryExtensions
    {
        [Conditional("DEBUG")]
        public static void Add(this IndividualHistory individualHistory, EHistoryEvent source, int uid, int newUid)
        {    
            if (individualHistory != null)
            {
                individualHistory.UnsafeAdd(source, uid, newUid);
            }
        }
    }
}
