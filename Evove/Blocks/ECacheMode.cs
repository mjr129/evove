﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    public enum ECacheMode
    {
        Unknown,
        Single,
        Multiple,
        Converges,
        Prohibited,
    }
}
