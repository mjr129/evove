﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    /// <summary>
    /// Used to maintain an <see cref="IndividualHistory"/>.
    /// Used only in debugging.
    /// </summary>
    public enum EHistoryEvent
    {
        Copy_of_individual,
        Copy_of_individual_for_debug,
        New_individual,
        Load_from_file,
        Single_mutation,
        Iterative_mutatation,
        Crossover_with_individual,
        Single_mutation_of_leaf_node,
        Single_mutation_of_branch_node,
        Point_reordering_including_node,
        Point_replacement_of_node,
        Point_insertion_before_node
    }
}
