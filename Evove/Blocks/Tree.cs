﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    /// <summary>
    /// Wraps up the root <see cref="Node"/> of a tree.
    /// </summary>
    public sealed class Tree
    {
        Node root;

        public Tree(Node root)
        {
            this.root = root;
        }

        public Tree Clone()
        {
            return new Tree(root.DeepCopy());
        }

        public override string ToString()
        {
            return "Tree";
        }

        [Conditional("DEBUG")]
        public void DEBUG_Validate(EvoveParameters parameters)
        {
            IEnumerable<Node> allNodes = IterateAllNodes(true);

            Debug.Assert(allNodes.Distinct().Count() == allNodes.Count());

            foreach (Node node in allNodes)
            {
                node.DEBUG_Validate(parameters.MaxTreeDepth);
            }
        }

        public IEnumerable<Node> IterateAllNodes(bool includeRoot)
        {
            return root.IterateAllNodes(includeRoot);
        }

        public void Save(string fileName)
        {
            var token = this.root.ToToken("individual");

            using (StreamWriter sb = new StreamWriter(fileName))
            {
                token.Write(sb);
            }                    
        }

        public IComparable Execute(Node.ExecutionContext exec)
        {
            IComparable result = (IComparable)root.Execute(exec);

            foreach (Node node in IterateAllNodes(true))
            {
                node.ClearRemainingFlags();
            }

            if (exec.IsTerminated)
            {
                return null;
            }

            return result;
        }

        internal int CountAllNodes()
        {
            return root.CountAllNodes();
        }

        public Node GetRoot()
        {
            return root;
        }
    }
}
