﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    /// <summary>
    /// A type of function parameter or function result (for strong-typing).
    /// Called "GpType" to disambiguate from the widely used "Type" in .NET.
    /// </summary>
    public sealed class GpType
    {
        /// <summary>
        /// The .NET type
        /// </summary>
        public readonly Type Type;                    

        /// <summary>
        /// The complete list of functions that return this type
        /// Used as the list of possibilities GP nodes that provide this type.
        /// </summary>
        private readonly List<GpFunction> _outputFunctions = new List<GpFunction>();

        /// <summary>
        /// A subset of _outputFunctions that are parameterless, requiring no input, and can
        /// therefore be used when a LEAF node is necessitated.
        /// </summary>
        private readonly List<GpFunction> _sourceFunctions = new List<GpFunction>();

        /// <summary>
        /// Functions that take this Type as a parameter
        /// For diagnostic purposes only.
        /// </summary>
        private readonly List<GpFunction> _inputFunctions = new List<GpFunction>();

        /// <summary>
        /// Functions that have this type both as an input and output (the intersection of
        /// _inputFunctions and _outputFunctions). This is used for the INSERT mutator.
        /// </summary>
        private readonly List<GpFunction> _inputOutputFunctions = new List<GpFunction>();

        /// <summary>
        /// CONSTRUCTOR
        /// </summary> 
        public GpType(Type type)
        {
            Type = type;
        }     

        public IReadOnlyList<GpFunction> OutputFunctions
        {
            get
            {
                return _outputFunctions;
            }
        }

        public IReadOnlyList<GpFunction> SourceFunctions
        {
            get
            {
                return _sourceFunctions;
            }
        }

        public IReadOnlyList<GpFunction> InputFunctions
        {
            get
            {
                return _inputFunctions;
            }
        }

        public IReadOnlyList<GpFunction> InputOutputFunctions
        {
            get
            {
                return _inputOutputFunctions;
            }
        }       

        public string Name
        {
            get
            {
                return EvoveHelper.NameOfType(Type);
            }
        }        

        public override string ToString()
        {
            return Name;
        }

        internal void AddFunctionToOutputs(GpFunction x)
        {
            _outputFunctions.Add(x);

            if (x.ParameterTypes.Length == 0)
            {
                _sourceFunctions.Add(x);
            }
        }

        internal void AddFunctionToInputs(GpFunction element)
        {
            _inputFunctions.Add(element);
        }

        internal void Complete()
        {
            this._inputOutputFunctions.AddRange(this._outputFunctions.Intersect(this._inputFunctions));
        }
    }
}
