﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    /// <summary>
    /// Describes a parameter to a GP function.
    /// </summary>
    public sealed class ParameterType
    {
        /// <summary>
        /// Type of the parameter.
        /// If the parameter is delegated (i.e. lazy) this is the result of the delegate.
        /// </summary>
        public readonly GpType Type;

        /// <summary>
        /// If the parameter is delegated (i.e. lazy) this is the type of delegate, otherwise it is
        /// NULL.
        /// </summary>
        public readonly Type DelegateType;

        /// <summary>
        /// Name of the parameter (for debugging only)
        /// </summary>
        public readonly string ParameterName;

        public ParameterType(string name, GpType type, Type delegateType)
        {
            ParameterName = name;
            Type = type;
            DelegateType = delegateType;
        }

        public string Name
        {
            get
            {
                if (DelegateType != null)
                {
                    return Type.Name + " " + ParameterName + "()";
                }

                return Type.Name + " " + ParameterName;
            }
        }
    }
}
