﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks.Data
{
    /// <summary>
    /// Contains information on the types of object used per-round or per-deme.
    /// These are placed into the per-evaluation objects via the Field.
    /// </summary>
    internal sealed class FixedFieldInfo<TArg>
    {
        public readonly FieldInfo Field;
        public readonly int TypeIndex;
        public readonly Factory<TArg> Factory;

        public FixedFieldInfo( FieldInfo field, int typeIndex, object[] constructorArguments )
        {
            this.Factory = new Factory<TArg>( field.FieldType, constructorArguments );
            this.Field = field;
            this.TypeIndex = typeIndex;
        }     

        public override string ToString()
        {
            return EvoveHelper.NameOfType( Field.DeclaringType ) + "::" + Field.Name + " = " + base.ToString();
        }
    }
}
