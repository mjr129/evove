﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks.Data
{
    public sealed class PerEvaluationInfo
    {
        public enum EMode
        {
            Fitness,
            CustomExtension,
            Extension,    
            FieldExtension,
        }

        public readonly Factory<InstantiationArgs> factory;
        private readonly FieldInfo field;
        public readonly int index;
        private readonly PerEvaluationInfo owner;
        public readonly EMode mode;

        public PerEvaluationInfo( PerEvaluationInfo source, Factory<InstantiationArgs> factory, int index, EMode mode)
        {
            EvoveHelper.CheckNotNull(nameof( factory ), factory );

            this.owner = source;
            this.factory = factory;
            this.index = index;
            this.mode = mode;
        }

        public PerEvaluationInfo(PerEvaluationInfo owner, FieldInfo field, int index, EMode mode)
        {
            EvoveHelper.CheckNotNull( nameof( owner ), owner );
            EvoveHelper.CheckNotNull( nameof( field ), field );

            this.owner = owner;
            this.field = field;
            this.index = index;
            this.mode = mode;
        }

        internal object Instantiate(InstantiationArgs args, object[] actualObjects)
        {
            Debug.Assert( args != null );
            Debug.Assert( actualObjects != null );

            if (factory != null)
            {
                return factory.Instantiate(args);
            }

            Debug.Assert(owner.index < index); // Owner must have been instantiated
            return field.GetValue(actualObjects[owner.index]);
        }

        public Type Type
        {
            get
            {
                if (factory != null)
                {
                    return factory.Type;
                }

                return field.FieldType;
            }
        }

        public PerEvaluationInfo FirstOwner
        {
            get
            {
                if (owner != null)
                {
                    return owner.FirstOwner;
                }

                return this;
            }
        }

        public int Depth
        {
            get
            {
                if (owner == null)
                {
                    return 0;
                }

                return owner.Depth + 1;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (owner != null)
            {
                sb.Append( ' ', Depth * 4 );
            }

            switch (mode)
            {
                case EMode.CustomExtension:
                    sb.Append( "+ " );
                    break;

                case EMode.Extension:
                    sb.Append( "extends " );
                    break;                  
                    
                case EMode.FieldExtension:
                    sb.Append( "includes " );
                    break;

                case EMode.Fitness:
                    //sb.Append( "fitness " );
                    break;     
            }

            sb.Append( factory );

            //sb.Append( " - " + NumberOfFunctions + " functions" );


            return sb.ToString();
        }
    }
}
