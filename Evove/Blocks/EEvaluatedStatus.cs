﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    /// <summary>
    /// How the node has been evaluated (only for debugging and visualisations).
    /// </summary>
    public enum EEvaluatedStatus
    {
        /// <summary>
        /// Execute never called
        /// </summary>
        None,

        /// <summary>
        /// GetDelegate called but the delegate was never called.
        /// </summary>
        Delegated,

        /// <summary>
        /// Execute called
        /// </summary>
        Evaluated,

        /// <summary>
        /// Execute was called, but the node was marked as dirty, necessitating reevaluation
        /// </summary>
        Flagged,
    }
}
