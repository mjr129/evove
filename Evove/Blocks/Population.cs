﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Evove.Operators.Migration;
using Evove.Operators.Stopping;

namespace Evove.Blocks
{
    /// <summary>
    /// Basically, everything.
    /// </summary>
    public sealed class Population : IDisposable
    {
        public readonly SyntaxManager SyntaxManager;
        public int Round { get; private set; }
        public Individual BestIndividual { get; private set; }
        public readonly object[] PerPopulationObjects;
        public readonly EvoveParameters Parameters;
        public readonly string OutputFolder;

        /// <summary>
        /// Thread handler for evaluations
        /// </summary>
        private ThreadedEvaluator _evaluator;

        /// <summary>
        /// Thread handler for breeding
        /// </summary>
        private ThreadedBreeder _breeder;

        /// <summary>
        /// Demes
        /// </summary>
        private Deme[] Demes;

        /// <summary>
        /// Designated observer
        /// </summary>
        private IProgressObserver ProgressWatcher;

        /// <summary>
        /// A stopwatch to time the simulation
        /// </summary>
        private Stopwatch _runTimer;

        /// <summary>
        /// Flagged if the fittess function changes
        /// </summary>
        private bool _fitnessFunctionChanged;

        /// <summary>
        /// The randomiser. This is thread-safe.
        /// </summary>
        public readonly Randomiser Randomiser;

        /// <summary>
        /// Logs are also sent here
        /// </summary>
        private readonly StreamWriter LogStream;

        public event EventHandler<RoundStartEventArgs> RoundStart;
        public event EventHandler<RoundEndEventArgs> RoundEnd;
        public event EventHandler<SimulationCompleteEventArgs> SimulationComplete;         

        public TimeSpan ElapsedTime
        {
            get
            {
                return _runTimer.Elapsed;
            }
        }

        private void RaiseRoundStart( RoundStartEventArgs args )
        {
            if (RoundStart != null)
            {
                RoundStart( this, args );
            }
        }

        private void RaiseRoundEnd( RoundEndEventArgs args )
        {
            if (RoundEnd != null)
            {
                RoundEnd( this, args );
            }
        }

        private void RaiseSimulationComplete( SimulationCompleteEventArgs args )
        {
            if (SimulationComplete != null)
            {
                SimulationComplete( this, args );
            }
        }

        public StreamWriter WriteOutputFile( string fileName )
        {
            return new StreamWriter( GetOutputFile( fileName ) );
        }

        public void Dispose()
        {
            foreach (Deme deme in Demes)
            {
                deme.Dispose();
            }

            foreach (object x in PerPopulationObjects)
            {
                if (x is IDisposable)
                {
                    ((IDisposable)x).Dispose();
                }
            }
        }

        public string GetOutputFile( string fileName )
        {
            string path = Path.Combine( OutputFolder, fileName );

            string directory = Path.GetDirectoryName( path );
            Directory.CreateDirectory( directory );

            return path;
        }

        public IEnumerable<Individual> AllIndividuals()
        {
            foreach (Deme d in Demes)
            {
                foreach (Individual i in d.Individuals)
                {
                    yield return i;
                }
            }
        }

        public int NumberOfCaches
        {
            get
            {
                return SyntaxManager.NumberOfTestCases;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary> 
        public Population( EvoveParameters parameters )
        {
            EvoveHelper.CheckNotNull( nameof( parameters ), parameters );
            EvoveHelper.Check( nameof( EvoveParameters.AllowIncomplete ), !parameters.AllowIncomplete, "An incomplete parameter set is for reflecting the class data only cannot be used to create a population." );

            ProgressWatcher = parameters.ProgressWatcher;
            parameters.Validate();
            SyntaxManager = new SyntaxManager( parameters );

            Parameters = parameters;
            _evaluator = new ThreadedEvaluator( this );
            _breeder = new ThreadedBreeder( this, parameters );

            // Create the run-folder
            OutputFolder = parameters.OutputFolder;

            if (Directory.Exists( OutputFolder ))
            {
                throw new InvalidOperationException( "The output directory \"" + OutputFolder + "\" already exists." );
            }

            Directory.CreateDirectory( OutputFolder );

            // Open the log file
            LogStream = new StreamWriter( GetOutputFile( "EvoveLog.txt" ) );

            // Initialise the population
            Demes = new Deme[this.Parameters.NumberOfDemes];
            this.Randomiser = new Randomiser( parameters.RandomSeed );

            InitialisePopulation();

            PopulationArgs args = new PopulationArgs( this );
            this.PerPopulationObjects = SyntaxManager.CreatePerPopulationObjects( args );

            this.Round = args.StartingRound;
        }

        /// <summary>
        /// Creates the initial population.         
        /// </summary>
        private void InitialisePopulation()
        {
            // Create demes
            for (int demeIndex = 0; demeIndex < Demes.Length; demeIndex++)
            {                                     
                Demes[demeIndex] = new Deme( this, demeIndex, SyntaxManager );
            }

            // Load or create random individuals
            if (string.IsNullOrEmpty( Parameters.ContinueFromFile ))
            {
                InitialiseRandomPopulation();
            }
            else
            {
                SimpleLog( "Initialising population: From file \"" + Parameters.ContinueFromFile + "\"" );

                Token root;

                using (StreamReader fs = new StreamReader( Parameters.ContinueFromFile ))
                {
                    root = Token.Read( fs );
                }

                Token population = root["population"];

                Round = population["round"].Integer;

                int n = 0;

                while (population.Contains( n.ToString() ))
                {
                    Individual r = new Individual( this.SyntaxManager, Demes, population[n.ToString()] );
                    r.Deme.Individuals.Add( r );

                    n++;
                }

                SimpleLog( "Resuming from round " + Round );
            }
        }

        /// <summary>
        /// Saves population to file (XML).
        /// </summary>                     
        public void Save( string fileName )
        {
            using (StreamWriter fs = new StreamWriter( fileName ))
            {
                ToToken( "population" ).Write( fs );
            }
        }

        private Token ToToken( string key )
        {
            Token result = new Token( key );

            result.Add( new Token( "round", Round.ToString() ) );
            int n = 0;

            foreach (Individual i in AllIndividuals())
            {
                result.Add( i.ToToken( n.ToString() ) );
                n++;
            }

            return result;
        }

        public void SimpleLog( string message )
        {
            LogStream.WriteLine( message );
            LogStream.Flush();
            ProgressWatcher.PopulationMessage( new MessageArgs( message ) );
        }

        private void InitialiseRandomPopulation()
        {
            SimpleLog( "Initialising population: Random population." );

            for (int demeIndex = 0; demeIndex < Parameters.NumberOfDemes; demeIndex++)
            {
                Demes[demeIndex].Individuals.Clear();

                for (int n = 0; n < Parameters.IndividualsPerDeme; n++)
                {
                    Demes[demeIndex].Individuals.Add( new Individual( Randomiser, SyntaxManager, Demes[demeIndex], Parameters ) );
                }
            }
        }

        /// <summary>
        /// Runs the main loop in the current thread.
        /// 
        /// If "startPaused" is set the similation will enter step-through mode and pause at the
        /// first step event and wait for a "StepOver" or "Play" command (which must be sent from
        /// another thread.
        /// </summary>
        public void Start()
        {
            try
            {
                AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException;
                if (_runTimer != null)
                {
                    throw new InvalidOperationException( nameof( Start ) + " cannot be called more than once for a given " + nameof( Population ) + "." );
                }

                // Start the stopping conditions
                foreach (IStoppingCondition stoppingCondition in this.Parameters.StoppingConditions)
                {
                    stoppingCondition.Reset();
                }

                // Start timer
                _runTimer = Stopwatch.StartNew();

                while (RunRound())
                {
                    // NA
                }

                NotifyStageChange( EStage.EndOfSimulation );
            }
            finally
            {
                AppDomain.CurrentDomain.FirstChanceException -= CurrentDomain_FirstChanceException;
            }
        }

        private void CurrentDomain_FirstChanceException( object sender, FirstChanceExceptionEventArgs e )
        {
            string fileName = GetOutputFile( "Exceptions.txt" );
            string message = $"An exception occured during round {Round}: {e.Exception.ToString()}";

            File.AppendAllText( fileName, message );
        }

        bool? RoundEvents( ESpecialEvents special )
        {
            if (special.HasFlag( ESpecialEvents.Extinction ))
            {
                SimpleLog( "PER-ROUND OBJECTS RAISED EXTINCTION EVENT - SET POPULATION TO RANDOM" );
                ProgressWatcher.OnSpecialEvent( ESpecialEvents.Extinction );
                InitialiseRandomPopulation();
                return true;
            }

            if (special.HasFlag( ESpecialEvents.Stop ))
            {
                SimpleLog( "PER-ROUND OBJECTS RAISED EARLY STOPPING EVENT - STOP SIMULATION" );
                ProgressWatcher.OnSpecialEvent( ESpecialEvents.Stop );
                return false;
            }

            if (special.HasFlag( ESpecialEvents.Restart ))
            {
                SimpleLog( "PER-ROUND OBJECTS RAISED POPULATION RESTART - REINITIALISE POPULATION, SET ROUND TO 0" );
                ProgressWatcher.OnSpecialEvent( ESpecialEvents.Restart );
                InitialisePopulation();
                Round = 0;
                return true;
            }

            return null;
        }

        private bool RunRound()
        {
            // Now is a good time to take out the trash
            // This avoids individuals evaluating during a collection getting unfarily excised
            GC.Collect();

            // Increment the round
            ++Round;
            SimpleLog( "ROUND " + Round + " - " + BestIndividual?.LatestResult?.Fitness );

            // Wait for a "step" in the debugger
            if (!NotifyStageChange( EStage.BeforeEvaluation ))
            {
                return false;
            }

            // Create a list of all the individuals to evaluate
            List<Individual> all = new List<Individual>( Demes.SelectMany( z => z.Individuals ) );

            // Raise round events
            RaiseRoundStart( new RoundStartEventArgs( this ) );

            // If the fitness function has changed all individuals need to lose their fitness
            if (_fitnessFunctionChanged)
            {
                SimpleLog( "FITNESS FUNCTION CHANGED" );
                _fitnessFunctionChanged = false;

                foreach (Individual individual in all)
                {
                    individual.ClearCache( "Fitness function changed" );
                }
            }

            // Shuffle the individuals
            if (Parameters.ShuffleBeforeEvaluate)
            {
                Randomiser.Shuffle( all );
            }

            // Evaluate the individuals
            _evaluator.Evaluate( all );

            // Check the highest fitness
            SortAndLog();

            // Check the stopping conditions
            StoppingConditionArgs args = new StoppingConditionArgs( this );

            foreach (IStoppingCondition stoppingCondition in Parameters.StoppingConditions)
            {            
                if (stoppingCondition.CheckStop( args ))
                {
                    SimpleLog( $"Ending due to stopping condition \"{stoppingCondition}\" with fitness \"{BestIndividual.LatestResult?.Fitness}\"." );
                    return false;
                }
            }

            // Wait for a "step" in the debugger
            if (!NotifyStageChange( EStage.BeforeBreeding ))
            {
                return false;
            }

            // Round end events
            RoundEndEventArgs roundEndArgs = new RoundEndEventArgs( this );
            RaiseRoundEnd( roundEndArgs );

            bool? events = RoundEvents( roundEndArgs.SpecialEvents );

            if (events.HasValue)
            {
                return events.Value;
            }

            // Create successor population
            CreateSuccessors();

            // Continue
            return true;
        }

        private bool NotifyStageChange( EStage stage )
        {
            var args = new StageChangeArgs( stage );
            ProgressWatcher.OnWaitingForStep( args );
            return !args.StopSimulation;
        }

        internal void CriticalHalt( List<Individual> probematicIndividuals, string errorDetails )
        {
            SimpleLog( "AN ERROR OCCURED: " + errorDetails );
            ProgressWatcher.OnError( new ErrorArgs( probematicIndividuals, errorDetails ) );
        }

        private void SortAndLog()
        {
            BestIndividual = null;

            for (int demeIndex = 0; demeIndex < Demes.Length; demeIndex++)
            {
                Deme deme = Demes[demeIndex];
                //Debug.Assert(deme.Individuals.All(z => z.Fitness != null));

                deme.Individuals.Sort( SortIndividuals );

                Individual bestInDeme = deme.Individuals.First();

                if (BestIndividual == null || SortIndividuals( bestInDeme, BestIndividual ) < 0)
                {
                    BestIndividual = bestInDeme;
                }
            }

            NotifyStageChange( EStage.EndOfRound );
        }

        /// <summary>
        /// below 0 = a is less than b                         | HIGHER fitness, SMALLER tree
        ///       0 = They are the same                        |
        /// above 0 = a is greater than b (b is less than a)   |
        /// </summary>                                  
        internal static int SortIndividuals( Individual a, Individual b )
        {
            IComparable fa = a.LatestResult.Fitness;
            IComparable fb = b.LatestResult.Fitness;

            if (fa == null)
            {
                if (fb == null)
                {
                    return 0;
                }

                return 1; 
            }
            else if (fb == null)
            {
                return -1;
            }

            int compare = fb.CompareTo( fa ); // -1 = a, 0 = neither, 1 = b

            if (compare == 0)
            {
                compare = a.LatestResult.TreeSize.CompareTo( b.LatestResult.TreeSize );
            }

            return compare;
        }

        private void CreateSuccessors()
        {
            Debug.Assert( Demes.Length != 0 );

            // PERFORM BREEDING    
            for (int demeIndex = 0; demeIndex < Demes.Length; demeIndex++)
            {
                Deme deme = Demes[demeIndex];
                               
                // Breed
                Debug.Assert( deme.Individuals.Count == Parameters.IndividualsPerDeme );
                deme.Individuals = _breeder.Breed( deme.Individuals );
                Debug.Assert( deme.Individuals.Count == Parameters.IndividualsPerDeme );

                // Check
#if DEBUG
                for (int i = 0; i < deme.Individuals.Count; i++)
                {
                    for (int j = i + 1; j < deme.Individuals.Count; j++)
                    {
                        Debug.Assert(!ReferenceEquals(deme.Individuals[i], deme.Individuals[j]), "Two references to the same individual exist in a deme after breeding. This probably indicates a problem with the breeding operator.");
                    }
                }
#endif
            } // next deme

            // PERFORM MIGRATION
            if (Parameters.MigrationOperator != null)
            {
                Parameters.MigrationOperator.Migrate( new MigrationOperatorArgs( this, Demes, Round ) );
            }
        }
    }
}
