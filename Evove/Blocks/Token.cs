﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Blocks
{
    /// <summary>
    /// Represents a DOM, reads from and writes to CSV.
    /// </summary>
    public sealed class Token
    {
        private readonly Dictionary<string, Token> Tokens = new Dictionary<string, Token>();
        private string key;
        private string value;

        public Token(string key, string value)
        {
            this.key = key;
            this.value = value;
        }

        public bool Contains(string key)
        {
            return Tokens.ContainsKey(key);
        }

        public Token(string key)
        {
            this.key = key;
        }

        public void Add(Token t)
        {
            Tokens.Add(t.key, t);
        }

        public Token this[string key]
        {
            get
            {
                return Tokens[key];
            }
        }

        public string String
        {
            get
            {
                return value;
            }
        }

        public int Integer
        {
            get
            {
                return int.Parse(value);
            }
        }

        public static Token Read(StreamReader sr)
        {
            Token root = new Token(null, null);

            Stack<Token> current = new Stack<Token>();
            current.Push(root);
            Token last = null;
            int lastCommas = 0;

            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();

                if (string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }

                int firstNoneComma = line.Length;

                for (int n = 0; n < line.Length; n++)
                {
                    if (line[n] != ',')
                    {
                        firstNoneComma = n;
                    }
                }

                if (firstNoneComma == lastCommas + 1)
                {
                    // Increment stack
                    current.Push(last);
                }
                else if (firstNoneComma < lastCommas)
                {
                    // Decrement stack
                    for (int n = firstNoneComma; n < lastCommas; n++)
                    {
                        current.Pop();
                    }
                }
                else if (firstNoneComma != lastCommas)
                {
                    throw new InvalidOperationException("Unexpected jump in file.");
                }

                line = line.Substring(firstNoneComma);

                int colon = line.IndexOf(',');

                string key = line.Substring(0, colon).ToLower().Trim();
                string value = line.Substring(colon + 1).Trim();

                last = new Token(key, value);
                lastCommas = firstNoneComma;
                current.Peek().Tokens.Add(last.key, last);
            }

            return root;
        }

        public string Write()
        {
            StringBuilder sb = new StringBuilder();
            Write(sb, 0);
            return sb.ToString();
        }

        public void Write(StringBuilder sb)
        {
            Write(sb, 0);
        }

        private void Write(StringBuilder sb, int depth)
        {
            sb.Append(new string(',', depth));
            sb.Append(key);
            sb.Append(",");
            sb.AppendLine(value);

            foreach (Token child in this.Tokens.Values)
            {
                child.Write(sb, depth + 1);
            }
        }

        public void Write(StreamWriter fs)
        {
            Write(fs, 0);
        }

        private void Write(StreamWriter fs, int depth)
        {
            fs.Write(new string(',', depth));
            fs.Write(key);
            fs.Write(": ");
            fs.WriteLine(value);

            foreach (Token child in this.Tokens.Values)
            {
                child.Write(fs, depth + 1);
            }
        }
    }
}