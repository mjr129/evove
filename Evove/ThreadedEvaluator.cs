﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove
{
    /// <summary>
    /// Evaluates individuals in a multithreaded manner.
    /// </summary>
    internal sealed class ThreadedEvaluator
    {
        Threader<bool, MyThreadContext> _threader;
        private List<Individual> _list;
        private Population _population;

        class MyThreadContext
        {
           
        }   

        /// <summary>
        /// Constructor.
        /// </summary>                       
        public ThreadedEvaluator( Population population )
        {
            _population = population;
            _threader = new Threader<bool, MyThreadContext>( population.Parameters.NumberOfEvaluatorThreads, Execute );
        }

        /// <summary>
        /// Evaluates the specified list of individuals.
        /// Blocks until all are evaluated.
        /// </summary>                     
        public void Evaluate( List<Individual> all )
        {
            _list = all;
            _threader.PartitionWorkload( all.Count );
        }

        /// <summary>
        /// Thread function.
        /// </summary>                                                 
        private bool Execute( MyThreadContext context, int start, int count )
        {
            int end = start + count;

            for (int i = start; i < end; i++)
            {
                _list[i].Evaluate( _population );
            }

            return true;

        }
    }
}
