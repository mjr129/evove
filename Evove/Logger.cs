﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove
{                            
    public class Logger
    {                         
        private static int Depth;
                              
        public static event Action<string> LogReceived;

        [Conditional("DEBUG")]
        internal static void Enter()
        {
            Depth++;
        }

        [Conditional("DEBUG")]
        internal static void Leave()
        {
            if (Depth == 0)
            {
                throw new InvalidOperationException("Cannot leave if not entered.");
            }

            Depth--;
        }

        [Conditional("DEBUG")]
        internal static void Log(string message, object suffix)
        {
            Log(message + suffix);
        }

        [Conditional("DEBUG")]
        internal static void Log(string message)
        {
            if (message.StartsWith("+"))
            {
                Enter();
            }
            else if (message.StartsWith("-"))
            {
                Leave();
            }

            if (LogReceived != null)
            {
                LogReceived(new string(' ', Depth * 4) + message);
            }
        }
    }
}
