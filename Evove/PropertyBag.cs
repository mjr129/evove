﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove
{
    /// <summary>
    /// A property bag is an extension design pattern which holds additional items ("properties").
    /// Adding a field of this type allows a user of a configuration class to add their own
    /// settings for use in their own procedures, even if the configuration class itself does not
    /// contain these settings.
    /// 
    /// This property bag is capable of addressing properties either by a unique name or through
    /// a unique type.
    /// </summary>
    public class PropertyBag : IEnumerable<object>
    {
        /// <summary>
        /// Named properties
        /// </summary>
        private Dictionary<string, object> values = new Dictionary<string, object>();

        /// <summary>
        /// Typed properties
        /// </summary>
        private Dictionary<Type, object> values2 = new Dictionary<Type, object>();

        /// <summary>
        /// CONSTRUCTOR
        /// Creates an empty property bag.
        /// </summary>
        public PropertyBag()
        {
        }

        /// <summary>
        /// CONSTRUCTOR
        /// See <see cref="AddRange(IEnumerable)"/>for details.
        /// </summary>                             
        public PropertyBag( IEnumerable items )
        {
            AddRange( items );
        }

        /// <summary>
        /// CONSTUCTOR
        /// See <see cref="AddRange(IEnumerable)"/>for details.
        /// </summary>                                         
        public PropertyBag( params object[] items )
            :this( (IEnumerable)items )
        {
            // NA
        }

        /// <summary>
        /// Adds a new property. If the property already exists an exception is thrown.
        /// </summary>         
        public void Add( string key, object value )
        {
            values.Add( key, value );
        }

        /// <summary>
        /// Adds a new property. If the property already exists an exception is thrown.
        /// </summary>         
        public void Add<T>( T value )
        {
            Add( typeof( T ), value );
        }

        /// <summary>
        /// Adds a new property. If the property already exists an exception is thrown.
        /// </summary>         
        public void Add( Type type, object value )
        {
            values2.Add( type, value );
        }

        /// <summary>
        /// Adds a new property. If the property already exists it is replaced.
        /// </summary>         
        public void Set( string key, object value )
        {
            values[key] = value;
        }

        /// <summary>
        /// Adds a new property. If the property already exists it is replaced.
        /// </summary>         
        public void Set<T>( T value )
        {
            Set( typeof( T ), value );
        }

        /// <summary>
        /// Adds a new property. If the property already exists it is replaced.
        /// </summary>         
        public void Set( Type type, object value )
        {
            values2[type] = value;
        }


        /// <summary>
        /// Clears the property bag.
        /// </summary>
        public void Clear()
        {
            values.Clear();
            values2.Clear();
        }

        /// <summary>
        /// Retrieves a property. If the property does not exist an exception is thrown.
        /// </summary>                                                                  
        public object Get( string key )
        {
            object v;

            if (!values.TryGetValue( key, out v ))
            {
                throw InvalidSelection( key );
            }

            return v;
        }                    

        /// <summary>
        /// Retrieves a property. If the property does not exist an exception is thrown.
        /// </summary>                                                                  
        public object Get( Type key )
        {
            object v;

            if (!values2.TryGetValue( key, out v ))
            {
                throw InvalidSelection( key );
            }

            return v;
        }

        /// <summary>
        /// Retrieves a property. If the property does not exist an exception is thrown.
        /// </summary>                                                                  
        public T Get<T>()
        {
            object v;

            if (!values2.TryGetValue( typeof( T ), out v ))
            {
                throw InvalidSelection( typeof( T ) );
            }

            return (T)v;
        }

        /// <summary>
        /// Retrieves a property. If the property does not exist null is returned.
        /// </summary>      
        public object GetOrDefault( string key )
        {
            object v;

            if (!values.TryGetValue( key, out v ))
            {
                return null;
            }

            return v;
        }

        /// <summary>
        /// Retrieves a property. If the property does not exist null is returned.
        /// </summary>      
        public object GetOrDefault( Type key )
        {
            object v;

            if (!values2.TryGetValue( key, out v ))
            {
                return null;
            }

            return v;
        }

        /// <summary>
        /// Retrieves a property. If the property does not exist then <code>default(T)</code> is returned.
        /// </summary>      
        public T GetOrDefault<T>()
        {
            object v;

            if (!values2.TryGetValue( typeof( T ), out v ))
            {
                return default( T );
            }

            return (T)v;
        }

        /// <summary>
        /// Returns if the property bag contains the specified property.
        /// </summary>      
        public bool Has( string key )
        {
            return values.ContainsKey( key );
        }

        /// <summary>
        /// Returns if the property bag contains the specified property.
        /// </summary>      
        public bool Has( Type key )
        {
            return values2.ContainsKey( key );
        }

        /// <summary>
        /// Returns if the property bag contains the specified property.
        /// </summary>      
        public bool Has<T>(  )
        {
            return Has( typeof( T ) );
        }   

        /// <summary>
        /// Gets or sets the specified property.
        /// This accessor wraps to <see cref="Get"/> and <see cref="Set"/>.
        /// </summary>                          
        public object this[string key]
        {
            get
            {
                return Get( key );
            }
            set
            {
                values[key] = value;
            }
        }

        /// <summary>
        /// Gets or sets the specified property.         
        /// This accessor wraps to <see cref="Get"/> and <see cref="Set"/>.
        /// </summary>                          
        public object this[Type key]
        {
            get
            {
                return Get( key );
            }
            set
            {
                values2[key] = value;
            }
        }

        /// <summary>
        /// PRIVATE: Creates an descriptive key-not-found exception.
        /// </summary>                               
        private Exception InvalidSelection( string key )
        {
            return new KeyNotFoundException( "A property named \"" + key + "\" is expected but has not been provided to the property bag." );
        }

        /// <summary>
        /// PRIVATE: Creates an descriptive key-not-found exception.
        /// </summary>                                                            
        private Exception InvalidSelection( Type key )
        {
            return new KeyNotFoundException( "A property of type \"" + key.FullName + "\" is expected but has not been provided to the property bag." );
        }

        /// <summary>
        /// EXPLICIT-ONLY: Implements <see cref="IEnumerable"/>.
        /// </summary>             
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<object>)this).GetEnumerator();
            
        }

        /// <summary>
        /// Adds a range of items to the bag (by type).
        /// </summary>                  
        public void AddRange( IEnumerable items )
        {
            foreach (object item in items)
            {  
                Add( item.GetType(), item );
            }
        }

        /// <summary>
        /// EXPLICIT-ONLY: Implements <see cref="IEnumerable"/>.
        /// </summary>      
        IEnumerator<object> IEnumerable<object>.GetEnumerator()
        {
            foreach (var x in this.values.Values)
            {
                yield return x;
            }

            foreach (var x in this.values2.Values)
            {
                yield return x;
            }
        }

        /// <summary>
        /// Implicit conversion from array, allows the user to set a property bag
        /// using array construction syntax.
        /// </summary>                     
        public static implicit operator PropertyBag( object[] array )
        {
            return new PropertyBag( array );
        }
    }
}
