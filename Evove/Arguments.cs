﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove
{
    /// <summary>
    /// When the objects containing the GP functions are instantiated they may
    /// take InstantiationArgs as the first parameter of their constructor.
    /// (any custom parameters may follow).
    /// 
    /// GP function objects are pooled and can be reused between evaluations.
    /// Different demes do not share the same object pools.
    /// </summary>
    public sealed class InstantiationArgs
    {
        public readonly Population Population;
        public readonly Deme Deme;

        public InstantiationArgs( Population population, Deme deme )
        {
            Population = population;
            Deme = deme;
        }
    }

    /// <summary>
    /// Fields on the GP-function objects marked EvoveStatic(Per = Population)
    /// may take PopulationArgs as the first parameter of their constructor
    /// (any custom parameters may follow).
    /// </summary>
    public sealed class PopulationArgs
    {
        /// <summary>
        /// Gets the current population.
        /// </summary>
        public readonly Population Population;

        public PopulationArgs( Population population )
        {
            Population = population;
        }

        public int StartingRound { get; set; }
    }

    /// <summary>
    /// Fields on the GP-function objects marked EvoveStatic(Per = Deme)
    /// may take DemeArgs as the first parameter of their constructor                                    n
    /// (any custom parameters may follow).
    /// </summary>
    public sealed class DemeArgs
    {
        public readonly Population Population;
        public readonly Deme Deme;

        public DemeArgs( Population population, Deme deme )
        {
            Population = population;
            Deme = deme;
        }
    }

    [Flags]
    public enum ESpecialEvents
    {
        /// <summary>
        /// No special events
        /// </summary>
        None = 0x0,

        /// <summary>
        /// The current population should be eradicated and replaced with a new, random population.
        /// </summary>
        Extinction = 0x1,

        /// <summary>
        /// The simulation should stop, as if the stopping conditions were reached.
        /// </summary>
        Stop = 0x2,

        /// <summary>
        /// The simulation should discard the current state and restart. Everything is
        /// reinitialised.
        /// </summary>
        Restart = 0x4,
    }
                  
    public sealed class SimulationCompleteEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the current population.
        /// </summary>
        public readonly Population Population;       

        /// <summary>
        /// CONSTRUCTOR
        /// </summary> 
        public SimulationCompleteEventArgs(Population population)
        {
            Population = population;
        }
    }

    public sealed class RoundStartEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the current population.
        /// </summary>
        public readonly Population Population;     

        /// <summary>
        /// CONSTRUCTOR
        /// </summary> 
        public RoundStartEventArgs(Population population)
        {
            Population = population;   
        }
    }

    public sealed class RoundEndEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the current population.
        /// </summary>
        public readonly Population Population;    

        /// <summary>
        /// Gets or sets the value indicating if any special events should take place prior to
        /// population evaluation. The default value is ESpecialEvents.None.
        /// </summary>
        public ESpecialEvents SpecialEvents;

        /// <summary>
        /// CONSTRUCTOR
        /// </summary> 
        public RoundEndEventArgs(Population population)
        {
            Population = population;
        }
    }             
}
