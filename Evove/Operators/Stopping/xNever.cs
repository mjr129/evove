﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Stopping
{
    public static partial class StoppingConditions
    {
        /// <summary>
        /// Never stops!
        /// 
        /// (Here because a stopping condition is mandatory to avoid mistakes)
        /// </summary>
        public sealed class Never : IStoppingCondition
        {
            public override string ToString()
            {
                return "Never stop";
            }

            /// <summary>
            /// IMPLEMENTS IStoppingCondition
            /// (Hidden -- constant)
            /// </summary>                          
            bool IStoppingCondition.CheckStop( StoppingConditionArgs args)
            {
                return false;
            }

            /// <summary>
            /// IMPLEMENTS IStoppingCondition
            /// (Hidden -- no functionality)
            /// </summary>         
            void IStoppingCondition.Reset()
            {
                // N/A (meaningless)
            }
        }
    }
}