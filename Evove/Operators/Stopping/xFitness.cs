﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Stopping
{
    public static partial class StoppingConditions
    {
        /// <summary>
        /// Stops when the specified fitness is reached (>=)
        /// </summary>
        public sealed class Fitness : IStoppingCondition
        {
            private readonly IComparable _fitness;

            public override string ToString()
            {
                return $"Fitness (fitness = {_fitness})";
            }

            /// <summary>
            /// CONSTRUCTOR
            /// </summary>
            /// <param name="fitness">Fitness value to stop at (inclusive)</param>
            public Fitness(IComparable fitness)
            {
                EvoveHelper.CheckNotNull(nameof(fitness), fitness);

                _fitness = fitness;
            }

            /// <summary>
            /// IMPLEMENTS IStoppingCondition
            /// </summary>                   
            public bool CheckStop(StoppingConditionArgs args)
            {   
                return args.Population.BestIndividual.LatestResult.Fitness.CompareTo(_fitness) >= 0;
            }

            /// <summary>
            /// IMPLEMENTS IStoppingCondition
            /// (Hidden -- no functionality)
            /// </summary>         
            void IStoppingCondition.Reset()
            {
                // NA (meaningless)
            }
        }
    }
}