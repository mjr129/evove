﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Stopping
{
    public static partial class StoppingConditions
    {
        /// <summary>
        /// Stops after a certain number of rounds.
        /// </summary>
        public sealed class Rounds : IStoppingCondition
        {
            private readonly int _numberOfRounds;
            private int _round;

            public override string ToString()
            {
                return $"Stop after rounds (count = {_numberOfRounds})";
            }

            /// <summary>
            /// CONSTRUCTOR
            /// </summary>
            /// <param name="numberOfRounds">Number of rounds to stop at (inclusive, so 1 means stop after 1 iteration)</param>
            public Rounds(int numberOfRounds)
            {   
                EvoveHelper.Check(nameof(numberOfRounds), numberOfRounds, 1, int.MaxValue);

                _numberOfRounds = numberOfRounds;
            }

            /// <summary>
            /// IMPLEMENTS IStoppingCondition
            /// </summary>                   
            public bool CheckStop(StoppingConditionArgs args)
            {
                _round++;

                args.Population.SimpleLog( "(StoppingCondition " + args.Population.Round + ") " + (_numberOfRounds - _round) + " rounds remaining");

                return _round >= _numberOfRounds;
            }

            /// <summary>
            /// IMPLEMENTS IStoppingCondition
            /// </summary>         
            public void Reset()
            {
                _round = 0;
            }
        }
    }
}
