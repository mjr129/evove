﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.Stopping
{
    /// <summary>
    /// Arguments passed to IStoppingCondition.CheckStop
    /// 
    /// (Passing these arguments as a single object allows users to add information to the arguments
    /// without having to change all of the classes that use them.)
    /// </summary>
    public sealed class StoppingConditionArgs
    {
        /// <summary>
        /// Current population
        /// </summary>
        public readonly Population Population;

        /// <summary>
        /// CONSTRUCTOR
        /// </summary> 
        public StoppingConditionArgs(Population population)
        {
            Population = population;
        }
    }
}
