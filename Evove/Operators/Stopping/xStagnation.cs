﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Stopping
{
    public static partial class StoppingConditions
    {
        /// <summary>
        /// Stops after the fitness value does not change for a certain number of rounds.
        /// </summary>
        public sealed class Stagnation : IStoppingCondition
        {
            private readonly int _numberOfRounds;
            private IComparable _fitness;
            private int _stagnated;

            public override string ToString()
            {
                return $"Stop after stagnation (count = {_numberOfRounds})";
            }

            /// <summary>
            /// CONSTRUCTOR
            /// </summary>
            /// <param name="numberOfRounds">Number of rounds fitness function should go unchanged before stopping.
            /// Inclusive so 1 means stop after it remains the same once.</param>
            public Stagnation(int numberOfRounds)
            {
                EvoveHelper.Check(nameof(numberOfRounds), numberOfRounds, 1, int.MaxValue);

                _numberOfRounds = numberOfRounds;
            }

            /// <summary>
            /// IMPLEMENTS IStoppingCondition
            /// </summary>                   
            public bool CheckStop(StoppingConditionArgs args)
            {
                var bestFitness = args.Population.BestIndividual.LatestResult.Fitness;

                if (_fitness == null || bestFitness.CompareTo(_fitness) > 0)
                {
                    _fitness = bestFitness;
                    _stagnated = 0;
                }
                else
                {
                    _stagnated++;
                }

                args.Population.SimpleLog( "(StoppingCondition " + args.Population.Round + ") " + (_numberOfRounds - _stagnated) + " stagnations remaining" );

                return _stagnated >= _numberOfRounds;
            }

            /// <summary>
            /// IMPLEMENTS IStoppingCondition
            /// </summary>         
            public void Reset()
            {
                _fitness = null;
                _stagnated = 0;
            }
        }
    }
}