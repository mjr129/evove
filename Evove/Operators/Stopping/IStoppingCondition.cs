﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Stopping
{
    /// <summary>
    /// Designates classes capable of stopping the simulation.
    /// 
    /// See StoppingConditions.* for a selection of inbuilt concrete implementations.
    /// </summary>
    public interface IStoppingCondition
    {
        /// <summary>
        /// Checks if is time to stop the simulation.
        /// </summary>
        /// <param name="args">Pertinent information</param>     
        /// <returns>true to stop the simulation, false to continue it</returns>
        bool CheckStop(StoppingConditionArgs args);

        /// <summary>
        /// Resets the state of the stopping condition object.
        /// 
        /// This only applies to state-based stopping conditions (max rounds, stagnation, run time, etc.)
        /// Stateless stopping conditions (max fitness) will be unaffected.
        /// 
        /// This is ALWAYS called when the simulation is started, allowing time-based stopping conditions
        /// to start the timer when the simulation is started, rather than when the program is waiting
        /// for the user to click OK.
        /// </summary>
        void Reset();
    }
}
