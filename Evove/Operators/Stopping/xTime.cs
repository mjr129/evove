﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Stopping
{
    public static partial class StoppingConditions
    {
        /// <summary>
        /// Stops after a period of time.
        /// 
        /// Note that the timer starts on construction of the object, so if you pause or debug the timer is still
        /// running.
        /// </summary>
        public sealed class Time : IStoppingCondition
        {
            private readonly int _totalMilliseconds;
            private readonly Stopwatch _stopWatch;

            public override string ToString()
            {
                return $"Stop after time (milliseconds = {_totalMilliseconds})";
            }

            /// <summary>
            /// CONSTRUCTOR
            /// </summary>
            /// <param name="seconds">Number of seconds to stop after. Note that stopping conditions are only checked at the end of a round.</param>
            public Time( int seconds )
            {
                EvoveHelper.Check( nameof( seconds ), seconds, 1, int.MaxValue );

                _stopWatch = Stopwatch.StartNew();
                _totalMilliseconds = seconds * 1000;
            }

            public Time( TimeSpan timeSpan )
            {
                EvoveHelper.Check( nameof( timeSpan ), timeSpan.TotalMilliseconds, 1, double.MaxValue );

                _stopWatch = Stopwatch.StartNew();
                _totalMilliseconds = (int)timeSpan.TotalMilliseconds;
            }

            /// <summary>
            /// IMPLEMENTS IStoppingCondition
            /// </summary>                   
            public bool CheckStop( StoppingConditionArgs args )
            {
                args.Population.SimpleLog( "(StoppingCondition " + args.Population.Round + ") " + (_totalMilliseconds - _stopWatch.ElapsedMilliseconds) + " milliseconds remaining" );

                return _stopWatch.ElapsedMilliseconds > _totalMilliseconds;
            }

            public void Reset()
            {
                _stopWatch.Restart();
            }
        }
    }
}