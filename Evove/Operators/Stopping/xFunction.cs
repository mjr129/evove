﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Stopping
{
    public static partial class StoppingConditions
    {
        /// <summary>
        /// Stops according to a function or delegate
        /// </summary>
        public sealed class Function : IStoppingCondition
        {
            private readonly Func<StoppingConditionArgs, bool> _function;

            public override string ToString()
            {
                return $"Stop when function (function = {_function})";
            }

            /// <summary>
            /// CONSTRUCTOR
            /// </summary>
            /// <param name="func">Function to ask</param>
            public Function(Func<StoppingConditionArgs, bool> func)
            {
                EvoveHelper.CheckNotNull(nameof(func), func);

                _function = func;
            }

            /// <summary>
            /// IMPLEMENTS IStoppingCondition
            /// </summary>                   
            public bool CheckStop(StoppingConditionArgs args)
            {
                return _function(args);
            }

            /// <summary>
            /// IMPLEMENTS IStoppingCondition
            /// (Hidden - no functionality)
            /// </summary>         
            void IStoppingCondition.Reset()
            {
                // N/A (meaningless)
            }
        }
    }
}