﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.SelectionMethods
{
    public static partial class SelectionMethods
    {
        public sealed class FitnessProportional : ISelectionMethod
        {
            public override string ToString()
            {
                return "Fitness proportional selection";
            }

            /// <summary>
            /// IMPLEMENTS ISelectionMethod
            /// </summary>      
            public Individual Select(SelectionMethodArgs args)
            {
                // Calculate the total weight
                double totalFitness = args.Individuals.Sum(z => Convert.ToDouble(z.LatestResult.Fitness));

                // Get the position
                double value = args.Population.Randomiser.NextDouble() * totalFitness;

                // Find the position
                foreach (Individual individual in args.Individuals)
                {
                    value -= Convert.ToDouble(individual.LatestResult.Fitness);

                    if (value <= 0)
                    {
                        return individual;
                    }
                }

                // Rounding error
                return args.Individuals[args.Individuals.Count - 1];
            }
        }
    }
}