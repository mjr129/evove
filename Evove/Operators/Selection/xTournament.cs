﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.SelectionMethods
{
    public static partial class SelectionMethods
    {
        /// <summary>
        /// Performs tournament selection.
        /// </summary>
        public sealed class Tournament : ISelectionMethod
        {
            private readonly int _tournamentSize;

            public override string ToString()
            {
                return $"Tournament selection (tournament size = {_tournamentSize})";
            }

            /// <summary>
            /// CONSTRUCTOR
            /// </summary>
            /// <param name="tournamentSize">Size of tournaments</param>
            public Tournament(int tournamentSize)
            {
                EvoveHelper.Check(nameof(tournamentSize), tournamentSize, 1, int.MaxValue);

                _tournamentSize = tournamentSize;
            }

            /// <summary>
            /// IMPLEMENTS ISelectionMethod
            /// </summary>      
            public Individual Select(SelectionMethodArgs args)
            {
                var rng = args.Population.Randomiser;
                rng.Shuffle(args.Individuals);

                List<Individual> tournament = new List<Individual>();

                while (tournament.Count < _tournamentSize)
                {
                    tournament.Add(args.Individuals[tournament.Count]);
                }

                tournament.Sort(Population.SortIndividuals);

                foreach (Individual individual in tournament)
                {
                    if (rng.Chance(0.7))
                    {
                        return individual;
                    }
                }

                return tournament[0];
            }
        }
    }
}
