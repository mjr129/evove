﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.SelectionMethods
{
    /// <summary>
    /// Designates classes capable of selecting individuals for breeding.
    /// 
    /// See SelectionMethods.* for inbuilt concrete implementations.
    /// </summary>
    public interface ISelectionMethod
    {
        /// <summary>
        /// Selects an individual.
        /// </summary>
        /// <param name="args">Arguments containing the list of candidate individuals.</param>
        /// <returns>The selected individual</returns>
        Individual Select(SelectionMethodArgs args);
    }
}
