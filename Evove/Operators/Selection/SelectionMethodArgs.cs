﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.SelectionMethods
{
    /// <summary>
    /// Arguments passed to ISelectionMethod.Select
    /// 
    /// (Passing these arguments as a single object allows users to add information to the arguments
    /// without having to change all of the classes that use them.)
    /// </summary>
    public sealed class SelectionMethodArgs
    {
        /// <summary>
        /// List of candidates. The list can be reordered but items should not be inserted or removed.
        /// </summary>
        public readonly List<Individual> Individuals;

        public readonly Population Population;

        /// <summary>
        /// CONSTRUCTOR
        /// </summary> 
        public SelectionMethodArgs(Population population, List<Individual> individuals)
        {
            this.Population = population;
            this.Individuals = individuals;
        }
    }
}
