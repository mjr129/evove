﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.Migration
{
    /// <summary>
    /// Arguments passed to IMigrationOperator.Migrate
    /// </summary>
    public sealed class MigrationOperatorArgs
    {
        /// <summary>
        /// List of demes, the Deme.Individuals lists should be modified for migration.
        /// </summary>
        public Deme[] Demes;

        /// <summary>
        /// Current round
        /// </summary>
        public readonly int Round;
        public readonly Population Population;



        /// <summary>
        /// CONSTRUCTOR
        /// </summary> 
        public MigrationOperatorArgs(Population population, Deme[] demes, int round)
        {
            this.Population = population;
            Demes = demes;
            Round = round;
        }
    }
}
