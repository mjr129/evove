﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Migration
{
    /// <summary>
    /// Designates classes capable of handling migration.
    /// 
    /// See MigrationOperators.* for a selection of inbuilt concrete implementations.
    /// </summary>
    public interface IMigrationOperator
    {
        /// <summary>
        /// Performs migration
        /// </summary>
        /// <param name="args">Pertinent information</param>
        void Migrate(MigrationOperatorArgs args);
    }
}
