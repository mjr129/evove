﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.Migration
{
    public static partial class MigrationOperators
    {
        /// <summary>
        /// Migrates individuals in a circular fashion, from deme n to deme n + 1
        /// </summary>
        public sealed class Circular : IMigrationOperator
        {
            private readonly int _number;
            private readonly int _period;

            public override string ToString()
            {
                return $"Circular migration (number = {_number}, period = {_period})";
            }

            /// <summary>
            /// CONSTRUCTOR
            /// </summary>
            /// <param name="period">How often (in rounds) migration occurs</param>
            /// <param name="number">How many individuals from each deme migrate at a time</param>
            public Circular(int period, int number)
            {
                _period = period;
                _number = number;
            }

            /// <summary>
            /// IMPLEMENTS IMigrationOperator
            /// </summary>                   
            public void Migrate(MigrationOperatorArgs args)
            {
                if ((args.Round % _period) != 0)
                {
                    return;
                }

                var rng = args.Population.Randomiser;

                for (int n = 0; n < args.Demes.Length; n++)
                {
                    Deme thisDeme = args.Demes[n];
                    Deme nextDeme = (n == args.Demes.Length - 1) ? args.Demes[0] : args.Demes[n + 1];

                    Individual random = rng.FromList(args.Demes[n].Individuals);

                    thisDeme.Individuals.Remove(random);
                    nextDeme.Individuals.Add(random);
                }
            }
        }
    }
}
