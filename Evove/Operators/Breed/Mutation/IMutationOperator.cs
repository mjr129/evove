﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Breed.Mutation
{
    /// <summary>
    /// Designates classes capable of mutating individuals (asexual reproduction).
    /// 
    /// This is for use with BreedOperators.Basic, which implements both mutation and crossover.
    /// See MutationOperators.* for a selection of inbuilt concrete implementations.
    /// </summary>
    public interface IMutationOperator
    {
        /// <summary>
        /// Mutates an individual.
        /// The individual is mutated directly (i.e. it should not be copied).
        /// </summary>
        /// <param name="args">Arguments containing the individual</param>
        void Mutate(MutateArgs args);
    }
}
