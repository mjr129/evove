﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.Breed.Mutation
{
    public static partial class MutationOperators
    {
        /// <summary>
        /// Basic mutation operator.
        /// 
        /// There is a [chanceOfNodeMutation] for each node that it will be mutated.
        /// [chanceOfInsertion], [chanceOfReordering] and [chanceOfReplacement] determine the type
        /// of mutation performed. These probabilities should ideally add up to 1.
        /// </summary>
        public sealed class PerNode : IMutationOperator
        {
            private readonly double _chanceOfInsertion;
            private readonly double _chanceOfReordering;
            private readonly double _chanceOfReplacement;
            private readonly double _chanceOfNodeMutation;

            public override string ToString()
            {
                return $"Per-node mutation (chance = {EvoveHelper.Percent( _chanceOfNodeMutation )}, insertion = {EvoveHelper.Percent( _chanceOfInsertion )}, reordering = {EvoveHelper.Percent( _chanceOfReordering- _chanceOfInsertion )}, replacement = {EvoveHelper.Percent( _chanceOfReplacement- _chanceOfReordering )})";
            }

            /// <summary>
            /// CONSTRUCTOR
            /// </summary>
            /// <param name="chanceOfNodeMutation">Chance that an individual node is mutated</param>
            /// <param name="chanceOfInsertion">Chance that the node is mutated via insertion</param>
            /// <param name="chanceOfReordering">Chance that the node is mutated via reordering within its parent</param>
            /// <param name="chanceOfReplacement">Chance that the node is mutated via replacement</param>
            public PerNode( double chanceOfNodeMutation, double chanceOfInsertion, double chanceOfReordering, double chanceOfReplacement )
            {
                EvoveHelper.Check( nameof( chanceOfNodeMutation ), chanceOfNodeMutation, 0.0d, 1.0d );
                EvoveHelper.Check( nameof( chanceOfInsertion ) + " + " + nameof( chanceOfReordering ) + " + " + nameof( chanceOfReplacement ), chanceOfInsertion + chanceOfReordering + chanceOfReplacement, 0.0d, 1.0d );

                _chanceOfNodeMutation = chanceOfNodeMutation;
                _chanceOfInsertion = chanceOfInsertion;
                _chanceOfReordering = chanceOfReordering + _chanceOfInsertion;
                _chanceOfReplacement = chanceOfReplacement + _chanceOfReordering;
            }

            /// <summary>
            /// IMPLEMENTS IMutationOperator
            /// </summary>                   
            public void Mutate( MutateArgs args )
            {
                var rnd = args.Population.Randomiser;
                args.Individual.History.Add( EHistoryEvent.Iterative_mutatation, 0, 0 );

                foreach (Node node in args.Individual.Genome.IterateAllNodes( false ).ToList()) // get copy of list so we don't mutate new nodes
                {
                    if (!node.IsDestroyed) // skip nodes dropped as an artifact of mutating earlier nodes
                    {
                        if (rnd.Chance( _chanceOfNodeMutation ))
                        {
                            MutateNode( args, node );
                        }
                    }
                }
            }

            void MutateNode( MutateArgs args, Node node )
            {
                double d = args.Population.Randomiser.NextDouble();
                Individual i = args.Individual;

                if (d < _chanceOfInsertion)
                {
                    if (MutateViaInsertion( args, node ))
                    {
                        i.History.Add( EHistoryEvent.Point_insertion_before_node, -node.TrackingId, 0 );
                    }
                }
                else if (d < _chanceOfReordering)
                {
                    if (MutateViaReordering( args, node ))
                    {
                        i.History.Add( EHistoryEvent.Point_reordering_including_node, -node.TrackingId, 0 );
                    }
                }
                else if (d < _chanceOfReplacement)
                {
                    if (MutateViaReplacement( args, node ))
                    {
                        i.History.Add( EHistoryEvent.Point_replacement_of_node, -node.TrackingId, 0 );
                    }
                }

                Debug.Assert( node.CountDepth() <= args.MaxTreeDepth, "Maximum tree depth violation" );
            }

            /// <summary>
            /// Reorders the two of the children of this.Parent, always including this
            /// </summary>                           
            public static bool MutateViaReordering( MutateArgs args, Node node )
            {
                // Get a random swap
                List<Node> possibilities = new List<Node>( node.Parent.Children.Where( z => (z != node) && (z.Function.ReturnType.Type == node.Function.ReturnType.Type) ) );

                if (possibilities.Count == 0)
                {
                    return false;
                }

                var swapWith = args.Population.Randomiser.FromList( possibilities );
                Debug.Assert( swapWith.Function.ReturnType.Type == node.Function.ReturnType.Type );

                // Get the parent
                var parent = node.Parent;
                Debug.Assert( parent.Function != null );
                Debug.Assert( parent == swapWith.Parent );
                Debug.Assert( swapWith != node );

                // Swap the children
                parent.Children[node.Index] = swapWith;
                parent.Children[swapWith.Index] = node;

                // Swap the indices
                int i = node.Index;
                node.Index = swapWith.Index;
                swapWith.Index = i;
                parent.CacheMarkAsDirty( "Reorder @ " + node.TrackingId );

                for (int j = 0; j < parent.Children.Length; j++)
                {
                    for (int k = j + 1; k < parent.Children.Length; k++)
                    {
                        Debug.Assert( parent.Children[j] != parent.Children[k] );
                    }
                }

                Debug.Assert( parent.Children[node.Index] == node );
                Debug.Assert( parent.Children[swapWith.Index] == swapWith );
                Debug.Assert( parent.Children.Length == parent.Function.ParameterTypes.Length );

                return true;
            }

            /// <summary>
            /// Inserts a new node between THIS and THIS.PARENT.
            /// </summary>                                      
            public static bool MutateViaInsertion( MutateArgs args, Node node )
            {
                // The new value must OUTPUT this.Element.ReturnType
                // but is must also ACCEPT this.Element.ReturnType (i.e this)

                if (node.CountDepth() == args.MaxTreeDepth)
                {
                    // Not enough space in branch
                    return false;
                }

                // Get the list of potential replacements --                                      
                IReadOnlyList<GpFunction> options = node.Function.ReturnType.InputOutputFunctions;

                if (options.Count == 0)
                {
                    // -- Nothing accepts my type and returns my type
                    return false;
                }

                // Select a random replacement
                GpFunction newValue = args.Population.Randomiser.FromList( options );
                Debug.Assert( newValue.ReturnType == node.Function.ReturnType );
                Debug.Assert( newValue.ParameterTypes.Select( z => z.Type ).Contains( node.Function.ReturnType ) );

                // Unlink from parent
                Node parent = node.Parent;
                Debug.Assert( parent.Children[node.Index] == node );

                // Add the replacement node at the parent
                Node[] children = new Node[newValue.ParameterTypes.Length];

                Node newNode = new Node( parent, node.Index, newValue, children, Node.GetNextDepth( parent ) );

                parent.CacheMarkAsDirty( "Insert @ " + node.TrackingId );
                parent.Children[node.Index] = newNode;

                // Depth of us and all children is now increased by 1
                List<Node> nodes = new List<Node>();
                node.GetAllNodes( nodes );
                nodes.ForEach( z => ++z.Depth );

                // Set the parent's children --
                // -- One of them will be this node
                // -- We also need to create any other children that the parent needs
                bool thisUsed = false;

                for (int n = 0; n < newValue.ParameterTypes.Length; n++)
                {
                    ParameterType reqSet = newValue.ParameterTypes[n];

                    if (reqSet.Type == node.Function.ReturnType && !thisUsed)
                    {
                        thisUsed = true;
                        node.Index = n;
                        newNode.Children[n] = node;
                        node.Parent = newNode;

                        Debug.Assert( node.Parent.Children[node.Index] == node );
                    }
                    else
                    {
                        var c = Node.CreateRandom(args.Population.Randomiser, newNode, n, reqSet.Type, args.MaxTreeDepth );
                        newNode.Children[n] = c;

                        Debug.Assert( c.Parent.Children[c.Index] == c );
                    }
                }

                Debug.Assert( parent.Children.Length == parent.Function.ParameterTypes.Length );

                Debug.Assert( newNode.Parent.Children[newNode.Index] == newNode );
                Debug.Assert( node.Parent.Children[node.Index] == node );

                Debug.Assert( newNode.Children.Length == newNode.Function.ParameterTypes.Length );
                Debug.Assert( node.Children.Length == node.Function.ParameterTypes.Length );

                Debug.Assert( thisUsed );

                return true;
            }

            public static bool MutateViaReplacement( MutateArgs args, Node node )
            {
                // Get the list of compatible functions
                GpFunction newElement;

                if (node.Depth == args.MaxTreeDepth)
                {
                    newElement = args.Population.Randomiser.FromList( node.Function.ReturnType.SourceFunctions );
                }
                else
                {
                    newElement = args.Population.Randomiser.FromList( node.Function.ReturnType.OutputFunctions );
                }

                // Cancel now if nothing has changed
                if (newElement == node.Function && !newElement.IsConstant)
                {
                    return false;
                }

                // Change this node's function
                node.Function = newElement;
                node.ConstantValue = null;

                // We have a new function but now the children may be invalid
                // -- Create a new child array, use existing children where possible
                Node[] newChildren = new Node[node.Function.ParameterTypes.Length];
                Node[] originalChildren = node.Children;

                for (int paramIndex=0; paramIndex < node.Function.ParameterTypes.Length; paramIndex++)
                {
                    var required = node.Function.ParameterTypes[paramIndex];

                    // Get a valid child
                    Node validChild = originalChildren.FirstOrDefault( z => z != null && z.Function.ReturnType == required.Type );

                    if (validChild != null)
                    {
                        // A valid child - use it...
                        originalChildren[validChild.Index] = null; // Protection to make sure we only use the child once!
                        validChild.Index = paramIndex;
                        newChildren[paramIndex] = validChild;
                    }
                    else
                    {
                        // No valid child - create a random one
                        newChildren[paramIndex] = Node.CreateRandom(args.Population.Randomiser, node, paramIndex, required.Type, args.MaxTreeDepth );
                    }                
                }

                // Destroy all the unused child nodes
                foreach (var node2 in originalChildren)
                {
                    if (node2 != null)
                    {
                        node2.Destroy();
                    }
                }

                // Set the new children
                node.Children = newChildren;
                node.CacheMarkAsDirty( "Alter @ " + node.TrackingId );

                Debug.Assert( node.Function != null );
                Debug.Assert( node.Children.Length == node.Function.ParameterTypes.Length );

                foreach (Node n in node.Children)
                {
                    Debug.Assert( !n.IsDestroyed, "A child node of an extanct node is marked as IsDestroyed as a result of the MutateViaReplacement method call." + EvoveHelper.INTERNAL_ERROR );
                }

                return true;
            }
        }
    }
}
