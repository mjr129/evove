﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.Breed.Mutation
{
    /// <summary>
    /// Arguments passed to IMutationOperator.Mutate
    /// </summary>
    public sealed class MutateArgs
    {
        /// <summary>
        /// The individual to mutate (this should be modified, not copied)
        /// </summary>
        public readonly Individual Individual;

        /// <summary>
        /// Maximum size of the resultant tree
        /// </summary>
        public readonly int MaxTreeDepth;
        public readonly Population Population;

        public MutateArgs(Population population, Individual individual, int maxTreeDepth)
        {
            this.Population = population;
            this.Individual = individual;
            this.MaxTreeDepth = maxTreeDepth;
        }
    }
}
