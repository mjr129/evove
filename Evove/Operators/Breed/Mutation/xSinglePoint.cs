﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.Breed.Mutation
{
    public static partial class MutationOperators
    {
        /// <summary>
        /// Replaces a single node with a new node, using [leafChance] that a leaf node is selected.
        /// </summary>
        public sealed class SinglePoint : IMutationOperator
        {
            private readonly double _leafChance;

            public override string ToString()
            {
                return $"Single point mutation (leaf = {EvoveHelper.Percent( _leafChance)})";
            }

            /// <summary>
            /// CONSTRUCTOR
            /// </summary>
            /// <param name="chanceOfSelectingLeaf">Chance of selecting a leaf node for mutation, as opposed to a branch node</param>
            public SinglePoint(double chanceOfSelectingLeaf)
            {
                EvoveHelper.Check(nameof(chanceOfSelectingLeaf), chanceOfSelectingLeaf, 0.0d, 1.0d);

                _leafChance = chanceOfSelectingLeaf;
            }

            /// <summary>
            /// IMPLEMENTS IMutationOperator
            /// </summary>      
            public void Mutate(MutateArgs args)
            {
                args.Individual.History.Add(EHistoryEvent.Single_mutation, 0, 0);

                bool selectLeaf = args.Population.Randomiser.Chance(_leafChance);

                List<Node> all = args.Individual.Genome.IterateAllNodes(false).ToList(); // TODO: slow?, hacky

                Node sel = null;

                while (all.Count != 0)
                {
                    int index = args.Population.Randomiser.Next(all.Count);

                    Node node = all[index];

                    if (node.Children.Length == 0 && selectLeaf) // TODO: I presume this is better than just filtering the list initially
                    {
                        sel = node;
                        break;
                    }
                    else if (node.Children.Length != 0 && !selectLeaf)
                    {
                        sel = node;
                        break;
                    }

                    all.RemoveAt(index);
                }

                if (sel == null)
                {
                    return; // TODO: Return success?
                }

                Debug.Assert(sel.Parent.Children[sel.Index] == sel);
                Node newNode = Node.CreateRandom(args.Population.Randomiser, sel.Parent, sel.Index, sel.Function.ReturnType, args.MaxTreeDepth);
                sel.Parent.Children[sel.Index] = newNode;

                sel.Parent.CacheMarkAsDirty("Mutate @ " + sel.TrackingId);

                args.Individual.History.Add(selectLeaf ? EHistoryEvent.Single_mutation_of_leaf_node : EHistoryEvent.Single_mutation_of_branch_node, -sel.TrackingId, 0);
            }
        }
    }
}
