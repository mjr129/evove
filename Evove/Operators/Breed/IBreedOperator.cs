﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.Breed
{
    /// <summary>
    /// Designates classes capable of breeding new individuals.
    /// 
    /// See BreedOperators.* for inbuilt concrete implementations.
    /// </summary>
    public interface IBreedOperator
    {
        /// <summary>
        /// Creates new individuals.
        /// Zero individuals can be returned, but at some point a result is expected or the program
        /// will freeze.
        /// </summary>
        /// <param name="args">Arguments. Use args.Select() to select individuals for breeding.</param>
        /// <returns>An IEnumerable containing 0 or more individuals OR null</returns>
        IEnumerable<Individual> Breed(BreedArgs args);
    }
}
