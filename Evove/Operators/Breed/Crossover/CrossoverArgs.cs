﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.Breed.Crossover
{
    /// <summary>
    /// Arguments passed to ICrossoverOperator.Crossover
    /// 
    /// (Passing these arguments as a single object allows users to add information to the arguments
    /// without having to change all of the classes that use them.)
    /// </summary>
    public sealed class CrossoverArgs
    {
        /// <summary>
        /// Maximum size of resultant trees
        /// </summary>
        public readonly int MaxTreeDepth;

        /// <summary>
        /// First individual (this should be modified, not copied)
        /// </summary>
        public readonly Individual Mother;

        /// <summary>
        /// Second individual (this should be modified, not copied)
        /// </summary>
        public readonly Individual Father;
        public readonly Population Population;

        /// <summary>
        /// CONSTRUCTOR
        /// </summary> 
        public CrossoverArgs(Population population, Individual mother, Individual father, int maxTreeDepth)
        {
            this.Population = population;
            this.Mother = mother;
            this.Father = father;
            this.MaxTreeDepth = maxTreeDepth;
        }
    }
}
