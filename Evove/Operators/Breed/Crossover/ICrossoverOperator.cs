﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Breed.Crossover
{
    /// <summary>
    /// Designates classes capable of crossing-over individuals (sexual reproduction).
    /// 
    /// This is for use with BreedOperators.Basic, which implements both mutation and crossover.
    /// See CrossoverOperators.* for a selection of inbuilt concrete implementations.
    /// </summary>
    public interface ICrossoverOperator
    {
        /// <summary>
        /// Crosses over two individuals.
        /// The individuals are crossed over directly (i.e. they should not be copied).
        /// </summary>
        /// <param name="args">Arguments containing the individuals</param>
        bool Crossover(CrossoverArgs args);
    }
}
