﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;

namespace Evove.Operators.Breed.Crossover
{
    /// <summary>
    /// Contains classes for implementations of ICrossoverOperator.
    /// </summary>
    public sealed class CrossoverOperators
    {
        /// <summary>
        /// The basic crossover operator.
        /// 
        /// Picks a random node in both parents and swaps them around.
        /// Depth is the same in both cases.
        /// </summary>
        public class CommonPoint : ICrossoverOperator
        {
            public override string ToString()
            {
                return "Common point crossover";
            }

            /// <summary>
            /// IMPLEMENTS ICrossoverOperator
            /// </summary>                   
            public bool Crossover(CrossoverArgs args)
            {
                args.Mother.History.Add(EHistoryEvent.Crossover_with_individual, args.Father.Uid, 0);
                args.Father.History.Add(EHistoryEvent.Crossover_with_individual, args.Mother.Uid, 0);

                return CrossoverTrees(args, args.Mother.Genome, args.Father.Genome);
            }

            /// <summary>
            /// Crosses over trees (destructive).
            /// </summary>         
            private static bool CrossoverTrees( CrossoverArgs args, Tree a, Tree b)
            {
                List<Node> aNodes = a.IterateAllNodes(false).ToList(); // TODO: Slow?
                Node aNode = args.Population.Randomiser.FromList(aNodes);

                List<Node> bNodes = b.IterateAllNodes(false).Where(
                    z => z.Function.ReturnType == aNode.Function.ReturnType
                      && z.Depth == aNode.Depth
                    ).ToList();

                if (bNodes.Count == 0)
                {
                    // No compatable node in second tree
                    // TODO: Really we shouldn't have selected an incompatable node!
                    return false;
                }

                Node bNode = args.Population.Randomiser.FromList(bNodes);

                CrossoverSwapPositions(aNode, bNode);

                return true;
            }

            /// <summary>
            /// Swaps the positions of two nodes.
            /// It is assumed these nodes occur in seperate trees, otherwise behaviour is undefined.
            /// </summary>
            public static void CrossoverSwapPositions(Node aNode, Node bNode)
            {
                Debug.Assert(aNode.Parent.Children[aNode.Index] == aNode);
                Debug.Assert(bNode.Parent.Children[bNode.Index] == bNode);

                // Swap children
                aNode.Parent.Children[aNode.Index] = bNode;
                bNode.Parent.Children[bNode.Index] = aNode;

                // Swap parents
                Node aNodeParent = aNode.Parent;
                int aNodeIndex = aNode.Index;
                int aNodeDepth = aNode.Depth;

                aNode.Parent = bNode.Parent;
                aNode.Index = bNode.Index;
                aNode.Depth = bNode.Depth;
                bNode.Parent = aNodeParent;
                bNode.Index = aNodeIndex;
                bNode.Depth = aNodeDepth;

                aNode.Parent.CacheMarkAsDirty("Crossover @ " + aNode.TrackingId);
                bNode.Parent.CacheMarkAsDirty("Crossover @ " + bNode.TrackingId);

                Debug.Assert(aNode.Parent.Children[aNode.Index] == aNode);
                Debug.Assert(bNode.Parent.Children[bNode.Index] == bNode);
            }
        }
    }
}
