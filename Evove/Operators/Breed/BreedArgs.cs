﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;
using Evove.Operators.SelectionMethods;

namespace Evove.Operators.Breed
{
    /// <summary>
    /// Arguments passed to IBreedOperator.Breed
    /// 
    /// (Passing these arguments as a single object allows users to add information to the arguments
    /// without having to change all of the classes that use them.)
    /// </summary>
    public sealed class BreedArgs
    {                 
        /// <summary>
        /// Maximum size of the resultant tree
        /// </summary>
        public readonly int MaxTreeDepth;
        public readonly Population Population;

        /// <summary>
        /// Selection method
        /// </summary>
        private readonly ISelectionMethod SelectionMethod;

        /// <summary>
        /// Arguments to the selection method
        /// </summary>
        private readonly SelectionMethodArgs SelectionMethodArgs;
                       
        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        public BreedArgs(Population population, ISelectionMethod selectionMethod, SelectionMethodArgs selArgs, int maxTreeDepth)
        {
            this.Population = population;
            MaxTreeDepth = maxTreeDepth;
            SelectionMethod = selectionMethod;
            SelectionMethodArgs = selArgs;                        
        }

        /// <summary>
        /// Selects a random individual using the designated selection method.
        /// </summary>                                                        
        public Individual Select()
        {
            return SelectionMethod.Select(SelectionMethodArgs);
        }

        /// <summary>
        /// Selects a copy of a random individual using the designated selection method.
        /// </summary>                                                        
        public Individual SelectCopy()
        {
            return new Individual(Select());
        }
    }
}
