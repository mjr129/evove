﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Blocks;
using Evove.Operators.Breed.Crossover;
using Evove.Operators.Breed.Mutation;

namespace Evove.Operators.Breed
{
    public static class BreedOperators
    {
        /// <summary>
        /// Basic breeding operator.
        /// 
        /// Uses [chanceOfCrossover] [chanceOfChildMutation] to apply a crossover.
        /// Uses [chanceOfMutation] to apply mutation.   
        /// </summary>
        public sealed class CrossoverAndMutation : IBreedOperator
        {
            public readonly double _chanceOfChildMutation;
            public readonly double _chanceOfCrossover;
            public readonly double _chanceOfCloneMutation;
            public readonly ICrossoverOperator _crossover;
            public readonly IMutationOperator _mutation;

            public override string ToString()
            {
                return $"Crosover and mutation (crossover = {EvoveHelper.Percent( _chanceOfCrossover )}, clone mutation = {EvoveHelper.Percent( _chanceOfCloneMutation )}, child mutation = {EvoveHelper.Percent( _chanceOfChildMutation )})";
            }

            /// <summary>
            /// CONSTRUCTOR
            /// </summary>
            /// <param name="crossover">Crossover operator</param>
            /// <param name="chanceOfCrossover">Chance of using crossover to generate new individuals (else cloning will be used) </param>
            /// <param name="mutation">Mutation operator</param>
            /// <param name="chanceOfCloneMutation">Chance of applying mutation to a clone</param>
            /// <param name="chanceOfChildMutation">Chance of applying mutation to a crossover child</param>
            public CrossoverAndMutation( ICrossoverOperator crossover, double chanceOfCrossover, IMutationOperator mutation, double chanceOfCloneMutation, double chanceOfChildMutation )
            {
                EvoveHelper.CheckNotNull(nameof(crossover), crossover);
                EvoveHelper.Check( nameof( chanceOfCrossover ), chanceOfCrossover, 0.0d, 1.0d );
                EvoveHelper.CheckNotNull(nameof(mutation), crossover);
                EvoveHelper.Check( nameof( chanceOfCloneMutation ), chanceOfCloneMutation, 0.0d, 1.0d );
                EvoveHelper.Check( nameof( chanceOfChildMutation ), chanceOfChildMutation, 0.0d, 1.0d );

                _crossover = crossover;
                _chanceOfCrossover = chanceOfCrossover;
                _mutation = mutation;
                _chanceOfCloneMutation = chanceOfCloneMutation;
                _chanceOfChildMutation = chanceOfChildMutation;
            }

            public IEnumerable<Individual> Breed( BreedArgs args )
            {
                Individual mother = args.SelectCopy();

                if (args.Population.Randomiser.Chance( _chanceOfCrossover ))
                {
                    Individual father = args.SelectCopy();

                    if (!_crossover.Crossover( new CrossoverArgs( args.Population, mother, father, args.MaxTreeDepth ) ))
                    {
                        // Failed, do something else
                        return null;
                    }

                    if (args.Population.Randomiser.Chance( _chanceOfChildMutation ))
                    {
                        _mutation.Mutate( new MutateArgs( args.Population, mother, args.MaxTreeDepth ) );
                    }

                    if (args.Population.Randomiser.Chance( _chanceOfChildMutation ))
                    {
                        _mutation.Mutate( new MutateArgs( args.Population, father, args.MaxTreeDepth ) );
                    }

                    return new[] { mother, father };
                }
                else
                {
                    if (args.Population.Randomiser.Chance( _chanceOfCloneMutation ))
                    {
                        _mutation.Mutate( new MutateArgs( args.Population, mother, args.MaxTreeDepth ) );
                    }

                    return new[] { mother };
                }
            }
        }

    }
}
