﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Helpers
{
    public static partial class FitnessValues         
    {
        /// <summary>
        /// Wraps a double, providing the a double fitness value with the capability to hold extra
        /// details when subclassed.
        /// </summary>                                                          
        public abstract class DoubleWrapper : IComparable, IConvertible
        {
            protected readonly double _fitness;

            public DoubleWrapper(double fitness)
            {
                _fitness = fitness;
            }

            public DoubleWrapper()
            {
                // NA
            }

            public virtual double Value { get { return _fitness; } }

            object IConvertible.ToType(Type conversionType, IFormatProvider provider) => ((IConvertible)Value).ToType(conversionType, provider);
            TypeCode IConvertible.GetTypeCode() => ((IConvertible)Value).GetTypeCode();
            bool IConvertible.ToBoolean(IFormatProvider provider) => ((IConvertible)Value).ToBoolean(provider);
            byte IConvertible.ToByte(IFormatProvider provider) => ((IConvertible)Value).ToByte(provider);
            char IConvertible.ToChar(IFormatProvider provider) => ((IConvertible)Value).ToChar(provider);
            DateTime IConvertible.ToDateTime(IFormatProvider provider) => ((IConvertible)Value).ToDateTime(provider);
            decimal IConvertible.ToDecimal(IFormatProvider provider) => ((IConvertible)Value).ToDecimal(provider);
            double IConvertible.ToDouble(IFormatProvider provider) => ((IConvertible)Value).ToDouble(provider);
            short IConvertible.ToInt16(IFormatProvider provider) => ((IConvertible)Value).ToInt16(provider);
            int IConvertible.ToInt32(IFormatProvider provider) => ((IConvertible)Value).ToInt32(provider);
            long IConvertible.ToInt64(IFormatProvider provider) => ((IConvertible)Value).ToInt64(provider);
            sbyte IConvertible.ToSByte(IFormatProvider provider) => ((IConvertible)Value).ToSByte(provider);
            float IConvertible.ToSingle(IFormatProvider provider) => ((IConvertible)Value).ToSingle(provider);
            string IConvertible.ToString(IFormatProvider provider) => ((IConvertible)Value).ToString(provider);
            ushort IConvertible.ToUInt16(IFormatProvider provider) => ((IConvertible)Value).ToUInt16(provider);
            uint IConvertible.ToUInt32(IFormatProvider provider) => ((IConvertible)Value).ToUInt32(provider);
            ulong IConvertible.ToUInt64(IFormatProvider provider) => ((IConvertible)Value).ToUInt64(provider);
            public override string ToString() => Value.ToString();
            public override int GetHashCode() => Value.GetHashCode();

            public override bool Equals(object obj)
            {
                if (obj == null || GetType() != obj.GetType())
                {
                    return false;
                }

                return Value.Equals(((DoubleWrapper)obj).Value);
            }

            int IComparable.CompareTo(object obj)
            {
                DoubleWrapper f = obj as DoubleWrapper;
                return f == null ? -1 : Value.CompareTo(f.Value);
            }
        }
    }
}