﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Termination
{
    /// <summary>
    /// Arguments passed to termination operators.
    /// </summary>
    public sealed class TerminationOperatorArgs
    {
        /// <summary>
        /// An arbitrary tag that may be used by the termination operator to hold data.
        /// The tag is unique per-individual and per-termination-operator.
        /// </summary>
        public object Tag;
    }
}
