﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Termination
{
    /// <summary>
    /// Designates classes capable of terminating executing individuals.
    /// 
    /// See TerminationOperators.* for a selection of inbuilt concrete implementations.
    /// </summary>
    public interface ITerminationOperator
    {
        /// <summary>
        /// Called when an individual begins its evaluation phase.
        /// Use to prime any information in args.Tag.
        /// </summary>                 
        void Instantiate(TerminationOperatorArgs args);

        /// <summary>
        /// Called when an individual enters a function evaluation to check for termination.
        /// 
        /// Terminated individuals receive no fitness function value.
        /// </summary>                 
        /// <returns>true to terminate, false to continue execution</returns>
        bool Terminate(TerminationOperatorArgs args);
    }
}
