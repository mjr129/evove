﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Termination
{
    public static partial class TerminationOperators
    {
        /// <summary>
        /// A termination operator that terminates individuals taking too long to execute.
        /// </summary>
        public sealed class Time : ITerminationOperator
        {
            private readonly long _maxExecuteTimeMillis;

            public override string ToString()
            {
                return $"Excise after time (milliseconds = {_maxExecuteTimeMillis})";
            }

            public Time(long maxExecuteTimeMillis)
            {
                _maxExecuteTimeMillis = maxExecuteTimeMillis;
            }

            public Time(TimeSpan maxExecuteTime)
            {
                _maxExecuteTimeMillis = (long)maxExecuteTime.TotalMilliseconds;
            }

            public void Instantiate(TerminationOperatorArgs args)
            {
                args.Tag = Stopwatch.StartNew();
            }

            public bool Terminate(TerminationOperatorArgs args)
            {
                return (((Stopwatch)args.Tag).ElapsedMilliseconds > _maxExecuteTimeMillis);
            }
        }
    }
}
