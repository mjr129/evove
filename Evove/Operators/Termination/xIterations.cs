﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Termination
{
    public static partial class TerminationOperators
    {
        /// <summary>
        /// A termination operator that terminates individuals executing too many functions.
        /// </summary>
        public sealed class Iterations : ITerminationOperator
        {
            private readonly int _maxIterations;

            public override string ToString()
            {
                return $"Excise after iterations (count = {_maxIterations})";
            }

            public Iterations(int maxIterations)
            {
                _maxIterations = maxIterations;
            }

            public void Instantiate(TerminationOperatorArgs args)
            {
                args.Tag = _maxIterations;
            }

            public bool Terminate(TerminationOperatorArgs args)
            {
                int v = ((int)args.Tag) - 1;
                args.Tag = v;
                return v >= 0;
            }
        }
    }
}
