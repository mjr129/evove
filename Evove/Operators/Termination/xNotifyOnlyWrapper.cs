﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Operators.Termination
{
    public static partial class TerminationOperators
    {
        /// <summary>
        /// A termination operator that wraps another termination operator, notifying the caller that termination
        /// was requested but forgoing actual termination.
        /// </summary>
        public sealed class NotifyOnlyWrapper : ITerminationOperator
        {
            private readonly ITerminationOperator _actualOperator;
            private readonly Action<TerminationOperatorArgs> _notifyTarget;

            public override string ToString()
            {
                return $"Notify only (operator = {_actualOperator}, target = {_notifyTarget})";
            }

            public NotifyOnlyWrapper(ITerminationOperator actualOperator, Action<TerminationOperatorArgs> notifyTarget)
            {
                _actualOperator = actualOperator;
                _notifyTarget = notifyTarget;
            }

            public void Instantiate(TerminationOperatorArgs args)
            {
                _actualOperator.Instantiate(args);
            }

            public bool Terminate(TerminationOperatorArgs args)
            {
                if (_actualOperator.Terminate(args))
                {
                    _notifyTarget(args);
                }

                return false;
            }
        }
    }
}
