﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove.Helpers.GpFunctions
{
    partial class GpFunctions
    {
        /// <summary>
        /// A suite of functions to manage a block of memory.
        /// Functions: Store, Recall
        /// </summary>
        /// <typeparam name="T">The type of memory</typeparam>       
        [EvoveClass( Caching = true, Persistence = true, Threading = true )]
        public sealed class Memory<T>      : IResettable
        {
            private T[] _memory;

            public Memory(int numberOfSlots)
            {
                _memory = new T[numberOfSlots];
            }             

            void IResettable.Reset()
            {
                for (int n = 0; n < _memory.Length; n++)
                {
                    _memory[n] = default(T);
                }
            }       

            [EvoveMethod(Cache = ECache.Uncached) ]
            public void Store(int slot, T value)
            {
                _memory[GetSlot(slot)] = value;
            }

            [EvoveMethod( Cache = ECache.Uncached )]
            public T Recall(int slot)
            {
                return _memory[GetSlot(slot)];
            }

            [EvoveMethod( Cache = ECache.Uncached )]
            private int GetSlot(int slot)
            {
                if (slot < 0)
                {
                    if (slot == int.MinValue)
                    {
                        slot = int.MaxValue;
                    }
                    else
                    {
                        slot = -slot;
                    }
                }

                return slot % _memory.Length;
            }
        }
    }
}
