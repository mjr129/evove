﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove.Helpers.GpFunctions
{
    partial class GpFunctions
    {
        /// <summary>
        /// A suite of functions for dealing with doubles.
        /// Functions: + * - / % Abs Sin Cos Tan Max Min 
        /// </summary>     
        [EvoveClass( Caching = true, Persistence = true, Threading = true )]
        public sealed class Double
        {
            private readonly Randomiser _randomiser;

            public Double( InstantiationArgs args )
            {
                _randomiser = args.Population.Randomiser;
            }

            [EvoveMethod( Role = EMethod.ConstantProvider )]
            public double CreateConstant()
            {
                return _randomiser.NextDouble();
            }

            public static double Add(double augend, double addend)
            {
                return augend + addend; // sum
            }

            public static double Multiply(double multiplicand, double multiplier)
            {
                return multiplicand * multiplier; // product
            }

            public static double Sub(double minuend, double subtrahend)
            {
                return minuend - subtrahend; // difference
            }

            public static double Divide(double dividend, double divisor)
            {
                return dividend / divisor; // quotient
            }

            public static double Remainder(double dividend, double divisor)
            {
                return Math.IEEERemainder(dividend, divisor); // remainder
            }

            public static double Abs(double value)
            {
                return Math.Abs(value);
            }

            //public static double Sin(double value)
            //{
            //    return Math.Sin(value);
            //}

            //public static double Cos(double value)
            //{
            //    return Math.Cos(value);
            //}

            //public static double Tan(double value)
            //{
            //    return Math.Tan(value);
            //}

            public static double Max(double value1, double value2)
            {
                return Math.Max(value1, value2);
            }

            public static double Min(double value1, double value2)
            {
                return Math.Min(value1, value2);
            }

            public static double Interpolate( double value1, double value2, double amount1 )
            {
                return (value1 + (value2 - value1) * amount1);
            }
        }
    }
}
