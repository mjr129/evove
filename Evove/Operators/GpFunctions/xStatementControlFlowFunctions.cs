﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove.Helpers.GpFunctions
{
    partial class GpFunctions
    {                          
        [EvoveClass( Caching = true, Persistence = true, Threading = true )]
        public static class StatementControlFlow
        {
            public static void IfThenElse(bool condition, STATEMENT then, STATEMENT @else)
            {
                if (condition)
                {
                    then();
                }
                else
                {
                    @else();
                }
            }

            public static void IfThen(bool condition, STATEMENT then)
            {
                if (condition)
                {
                    then();
                }
            }
        }
                               
        [EvoveClass( Caching = true, Persistence = true, Threading = true )]
        public static class Statement
        {
            public static void TwoStatements(STATEMENT first, STATEMENT second)
            {
                first();
                second();
            }
        }
    }
}
