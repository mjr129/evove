﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove.Helpers.GpFunctions
{
    partial class GpFunctions
    {
        /// <summary>
        /// A suite of basic boolean functions.
        /// Functions: ¬ ⋏ ⋎ ⊻
        /// </summary>    
        [EvoveClass( Caching = true, Persistence = true, Threading = true )]
        public sealed class Boolean
        {
            private readonly Randomiser Randomiser;

            public Boolean( InstantiationArgs args )
            {
                this.Randomiser = args.Population.Randomiser;
            }

            [EvoveMethod( Role = EMethod.ConstantProvider )]
            public bool Constant()
            {
                return Randomiser.Next( 2 ) == 1;
            }

            public static bool Not( bool value )
            {
                return !value;
            }

            public static bool And( bool left, BOOLEAN right )
            {
                return left && right();
            }

            public static bool Or( bool left, BOOLEAN right )
            {
                return left || right();
            }

            public static bool Xor( bool left, bool right )
            {
                return left ^ right;
            }

            public static bool Ternary( bool a, BOOLEAN b, BOOLEAN c )
            {
                return a ? b() : c();
            }
        }

        /// <summary>
        /// Implements three-fold AND and a three-fold OR.
        /// </summary>
        [EvoveClass( Caching = true, Persistence = true, Threading = true )]
        public static class Boolean3
        {            
            public static bool And3( bool a, BOOLEAN b, BOOLEAN c )
            {
                return a && b() && c();
            }

            public static bool Or3( bool a, BOOLEAN b, BOOLEAN c )
            {
                return a || b() || c();
            }
        }
    }
}
