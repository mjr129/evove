﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove.Helpers.GpFunctions
{
    partial class GpFunctions
    {                              
        [EvoveClass( Caching = false, Persistence = true, Threading = true )]
        public sealed class SimpleDouble
        {
            private readonly Randomiser _randomiser;

            public SimpleDouble( InstantiationArgs args )
            {
                _randomiser = args.Population.Randomiser;
            }

           [EvoveMethod( Role = EMethod.ConstantProvider)]
            public double CreateConstant()
            {
                return _randomiser.NextDouble();
            }

            public static double Add( double augend, double addend )
            {
                return augend + addend;
            }

            public static double Multiply( double multiplicand, double multiplier )
            {
                return multiplicand * multiplier;
            }

            public static double Sub( double minuend, double subtrahend )
            {
                return minuend - subtrahend;
            }

            public static double Divide( double dividend, double divisor )
            {
                return dividend / divisor;
            }
        }
    }
}