﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove.Helpers.GpFunctions
{
    partial class GpFunctions
    {
        /// <summary>
        /// A suite of functions for comparing a value.
        /// Functions: ≤ ≥ ＜ ＞ ≠ =
        /// </summary>       
        [EvoveClass( Caching = true, Persistence = true, Threading = true )]
        public static class Comparisson<T>
            where T : IComparable
        {

            public static bool LessThanOrEqualTo(T left, T right)
            {
                return left.CompareTo(right) <= 0;
            }

            public static bool MoreThanOrEqualTo(T left, T right)
            {
                return left.CompareTo(right) >= 0;
            }

            public static bool LessThan(T left, T right)
            {
                return left.CompareTo(right) < 0;
            }

            public static bool MoreThan(T left, T right)
            {
                return left.CompareTo(right) > 0;
            }

            public static bool NotEqualTo(T left, T right)
            {
                return left.CompareTo(right) != 0;
            }

            public static bool EqualTo(T left, T right)
            {
                return left.CompareTo(right) == 0;
            }
        }
    }
}
