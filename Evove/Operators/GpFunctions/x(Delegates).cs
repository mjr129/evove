﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Helpers.GpFunctions
{
    /// <summary>
    /// Contains various predefined functions for use in your evolutions.
    /// </summary>
    public static partial class GpFunctions
    {
        /// <summary>
        /// A function with no return value.
        /// </summary>
        public delegate void STATEMENT();

        /// <summary>
        /// A function with a boolean return value.
        /// </summary>
        public delegate bool BOOLEAN();

        /// <summary>
        /// A function with a double return value.
        /// </summary>
        public delegate double DOUBLE();

        /// <summary>
        /// A function with an integer return value.
        /// </summary>
        public delegate int INTEGER();

        /// <summary>
        /// A function with a double array return value.
        /// </summary>
        public delegate double[] ARRAY();
    }
}
