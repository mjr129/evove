﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove.Helpers.GpFunctions
{
    partial class GpFunctions
    {
        /// <summary>
        /// A suite of functions for dealing with doubles.
        /// Functions: + * - / % Abs Sin Cos Tan Max Min 
        /// </summary>     
        [EvoveClass( Caching = true, Persistence = true, Threading = true )]
        public sealed class ConversionIntegerDouble
        {
            public double ToDouble( int @integer )
            {
                return (double)@integer;
            }

            public int ToInteger(double @double )
            {
                if (double.IsNaN( @double ) || double.IsInfinity( @double ))
                {
                    return 0;
                }

                return (int)@double;
            }
        }
    }
}
