﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove.Helpers.GpFunctions
{
    partial class GpFunctions
    {                           
        /// <summary>
        /// Ternary operator, from boolean and to boolean
        /// </summary>
        [EvoveClass( Caching = true, Persistence = true, Threading = true )]
        public static class IntegerControlFlow
        {
            public static double TernaryOperator(bool condition, INTEGER then, INTEGER @else)
            {
                return condition ? then() : @else();
            }

            public static int FromBoolean(bool boolean)
            {
                return boolean ? 1 : 0;
            }

            public static bool ToBoolean(int integer)
            {
                return integer != 0;
            }
        }
    }
}
