﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove.Helpers.GpFunctions
{
    partial class GpFunctions
    {                     
        [EvoveClass( Caching = false, Persistence = true, Threading = true )]
        public sealed class Array : IResettable
        {
            private Randomiser _randomiser;
            private int _arraySize = 10;
            private double _currentArrayIndex;

            public Array( InstantiationArgs args, int arraySize )
            {
                EvoveHelper.Check(nameof(arraySize), arraySize, 1, int.MaxValue);

                this._randomiser = args.Population.Randomiser;
                this._arraySize = arraySize;       
            }

            void IResettable.Reset()
            {
                _currentArrayIndex = 0.0d;
            }

            [EvoveMethod( Role = EMethod.ConstantProvider )]
            public double[] CreateConstant()
            {
                int count =  _randomiser.Next(1, _arraySize + 1);

                double[] result = new double[count];

                for (int i = 0; i < count; i++)
                {
                    result[i] = _randomiser.NextDouble();
                }

                return result;
            }

            public double[] Create(double count, DOUBLE creator)
            {
                int trueCount = EvoveHelper.Wrap((int)count, 1, _arraySize + 1);

                double[] result = new double[trueCount];

                double _previousCurrentArrayIndex = _currentArrayIndex;

                for (int i = 0; i < trueCount; i++)
                {
                    _currentArrayIndex = i;
                    result[i] = creator();
                }

                _currentArrayIndex = _previousCurrentArrayIndex;

                return result;
            }

            public double CurrentArrayIndex()
            {
                return _currentArrayIndex;
            }

            public double[] ForEach(double[] input, DOUBLE @operator)
            {
                double[] result = new double[input.Length];

                double _previousCurrentArrayIndex = _currentArrayIndex;

                for (int i = 0; i < input.Length; i++)
                {
                    _currentArrayIndex = i;
                    result[i] = @operator();
                }

                _currentArrayIndex = _previousCurrentArrayIndex;

                return result;
            }

            public static double[] Smooth(double[] array)
            {
                if (array.Length <= 1)
                {
                    return array;
                }

                double[] result = new double[array.Length];

                for (int i = 0; i < array.Length; i++)
                {
                    if (i == 0)
                    {
                        result[i] = (array[i] + array[i + 1]) / 2;
                    }
                    else if (i == array.Length - 1)
                    {
                        result[i] = (array[i - 1] + array[i]) / 2;
                    }
                    else
                    {
                        result[i] = (array[i - 1] + array[i] + array[i + 1]) / 3;
                    }
                }

                return result;
            }

            public static double Min(double[] array)
            {
                double total = double.MaxValue;

                foreach (double d in array)
                {
                    if (d < total)
                    {
                        total = d;
                    }
                }

                return total;
            }

            public static double Max(double[] array)
            {
                double total = double.MinValue;

                foreach (double d in array)
                {
                    if (d > total)
                    {
                        total = d;
                    }
                }

                return total;
            }

            public static double Sum(double[] array)
            {
                double total = 0.0d;

                foreach (double d in array)
                {
                    total += d;
                }

                return total;
            }

            public static double Mean(double[] array)
            {
                double total = 0.0d;

                foreach (double d in array)
                {
                    total += d;
                }

                return total / array.Length;
            }
        }
    }
}
