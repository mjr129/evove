﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove.Helpers.GpFunctions
{
    partial class GpFunctions
    {
        /// <summary>
        /// A suite of functions for dealing with integers.
        /// Functions ++ -- + * - / % Abs Min Max ?
        /// </summary>      
        [EvoveClass( Caching = true, Persistence = true, Threading = true )]
        public sealed class Integer
        {
            private readonly Randomiser _randomiser;

            public Integer( InstantiationArgs args )
            {
                _randomiser = args.Population.Randomiser;
            }

            [EvoveMethod( Role = EMethod.ConstantProvider )]
            public int Constant()
            {
                return _randomiser.Next();
            }

            public static int Increment( int augend )
            {
                return augend + 1;
            }

            public static int Decrement( int minuend )
            {
                return minuend - 1;
            }

            public static int Add( int augend, int addend )
            {
                return augend + addend;
            }

            public static int Multiply( int multiplicand, int multiplier )
            {
                return multiplicand * multiplier;
            }

            public static int Sub( int minuend, int right )
            {
                return minuend - right;
            }

            public static int Divide( int dividend, int divisor )
            {
                if (divisor == 0)
                {
                    return int.MaxValue;
                }

                if (dividend == int.MinValue)
                {
                    return 0;
                }

                return dividend / divisor;
            }

            public static int Modulo( int dividend, int divisor )
            {
                if (divisor == 0)
                {
                    return dividend;
                }

                if (dividend == int.MinValue)
                {
                    return 0;
                }

                return dividend % divisor;
            }

            public static int Abs( int value )
            {
                return EvoveHelper.SafeAbs( value );
            }

            public static int Min( int value1, int value2 )
            {
                return Math.Min( value1, value2 );
            }

            public static int Max( int value1, int value2 )
            {
                return Math.Max( value1, value2 );
            }
        }
    }
}
