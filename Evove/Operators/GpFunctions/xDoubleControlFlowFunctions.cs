﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove.Helpers.GpFunctions
{
    partial class GpFunctions
    {                      
        [EvoveClass( Caching = true, Persistence = true, Threading = true )]
        public static class DoubleControlFlow
        {
            public static bool IsValid(double value)
            {
                return !double.IsNaN(value) && !double.IsInfinity(value);
            }

            public static double TernaryOperator(bool condition, DOUBLE then, DOUBLE @else)
            {
                return condition ? then() : @else();
            }

            public static double FromBoolean(bool boolean)
            {
                return boolean ? 1.0d : 0.0d;
            }

            public static bool ToBoolean(double @double)
            {
                return @double != 0.0d;
            }
        }
    }
}
