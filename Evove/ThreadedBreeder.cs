﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Evove.Blocks;
using Evove.Operators.Breed;
using Evove.Operators.SelectionMethods;

namespace Evove
{
    /// <summary>
    /// Evaluates individuals in a multithreaded manner.
    /// </summary>
    internal sealed class ThreadedBreeder
    {
        Threader<List<Individual>, MyThreadContext> _threader;
        private readonly EvoveParameters _parameters;
        private List<Individual> _newPopulation;
        private List<Individual> _existing;
        private readonly Population _population;

        class MyThreadContext
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>                       
        public ThreadedBreeder(Population population, EvoveParameters parameters)
        {
            _population = population;
            _parameters = parameters;
            _threader = new Threader<List<Individual>, MyThreadContext>(parameters.NumberOfBreederThreads, Execute);
        }

        /// <summary>
        /// Breeds next generation.
        /// </summary>             
        public List<Individual> Breed( List<Individual> existing)
        {
            _newPopulation = new List<Individual>();
            _existing = existing.ToList();

            // Assert sorted
            Debug.Assert( IsSorted( existing ), "Expected list to be sorted." );

            // ELITISM - preserve elites   
            for (int n = 0; n < _parameters.NumberOfElites; n++)
            {
                _newPopulation.Add(existing[n]); // preserve fittest
            }

            // CULLING - drop bottom
            if (_parameters.NumberCulled != 0)
            {                                                 
                existing.RemoveRange( existing.Count - _parameters.NumberCulled, _parameters.NumberCulled );
            }

            foreach (List<Individual> result in _threader.PartitionWorkload(_parameters.IndividualsPerDeme - _parameters.NumberOfElites))
            {
                _newPopulation.AddRange(result);
            }

            while (_newPopulation.Count > _parameters.IndividualsPerDeme)
            {
                _newPopulation.RemoveAt(_newPopulation.Count - 1);
            }

            return _newPopulation;
        }

        private static bool IsSorted( List<Individual> existing )
        {
            for (int n = 1; n < existing.Count; n++)
            {
                if (Population.SortIndividuals( existing[n - 1], existing[n] ) > 0)
                {
                    return false;
                }
            }

            return true;
        }

        private IEnumerable<bool> While()
        {
            while (_newPopulation.Count < _parameters.IndividualsPerDeme)
            {
                yield return true;
            }
        }

        /// <summary>
        /// Thread function.
        /// </summary>                
        private List<Individual> Execute( MyThreadContext context, int start, int count)
        {
            List<Individual> myList = new List<Individual>();
            List<Individual> mySourceList = _existing.ToList(); // copy because we mess with it

            SelectionMethodArgs smArgs = new SelectionMethodArgs(_population, mySourceList);
            BreedArgs args = new BreedArgs(_population, _parameters.SelectionMethod, smArgs, _parameters.MaxTreeDepth);

            while (myList.Count < count)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();

                IEnumerable<Individual> results = _parameters.BreedingMethod.Breed(args);

                long timer = (int)(stopwatch.Elapsed.TotalMilliseconds * 1000);

                if (results != null)
                {
                    foreach (Individual newling in results)
                    {
                        Debug.Assert(!myList.Contains(newling), "The breeder is attempting to add an individual to the results which already exists in the results. This usually indicates a problem with the breeder operator.");
                        newling.Genome.DEBUG_Validate(_parameters);
                        newling.BreedTime = timer;
                        myList.Add(newling);
                    }
                }
            }

            return myList;
        }

        /// <summary>
        /// Selects an individual
        /// </summary>     
        private Individual Selection(List<Individual> fromList)
        {
            var args = new SelectionMethodArgs(_population, fromList);
            return _parameters.SelectionMethod.Select(args);
        }
    }
}
