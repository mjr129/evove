﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;
using Evove.Operators.Breed;
using Evove.Operators.Breed.Crossover;
using Evove.Operators.Breed.Mutation;
using Evove.Operators.Migration;
using Evove.Operators.SelectionMethods;
using Evove.Operators.Stopping;
using Evove.Operators.Termination;

namespace Evove
{
    /// <summary>
    /// The settings for Evove are all wrapped up in this class.
    /// 
    /// To avoid surprises Evove is quite pedantic and generally expects a value for everything,
    /// for instance prefering empty arrays over null. If something is not right you should get
    /// an error when creating the population.
    /// </summary>
    [Serializable]
    public class EvoveParameters : ISupportsCsv
    {
        /// <summary>
        /// The number of test cases
        /// 
        /// ONLY WHEN the evaluations involve multiple test cases AND where evaluation
        /// caching is enabled AND where there is no attribute already denoting the number of test
        /// cases.
        /// </summary>
        public int NumberOfTestCases;

        /// <summary>
        /// Specifies an optional target to inform about the evolution progress.
        /// Note: This field is not serialized.
        /// </summary>     
        public IProgressObserver ProgressWatcher;

        /// <summary>
        /// How many programs per deme
        /// </summary>
        public int IndividualsPerDeme;

        /// <summary>
        /// Maximum size of the GP trees.
        /// </summary>
        public int MaxTreeDepth;

        /// <summary>
        /// The termination operator is able to terminate executing individuals, for instance ones that take too long to execute.
        /// Terminated individuals return no fitness value.
        /// Only one of the provided operators need request termination for an individual to be terminated.
        /// A value is mandatory but the array may be empty, designating no termination condition.
        /// </summary>
        public ITerminationOperator[] TerminationOperators;

        /// <summary>
        /// Number of individuals preserved
        /// </summary>
        public int NumberOfElites;

        /// <summary>
        /// Number of bottom rankers killed.
        /// </summary>
        public int NumberCulled;

        /// <summary>
        /// Stopping conditions.
        /// Only one of the provided operators need request stopping for the simulation to be stopped.
        /// A value is mandatory but the array may be empty, designating no stopping condition.
        /// 
        /// See StoppingConditions.* for examples.
        /// </summary>
        public IStoppingCondition[] StoppingConditions;

        /// <summary>
        /// How individuals are bred
        /// </summary>
        public IBreedOperator BreedingMethod;

        /// <summary>
        /// If set to a directory loads the initial population from that directory
        /// 
        /// Allows continuing a previous evolution
        /// </summary>
        public string ContinueFromFile;

        /// <summary>
        /// Number of island populations
        /// </summary>
        public int NumberOfDemes;

        /// <summary>
        /// This is the SUBDIRECTORY we store the progress in.
        /// This can be NULL and progress will not be recorded.
        /// </summary>
        public string OutputFolder;

        /// <summary>
        /// How individuals are selected
        /// 
        /// See the SelectionMethods.* class for examples.
        /// </summary>
        public ISelectionMethod SelectionMethod;

        /// <summary>
        /// Number of threads spawned for evaluation.
        /// 
        /// In general this should be 1 (for single threaded operations), or equal to the number of cores (for
        /// multi-threaded operations).
        /// 
        /// IMPORTANT NOTE
        /// Multithreading is generally slower if the evaluation functions are fast, presumably due to the loss of 
        /// single-threaded optimisations. Multi-threading is therefore only recommended for very complex evaluations
        /// or very large populations that are naturally slow. THIS IS NOT A PANACEA, YOUR GP FUNCTIONS MUST ALSO
        /// SUPPORT MULTITHREADING AND IMPLEMENT ANY NECESSARY LOCKING.
        /// </summary>
        public int NumberOfEvaluatorThreads;

        /// <summary>
        /// Number of threads spawned for breeding.
        /// 
        /// IMPORTANT NOTES: See <see cref=">NumberOfEvaluatorThreads"/>.
        /// </summary>
        public int NumberOfBreederThreads;

        /// <summary>
        /// Factory defining the type containing the fitness function to evolve, and any arguments
        /// objects of this type require for construction.
        /// </summary>
        public Factory<InstantiationArgs> FitnessFunction;

        /// <summary>
        /// Factories defining the types of any additional function sets.
        /// </summary>
        public Factory<InstantiationArgs>[] AdditionalFunctions;

        /// <summary>
        /// Whether to allow value caching to avoid repeated calculations at the cost of memory.
        /// 
        /// IMPORTANT NOTE:
        /// THIS IS NOT A PANACEA, YOUR GP FUNCTIONS MUST BE AMENABLE TO CACHING.
        /// Non-deterministic GP functions, or functions which vary with the test case must be
        /// marked with the necessary attributes. To avoid unexpected errors the class must be
        /// also be marked as multi-threading capable.
        /// </summary>
        public bool AllowCaching;

        /// <summary>
        /// Whether to include support for method overloads.
        /// This makes save files bigger since descriptions must include parameter details.
        /// Note that Evove, like CLI but unlike C#, does support overloads on return types if this
        /// is set.
        /// </summary>
        public bool SupportMethodOverloads;

        /// <summary>
        /// Whether to include support for multithreading.
        /// This means predictable behaviour cannot be guaranteed and a randomiser must be created
        /// for each thread.
        /// </summary>
        public bool SupportMultithreading => NumberOfBreederThreads > 1 || NumberOfEvaluatorThreads > 1;

        /// <summary>
        /// Shuffles the individuals before the evaluation step
        /// (Only set if individuals need to compete)
        /// </summary>
        public bool ShuffleBeforeEvaluate;

        /// <summary>
        /// Whether to allow running in debug mode.
        /// 
        /// This is here as debug mode is considerably slower and acknowledgement is therfore
        /// requested to avoid surprises.
        /// </summary>
        public bool SupportDebugging;

        /// <summary>
        /// Runs each evaluation twice, once with caching and once without, then compares the result.
        /// An exception is thrown if there's a discrepancy.
        /// It might be good to use this a couple of times if working on classes marked with the
        /// "EvoveSupportsCacheAttribute".
        /// </summary>
        public bool CheckForCacheErrors;

        /// <summary>
        /// The migration operator
        /// </summary>
        public IMigrationOperator MigrationOperator;

        /// <summary>
        /// A holder for custom objects. You can use this to pass configuration data to your
        /// population/deme/evaluation objects and round event handlers. These 
        /// can access this object through args.Population.Parameters.Properties.
        /// </summary>
        public PropertyBag Properties = new PropertyBag();

        /// <summary>
        /// This is the random number generator seed used for Population.Randomiser.
        /// Use it to get consistant results (assuming your functions use Population.Randomiser).
        /// Set to -1 to randomise the seed.
        /// 
        /// Note in multi-threaded mode execution order is indeterminate, rendering this field
        /// ineffective.
        /// </summary>
        public int RandomSeed;

        /// <summary>
        /// Per-evaluation objects are normally kept in a pool (to avoid GC lag) and assigned to
        /// each evaluation. Set this to true to refresh the evaluation objects per evaluation.
        /// </summary>
        public bool AbandonPerEvaluationObjects;

        /// <summary>
        /// An arbitrary title (unused by Evove)
        /// </summary>
        public string Title;

        /// <summary>
        /// Allows the syntax-manager to be created with an incomplete parameter set.
        /// For obtaining details from parsing types only, not for actual use.
        /// </summary>
        public bool AllowIncomplete;

        /// <summary>
        /// May be set to a flags object (any type convertable to <see cref="long"/>).
        /// Methods and classes marked with <see cref="EvoveMethodAttribute.Set"/> or <see cref="EvoveExtendsAttribute.Set"/>
        /// will only be included if the value includes the set of flags marked here.
        /// 
        /// If this is NULL all methods and classes will be included.
        /// Methods and classes with no Set attribute will always be included.
        /// </summary>
        public object Set;

        /// <summary>
        /// Creates a basic parameter set.
        /// </summary>
        /// <param name="templateType">Type containing the evolvable functions.</param>
        /// <param name="numberOfIndividuals">The number of individuals in the population.</param>
        public static EvoveParameters CreateBasicSet( Factory<InstantiationArgs> templateType, int numberOfIndividuals = 25 )
        {
            return new EvoveParameters()
            {
                IndividualsPerDeme = numberOfIndividuals / 4,
                NumberOfElites = Math.Max( 1, numberOfIndividuals / 25 ),
                NumberCulled = 0,
                BreedingMethod = new BreedOperators.CrossoverAndMutation(
                                        new CrossoverOperators.CommonPoint(),
                                        0.25,
                                        new MutationOperators.PerNode( 0.05, 0.33, 0.33, 0.33 ),
                                        0.95, 0.25 ),
                ContinueFromFile = null,
                NumberOfDemes = 4,
                OutputFolder = "Test",
                SelectionMethod = new SelectionMethods.Tournament( numberOfIndividuals / 5 ),
                NumberOfBreederThreads = 0,
                NumberOfEvaluatorThreads = 0,
                FitnessFunction = templateType,
                AdditionalFunctions = new Factory<InstantiationArgs>[0],
                AllowCaching = false,
                SupportMethodOverloads = false,
                SupportDebugging = true,
                StoppingConditions = new IStoppingCondition[] { new StoppingConditions.Stagnation( 100 ) },
                TerminationOperators = new ITerminationOperator[] { new TerminationOperators.Time( 1000 ) },
                MaxTreeDepth = 5,
                AbandonPerEvaluationObjects = false,
                CheckForCacheErrors = false,
                MigrationOperator = new MigrationOperators.Circular( numberOfIndividuals, 2 ),
                NumberOfTestCases = 0,
                ProgressWatcher = null,
                RandomSeed = 1,
                Set = null,
                AllowIncomplete = false,
                Properties = new PropertyBag(),
                Title = "Basic parameter set for " + templateType.ToString(),
            };
        }

        /// <summary>
        /// Creates a shallow copy of the parameters.
        /// </summary>                               
        public EvoveParameters Clone()
        {
            return (EvoveParameters)MemberwiseClone();
        }

        /// <summary>
        /// Describes the parameter set as a string.
        /// </summary>                              
        public override string ToString()
        {
            return CsvWriter.AsString( this );
        }

        /// <summary>
        /// Implements ISupportsCsv.
        /// </summary>                
        void ISupportsCsv.ToCsv( CsvWriter csv )
        {
            csv.Serialise( this );
        }         

        /// <summary>
        /// Quickly validates the parameter set.
        /// </summary>
        public void Validate()
        {
            EvoveHelper.Check( nameof( IndividualsPerDeme ), IndividualsPerDeme, 1, int.MaxValue );
            EvoveHelper.CheckNotNull( nameof( FitnessFunction ), FitnessFunction );
            EvoveHelper.CheckNotNull( nameof( AdditionalFunctions ), AdditionalFunctions );
            EvoveHelper.CheckNotNull( nameof( TerminationOperators ), TerminationOperators );
            EvoveHelper.CheckNotNull( nameof( StoppingConditions ), StoppingConditions );
            EvoveHelper.CheckNotNull( nameof( BreedingMethod ), BreedingMethod );
            EvoveHelper.Check( nameof( ContinueFromFile ), string.IsNullOrEmpty( ContinueFromFile ) || Directory.Exists( ContinueFromFile ), "The directory must exist or no directory should be specified." );
            EvoveHelper.Check( nameof( NumberOfDemes ), NumberOfDemes, 1, int.MaxValue );
            EvoveHelper.Check( nameof( NumberOfElites ), NumberOfElites, 0, IndividualsPerDeme );
            EvoveHelper.Check( nameof( NumberCulled ), NumberCulled, 0, IndividualsPerDeme );
            EvoveHelper.CheckNotNull( nameof( SelectionMethod ), SelectionMethod );
            EvoveHelper.Check( nameof( MigrationOperator ) + " with " + nameof( NumberOfDemes ), (MigrationOperator == null) == (NumberOfDemes == 1), "A MigrationOperator must be set when, and only when, NumberOfDemes is not 1." );
            EvoveHelper.Check( nameof( NumberOfBreederThreads ), NumberOfBreederThreads, 0, int.MaxValue );
            EvoveHelper.Check( nameof( NumberOfEvaluatorThreads ), NumberOfEvaluatorThreads, 0, int.MaxValue );
            EvoveHelper.Check( nameof( CheckForCacheErrors ) + " with " + nameof( AllowCaching ), (!CheckForCacheErrors) || AllowCaching, "The selected value cannot be specified for this mode." );
            EvoveHelper.Check( nameof( NumberOfEvaluatorThreads ) + " with " + nameof( CheckForCacheErrors ), (!CheckForCacheErrors) || (NumberOfEvaluatorThreads <= 1), "The selected value cannot be specified for this mode." );
            EvoveHelper.Check( nameof( OutputFolder ), !string.IsNullOrEmpty( OutputFolder ), "Cannot be null or empty." );
            EvoveHelper.Check( nameof( OutputFolder ), !Directory.Exists( OutputFolder ), "Directory cannot already exist." );
            EvoveHelper.Check( nameof( SupportMultithreading ), !SupportMultithreading || (RandomSeed == -1), "In multithreaded mode a random seed is not supported (it would be ineffective)." );

#if DEBUG
            EvoveHelper.Check(nameof(SupportDebugging), SupportDebugging, "You must set SupportDebugging to true to acknowledge additional checks and verifications performed during the running of a DEBUG build. The program will run considerably slower due to these checks.");
#endif
        }
    }
}
