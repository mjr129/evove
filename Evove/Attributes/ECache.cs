﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Attributes
{
    /// <summary>
    /// How method results are cached
    /// </summary>
    public enum ECache
    {
        /// <summary>
        /// Value is fixed for a given input.
        /// (The result of a given input it cached).
        /// </summary>
        Singular,

        /// <summary>
        /// Value is not fixed for a given input.
        /// Use for stochastic, state-based or state-altering functions.
        /// (No results are cached).
        /// </summary>
        Uncached,

        /// <summary>
        /// Value is fixed for a given input and test case.
        /// Use for functions that vary only with the test case.
        /// (Results are cached on a per-test-case basis)
        /// </summary>
        TestCase,
    }
}
