﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Attributes
{
    /// <summary>
    /// Designates the supported features of a class suitable for providing GP
    /// function nodes and/or fitness functions to Evove.
    /// </summary>
    [AttributeUsage( AttributeTargets.Class )]
    public sealed class EvoveClassAttribute : Attribute
    {
        /// <summary>
        /// SUPPORTS EVALUATION CACHING.
        /// 
        /// DEFAULT: FALSE
        /// 
        /// GOOD BECAUSE :  Avoids unnecessary repeated evaluations
        /// IMPL REQUIRD:  Class can't contain stochastic
        ///                 or non-stateless functions unless they are marked
        ///                 with <see cref="EvoveMethodAttribute.Cache"/>.
        /// </summary>
        public bool Caching { get; set; } = false;

        /// <summary>
        /// SUPPORTS MULTI-THREADING.
        /// 
        /// DEFAULT: FALSE
        ///                                 
        /// GOOD BECAUSE: Allows multithreading.
        /// IMPL REQUIRD: Function set must be thread safe.
        /// </summary>
        public bool Threading { get; set; } = false;

        /// <summary>
        /// SUPPORTS OBJECT PERSISTENCE.
        /// 
        /// DEFAULT: TRUE
        /// 
        /// GOOD BECAUSE: Objects don't require reconstruction
        ///               each round. Less to construct, less to clean up.
        /// IMPL REQUIRD: Must manually wipe previous state (if any)
        ///               at the start/end of the fitness function.
        /// </summary>
        public bool Persistence { get; set; } = true;              

        /// <summary>
        /// Gets any applied attribute, or the default if none.
        /// </summary>                                         
        public static EvoveClassAttribute Get( MemberInfo info )
        {
            return info.GetCustomAttribute<EvoveClassAttribute>( false );
        }
    }
}
