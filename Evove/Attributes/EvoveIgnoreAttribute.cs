﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Attributes
{
    /// <summary>
    /// Hide a method, class or delegate field from Evove.
    /// </summary>
    [AttributeUsage( AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Field )]
    public sealed class EvoveIgnoreAttribute : Attribute
    {
        // No content 
    }
}
