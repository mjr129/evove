﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Attributes
{
    /// <summary>
    /// Quick way of signalling to use methods from an EvoveExtension class.
    /// </summary>
    [AttributeUsage( AttributeTargets.Class, AllowMultiple = true )]
    public sealed class EvoveExtendsAttribute : Attribute
    {
        public object[] ConstructorArguments;
        public Type Type;
        public object Set;

        public EvoveExtendsAttribute( Type type, params object[] constructorArguments )
        {
            Type = type;
            ConstructorArguments = constructorArguments;
        }        

        public Factory<InstantiationArgs> CreateFactory()
        {
            return new Factory<InstantiationArgs>( Type, ConstructorArguments );
        }

        public override string ToString()
        {
            return Type.NameOfType() + "(" + string.Join( ", ", ConstructorArguments ) + ")";
        }
    }
}
