﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Attributes
{
    /// <summary>
    /// How methods are used
    /// </summary>
    public enum EMethod
    {
        /// <summary>
        /// Method provides methods for method GP-nodes (default)
        /// This is the default.
        /// </summary>
        Node = 0,

        /// <summary>
        /// Method provides constants for constant GP-nodes.
        /// </summary>
        ConstantProvider,

        /// <summary>
        /// Method is a fitness function/entry point
        /// </summary>
        FitnessFunction,
    }
}
