﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Attributes
{
    /// <summary>
    /// See <see cref="EvoveFieldAttribute"/>
    /// </summary>
    public enum EField
    {
        /// <summary>
        /// Field is ignored by Evove. This is the default.
        /// </summary>
        Ignored,

        /// <summary>
        /// Value can obtained to determine the current test case.
        /// For use with <see cref="ECache.TestCase"/>.
        /// The field must be of type <see cref="int"/>.
        /// </summary>
        TestCase,

        /// <summary>
        /// The value is fixed for all members of the same deme.
        /// The value will be set by Evove and must be marked as readonly, do not set it manually.
        /// Use the <see cref="EvoveFieldAttribute"/> to provide the constructor arguments.
        /// </summary>
        DemeStatic,

        /// <summary>
        /// The value is fixed for all members of population.
        /// The value will be set by Evove and must be marked as readonly, do not set it manually.
        /// Use the <see cref="EvoveFieldAttribute"/> to provide the constructor arguments).
        /// </summary>
        PopulationStatic,

        /// <summary>
        /// The field provides further methods to Evove.
        /// The value will be set by Evove and must be marked as readonly, do not set it manually.
        /// Use the <see cref="EvoveFieldAttribute"/> to provide the constructor arguments.
        /// </summary>
        Extension,
    }
}
