﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Attributes
{
    /// <summary>
    /// Specifies features about this method or delegate field.
    /// (This is not mandatory as all visible methods are included by default.)
    /// </summary>
    [AttributeUsage( AttributeTargets.Method | AttributeTargets.Field )]
    public sealed class EvoveMethodAttribute : Attribute
    {
        /// <summary>
        /// Method usage, see <see cref="EMethod"/>.
        /// </summary>
        public EMethod Role;

        /// <summary>
        /// See <see cref="EvoveParameters.Set"/> for details.
        /// </summary>
        public object Set;

        /// <summary>
        /// Quick way of increasing the chances of this method being selected.
        /// Creates "n" copies.
        /// The minimum value is 1.
        /// </summary>
        public int Duplicate;

        /// <summary>
        /// Cache mode, see <see cref="ECache"/>.
        /// </summary>
        public ECache Cache;
    }

    

   
}
