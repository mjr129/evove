﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove.Attributes
{
    /// <summary>
    /// Indicates the field has some special meaning to Evove.
    /// 
    /// Note: Use <see cref="EvoveMethodAttribute"/> for delegate fields, as these will be seen
    /// by Evove as methods.
    /// </summary>
    [AttributeUsage( AttributeTargets.Field )]
    public sealed class EvoveFieldAttribute : Attribute
    {
        /// <summary>
        /// See <see cref="EField"/> for details.
        /// </summary>
        public EField Use { get; set; }

        /// <summary>
        /// Arguments passed to the constructor of the field when <see cref="Use"/>  is <see cref="EField.DemeStatic"/>,
        /// <see cref="EField.PopulationStatic"/>. The type of the field may also
        /// optionally take a <see cref="DemeArgs"/> or <see cref="PopulationArgs"/> as its FIRST argument.
        /// 
        /// This property is ignored for all other field types.
        /// </summary>
        public object[] ConstructorArguments { get; set; }

        /// <summary>
        /// See <see cref="EvoveParameters.Set"/> for details.
        /// </summary>
        public object Set;

        public EvoveFieldAttribute(EField per)
        {
            this.Use = per;
            this.ConstructorArguments = new object[0];
        }

        public EvoveFieldAttribute( EField per, params object[] constructorArguments )
        {
            this.Use = per;
            this.ConstructorArguments = constructorArguments;
        }
    }
}
