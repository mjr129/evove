﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove
{
    /// <summary>
    /// GP objects usually persist (to avoid excessive new() and GC operations).
    /// If an object want to "reset" prior to evaluation it should implement
    /// this interface.
    /// </summary>
    interface IResettable
    {
        void Reset();
    }
}
