﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Evove
{
    internal sealed class Threader<TResult, TExecutionContext>
        where TExecutionContext : new()
    {
        public delegate TResult ThreaderFunction( TExecutionContext context, int start, int end );
        private ThreaderFunction _function;
        private Details[] _contexts;

        class Details
        {
            public Thread Thread;
            public TExecutionContext Context = new TExecutionContext();
            public int PartitionStart;
            public int PartitionCount;
            public TResult Result;
        }                              

        public Threader( int numberOfThreads, ThreaderFunction function )
        {
            _function = function;

            if (numberOfThreads < 1)
            {
                numberOfThreads = 1;
            }

            _contexts = new Details[numberOfThreads];

            for (int n = 0; n < _contexts.Length; n++)
            {
                _contexts[n] = new Details();
            }
        }

        public void ThreadEntry( object indexAsObject )
        {
            Details details = _contexts[( int)indexAsObject];

            while (true)
            {
                details.Result = _function( details.Context, details.PartitionStart, details.PartitionCount );
            }
        }

        public TResult[] PartitionWorkload( int total )
        {
            // If only 1 thead we can do everything in the main thread
            if (_contexts.Length == 1)
            {
                return new TResult[] { _function( _contexts[0].Context, 0, total ) };
            }

            // 19/4 = 4.75              17/4=4.25
            // 0..5                     0..4
            // 5..10                    4..8
            // 10..15                   8..12
            // 15..20(19)               12..16(17)
            int count = (int)(((double)total / _contexts.Length) + 0.5d);
            int start = 0;

            for (int i = 0; i < _contexts.Length; i++)
            {
                if (i == _contexts.Length - 1)
                {
                    count = total - start;
                }

                Details d = _contexts[i];

                d.PartitionStart = start;
                d.PartitionCount = count;
                d.Thread = new Thread( ThreadEntry );
                d.Thread.Name = "THREADER " + i;
                d.Thread.Start( i );

                start += count;
            }

            for (int i = 0; i < _contexts.Length; i++)
            {
                _contexts[i].Thread.Join();
            }

            return _contexts.Select( z => z.Result ).ToArray();
        }
    }
}
