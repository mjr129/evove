﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;

namespace Evove
{
    public static class EvoveHelper
    {
        public const string PROGRAM_NAME = "EvovƎ";

        public const int MILLISECONDS_IN_ONE_SECOND = 1000;
        public const int SECONDS_IN_ONE_HOUR = 3600;
        public const int SECONDS_IN_ONE_QUARTER_HOUR = 900;
        public const int SECONDS_IN_TEN_MINUTES = 600;
        public const int SECONDS_IN_ONE_MINUTE = 60;
        public const int SECONDS_IN_ONE_HALF_DAY = 43200;
        public const int SECONDS_IN_ONE_DAY = 86400;
        internal const string INTERNAL_ERROR = " This indicates an internal error in Evove. Please consider submitting a bug report.";

        /// <summary>
        /// For getting rid of the warning of the same name in lieu of null.
        /// </summary>                                      
        public static T CS0649<T>()
        {
            return default( T );
        }

        /// <summary>
        /// Wraps a value.
        /// </summary>
        /// <param name="value">Value to wrap</param>
        /// <param name="max">Upper bound (exclusive)</param>
        /// <returns>Wrapped value</returns>
        public static int Wrap( int value, int max )
        {
            value = SafeAbs( value );
            return value % max;
        }

        /// <summary>
        /// Abs without the overflow on int.MinValue.
        /// </summary>                               
        public static int SafeAbs( int value )
        {
            if (value < 0)
            {
                if (value == int.MinValue)
                {
                    value = int.MaxValue;
                }
                else
                {
                    value = -value;
                }
            }

            return value;
        }

        internal static object Percent( double v )
        {   
            return (((int)(v * 10000+0.5)) / 100) + "%";
        }

        /// <summary>
        /// Wraps a value.
        /// </summary>
        /// <param name="value">Value to wrap</param>
        /// <param name="min">Minimum value (inclusive)</param>
        /// <param name="max">Maximum value (exclusive)</param>
        /// <returns>Wrapped value</returns>
        public static int Wrap( int value, int min, int max )
        {
            int range = max - min;


            int result = Wrap( value, range ) + min;

            Debug.Assert( result >= min && result <= max );

            return result;
        }

        /// <summary>
        /// Returns a value between 0 and 1, where 1 indicates the correct class
        /// has been chosen.
        /// </summary>      
        [EvoveIgnore]
        public static double ClassDistance( double predicted, double actual )
        {
            if (double.IsInfinity( predicted ) || double.IsNaN( predicted ))
            {
                return 0.0d;
            }

            double distance = Math.Abs( predicted - actual );

            return Math.Max( 0.0d, 1.0d - distance );
        }   

        /// <summary>
        /// EXTENSION
        /// Returns the name of the [type]. Unlike type.Name this is capable of handling generics.
        /// </summary>                                                                            
        [EvoveIgnore]
        public static string NameOfType( this Type type )
        {
            if (type == null)
            {
                return "(none)";
            }
            else if (type.IsGenericType)
            {
                return type.Name.Substring( 0, type.Name.IndexOf( '`' ) ) + "<" + string.Join( ",", type.GetGenericArguments().Select( NameOfType ) ) + ">";
            }
            else
            {
                return type.Name;
            }
        }

        /// <summary>
        /// Validation function. Checks an enum.
        /// </summary>          
        public static void CheckEnum( string paramater, object value )
        {
            Array values = Enum.GetValues( value.GetType() );

            Check( paramater, values.Cast<object>().Contains( value ), "Value should be a valid member of the enum. One of: " + string.Join( ", ", values ) );
        }

        /// <summary>
        /// Validation function. Checks a value for null.
        /// </summary>        
        internal static void CheckNotNull<T>( string parameter, T value )
            where T : class // Use generics as it prevents accidentally typing CheckNotNull(x, x != null).
        {
            Check( parameter, value != null, "The parameter cannot be null." );
        }

        /// <summary>
        /// Validation function. Checks an assertion.
        /// </summary>          
        public static void Check( string parameter, bool assertion, string message )
        {
            if (!assertion)
            {
                throw new InvalidOperationException( "The value of \"" + parameter + "\" is invalid: " + message );
            }
        }                                            

        /// <summary>
        /// Validation function. Checks an double.
        /// </summary>          
        public static void Check( string parameter, double value, double min, double max )
        {
            Check( parameter, value >= min && value <= max, "The value is " + value + " but it should lie in the range " + min + ".." + max );
        }

        /// <summary>
        /// Validation function. Checks an integer.
        /// </summary>          
        public static void Check( string parameter, int value, int min, int max )
        {
            Check( parameter, value >= min && value <= max, "The value is " + value + " but it should lie in the range " + min + ".." + max );
        }

        public static string ToReadableString( object value )
        {
            if (value == null)
            {
                return string.Empty;
            }

            if (value is IEnumerable && !(value is string))
            {
                IEnumerable<object> v = ((IEnumerable)value).Cast<object>();
                int c = v.Count();

                if (c == 0)
                {
                    return string.Empty;
                }
                else if (c == 1)
                {
                    return ToReadableString( v.First() );
                }

                return "{ " + string.Join( "; ", v.Select( z => ToReadableString( z ) ) ) + " }";
            }

            if (value is Type)
            {
                return NameOfType( (Type)value );
            }

            if (value is double)
            {
                if ((double)value == double.MaxValue)
                {
                    return "double.MaxValue";
                }
                else if ((double)value == double.MinValue)
                {
                    return "double.MinValue";
                }
            }

            if (value is int)
            {
                if ((int)value == int.MaxValue)
                {
                    return "int.MaxValue";
                }
                else if ((int)value == int.MinValue)
                {
                    return "int.MinValue";
                }
            }

            return value.ToString();
        }

        /// <summary>
        /// Returns if a class is an Evove extension. That is if it has an EvoveSupportsAttribute but does not provide
        /// its own fitness function.
        /// </summary>
        public static bool IsEvoveExtension( Type t )
        {
            return IsEvoveClass( t ) && (_GetEvoveFitnessFunctionInternal( t ) == null);
        }

        /// <summary>
        /// Returns if a type has the EvoveSupportsAttribute.
        /// </summary>                                        
        public static  bool IsEvoveClass( Type t )
        {
            return t.GetCustomAttribute<EvoveClassAttribute>( false ) != null;
        }

        /// <summary>
        /// Gets the fitness function, regardless of whether the type is supported by Evove.
        /// </summary>                                                                       
        public static MethodInfo _GetEvoveFitnessFunctionInternal( Type t )
        {
            foreach (MethodInfo method in t.GetMethods())
            {
                var attr = method.GetCustomAttribute<EvoveMethodAttribute>();

                if (attr != null && attr.Role == EMethod.FitnessFunction)
                {
                    return method;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the fitness function if the type provides one to Evove.
        /// </summary>                                                  
        public static  MethodInfo GetEvoveFitnessFunction( Type t )
        {
            return IsEvoveClass( t ) ? _GetEvoveFitnessFunctionInternal( t )  : null;
        }

        /// <summary>
        /// Determines if the class provides a fitness function to Evove.
        /// </summary>                                                  
        public static  bool IsEvoveFitnessFunction( Type t )
        {
            return IsEvoveClass( t ) && (_GetEvoveFitnessFunctionInternal( t ) != null);
        }
    }
}
