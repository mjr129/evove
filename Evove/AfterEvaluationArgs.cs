﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove
{
    public sealed class AfterEvaluationArgs
    {
        public long EvaluationTime { get; private set; }
        public int Round { get; private set; }
        public int TreeSize { get; private set; }
        public IComparable Fitness { get; set; }

        public AfterEvaluationArgs(int round, IComparable _lastResult, int treeSize, long evaluationTime)
        {
            this.Round = round;
            this.Fitness = _lastResult;
            this.TreeSize = treeSize;
            this.EvaluationTime = evaluationTime;
        }
    }
}
