﻿using Evove.Blocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evove
{
    public class MessageArgs
    {
        public readonly string Message;

        public MessageArgs(string message)
        {
            Message = message;
        }
    }

    public enum EStage
    {
        BeforeEvaluation,
        BeforeBreeding,
        EndOfRound,
        EndOfSimulation,
    }

    public class ErrorArgs
    {
        public readonly List<Individual> ProblemIndividuals;
        public readonly string Details;

        public ErrorArgs(List<Individual> problemIndividuals, string details)
        {                       
            this.ProblemIndividuals = problemIndividuals;
            this.Details = details;   
        }
    }

    public class StageChangeArgs
    {                           
        public readonly EStage Stage;    
        public bool StopSimulation;

        public StageChangeArgs(EStage stage)
        {
            this.Stage = stage;    
            this.StopSimulation = false;   
        }
    }         

    /// <summary>
    /// Allows the population progress to be obserbed.
    /// </summary>
    public interface IProgressObserver
    {
        /// <summary>
        /// Sends simple messages on the population.
        /// </summary>                    
        void PopulationMessage(MessageArgs args);

        /// <summary>
        /// In step-through mode notifies the observer that the population is in step-through mode
        /// and is paused until a step command is received.
        /// </summary>                 
        void OnWaitingForStep(StageChangeArgs args);

        void OnError(ErrorArgs args);

        void OnSpecialEvent(ESpecialEvents stop);
    }
}
