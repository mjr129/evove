﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Evove
{
    /// <summary>
    /// A randomiser class, thread-safe.
    /// </summary>
    public sealed class Randomiser
    {
        private ThreadLocal<Random> _generators;
        private readonly int _seed;

        private Random Generator
        {
            get
            {
#if DEBUG
                if (_seed >= 0 && _generators.Values.Count > 1)
                {
                    throw new InvalidOperationException( "Didn't expect more than one random-number generator to be created with the same seed. Please check that multi-threading isn't enabled at the same time as setting a fixed random seed, or that the random number generator hasn't been used by any external threads." );
                }
#endif

                return _generators.Value;
            }
        }

        //
        // Summary:
        //     Returns a random floating-point number that is greater than or equal to 0.0,
        //     and less than 1.0.
        //
        // Returns:
        //     A double-precision floating point number that is greater than or equal to 0.0,
        //     and less than 1.0.
        public double NextDouble()
        {
            return Generator.NextDouble();
        }

        //
        // Summary:
        //     Returns a non-negative random integer.
        //
        // Returns:
        //     A 32-bit signed integer that is greater than or equal to 0 and less than System.Int32.MaxValue.
        public int Next()
        {
            return Generator.Next();
        }

        //
        // Summary:
        //     Returns a non-negative random integer.
        //
        // Returns:
        //     A 32-bit signed integer that is greater than or equal to 0 and less than System.Int32.MaxValue.
        public int Next( int minValue, int maxValue )
        {
            return Generator.Next( minValue, maxValue );
        }

        /// <summary>
        /// CONSTRUCTOR
        /// Please use <see cref="Population.Randomiser"/> to get a thread-safe instance.
        /// </summary>                                                                   
        public Randomiser( int seed )    
        {
            _seed = seed;

#if DEBUG
            _generators = new ThreadLocal<Random>( Factory, true );
#else
            _generators = new ThreadLocal<Random>( Factory, false );
#endif
        }

        private Random Factory()
        {
            if (_seed >= 0)
            {
                return new Random( _seed );
            }
            else
            {
                return new Random();
            }
        }

        public bool Boolean()
        {
            return Generator.Next( 0, 2 ) == 1;
        }

        public T FromList<T>( IReadOnlyList<T> possibilities )
        {
            Debug.Assert( possibilities.Count != 0 );

            return possibilities[Generator.Next( possibilities.Count )];
        }
                    
        /// <summary>
        /// Returns a non-negative random integer that is less than the specified maximum.
        /// </summary>
        /// <param name="maxValue">The exclusive upper bound of the random number to be generated. maxValue must
        ///     be greater than or equal to 0.</param>
        /// <returns>A 32-bit signed integer that is greater than or equal to 0, and less than maxValue;
        ///     that is, the range of return values ordinarily includes 0 but not maxValue. However,
        ///     if maxValue equals 0, maxValue is returned.</returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">maxValue is less than 0.</exception>
        public int Next( int maxValue )
        {
            return Generator.Next( maxValue );
        }

        public bool Chance( double rate )
        {
            return Generator.NextDouble() < rate;
        }

        public void Shuffle<T>( IList<T> list )
        {
            int n = list.Count;

            while (n > 1)
            {
                n--;
                int k = Generator.Next( n + 1 );
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
