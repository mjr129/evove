﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evove.Attributes;
using Evove.Blocks;
using Evove.Blocks.Data;

namespace Evove
{
    /// <summary>
    /// A generic factory (creates objects of the specified type).
    /// 
    /// The type is specified in the Factory constructor.
    /// The type itself should have a constructor capable of taking arguments
    /// specified in the Factory constructor, optionally supplemented with
    /// <typeparamref name="TCtorArg"/> as the first argument.
    /// </summary>
    /// <typeparam name="TCtorArg">The type of the optional constructor argument.</typeparam>
    public sealed class Factory <TCtorArg>    : Factory
    {
        private readonly bool UseTypedConstructor;

        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        /// <param name="type">Type to instantiate</param>
        /// <param name="constructorArguments">Fixed constructor arguments</param>
        public Factory( Type type, object[] constructorArguments )
            : base(type, constructorArguments)
        {   
            UseTypedConstructor = !this.IsStatic && SyntaxManager.HasTypedConstructor( type, typeof( TCtorArg ), constructorArguments );
        }

        /// <summary>
        /// CONSTRUCTOR
        /// </summary>
        /// <param name="type">Type to instantiate (no fixed constructor arguments)</param>
        public Factory( Type type )
            : this( type, new object[0] )
        {
            // NA
        }

        /// <summary>
        /// Implicit conversion from the type alone.
        /// </summary>                              
        public static implicit operator Factory<TCtorArg>( Type type )
        {
            if (type == null)
            {
                return null;
            }

            return new Factory<TCtorArg>( type );
        }

        /// <summary>
        /// Instantiates the class
        /// </summary>
        /// <param name="args">First constructor argument (if constructor available)</param>
        /// <returns></returns>
        public object Instantiate( TCtorArg args )
        {
            if (this.IsStatic)
            {
                return null;
            }
            else if (this.UseTypedConstructor)
            {
                object[] argArray = new object[ConstructorArguments.Length + 1];
                argArray[0] = args;
                Array.Copy( ConstructorArguments, 0, argArray, 1, ConstructorArguments.Length );
                return Activator.CreateInstance( Type, args );
            }
            else
            {
                return Activator.CreateInstance( Type, ConstructorArguments );
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append( EvoveHelper.NameOfType( Type ) );

            sb.Append( "(" );

            if (UseTypedConstructor)
            {
                sb.Append( EvoveHelper.NameOfType( typeof( TCtorArg ) ) );
                sb.Append( "*" );
            }

            sb.Append( EvoveHelper.ToReadableString( ConstructorArguments ) );
            sb.Append( ")" );   

            return sb.ToString();
        }
    }

    /// <summary>
    /// Base class for <see cref="Factory{TCtorArg}"/>.
    /// </summary>
    public abstract class Factory
    {
        public readonly Type Type;
        public readonly object[] ConstructorArguments;

        protected readonly bool IsStatic;        

        public Factory( Type type, object[] constructorArguments )
        {
            EvoveHelper.CheckNotNull( nameof( type ), type );
            EvoveHelper.CheckNotNull( nameof( constructorArguments ), constructorArguments );

            Type = type;
            ConstructorArguments = constructorArguments;
            IsStatic = type.IsAbstract;                                          
        }            
    }
}
