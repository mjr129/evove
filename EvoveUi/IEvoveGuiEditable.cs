﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvoveUi
{
    public interface IEvoveGuiEditable
    {
        /// <summary>
        /// Edits the object in the GUI.
        /// </summary>
        /// <param name="owner">Parent window (may be null)</param>
        /// <returns>The edited object, which may or may not be the same object. Return null on cancel.</returns>
        IEvoveGuiEditable EditInGui( IWin32Window owner );
    }
}
