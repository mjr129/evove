﻿using Evove.Blocks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Evove;
using Evove.Operators.Breed.Crossover;
using Evove.Operators.Breed.Mutation;
using Evove.Operators.Breed;
using MCharting;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;

namespace EvoveUi
{
    internal partial class FrmMain : Form, IProgressObserver
    {
        Population _population;
        private EvoveParameters _parameters;
        private Individual _displayed;
        private Individual _crossover1;
        private Individual _mother;
        private Individual _father;
        private Individual _brother;
        private Individual _sister;
        private Node _swap1;
        private StringBuilder _debugLog = new StringBuilder();

        public readonly List<int> _specialEvents = new List<int>();

        private readonly ManualResetEvent _stepEvent = new ManualResetEvent( false );

        Stopwatch _lastOnRoundComplete = Stopwatch.StartNew();

        List<PlotSet> _fitness = new List<PlotSet>();
        List<PlotSet> _time = new List<PlotSet>();
        List<PlotSet> _size = new List<PlotSet>();
        List<PlotSet> _rounds = new List<PlotSet>();

        object _latestBest2sgpFitnessLock = new object();
        int[] _latestBest2sgpFitnessCounts;
        int _latestBest2sgpFitnessRound;

        private volatile bool _unattendedMode;
        private volatile bool _stepThrough;
        private volatile bool _isStopping;
        private volatile bool _isEnded;

        private volatile bool _isWaiting;

        private Exception _exception;
        private volatile int _breakpoint;
        private bool _errorStatus;

        private int _plotDelayInMilliseconds;
        private volatile bool _autoPlot = false;
        private bool _allowClose;

        int PlotDelayInMilliseconds
        {
            get
            {
                return _plotDelayInMilliseconds;
            }
            set
            {
                _plotDelayInMilliseconds = value;
                _chkAutoPlot.ToolTipText = "Automatically replots every " + PlotDelayInMilliseconds + " milliseconds.";
            }
        }

        class PlotSet
        {
            public readonly double Min = double.MaxValue;
            public readonly double Max = double.MinValue;
            public readonly double Average = 0;
            public readonly double Elite;
            private readonly double[] All;
            public readonly double Count;

            public double this[int index]
            {
                get
                {
                    if (All != null)
                    {
                        return All[index];
                    }
                    else
                    {
                        return Min;
                    }
                }
            }

            public PlotSet( int round, int count )
            {
                Min = round;
                Max = round;
                Average = round;
                Elite = round;
                All = null;
                Count = count;
            }

            public PlotSet( IEnumerable<Individual> all, Individual elite, Func<Individual, double> convertor )
            {
                All = all.Select( convertor ).ToArray();
                Elite = convertor( elite );

                foreach (double n in All)
                {
                    if (n < Min)
                    {
                        Min = n;
                    }

                    if (n > Max)
                    {
                        Max = n;
                    }

                    Average += n;
                }

                Count = All.Length;
                Average /= Count;
            }
        }

        internal static void Start( IWin32Window owner, EvoveParameters runParams, bool automated )
        {
            Debug.Assert( runParams != null );

            using (FrmMain frm = new FrmMain( runParams, automated ))
            {
                frm.ShowDialog( owner );
            }
        }

        private FrmMain()
        {
            InitializeComponent();
            this.Icon = Evove.Properties.Resources.Icon;

            PlotDelayInMilliseconds = 2000;

            SetupListView( _lstCurrent );
            SetupListView( _lstPrevious );
            SetupListView( _lstPreviousIndividuals );
            SetupListView( _lstProblemIndividuals );
            SetupListView( listView3 );

            CreateListViewHeaders( _lstCurrent );
            CreateListViewHeaders( _lstPreviousIndividuals );
            CreateListViewHeaders( _lstProblemIndividuals );
            listView3.Columns.Add( "Log" );

            this.Text = EvoveHelper.PROGRAM_NAME + " GUI";
            this.label3.Text = EvoveHelper.PROGRAM_NAME + " by Martin Rusilowicz. The University of York. 2016. " + EvoveHelper.PROGRAM_NAME + " is free software.";

#if !DEBUG
            this._lblIDebugging.Visible = false;
#endif
        }

        private void SetupListView( ListView lv )
        {
            lv.FullRowSelect = true;
            lv.HideSelection = false;
            lv.MultiSelect = false;
            lv.GridLines = true;
            lv.SmallImageList = imageList1;
            lv.LargeImageList = imageList1;
        }

        public FrmMain( EvoveParameters parameters, bool unattendedMode )
                    : this()
        {
            if (parameters.ProgressWatcher != null)
            {
                throw new InvalidOperationException( nameof( EvoveParameters ) + "." + nameof( EvoveParameters.ProgressWatcher ) + " must be null when used in GUI mode." );
            }

            parameters.ProgressWatcher = this;
            _chkUnattendedMode.Checked = unattendedMode;
            Debug.Assert( parameters != null );
            this._parameters = parameters;

            UpdateButtonStates();

            Text += " - " + string.Join( ", ", parameters.FitnessFunction.ToString() );
        }

        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );

            if (_unattendedMode)
            {
                StartNow();
            }
        }

        protected override void OnClosing( CancelEventArgs e )
        {
            base.OnClosing( e );

            if (!_allowClose)
            {
                if (_tabTabControl.SelectedTab != _pgLock)
                {   
                    MessageBox.Show( this, "The window cannot be closed whilst the simulation is still running." );
                }

                e.Cancel = true;
            }
        }

        protected override void OnClosed( EventArgs e )
        {
            base.OnClosed( e );

            if (_population != null)
            {   
                _population.Dispose();
            }

            _parameters.ProgressWatcher = null;
        }

        private void CreateListViewHeaders( ListView lv )
        {
            lv.Columns.Clear();
            lv.Columns.Add( "" );
            lv.Columns.Add( "Unique Identifier" );
            lv.Columns.Add( "Deme index" );
            lv.Columns.Add( "Fitness" );
            lv.Columns.Add( "Size of tree" );
            lv.Columns.Add( "Evaluation time (ns)" );
            lv.Columns.Add( "Breed time (ns)" );

            lv.AutoResizeColumns( ColumnHeaderAutoResizeStyle.HeaderSize );
        }

        private void UpdateListViewItem( ListViewItem lvi, Individual obj )
        {
            lvi.SubItems.Clear();
            lvi.ImageIndex = obj.LatestResult != null ? 1 : 0;
            lvi.SubItems.Add( obj.Uid.ToString() );
            lvi.SubItems.Add( obj.Deme.ToString() );

            if (obj.LatestResult == null)
            {
                lvi.SubItems.Add( "(not evaluated)" );
                lvi.SubItems.Add( "(not evaluated)" );
                lvi.SubItems.Add( "(not evaluated)" );
            }
            else
            {
                if (obj.LatestResult.WasKilled)
                {
                    lvi.SubItems.Add( "(TERMINATED)" );
                }
                else if (obj.LatestResult.Fitness == null)
                {
                    lvi.SubItems.Add( "(MISSING)" );
                }
                else
                {
                    lvi.SubItems.Add( obj.LatestResult.Fitness.ToString() );
                }

                lvi.SubItems.Add( obj.LatestResult.TreeSize.ToString() );
                lvi.SubItems.Add( obj.LatestResult.EvaluationTime.ToString() );
            }

            lvi.SubItems.Add( obj.BreedTime.ToString() );
            lvi.Tag = obj;
        }

        /// <summary>
        /// IProgressObserver: Log received
        /// </summary>      
        void IProgressObserver.PopulationMessage( MessageArgs args )
        {
            if (InvokeRequired)
            {
                BeginInvoke( (Action<MessageArgs>)((IProgressObserver)this).PopulationMessage, args );
                return;
            }

            ListViewItem lvi = new ListViewItem( args.Message );

            this.listView3.Items.Add( lvi );

            lvi.EnsureVisible();

            _lblLastLog.Text = args.Message;
        }

        /// <summary>
        /// IProgressObserver: Special events
        /// </summary>                       
        void IProgressObserver.OnSpecialEvent( ESpecialEvents specialEvent )
        {
            lock (_specialEvents)
            {
                _specialEvents.Add( _population.Round );
            }
        }

        /// <summary>
        /// IProgressObserver: Stage changes
        /// </summary>                    
        void IProgressObserver.OnWaitingForStep( StageChangeArgs args )
        {
            switch (args.Stage)
            {
                case EStage.BeforeBreeding:
                    WaitOnStepList( "Before breed" );
                    break;

                case EStage.BeforeEvaluation:
                    WaitOnStepList( "Before evaluate" );
                    break;

                case EStage.EndOfRound:
                    RecordRoundInfo();
                    break;

                case EStage.EndOfSimulation:   
                    break;

                default:
                    break;
            }

            args.StopSimulation = _isStopping;
        }

        private void WaitOnStepList( string message )
        {
            if (_stepThrough)
            {
                Individual[] toShow = CreateListAndBackrecord( _population.AllIndividuals(), _population.Round, message );
                UpdateListView( _lstCurrent, toShow );
                WaitForManualResetEvent();
            }
        }

        void IProgressObserver.OnError( ErrorArgs args )
        {
            if (_unattendedMode)
            {
                return;
            }

            Invoke( (MethodInvoker)delegate ()
             {
                 _txtError.Text = args.Details;
             } );

            Individual[] toShow = CreateListAndBackrecord( args.ProblemIndividuals, _population.Round, "Error occured" );
            UpdateListView( _lstProblemIndividuals, toShow );

            _stepThrough = true;
            _errorStatus = true;
            WaitForManualResetEvent();
        }

        private void RecordRoundInfo() // POPULATION THREAD
        {
            IEnumerable<Individual> all = _population.AllIndividuals();
            Individual best = _population.BestIndividual;

            _fitness.Add( new PlotSet( all, best, z => Convert.ToDouble( z.LatestResult.Fitness ) ) );
            _time.Add( new PlotSet( all, best, z => Convert.ToDouble( z.LatestResult.EvaluationTime ) ) );
            _size.Add( new PlotSet( all, best, z => Convert.ToDouble( z.LatestResult.TreeSize ) ) );
            _rounds.Add( new PlotSet( _population.Round, all.Count() ) );

            lock (_latestBest2sgpFitnessLock)
            {
                I2StageGp x = (I2StageGp)_population.PerPopulationObjects.FirstOrDefault( z => z is I2StageGp );

                int[] counts = x?.Counts;

                if (counts != null)
                {
                    _latestBest2sgpFitnessCounts = counts;
                    _latestBest2sgpFitnessRound = _population.Round;
                }
            }

            if (_lastOnRoundComplete.ElapsedMilliseconds < PlotDelayInMilliseconds || !_autoPlot)
            {
                return;
            }

            Invoke( (Action)delegate () // not BeginInvoke or the UI becomes saturated
            {
                Stopwatch timeTakenToPlot = Stopwatch.StartNew();

                if (_tabTabControl.SelectedTab == tabPage5)
                {
                    _btnRefreshPlot.PerformClick();
                }

                if (timeTakenToPlot.ElapsedMilliseconds > (PlotDelayInMilliseconds / 2))
                {
                    PlotDelayInMilliseconds *= 2;
                }
            } );

            // Restart timer AFTER plotting (otherwise if plotting takes the full time we will replot immediately)
            _lastOnRoundComplete.Restart();
        }

        private void UpdateButtonStates()
        {
            _btnPlay.Enabled = _btnPlay2.Enabled = _btnPlay3.Enabled
                = _population != null && _stepThrough && !_errorStatus;

            _btnPause.Enabled = _btnPause2.Enabled = _btnPause3.Enabled
                = _population != null && !_stepThrough;

            _btnStop.Enabled = _btnStop2.Enabled = _btnStop3.Enabled
                = _population != null && !_isEnded && !_errorStatus;

            _btnStep.Enabled = _btnStep2.Enabled = _btnStep3.Enabled
                = _population != null && _isWaiting && !_errorStatus;

            _btnBreakpoint.Enabled = _btnBreakpoint2.Enabled = _btnBreakpoint3.Enabled
                = _population == null || _isWaiting && !_errorStatus;

            _btnEditParameters.Text = "  " + (_btnEditParameters2.Text = _btnEditParameters3.Text
                = (_population == null ? "Edit parameters..." : "View parameters..."));

            _btnStart.Enabled = _btnStart2.Enabled = _btnStart3.Enabled
                = _population == null;

            _lblIPaused.Visible = _isWaiting;
            _lblIStepThrough.Visible = _stepThrough;
            _lblIAutomatic.Visible = _unattendedMode;
            _lblIError.Visible = _errorStatus;
            _lblINoSession.Visible = _population == null;
            _lblIStopped.Visible = _isEnded;
            _lblIStopping.Visible = _isStopping;
            _lblIBusy.Visible = !_isWaiting && !_isEnded && _population != null;

            _btnIgnoreError.Enabled = _errorStatus;
        }

        private void WaitForManualResetEvent()
        {
            _isWaiting = true;
            this._stepEvent.Reset();
            Invoke( (MethodInvoker)UpdateButtonStates );
            this._stepEvent.WaitOne();
            _isWaiting = false;
        }

        void UpdatePopulationList( bool evaluated )
        {
            Individual[] toShow;

            if (evaluated)
            {
                toShow = CreateListAndBackrecord( _population.AllIndividuals(), _population.Round, "After breed" );
            }
            else
            {
                toShow = CreateListAndBackrecord( _population.AllIndividuals(), _population.Round, "After evaluation" );
            }

            ClearCurrentGenerationList();

            if (_population == null)
            {
                return;
            }

            _lblPopulation.Text = "GENERATION " + _population.Round;
            _lblPreviousPopulation.Text = "GENERATION " + (_population.Round - 1);
            UpdateListView( _lstCurrent, toShow );
        }

        private Individual[] CreateListAndBackrecord( IEnumerable<Individual> allIndividuals, int round, string description )
        {
            Individual[] copy = allIndividuals.Select( z => z = new Individual( z, false ) ).ToArray();

            ListViewItem lvi = new ListViewItem( round.ToString() );
            lvi.SubItems.Add( description );
            lvi.Tag = copy;
            lvi.Selected = true;
            lvi.EnsureVisible();
            _lstPrevious.Items.Add( lvi );

            return copy;
        }

        private void button1_Click( object sender, EventArgs e )
        {
            Debugger.Break();
        }

        void PopulateSyntaxTree()
        {
            if (_population == null)
            {
                return;
            }

            _txtSyntax.Text = "SYNTAX TREE";

            var sm = _population.SyntaxManager;

            treeView1.Nodes.Clear();

            if (!_parameters.AllowCaching)
            {
                _btnEnableCaching.Visible = true;
            }

            IReadOnlyList<GpType> types = _population.SyntaxManager.GpTypes;

            foreach (GpType t in types)
            {
                TreeNode parent;

                parent = treeView1.Nodes.Add( t.ToString() );
                parent.ForeColor = _lblDataType.ForeColor;

                if (_chkProviders.Checked)
                {
                    foreach (GpFunction element in t.OutputFunctions)
                    {
                        TreeNode input = parent.Nodes.Add( "● " + element.ToString() );
                        input.Tag = element;

                        AddParamaters( input, element );
                    }
                }

                if (_chkReceivers.Checked)
                {
                    foreach (GpFunction element in t.InputFunctions)
                    {
                        TreeNode output = parent.Nodes.Add( "○ " + element.ToString() );

                        output.ForeColor = _lblReceiver.ForeColor;

                        AddParamaters( output, element );
                    }
                }
            }

            toolStripComboBox1.Items.Add( "Summary" );

            for (int n = 0; n < _population.SyntaxManager.NumberOfTestCases; n++)
            {
                toolStripComboBox1.Items.Add( n );
            }

            toolStripComboBox1.SelectedIndex = 0;
        }

        private void AddParamaters( TreeNode treeNode, GpFunction element )
        {
            foreach (ParameterType parameter in element.ParameterTypes)
            {
                treeNode.Nodes.Add( parameter.Name );

                treeNode.ForeColor = _lblProvider.ForeColor;
            }
        }

        private void _btnStart_Click( object sender, EventArgs e )
        {
            if (Directory.Exists( _parameters.OutputFolder ))
            {
                if (_stepThrough)
                {
                    repeatDir:

                    switch (MessageBox.Show( this, "The output directory already exists: \"" + _parameters.OutputFolder + "\". Do you want to delete it?", Text, MessageBoxButtons.OKCancel ))
                    {
                        case DialogResult.OK:
                            try
                            {
                                Directory.Delete( _parameters.OutputFolder, true );
                            }
                            catch
                            {
                                goto repeatDir;
                            }
                            break;

                        case DialogResult.Cancel:
                        default:
                            return;
                    }
                }
            }

            StartNow();
        }

        private void StartNow()
        {
            int n = 0;
            string orig = _parameters.OutputFolder;

            while (Directory.Exists( _parameters.OutputFolder ))
            {
                n++;
                _parameters.OutputFolder = orig + " [" + n + "]";
            }

            Debug.Assert( _population == null );
            Debug.Assert( !_isEnded );

            _stepThrough = !_unattendedMode;

            ClearCurrentGenerationList();

            backgroundWorker1.RunWorkerAsync();
        }

        /// <summary>
        /// Runs the population!
        /// </summary>          
        private void backgroundWorker1_DoWork( object sender, DoWorkEventArgs e )
        {
            try
            {
                Thread.CurrentThread.Name = "POPULATION THREAD";

                _population = new Population( _parameters );

                Invoke( (Action)PopulateSyntaxTree );
                Invoke( (Action)UpdateButtonStates );

                _population.Start();
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }

        private void _btnPlay_Click( object sender, EventArgs e )
        {
            ClearCurrentGenerationList();

            Debug.Assert( _population != null );
            Debug.Assert( _stepThrough );
            Debug.Assert( !_isEnded );

            ExitAnyWaiting();
            _stepThrough = false;
            UpdateButtonStates();
        }

        private void ExitAnyWaiting()
        {
            if (_isWaiting)
            {
                this._isWaiting = false;
                this._stepEvent.Set();
            }
        }

        private void ClearCurrentGenerationList()
        {
            _lblPopulation.Text = "The simulation must be paused before examining the current generation";
            _lstCurrent.Items.Clear();
        }

        private void _btnPause_Click( object sender, EventArgs e )
        {
            ClearCurrentGenerationList();

            Debug.Assert( _population != null );
            Debug.Assert( !_stepThrough );
            Debug.Assert( !_isWaiting );
            Debug.Assert( !_isEnded );

            _stepThrough = true;
            UpdateButtonStates();
        }

        private void _btnStop_Click( object sender, EventArgs e )
        {
            ClearCurrentGenerationList();

            Debug.Assert( _population != null );
            Debug.Assert( !_isEnded );

            ExitAnyWaiting();
            _isStopping = true;
            _stepThrough = false;

            UpdateButtonStates();
        }

        private void backgroundWorker1_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            _chkUnattendedMode.Enabled = false;
            _chkAttendedMode3.Enabled = false;
            _isEnded = true;

            UpdateButtonStates();

            _allowClose = true;

            if (_unattendedMode)
            {   
                Close();
                return;
            }

            if (_exception != null)
            {
                MessageBox.Show( this, _exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
        }

        private void listView1_ItemActivate( object sender, EventArgs e )
        {
            ListView listView = (ListView)sender;

            if (listView.SelectedItems.Count != 0)
            {
                Individual individual = (Individual)listView.SelectedItems[0].Tag;
                Display( individual );
            }
        }

        private void Display( Individual individual )
        {
            _tvwIndividual.Nodes.Clear();

            if (individual == null)
            {
                _lblIndividual.Text = "SELECT AN INDIVIDUAL FROM THE CURRENT OR PREVIOUS ROUNDS TO EXAMINE IT";
                return;
            }

            _displayed = individual;
            _lblIndividual.Text = individual.Uid.ToString();

            _tvwIndividual.Visible = false;
            AddNode( _displayed.Genome.GetRoot(), _tvwIndividual.Nodes, "IComparable FITNESS" );

            _tvwIndividual.ExpandAll();
            _tvwIndividual.Nodes[0].EnsureVisible();
            _tvwIndividual.Visible = true;

            _tabTabControl.SelectedTab = _tabIndividual;
        }

        private void AddNode( Node gpNode, TreeNodeCollection treeNodes, string prefix )
        {
            TreeNode treeNode;

            if (_chkSimpleView.Checked)
            {
                if (gpNode.Function.IsConstant)
                {
                    treeNode = treeNodes.Add( prefix + " ➨ " + gpNode.ConstantValue );
                }
                else
                {
                    treeNode = treeNodes.Add( prefix + " ➨ " + ((gpNode.Function.Field != null) ? gpNode.Function.Field.Name : gpNode.Function.Method.Name) );
                }
            }
            else
            {
                treeNode = treeNodes.Add( "【" + gpNode.TrackingId + "】 " + prefix + " ➨ " + gpNode.Function.RelativeName( gpNode, false ) );
            }
            treeNode.Tag = gpNode;

            switch (gpNode.Cache.Status)
            {
                case EEvaluatedStatus.None:
                    treeNode.Text += " [Not evaluated]";
                    treeNode.ImageKey = "EMPTY";
                    treeNode.ForeColor = _lblNotEvaluated.ForeColor;
                    break;

                case EEvaluatedStatus.Delegated:
                    treeNode.Text += " [Delegated, not evaluated]";
                    treeNode.ImageKey = "DELEGATED";
                    treeNode.ForeColor = _lblDeferred.ForeColor;
                    break;

                case EEvaluatedStatus.Evaluated:
                    treeNode.ImageKey = "EVALUATED";
                    treeNode.ForeColor = _lblEvaluated.ForeColor;
                    break;

                case EEvaluatedStatus.Flagged:
                    treeNode.ImageKey = "FLAGGED";
                    treeNode.Text += " [Flagged: " + gpNode.Cache.ClearReason + "]";
                    treeNode.ForeColor = _lblFlagged.ForeColor;
                    break;
            }

            treeNode.SelectedImageKey = treeNode.ImageKey;

            object singleValue = gpNode.Cache.InspectValue();

            if (singleValue != null)
            {
                treeNode.Text += " = Always " + AsString( singleValue );
            }

            object[] multiValue = gpNode.Cache.InspectMultiValue();

            if (multiValue != null)
            {
                int filledOut = 0;

                for (int n = 0; n < multiValue.Length; n++)
                {
                    if (multiValue[n] != null)
                    {
                        filledOut++;
                    }
                }

                object item = toolStripComboBox1.SelectedItem;

                if (item is int)
                {
                    int index = (int)item;
                    if (index >= multiValue.Length)
                    {
                        treeNode.Text += " = Multiple values [" + index + "] = No such cache";
                    }
                    else
                    {
                        treeNode.Text += " = Multiple values [" + index + "] = " + AsString( multiValue[index] );
                    }
                }
                else
                {
                    treeNode.Text += " = Multiple values (" + filledOut + " of " + multiValue.Length + " evaluated)";
                }

                treeNode.ForeColor = _lblEvaluated2.ForeColor;
            }

            for (int childIndex = 0; childIndex < gpNode.Children.Length; childIndex++)
            {
                AddNode( gpNode.Children[childIndex], treeNode.Nodes, gpNode.Function.ParameterTypes[childIndex].ParameterName );
            }
        }

        private string AsString( object value )
        {
            if (value == null)
            {
                return "(not evaluated)";
            }

            return value.ToString();
        }

        private void _btnRunIndividual_Click( object sender, EventArgs e )
        {
            if (_displayed != null && _crossover1 != _displayed)
            {
                Individual a = new Individual( _crossover1 );
                Individual b = new Individual( _displayed );

                new CrossoverOperators.CommonPoint().Crossover( new CrossoverArgs( _population, a, b, _parameters.MaxTreeDepth ) );

                SetFamily( _crossover1, _displayed, a, b );
            }

            CancelSelection();
        }

        private void SetFamily( Individual m, Individual f, Individual s, Individual b )
        {
            _mother = m;
            _father = f;
            _sister = s;
            _brother = b;

            _btnMother.Enabled = m != null;
            _btnFather.Enabled = f != null;
            _btnSister.Enabled = s != null;
            _btnBrother.Enabled = b != null;
        }

        private void _btnRun_Click( object sender, EventArgs e )
        {
            if (_displayed != null)
            {
                _displayed.Evaluate( _population );

                Display( _displayed );
            }
        }

        private void _btnCrossover1_Click( object sender, EventArgs e )
        {
            CancelSelection();

            if (_displayed != null)
            {
                _crossover1 = _displayed;
                _btnCrossover1.Text = "x " + _crossover1.Uid.ToString();
                _btnCrossover2.Enabled = true;

                SetFamily( _crossover1, null, null, null );

                _btnCancel.Enabled = true;
            }
        }

        private void _btnSwap1_Click( object sender, EventArgs e )
        {
            CancelSelection();

            if (_tvwIndividual.SelectedNode != null)
            {
                Node node = (Node)_tvwIndividual.SelectedNode.Tag;

                _swap1 = node;
                _crossover1 = _displayed;
                _btnSwap1.Text = "x " + node.Index + "," + node.Depth;
                _btnSwap2.Enabled = true;

                _btnCancel.Enabled = true;
            }
        }

        private void _btnMutate_Click( object sender, EventArgs e )
        {
            if (_displayed != null)
            {
                Individual m = _crossover1;
                Individual c = new Individual( m );

                new MutationOperators.PerNode( 0.05, 0.33, 0.33, 0.33 ).Mutate( new MutateArgs( _population, c, _parameters.MaxTreeDepth ) );

                SetFamily( m, null, c, null );
            }

            CancelSelection();
        }

        private void toolStripButton1_Click( object sender, EventArgs e )
        {
            if (_displayed != null)
            {
                Individual m = _crossover1;
                Individual c = new Individual( m );

                new MutationOperators.SinglePoint( 0.5 ).Mutate( new MutateArgs( _population, c, _parameters.MaxTreeDepth ) );

                SetFamily( m, null, c, null );
            }

            CancelSelection();
        }

        private void _btnSwap2_Click( object sender, EventArgs e )
        {
            if (_tvwIndividual.SelectedNode != null && _swap1 != null)
            {
                Individual ra = new Individual( _crossover1 );
                Individual rb = new Individual( _displayed );

                Node a = ra.FindEquivalentNode( _swap1 );
                Node b = rb.FindEquivalentNode( (Node)_tvwIndividual.SelectedNode.Tag );

                CrossoverOperators.CommonPoint.CrossoverSwapPositions( a, b );

                SetFamily( _crossover1, _displayed, ra, rb );
            }

            CancelSelection();
        }

        private void toolStripButton2_Click( object sender, EventArgs e )
        {
            CancelSelection();
        }

        private void CancelSelection()
        {
            _crossover1 = null;
            _swap1 = null;
            _btnCrossover2.Enabled = false;
            _btnSwap2.Enabled = false;
        }

        private void _btnInsert_Click( object sender, EventArgs e )
        {
            if (_tvwIndividual.SelectedNode != null)
            {
                Individual m = new Individual( _displayed );
                Node a = m.FindEquivalentNode( (Node)_tvwIndividual.SelectedNode.Tag );

                MutationOperators.PerNode.MutateViaInsertion( new MutateArgs( _population, m, _parameters.MaxTreeDepth ), a );

                SetFamily( _displayed, null, m, null );
            }

            CancelSelection();
        }

        private void _btnReorder_Click( object sender, EventArgs e )
        {
            if (_tvwIndividual.SelectedNode != null)
            {
                Individual m = new Individual( _displayed );
                Node a = m.FindEquivalentNode( (Node)_tvwIndividual.SelectedNode.Tag );

                MutationOperators.PerNode.MutateViaReordering( new MutateArgs( _population, m, _parameters.MaxTreeDepth ), a );

                SetFamily( _displayed, null, m, null );
            }

            CancelSelection();
        }

        private void _btnMother_Click( object sender, EventArgs e )
        {
            Display( _mother );
        }

        private void _btnFather_Click( object sender, EventArgs e )
        {
            Display( _father );
        }

        private void _btnSister_Click( object sender, EventArgs e )
        {
            Display( _sister );
        }

        private void _btnBrother_Click( object sender, EventArgs e )
        {
            Display( _brother );
        }

        private void toolStripComboBox1_Click( object sender, EventArgs e )
        {

        }

        private void toolStripComboBox1_SelectedIndexChanged( object sender, EventArgs e )
        {
            Display( _displayed );
        }

        private void _btnEnableCaching_Click( object sender, EventArgs e )
        {

        }

        private void treeView2_AfterSelect( object sender, TreeViewEventArgs e )
        {
            if (_tvwIndividual.SelectedNode == null)
            {
                return;
            }

            Node node = (Node)_tvwIndividual.SelectedNode.Tag;

            _lstNodeDetails.Items.Clear();

            AddNodeDetail( _lstNodeDetails, "CachedValue", node.Cache.InspectValue() != null );
            AddNodeDetail( _lstNodeDetails, "CachedValues", node.Cache.InspectMultiValue() != null );
            AddNodeDetail( _lstNodeDetails, "Children", node.Children.Length );
            AddNodeDetail( _lstNodeDetails, "Depth", node.Depth );
            AddNodeDetail( _lstNodeDetails, "Element", node.Function.Name );
            AddNodeDetail( _lstNodeDetails, "Parameter", node.ConstantValue );
            AddNodeDetail( _lstNodeDetails, "EvaluatedStatus", node.Cache.Status );
            AddNodeDetail( _lstNodeDetails, "Index", node.Index );
            AddNodeDetail( _lstNodeDetails, "Parent", node.Parent != null );
            AddNodeDetail( _lstNodeDetails, "CacheStorage", node.Cache.Mode );
            AddNodeDetail( _lstNodeDetails, "Uid", node.TrackingId );
        }

        private void AddNodeDetail( ListView listView, string p1, object p2 )
        {
            listView.Items.Add( p1 ).SubItems.Add( p2 != null ? p2.ToString() : "(null)" );
        }

        private void _btnShowSyntax_Click( object sender, EventArgs e )
        {
            if (_tvwIndividual.SelectedNode == null)
            {
                return;
            }

            Node node = (Node)_tvwIndividual.SelectedNode.Tag;

            _tabTabControl.SelectedTab = _tabFunctions;

            FindInSyntax( treeView1.Nodes, node.Function );
        }

        private bool FindInSyntax( TreeNodeCollection treeNodeCollection, GpFunction element )
        {
            foreach (TreeNode treeNode in treeNodeCollection)
            {
                if (treeNode.Tag == element)
                {
                    TreeNode parent = treeNode.Parent;

                    while (parent != null)
                    {
                        parent.Expand();
                        parent = parent.Parent;
                    }

                    treeNode.EnsureVisible();
                    treeView1.SelectedNode = treeNode;
                    treeView1.Focus();
                    return true;
                }

                if (FindInSyntax( treeNode.Nodes, element ))
                {
                    return true;
                }
            }

            return false;
        }

        private void treeView1_AfterSelect( object sender, TreeViewEventArgs e )
        {
            if (treeView1.SelectedNode == null)
            {
                return;
            }

            GpFunction element = (GpFunction)treeView1.SelectedNode.Tag;

            if (element == null)
            {
                return;
            }

            _lstSyntaxDetails.Items.Clear();

            AddNodeDetail( _lstSyntaxDetails, "CacheMode", element.CacheMode );
            AddNodeDetail( _lstSyntaxDetails, "IsConstant", element.IsConstant );
            AddNodeDetail( _lstSyntaxDetails, "Field", element.Field );
            AddNodeDetail( _lstSyntaxDetails, "Id", element.Id );
            AddNodeDetail( _lstSyntaxDetails, "Method", element.Method );
            AddNodeDetail( _lstSyntaxDetails, "Parameters", element.ParameterTypes.Length );
            AddNodeDetail( _lstSyntaxDetails, "Return type", element.ReturnType );
        }

        private void _btnHistory_Click( object sender, EventArgs e )
        {
            if (_displayed != null)
            {
                MessageBox.Show( this, _displayed.History.ToString(), "History of " + _displayed.Uid.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Information );
            }
        }

        private void _btnDetails_Click( object sender, EventArgs e )
        {
            if (_displayed == null)
            {
                return;
            }

            FrmTreeView.Show( this, _displayed.LatestResult.Fitness );
        }

        private void _btnStep_Click( object sender, EventArgs e )
        {
            ClearCurrentGenerationList();

            Debug.Assert( _isWaiting );

            _isWaiting = false;
            _stepEvent.Set();
            UpdateButtonStates();
        }

        private void UpdateListView( ListView list, Individual[] contents )
        {
            if (InvokeRequired)
            {
                BeginInvoke( (Action<ListView, Individual[]>)UpdateListView, list, contents );
                return;
            }

            if (contents == null)
            {
                return;
            }

            list.Items.Clear();

            list.Visible = false;

            foreach (Individual r in contents.OrderByDescending( z => z.LatestResult?.Fitness ))
            {
                ListViewItem lvi = new ListViewItem();

                UpdateListViewItem( lvi, r );

                list.Items.Add( lvi );
            }

            list.Visible = true;
        }

        private void _chkProviders_CheckedChanged( object sender, EventArgs e )
        {
            PopulateSyntaxTree();
        }

        private void _chkReceivers_CheckedChanged( object sender, EventArgs e )
        {
            PopulateSyntaxTree();
        }

        private void _btnEnableCaching_Click_1( object sender, EventArgs e )
        {
            if (_population.SyntaxManager.IsCachingSupported())
            {
                _parameters.AllowCaching = true;
                _btnEnableCaching.Visible = false;
            }
            else
            {
                MessageBox.Show( this, "Cannot enable caching because the one or more target types do not support it." );
                _btnEnableCaching.Enabled = false;
            }
        }

        private void _btnClearTreeCache_Click( object sender, EventArgs e )
        {
            if (_displayed != null)
            {
                _displayed.ClearCache( "User request" );

                Display( _displayed );
            }
        }

        private void _btnClearNodeCache_Click( object sender, EventArgs e )
        {
            if (_tvwIndividual.SelectedNode != null)
            {
                Node a = (Node)_tvwIndividual.SelectedNode.Tag;

                a.CacheMarkAsDirty( "User request @ " + a.TrackingId );

                Display( _displayed );
            }
        }

        private void _btnEditParameters_Click( object sender, EventArgs e )
        {
            var p = FrmParameterEditor.Show( this, _parameters, _population != null );

            if (p != null)
            {
                Debug.Assert( _population == null );
                _parameters = p;
            }
        }

        private void _chkAutoClose_CheckedChanged( object sender, EventArgs e )
        {
            _unattendedMode = _chkUnattendedMode.Checked;
            UpdateButtonStates();
        }

        private void _btnIgnoreError_Click( object sender, EventArgs e )
        {
            _errorStatus = false;
            UpdateButtonStates();
        }

        private void _lstPrevious_ItemActivate( object sender, EventArgs e )
        {
            if (_lstPrevious.SelectedItems.Count == 1)
            {
                var item = _lstPrevious.SelectedItems[0];
                Individual[] tag = (Individual[])item.Tag;

                toolStripLabel12.Text = "ROUND " + item.Text + " - " + item.SubItems[1].Text;

                UpdateListView( _lstPreviousIndividuals, tag );
            }
        }

        private void attendedModeToolStripMenuItem_Click( object sender, EventArgs e )
        {
            _chkUnattendedMode.Checked = !_chkUnattendedMode.Checked;
        }

        private void _btnBreakpoint_Click( object sender, EventArgs e )
        {
            _breakpoint = FrmBreakpoint.Show( this, _breakpoint );
        }

        private void toolStripButton2_CheckedChanged( object sender, EventArgs e )
        {
            if (_btnDebug.Checked)
            {
                _debugLog.AppendLine( "LOGGING ENABLED" );
                Logger.LogReceived += Logger_LogReceived;
            }
            else
            {
                _debugLog.AppendLine( "LOGGING DISABLED" );
                Logger.LogReceived -= Logger_LogReceived;
            }
        }

        private void Logger_LogReceived( string details )
        {
            _debugLog.AppendLine( details );
        }

        private void _btnRefresh_Click( object sender, EventArgs e )
        {
            textBox1.Text = _debugLog.ToString();
        }

        private void _btnClearLog_Click( object sender, EventArgs e )
        {
            _debugLog.Clear();
        }

        private void _btnRefreshPlot_Click( object sender, EventArgs e )
        {
            MCharting.Plot plot = new MCharting.Plot();
            MCharting.Series seriesMax = new MCharting.Series();
            MCharting.Series seriesMin = new MCharting.Series();
            MCharting.Series seriesAvg = new MCharting.Series();
            MCharting.Series seriesElite = new MCharting.Series();
            MCharting.Series seriesAll = new MCharting.Series();
            MCharting.Series seriesSpecial = new MCharting.Series();

            _plot.Style.Draft = true;

            if (_chkYRound.Checked)
            {
                seriesMax.Style.DrawLines = new Pen( Color.Gray, 1 );
                seriesMin.Style.DrawLines = new Pen( Color.Gray, 1 );
                seriesAvg.Style.DrawLines = new Pen( Color.Green, 3 );
                seriesElite.Style.DrawLines = new Pen( Color.Blue, 3 );
                seriesSpecial.Style.DrawVLines = new Pen( Color.Red, 1 );
            }
            else
            {
                // seriesMax = meaningless
                // seriesMin = meaningless                         
                // seriesAvg = meaningless
                // seriesSpecial = impossible to plot
                seriesElite.Style.DrawPoints = new SolidBrush( Color.Blue );
                seriesElite.Style.DrawPointsShape = MCharting.SeriesStyle.DrawPointsShapes.Circle;
                seriesElite.Style.DrawPointsSize = 5;
            }

            seriesAll.Style.DrawPoints = new SolidBrush( Color.FromArgb( 64, 0, 0, 0 ) );
            seriesAll.Style.DrawPointsShape = MCharting.SeriesStyle.DrawPointsShapes.Square;
            seriesAll.Style.DrawPointsLine = Pens.Black;
            seriesAll.Style.DrawPointsSize = 5;


            plot.Style.ShowZeroX = false;

            plot.Series.Add( seriesMax );
            plot.Series.Add( seriesMin );
            plot.Series.Add( seriesAvg );
            plot.Series.Add( seriesElite );
            plot.Series.Add( seriesAll );
            plot.Series.Add( seriesSpecial );

            List<PlotSet> yAxis = null;
            List<PlotSet> xAxis = null;
            string n1;
            string n2;

            if (_chkTree.Checked)
            {
                yAxis = _size;
                plot.YLabel = "Number of nodes";
                n1 = "tree sizes";
            }
            else if (_chkExecution.Checked)
            {
                yAxis = _time;
                plot.YLabel = "Execution time";
                n1 = "execution times";
            }
            else if (_chkRound.Checked)
            {
                yAxis = _rounds;
                plot.YLabel = "Round";
                n1 = "rounds";
            }
            else
            {
                yAxis = _fitness;
                plot.YLabel = "Fitness";
                n1 = "fitness values";
            }

            if (_chkYSize.Checked)
            {
                xAxis = _size;
                plot.XLabel = "Number of nodes";
                n2 = "tree sizes";
            }
            else if (_chkYTime.Checked)
            {
                xAxis = _time;
                plot.XLabel = "Execution time";
                n2 = "execution times";
            }
            else if (_chkYRound.Checked)
            {
                xAxis = _rounds;
                plot.XLabel = "Round";
                n2 = "rounds";
            }
            else
            {
                xAxis = _fitness;
                plot.XLabel = "Fitness";
                n2 = "fitness values";
            }

            plot.Title = "Population " + n1 + " by " + n2 + " as of round ";

            int start;

            lock (xAxis)
            {
                lock (yAxis)
                {
                    lock (_rounds)
                    {
                        start = _chkTruncate.Checked ? Math.Max( 0, xAxis.Count - 200 ) : 0;
                        plot.Title += _rounds.Count == 0 ? "(none)" : _rounds[_rounds.Count - 1].Min.ToString();

                        bool max = _chkHighest.Checked;
                        bool min = _chkLowest.Checked;
                        bool avg = _chkMean.Checked;
                        bool elite = _chkElite.Checked;
                        bool all = _chkAll.Checked;

                        for (int i = start; i < xAxis.Count; i++)
                        {
                            PlotSet x = xAxis[i];
                            PlotSet y = yAxis[i];

                            if (max) seriesMax.Points.Add( new MCharting.DataPoint( x.Max, y.Max ) );
                            if (min) seriesMin.Points.Add( new MCharting.DataPoint( x.Min, y.Min ) );
                            if (avg) seriesAvg.Points.Add( new MCharting.DataPoint( x.Average, y.Average ) );
                            if (elite) seriesElite.Points.Add( new MCharting.DataPoint( x.Elite, y.Elite ) );

                            if (all)
                            {
                                for (int m = start; m < x.Count; m++)
                                {
                                    seriesAll.Points.Add( new MCharting.DataPoint( x[m], y[m] ) );
                                }
                            }
                        }
                    }
                }
            }

            if (_chkYRound.Checked)
            {
                lock (_specialEvents)
                {
                    foreach (int r in _specialEvents)
                    {
                        if (r >= start)
                        {
                            seriesSpecial.Points.Add( new MCharting.DataPoint( r ) );
                        }
                    }
                }
            }

            _plot.SetPlot( plot );
        }

        private void toolStripButton2_Click_1( object sender, EventArgs e )
        {
            if (_tvwIndividual.SelectedNode != null)
            {
                Individual m = new Individual( _displayed );
                Node a = m.FindEquivalentNode( (Node)_tvwIndividual.SelectedNode.Tag );

                MutationOperators.PerNode.MutateViaReplacement( new MutateArgs( _population, m, _parameters.MaxTreeDepth ), a );

                SetFamily( _displayed, null, m, null );
            }

            CancelSelection();
        }

        private void _chkFitness_Click( object sender, EventArgs e )
        {
            _chkFitness.Checked = sender == _chkFitness;
            _chkTree.Checked = sender == _chkTree;
            _chkExecution.Checked = sender == _chkExecution;
            _chkRound.Checked = sender == _chkRound;
        }

        private void _chkYFitness_Click( object sender, EventArgs e )
        {
            _chkYFitness.Checked = sender == _chkYFitness;
            _chkYSize.Checked = sender == _chkYSize;
            _chkYTime.Checked = sender == _chkYTime;
            _chkYRound.Checked = sender == _chkYRound;
        }

        private void _chkElite_Click( object sender, EventArgs e )
        {
            ToolStripButton tsb = (ToolStripButton)sender;

            tsb.Checked = !tsb.Checked;
        }

        private void _chkTruncate_Click( object sender, EventArgs e )
        {
            _chkTruncate.Checked = !_chkTruncate.Checked;
        }

        private void _btnCollapse_Click( object sender, EventArgs e )
        {
            _tvwIndividual.CollapseAll();
        }

        private void _btnExpand_Click( object sender, EventArgs e )
        {
            _tvwIndividual.ExpandAll();
        }

        private void _btnRefresh2Sgp_Click( object sender, EventArgs e )
        {
            MCharting.Plot plot = new MCharting.Plot();
            MCharting.Series series = new MCharting.Series();
            series.Style.DrawVLines = Pens.Black;
            MCharting.Series seriesB = new MCharting.Series();
            seriesB.Style.DrawPoints = Brushes.Blue;
            seriesB.Style.DrawPointsSize = 5;
            plot.Style.GridStyle = new Pen( Color.Silver );
            plot.Style.GridStyle.DashStyle = DashStyle.Dot;

            plot.XLabel = "Variable index";
            plot.YLabel = "Number of times used";

            lock (_latestBest2sgpFitnessLock)
            {
                plot.Title = "Variable counts as of round " + _latestBest2sgpFitnessRound;

                var counts = _latestBest2sgpFitnessCounts;

                if (counts != null)
                {
                    for (int x = 0; x < counts.Length; x++)
                    {
                        int y = counts[x];

                        if (y != 0)
                        {
                            series.Points.Add( new MCharting.DataPoint( x, 0, y ) );
                            seriesB.Points.Add( new MCharting.DataPoint( x, y ) );
                        }
                    }
                }
            }

            plot.Series.Add( series );
            plot.Series.Add( seriesB );

            //mChart1.SetPlot(plot);
        }

        private void _chkSimpleView_Click( object sender, EventArgs e )
        {
            _chkSimpleView.Checked = !_chkSimpleView.Checked;
        }

        private void _chkAutoPlot_Click( object sender, EventArgs e )
        {
            _autoPlot = !_autoPlot;
            _chkAutoPlot.Checked = _autoPlot;
        }

        private void _tabTabControl_SelectedIndexChanged( object sender, EventArgs e )
        {
            bool locked = _tabTabControl.SelectedTab == _pgLock;

            this.ControlBox = menuStrip1.Enabled = toolStrip3.Enabled = !locked;
        }
    }
}
