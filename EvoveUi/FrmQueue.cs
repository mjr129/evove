﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Evove;

namespace EvoveUi
{
    public partial class FrmQueue : Form
    {
        const int TIME_BETWEEN = 10000;
        private int _parameters;
        Stopwatch _stopwatch = Stopwatch.StartNew();
        private readonly Font _strikeOut;
        private readonly RunMode _mode;
        private ListViewItem _lvi;
        private readonly EvoveParameters[] _source;
        private readonly EvoveParameters[] _results;

        public static EvoveParameters[] Show( IWin32Window owner, IEnumerable<EvoveParameters> parameters, RunMode mode )
        {
            using (FrmQueue frm = new FrmQueue( parameters, mode ))
            {
                frm.ShowDialog( owner );
                return frm._results;
            } 
        }

        public FrmQueue( IEnumerable<EvoveParameters> parameters, RunMode mode )
        {
            InitializeComponent();
            this.Icon = Evove.Properties.Resources.Icon;

            _source = parameters.ToArray();
            _mode = mode;

            for(int index=0; index < _source.Length; index++)
            {
                string title = string.IsNullOrEmpty( _source[index].Title ) ? "Untitled" : _source[index].Title;
                ListViewItem lvi = new ListViewItem( title );
                lvi.Tag = index;

                listView1.Items.Add( lvi );
            }

            _results = new EvoveParameters[_source.Length];

            listView1.Columns[0].AutoResize( ColumnHeaderAutoResizeStyle.ColumnContent );

            _strikeOut = new Font( listView1.Font, FontStyle.Strikeout );

            Countdown();
        }

        private void Countdown()
        {
            foreach (ListViewItem lvi in listView1.Items)
            {
                if (lvi.Tag != null)
                {
                    _parameters = (int)lvi.Tag;
                    lvi.Tag = null;
                    _lvi = lvi;
                    lvi.ForeColor = Color.Red;

                    _stopwatch.Restart();
                    timer1.Enabled = true;
                    return;
                }
            }

            DialogResult = DialogResult.OK;
            Close();
        }     

        private void timer1_Tick( object sender, EventArgs e )
        {
            int remaining = (TIME_BETWEEN - (int)_stopwatch.ElapsedMilliseconds);
            toolStripButton6.Text = "NEXT TEST WILL RUN IN " + (remaining / 1000) + " SECONDS";

            if (remaining <= 0)
            {
                StartNow();
            }
        }

        private void StartNow()
        {
            timer1.Enabled = false;

            _lvi.ForeColor = Color.Gray;
            _lvi.Font = _strikeOut;
            _lvi = null;

            Hide();
            _results[_parameters] = _source[_parameters].Start( this, _mode );
            Show();

            Countdown();
        }

        private void toolStripButton1_Click( object sender, EventArgs e )
        {
            _stopwatch.Stop();
            timer1.Enabled = false;
            toolStripButton1.Enabled = false;
            toolStripButton3.Enabled = true;
            toolStripButton6.Text = "PAUSED";
        }

        private void toolStripButton3_Click( object sender, EventArgs e )
        {
            _stopwatch.Restart();
            timer1.Enabled = true;
            toolStripButton1.Enabled = true;
            toolStripButton3.Enabled = false;
        }

        private void toolStripButton2_Click( object sender, EventArgs e )
        {
            Close();
        }

        private void toolStripButton4_Click( object sender, EventArgs e )
        {
            StartNow();
        }

        private void toolStripButton6_Click( object sender, EventArgs e )
        {
            if (toolStripButton3.Enabled)
            {
                toolStripButton3.PerformClick();
            }
            else
            {
                toolStripButton4.PerformClick();
            }
        }
    }
}
