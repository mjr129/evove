﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Evove;

namespace EvoveUi
{
    public partial class FrmParameters : Form
    {
        public static object Show( IWin32Window owner, object parameters, bool supportsCancel, bool readOnly )
        {
            if (!readOnly && parameters is IEvoveGuiEditable)
            {
                IEvoveGuiEditable parametersx = (IEvoveGuiEditable)parameters;

                return parametersx.EditInGui( owner );
            }

            using (FrmParameters frm = new FrmParameters())
            {
                frm.propertyGrid1.SelectedObject = parameters;
                frm.button2.Enabled = supportsCancel;
                frm.label1.Visible = !supportsCancel;
                frm.propertyGrid1.Enabled = !readOnly;

                if (readOnly)
                {
                    frm.button1.Enabled = false;
                }

                return frm.ShowDialog( owner ) == DialogResult.OK ? parameters : null;
            }
        }

        public FrmParameters()
        {
            InitializeComponent();
            this.Icon = Evove.Properties.Resources.Icon;

            Text = EvoveHelper.PROGRAM_NAME + " - Parameter Editor";
        }
    }
}
