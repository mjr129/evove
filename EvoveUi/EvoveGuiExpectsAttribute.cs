﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvoveUi
{
    /// <summary>
    /// Indicates that a class expects a configuration of the specified type to be added to
    /// the <see cref="PropertyBag"/> held in <see cref="EvoveParameters"/> .Properties .
    /// 
    /// This attribute is in the GUI rather than Evove since Evove itself doesn't actually care
    /// whether the property is there - an error will be thrown when the algorithm tries to get the
    /// property anyway, but in order to prompt the user for parameters the GUI needs to know.
    /// </summary>
    [AttributeUsage( AttributeTargets.Class )]
    public class EvoveGuiExpectsAttribute : Attribute
    {     
        public EvoveGuiExpectsAttribute( string title, Type type )
        {
            Title = title;
            Type = type;
        }

        public EvoveGuiExpectsAttribute( Type type )
            : this( type.FullName, type )
        {
        }

        public string Title { get; set; }
        public Type Type { get; set; }
    }
}
