﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Evove;
using Evove.Attributes;
using Evove.Blocks;
using Evove.Operators.Breed;
using Evove.Operators.Breed.Crossover;
using Evove.Operators.Breed.Mutation;
using Evove.Operators.Migration;
using Evove.Operators.SelectionMethods;
using Evove.Operators.Stopping;
using Evove.Operators.Termination;
using Evove.Blocks.Data;
using System.Diagnostics;
using MGui.Controls;

namespace EvoveUi
{
    public partial class FrmParameterEditor : Form
    {
        private EvoveParameters _result;
        private EvoveParameters _parameters;
        private readonly bool _readOnly;

        public static EvoveParameters Show( IWin32Window owner, EvoveParameters parameters, bool readOnly )
        {
            using (FrmParameterEditor frm = new FrmParameterEditor( parameters, readOnly ))
            {
                if (frm.ShowDialog() == DialogResult.OK && !readOnly)
                {
                    return frm._result;
                }

                return null;
            }
        }

        public FrmParameterEditor( EvoveParameters parameters, bool readOnly )
        {
            InitializeComponent();
            this.Icon = Evove.Properties.Resources.Icon;           
            Text = EvoveHelper.PROGRAM_NAME + " - Configure parameters";

            _lstFunctions.AutoSize = true;
            _lstTermination.AutoSize = true;
            _lstStopping.AutoSize = true;
            _lstCustomData.AutoSize = true;

            LoadParameters( parameters );

            _readOnly = readOnly;

            if (readOnly)
            {
                _btnOk.Visible = false;
                loadAnAssemblyToolStripMenuItem.Enabled = false;
                loadConfigurationToolStripMenuItem.Enabled = false;
            }
        }

        private void SetBreedOperatorDerivatives()
        {
            IBreedOperator breedOp = UnMakeBox<IBreedOperator>( _lstBreeding );

            if (breedOp is BreedOperators.CrossoverAndMutation)
            {
                BreedOperators.CrossoverAndMutation m = (BreedOperators.CrossoverAndMutation)breedOp;

                MakeBox( _lstMutation, m._mutation );
                MakeBox( _lstCrossover, m._crossover );
            }

            UpdateBreedButtons();
        }

        private void UpdateBreedButtons()
        {
            _lstMutation.Enabled = _lblMutation.Enabled = _lstCrossover.Enabled = _lblCrossover.Enabled = UnMakeBox<IBreedOperator>( _lstBreeding ) is BreedOperators.CrossoverAndMutation;
        }

        private class Placeholder : IMutationOperator, ICrossoverOperator
        {
            bool ICrossoverOperator.Crossover( CrossoverArgs args )
            {
                throw new NotImplementedException("No crossover operator has been specified.");
            }

            void IMutationOperator.Mutate( MutateArgs args )
            {
                throw new NotImplementedException("No mutation operator has been specified");
            }
        }   

        private void ApplyBreedOperatorDerivatives()
        {
            IBreedOperator breedOp = UnMakeBox<IBreedOperator>( _lstBreeding );

            if (breedOp is BreedOperators.CrossoverAndMutation)
            {
                BreedOperators.CrossoverAndMutation m = (BreedOperators.CrossoverAndMutation)breedOp;

                var xover = UnMakeBox<ICrossoverOperator>( _lstCrossover ) ?? new Placeholder();
                var mut = UnMakeBox<IMutationOperator>( _lstMutation ) ?? new Placeholder( );
                m = new BreedOperators.CrossoverAndMutation(xover , m._chanceOfCrossover, mut, m._chanceOfCloneMutation, m._chanceOfChildMutation );

                MakeBox( _lstBreeding, m );
            }

            UpdateBreedButtons();
        }

        private void button1_Click( object sender, EventArgs e )
        {
            var x = GetParameters();

            if (x == null)
            {
                return;
            }

            _result = x;
            DialogResult = DialogResult.OK;
        }  

        private T UnMakeBox<T>( Button b )
        {
            return (T)b.Tag;
        }

        private T[] UnMakeList<T>( ListBox l )
        {
            return l.Items.Cast<T>().ToArray();
        }

        private void MakeBox( Button b, object x )
        {
            b.Tag = x;

            if (x == null)
            {
                b.Text = "(none)";
            }
            else
            {
                b.Text = x.ToString().Replace( ", ", "\r\n    " ).Replace( "(", "\r\n    " ).Replace( ")", "" ).Trim();
            }

            toolTip1.SetToolTip( b, b.Text );
        }

        private void MakeList( ListBox l, IEnumerable x )
        {
            if (x != null)
            {
                foreach (var y in x)
                {
                    l.Items.Add( y );
                }
            }

            UpdateList( l );
        }         

        private void _lstMigration_Click( object sender, EventArgs e )
        {
            EditBox<IMigrationOperator>( _lstMigration );
        }

        private void EditBox<T>( Button b )
        {
            object result;

            if (FrmValueEditor.Show( this, FrmValueEditor.ESet.Object, new[] { typeof( T ) }, out result ))
            {
                MakeBox( b, (T)result );
            }
        }

        private void _lstSelection_Click( object sender, EventArgs e )
        {
            EditBox<ISelectionMethod>( _lstSelection );
        }

        private void _lstBreeding_Click( object sender, EventArgs e )
        {
            EditBox<IBreedOperator>( _lstBreeding );

            ApplyBreedOperatorDerivatives();
        }

        private void _lstMutation_Click( object sender, EventArgs e )
        {
            EditBox<IMutationOperator>( _lstMutation );

            ApplyBreedOperatorDerivatives();
        }

        private void _lstCrossover_Click( object sender, EventArgs e )
        {
            EditBox<ICrossoverOperator>( _lstCrossover );

            ApplyBreedOperatorDerivatives();
        }

        private void _numMaxTree_ValueChanged( object sender, EventArgs e )
        {

        }

        private void button4_Click( object sender, EventArgs e )
        {
            EditList<IStoppingCondition>( _lstStopping );
        }

        private void EditList<T>( ListBox l )
        {
            object result;

            if (FrmValueEditor.Show( this, FrmValueEditor.ESet.Object, new[] { typeof( T ) }, out result ))
            {
                l.Items.Add( result );
            }

            UpdateList( l );
        }

        private void UpdateList( ListBox l )
        {
            StringBuilder sb = new StringBuilder();

            foreach (var i in l.Items)
            {
                sb.AppendLine( i.ToString() );
            }

            toolTip1.SetToolTip( l, sb.ToString() );
        }

        private void button3_Click( object sender, EventArgs e )
        {
            EditList<ITerminationOperator>( _lstTermination );
        }

        private void _lstTemplates_KeyPress( object sender, KeyPressEventArgs e )
        {

        }

        private void button5_Click( object sender, EventArgs e )
        {
            object result;

            if (FrmValueEditor.Show( this, FrmValueEditor.ESet.FitnessFunction, null, out result ))
            {
                MakeBox( _btnFitness, result );

                // Remove extensions of previous fitness function
                RemoveFitnessFunctionExtensions();

                // Add extensions of new fitness function   
                if (!AddExtensions( (Factory<InstantiationArgs>)result, false ))
                {
                    MakeBox( _btnFitness, null );
                }

                UpdateList( _lstFunctions );
            }
        }

        private void _btnAddFunction_Click( object sender, EventArgs e )
        {
            object result;

            if (FrmValueEditor.Show( this, FrmValueEditor.ESet.GpExtension, null, out result ))
            {
                // Add extensions of new fitness function                      
                AddExtensions( (Factory<InstantiationArgs>)result, true );

                UpdateList( _lstFunctions );
            }
        }

        private void RemoveFitnessFunctionExtensions()
        {
            var toRemove = _lstFunctions.Items.Cast<PerEvaluationInfo>().Where( z => z.FirstOwner.mode == PerEvaluationInfo.EMode.Fitness ).ToArray();

            foreach (var t in toRemove)
            {
                _lstFunctions.Items.Remove( t );
            }
        }       

        private bool AddExtensions( Factory<InstantiationArgs> factory, bool isExtension )
        {
            if (factory == null)
            {
                return false;
            }

            try
            {
                EvoveParameters parameters = EvoveParameters.CreateBasicSet( isExtension ? null : factory );

                if (isExtension)
                {
                    parameters.AdditionalFunctions = new[] { factory };
                }
                parameters.AllowIncomplete = true;

                SyntaxManager sm = new SyntaxManager( parameters );
                _lstFunctions.Items.AddRange( sm.PerEvaluationTypes );
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show( this, ex.Message );
                return false;
            }
        }

        private void _lstStopping_KeyDown( object sender, KeyEventArgs e )
        {
            if (e.KeyCode == Keys.Delete)
            {
                ListBox listBox = (ListBox)sender;

                if (listBox.SelectedItem != null)
                {
                    listBox.Items.Remove( listBox.SelectedItem );
                }
            }
        }

        private void _lstTemplates_KeyDown( object sender, KeyEventArgs e )
        {
            if (e.KeyCode == Keys.Delete)
            {
                PerEvaluationInfo t = _lstFunctions.SelectedItem as PerEvaluationInfo;

                if (t != null)
                {
                    t = t.FirstOwner;

                    if (t.mode == PerEvaluationInfo.EMode.Fitness)
                    {
                        _btnFitness.PerformClick();
                        return;
                    }

                    RemoveListItem( t );
                }
            }
        }

        private void RemoveListItem( PerEvaluationInfo firstOwner )
        {
            var toRemove = _lstFunctions.Items.Cast<PerEvaluationInfo>().Where( z => z.FirstOwner == firstOwner ).ToArray();

            foreach (PerEvaluationInfo t in toRemove)
            {
                _lstFunctions.Items.Remove( t );
            }
        }

        private void button5_Click_1( object sender, EventArgs e )
        {
            groupBox3.Visible = groupBox6.Visible = true;
            groupBox8.Visible = false;
        }

        private void loadAnAssemblyToolStripMenuItem_Click( object sender, EventArgs e )
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = ".NET Assemblies (*.exe, *.dll)|*.exe;*.dll|All files (*.*)|*.*";

                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    Assembly.LoadFrom( ofd.FileName );
                }
            }
        }

        private void _chkCaching_CheckedChanged( object sender, EventArgs e )
        {
            _lblTestCases.Visible = _numTestCases.Visible = _chkCaching.Checked;
        }

        private void exitToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Close();
        }

        private void aboutToolStripMenuItem_Click( object sender, EventArgs args )
        {
            AboutForm.Show( this, Assembly.GetExecutingAssembly() );
        }

        private void saveConfigurationToolStripMenuItem_Click( object sender, EventArgs e )
        {
            var x = GetParameters();

            if (x == null)
            {
                return;
            }

            // Don't serialise this stuff
            x.ProgressWatcher = null;
            //x.Tag = null;

            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = ".NET objects (*.nrbf)|*.nrbf";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        BinaryFormatter xml = new BinaryFormatter();

                        using (FileStream sw = File.OpenWrite( sfd.FileName ))
                        {
                            xml.Serialize( sw, x );
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show( this, ex.Message );
                    }
                }
            }
        }

        private void loadConfigurationToolStripMenuItem_Click( object sender, EventArgs e )
        {
            using (OpenFileDialog sfd = new OpenFileDialog())
            {
                sfd.Filter = ".NET objects (*.nrbf)|*.nrbf|All files (*.*)|*.*";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        BinaryFormatter xml = new BinaryFormatter();

                        using (FileStream sw = File.OpenRead( sfd.FileName ))
                        {
                            EvoveParameters x = (EvoveParameters)xml.Deserialize( sw );

                            // Restore context specific data
                            //x.Tag = _parameters.Tag;
                            x.ProgressWatcher = _parameters.ProgressWatcher;

                            LoadParameters( x );
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show( this, ex.Message );
                    }
                }
            }
        }

        private void LoadParameters( EvoveParameters parameters )
        {
            if (parameters == null)
            {
                parameters = new EvoveParameters();
            }

            _parameters = parameters;

            _numTestCases.Value = parameters.NumberOfTestCases;
            _numIndividuals.Value = parameters.IndividualsPerDeme;
            _numMaxTree.Value = parameters.MaxTreeDepth;
            _numElites.Value = parameters.NumberOfElites;
            _txtInput.Text = parameters.ContinueFromFile;
            _numDemes.Value = parameters.NumberOfDemes;
            _txtOutput.Text = parameters.OutputFolder;
            _numEThreads.Value = parameters.NumberOfEvaluatorThreads;
            _numBThreads.Value = parameters.NumberOfBreederThreads;
            _chkCaching.Checked = parameters.AllowCaching;
            _chkOverloads.Checked = parameters.SupportMethodOverloads;
            _chkDebugging.Checked = parameters.SupportDebugging;
            _chkErrors.Checked = parameters.CheckForCacheErrors;
            _chkAbandonPEOs.Checked = parameters.AbandonPerEvaluationObjects;
            _numRandomSeed.Value = parameters.RandomSeed;
            _chkSubsetMethods.Checked = parameters.Set != null;
            _numMethodSet.Value = Convert.ToInt64( parameters.Set ?? 0 );
            _txtTitle.Text = parameters.Title;

            MakeBox( _btnFitness, parameters.FitnessFunction );
            AddExtensions( parameters.FitnessFunction, false );

            if (parameters.AdditionalFunctions != null)
            {
                foreach (var x in parameters.AdditionalFunctions)
                {
                    AddExtensions( x, true );
                }
            }

            MakeList( _lstTermination, parameters.TerminationOperators );
            MakeList( _lstStopping, parameters.StoppingConditions );

            MakeBox( _lstBreeding, parameters.BreedingMethod );
            SetBreedOperatorDerivatives();

            MakeBox( _lstSelection, parameters.SelectionMethod );
            MakeBox( _lstMigration, parameters.MigrationOperator );

            MakeList( _lstCustomData, parameters.Properties );            
        }

        private EvoveParameters GetParameters()
        {
            List<Factory<InstantiationArgs>> attrs = new List<Factory<InstantiationArgs>>();

            foreach (PerEvaluationInfo t in _lstFunctions.Items)
            {
                if (t.mode == PerEvaluationInfo.EMode.CustomExtension)
                {
                    attrs.Add( t.factory );
                }
            }

            EvoveParameters parameters = new EvoveParameters()
            {
                NumberOfTestCases = (int)_numTestCases.Value,
                IndividualsPerDeme = (int)_numIndividuals.Value,
                MaxTreeDepth = (int)_numMaxTree.Value,
                NumberOfElites = (int)_numElites.Value,
                ContinueFromFile = _txtInput.Text,
                NumberOfDemes = (int)_numDemes.Value,
                OutputFolder = _txtOutput.Text,
                NumberOfEvaluatorThreads = (int)_numEThreads.Value,
                NumberOfBreederThreads = (int)_numBThreads.Value,
                AllowCaching = _chkCaching.Checked,
                SupportMethodOverloads = _chkOverloads.Checked,
                SupportDebugging = _chkDebugging.Checked,
                CheckForCacheErrors = _chkErrors.Checked,

                TerminationOperators = UnMakeList<ITerminationOperator>( _lstTermination ),
                StoppingConditions = UnMakeList<IStoppingCondition>( _lstStopping ),
                FitnessFunction = UnMakeBox<Factory<InstantiationArgs>>( _btnFitness ),
                BreedingMethod = UnMakeBox<IBreedOperator>( _lstBreeding ),

                SelectionMethod = UnMakeBox<ISelectionMethod>( _lstSelection ),
                MigrationOperator = UnMakeBox<IMigrationOperator>( _lstMigration ),
                Properties = new PropertyBag( _lstCustomData.Items ),
                AbandonPerEvaluationObjects = _chkAbandonPEOs.Checked,
                AllowIncomplete = false,
                RandomSeed = (int)_numRandomSeed.Value,
                ProgressWatcher = null,
                Set = _chkSubsetMethods.Checked ? (object)_numMethodSet.Value : null,
                AdditionalFunctions = attrs.ToArray(),
                Title = _txtTitle.Text,
            };                                               

            try
            {
                parameters.Validate();
                return parameters;
            }
            catch (Exception ex)
            {
                MessageBox.Show( this, ex.Message );
                return null;
            }
        }

        private void _lstFunctions_SelectedIndexChanged( object sender, EventArgs e )
        {

        }

        private void _btnAddCustomData_Click( object sender, EventArgs e )
        {
            List<EvoveGuiExpectsAttribute> typesExpected = new List<EvoveGuiExpectsAttribute>();

            foreach (PerEvaluationInfo pei in _lstFunctions.Items)
            {
                var expects = pei.Type.GetCustomAttributes<EvoveGuiExpectsAttribute>();

                foreach (var t in expects)
                {
                    typesExpected.Add( t );
                }
            }

            if (typesExpected.Count == 0)
            {
                MessageBox.Show( this, "None of the GP classes you have provided expect any custom data." );
                return;
            }

            object result;

            if (!FrmValueEditor.Show( this, FrmValueEditor.ESet.Object, typesExpected.Select( z => z.Type ), out result ))
            {
                return;
            }

            ArrayList toRemove = new ArrayList();

            foreach (var x in _lstFunctions.Items)
            {
                if (x.GetType() == result.GetType())
                {
                    toRemove.Add( x );
                }
            }

            foreach(var x in toRemove)
            {
                _lstCustomData.Items.Remove( x );
            }

            result = FrmParameters.Show( this, result, false, false );

            if (result != null)
            {
                _lstCustomData.Items.Add( result );
            }
        }

        private void _lstCustomData_KeyDown( object sender, KeyEventArgs e )
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (_lstCustomData.SelectedIndex != -1)
                {
                    _lstCustomData.Items.RemoveAt( _lstCustomData.SelectedIndex );
                }
            }
        }

        private void _lstCustomData_DoubleClick( object sender, EventArgs e )
        {
            if (_lstCustomData.SelectedIndex != -1)
            {
                var x = _lstCustomData.SelectedItem;
                x = FrmParameters.Show( this, x, false, false );

                if (x != null)
                {
                    _lstCustomData.Items.RemoveAt( _lstCustomData.SelectedIndex );
                    _lstCustomData.Items.Add( x );
                }
            }
        }

        private void _lstCustomData_SelectedIndexChanged( object sender, EventArgs e )
        {

        }

        private void _chkSubsetMethods_CheckedChanged( object sender, EventArgs e )
        {
            _numMethodSet.Visible = _chkSubsetMethods.Checked;
        }
    }
}
