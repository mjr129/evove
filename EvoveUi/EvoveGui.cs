﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Evove;
using EvoveUi.Properties;

namespace EvoveUi
{
    public static class EvoveGui
    {      
        /// <summary>
        /// Entry point when used as stand-alone EXE.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Start( EvoveParameters.CreateBasicSet( null, 200 ), null, RunMode.ConfigureParameters | RunMode.StartInDebugMode );
        }

        /// <summary>
        /// This extension method wraps to <see cref="EvoveGui.Start"/>.
        /// </summary>
        public static EvoveParameters Start( this EvoveParameters parameters )
        {
            return Start( parameters, null, RunMode.StartInDebugMode );
        }    

        /// <summary>
        /// Starts the Evove UI (for a single simulation).               
        /// </summary>
        /// <param name="owner">Parent form, if any</param>
        /// <param name="parameters">Default parameters (can be NULL if runMode has the ConfigureParameters flag)</param>
        /// <param name="runMode">What to show and/or do. Configuration is always assumed when this overload is called.</param>
        /// <returns>Returns the configured parameter set. If cancelled returns null.</returns>
        public static EvoveParameters Start( IWin32Window owner, RunMode runMode )
        {
            EvoveParameters parameters = new EvoveParameters();
            return Start( parameters, owner, runMode | RunMode.ConfigureParameters );
        }

        /// <summary>
        /// Starts the Evove UI (for a single simulation).               
        /// </summary>
        /// <param name="owner">Parent form, if any</param>
        /// <param name="parameters">Default parameters (can be NULL if runMode has the ConfigureParameters flag)</param>
        /// <param name="runMode">What to show and/or do/</param>
        /// <returns>Returns the configured parameter set. If cancelled returns null.</returns>
        public static EvoveParameters Start( this EvoveParameters parameters,  IWin32Window owner, RunMode runMode )
        {
            if (runMode.HasFlag( RunMode.ConfigureParameters ))
            {
                parameters = FrmParameterEditor.Show( owner, parameters, false );

                if (parameters == null)
                {
                    return null;
                }
            }

            try
            {
                parameters.Validate();

                if (runMode.HasFlag( RunMode.StartInDebugMode ))
                {
                    FrmMain.Start( owner, parameters, false );
                }
                else if (runMode.HasFlag( RunMode.StartInUnattendedMode ))
                {
                    FrmMain.Start( owner, parameters, true );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show( owner, ex.Message + "\r\n\r\nDetails:\r\n" + ex.ToString(), "Exception", MessageBoxButtons.OK, MessageBoxIcon.Error );
                return null;
            }

            return parameters;
        }

        /// <summary>
        /// Starts the Evove UI (multiple simulations run in succession).
        /// </summary>
        /// <param name="owner">Parent form, if any</param>
        /// <param name="parameters">Collection of configurations to use</param>
        /// <param name="mode">Run mode.</param>
        /// <returns>Returns the configured parameter sets. This is always an array with the same number of items as the set passed in. Any cancelled items are set to NULL.</returns>
        public static EvoveParameters[] Start( this IEnumerable<EvoveParameters> parameters, IWin32Window owner, RunMode mode )
        {
            return FrmQueue.Show( owner, parameters, mode );
        }    
    }

    /// <summary>
    /// What GUIs to show in what mode.
    /// </summary>
    [Flags]
    public enum RunMode
    {
        None = 0x00000000,

        /// <summary>
        /// Prompts the used to create or modify the parameter set.
        /// </summary>
        ConfigureParameters = 0x00000001,

        /// <summary>
        /// Starts the simulation in debug mode.
        /// </summary>
        StartInDebugMode = 0x00000002,

        /// <summary>
        /// Starts the simulation in unattended mode.
        /// Ignored if StartInDebugMode is set.
        /// </summary>
        StartInUnattendedMode = 0x00000004,
    }
}
