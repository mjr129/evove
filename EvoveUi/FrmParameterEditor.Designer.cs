﻿namespace EvoveUi
{
    partial class FrmParameterEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._numIndividuals = new System.Windows.Forms.NumericUpDown();
            this._numMaxTree = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._lstTermination = new System.Windows.Forms.ListBox();
            this._numElites = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this._lstStopping = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this._lblBreeding = new System.Windows.Forms.Label();
            this._lblCrossover = new System.Windows.Forms.Label();
            this._lblMutation = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._txtInput = new System.Windows.Forms.TextBox();
            this._numDemes = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this._txtOutput = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this._numEThreads = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this._numBThreads = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._chkCaching = new System.Windows.Forms.CheckBox();
            this._chkOverloads = new System.Windows.Forms.CheckBox();
            this._chkDebugging = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this._lblTag = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this._txtTitle = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this._chkAbandonPEOs = new System.Windows.Forms.CheckBox();
            this._chkErrors = new System.Windows.Forms.CheckBox();
            this._lblTestCases = new System.Windows.Forms.Label();
            this._numTestCases = new System.Windows.Forms.NumericUpDown();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._numMethodSet = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this._btnAddCustomData = new System.Windows.Forms.Button();
            this._chkSubsetMethods = new System.Windows.Forms.CheckBox();
            this._lstCustomData = new System.Windows.Forms.ListBox();
            this._btnAddFunction = new System.Windows.Forms.Button();
            this._btnFitness = new System.Windows.Forms.Button();
            this._numRandomSeed = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this._lstFunctions = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this._lstSelection = new System.Windows.Forms.Button();
            this._lstBreeding = new System.Windows.Forms.Button();
            this._lstMutation = new System.Windows.Forms.Button();
            this._lstCrossover = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this._lstMigration = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._btnOk = new System.Windows.Forms.Button();
            this._btnCancel = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.button5 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.loadAnAssemblyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this._numIndividuals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numMaxTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numElites)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numDemes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numEThreads)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numBThreads)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._numTestCases)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._numMethodSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._numRandomSeed)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // _numIndividuals
            // 
            this._numIndividuals.Location = new System.Drawing.Point(8, 28);
            this._numIndividuals.Margin = new System.Windows.Forms.Padding(0);
            this._numIndividuals.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this._numIndividuals.Name = "_numIndividuals";
            this._numIndividuals.Size = new System.Drawing.Size(388, 25);
            this._numIndividuals.TabIndex = 1;
            // 
            // _numMaxTree
            // 
            this._numMaxTree.Dock = System.Windows.Forms.DockStyle.Top;
            this._numMaxTree.Location = new System.Drawing.Point(8, 283);
            this._numMaxTree.Margin = new System.Windows.Forms.Padding(0);
            this._numMaxTree.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this._numMaxTree.Name = "_numMaxTree";
            this._numMaxTree.Size = new System.Drawing.Size(388, 25);
            this._numMaxTree.TabIndex = 3;
            this._numMaxTree.ValueChanged += new System.EventHandler(this._numMaxTree_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 266);
            this.label2.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Maximum tree depth";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Excision conditions";
            // 
            // _lstTermination
            // 
            this._lstTermination.Dock = System.Windows.Forms.DockStyle.Top;
            this._lstTermination.FormattingEnabled = true;
            this._lstTermination.IntegralHeight = false;
            this._lstTermination.ItemHeight = 17;
            this._lstTermination.Location = new System.Drawing.Point(8, 25);
            this._lstTermination.Margin = new System.Windows.Forms.Padding(0);
            this._lstTermination.MinimumSize = new System.Drawing.Size(4, 27);
            this._lstTermination.Name = "_lstTermination";
            this._lstTermination.Size = new System.Drawing.Size(388, 27);
            this._lstTermination.TabIndex = 6;
            this._lstTermination.KeyDown += new System.Windows.Forms.KeyEventHandler(this._lstStopping_KeyDown);
            // 
            // _numElites
            // 
            this._numElites.Dock = System.Windows.Forms.DockStyle.Top;
            this._numElites.Location = new System.Drawing.Point(8, 25);
            this._numElites.Margin = new System.Windows.Forms.Padding(0);
            this._numElites.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this._numElites.Name = "_numElites";
            this._numElites.Size = new System.Drawing.Size(388, 25);
            this._numElites.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Number of elites";
            // 
            // _lstStopping
            // 
            this._lstStopping.Dock = System.Windows.Forms.DockStyle.Top;
            this._lstStopping.FormattingEnabled = true;
            this._lstStopping.IntegralHeight = false;
            this._lstStopping.ItemHeight = 17;
            this._lstStopping.Location = new System.Drawing.Point(8, 25);
            this._lstStopping.Margin = new System.Windows.Forms.Padding(0);
            this._lstStopping.MinimumSize = new System.Drawing.Size(4, 27);
            this._lstStopping.Name = "_lstStopping";
            this._lstStopping.Size = new System.Drawing.Size(388, 27);
            this._lstStopping.TabIndex = 12;
            this._lstStopping.KeyDown += new System.Windows.Forms.KeyEventHandler(this._lstStopping_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 8);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "Stopping conditions";
            // 
            // _lblBreeding
            // 
            this._lblBreeding.AutoSize = true;
            this._lblBreeding.Location = new System.Drawing.Point(8, 110);
            this._lblBreeding.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this._lblBreeding.Name = "_lblBreeding";
            this._lblBreeding.Size = new System.Drawing.Size(116, 17);
            this._lblBreeding.TabIndex = 14;
            this._lblBreeding.Text = "Breeding operator";
            // 
            // _lblCrossover
            // 
            this._lblCrossover.AutoSize = true;
            this._lblCrossover.Location = new System.Drawing.Point(8, 214);
            this._lblCrossover.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this._lblCrossover.Name = "_lblCrossover";
            this._lblCrossover.Size = new System.Drawing.Size(123, 17);
            this._lblCrossover.TabIndex = 16;
            this._lblCrossover.Text = "Crossover operator";
            // 
            // _lblMutation
            // 
            this._lblMutation.AutoSize = true;
            this._lblMutation.Location = new System.Drawing.Point(8, 162);
            this._lblMutation.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this._lblMutation.Name = "_lblMutation";
            this._lblMutation.Size = new System.Drawing.Size(116, 17);
            this._lblMutation.TabIndex = 18;
            this._lblMutation.Text = "Mutation operator";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 58);
            this.label10.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 17);
            this.label10.TabIndex = 20;
            this.label10.Text = "Continue from file";
            // 
            // _txtInput
            // 
            this._txtInput.Dock = System.Windows.Forms.DockStyle.Top;
            this._txtInput.Location = new System.Drawing.Point(8, 75);
            this._txtInput.Margin = new System.Windows.Forms.Padding(0);
            this._txtInput.Name = "_txtInput";
            this._txtInput.Size = new System.Drawing.Size(388, 25);
            this._txtInput.TabIndex = 19;
            // 
            // _numDemes
            // 
            this._numDemes.Dock = System.Windows.Forms.DockStyle.Top;
            this._numDemes.Location = new System.Drawing.Point(8, 73);
            this._numDemes.Margin = new System.Windows.Forms.Padding(0);
            this._numDemes.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this._numDemes.Name = "_numDemes";
            this._numDemes.Size = new System.Drawing.Size(388, 25);
            this._numDemes.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 56);
            this.label11.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(116, 17);
            this.label11.TabIndex = 21;
            this.label11.Text = "Number of islands";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 8);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(87, 17);
            this.label12.TabIndex = 24;
            this.label12.Text = "Output folder";
            // 
            // _txtOutput
            // 
            this._txtOutput.Dock = System.Windows.Forms.DockStyle.Top;
            this._txtOutput.Location = new System.Drawing.Point(8, 25);
            this._txtOutput.Margin = new System.Windows.Forms.Padding(0);
            this._txtOutput.Name = "_txtOutput";
            this._txtOutput.Size = new System.Drawing.Size(388, 25);
            this._txtOutput.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 58);
            this.label13.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 17);
            this.label13.TabIndex = 26;
            this.label13.Text = "Selection method";
            // 
            // _numEThreads
            // 
            this._numEThreads.Dock = System.Windows.Forms.DockStyle.Top;
            this._numEThreads.Location = new System.Drawing.Point(8, 25);
            this._numEThreads.Margin = new System.Windows.Forms.Padding(0);
            this._numEThreads.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this._numEThreads.Name = "_numEThreads";
            this._numEThreads.Size = new System.Drawing.Size(388, 25);
            this._numEThreads.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 8);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(178, 17);
            this.label14.TabIndex = 27;
            this.label14.Text = "Number of evaluator threads";
            // 
            // _numBThreads
            // 
            this._numBThreads.Dock = System.Windows.Forms.DockStyle.Top;
            this._numBThreads.Location = new System.Drawing.Point(8, 75);
            this._numBThreads.Margin = new System.Windows.Forms.Padding(0);
            this._numBThreads.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this._numBThreads.Name = "_numBThreads";
            this._numBThreads.Size = new System.Drawing.Size(388, 25);
            this._numBThreads.TabIndex = 30;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 58);
            this.label15.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(171, 17);
            this.label15.TabIndex = 29;
            this.label15.Text = "Number of breeder threads";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 8);
            this.label16.Margin = new System.Windows.Forms.Padding(0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 17);
            this.label16.TabIndex = 31;
            this.label16.Text = "Fitness model";
            // 
            // _chkCaching
            // 
            this._chkCaching.AutoSize = true;
            this._chkCaching.Location = new System.Drawing.Point(8, 8);
            this._chkCaching.Margin = new System.Windows.Forms.Padding(0);
            this._chkCaching.Name = "_chkCaching";
            this._chkCaching.Size = new System.Drawing.Size(169, 21);
            this._chkCaching.TabIndex = 33;
            this._chkCaching.Text = "Allow evaluation caching";
            this._chkCaching.UseVisualStyleBackColor = true;
            this._chkCaching.CheckedChanged += new System.EventHandler(this._chkCaching_CheckedChanged);
            // 
            // _chkOverloads
            // 
            this._chkOverloads.AutoSize = true;
            this._chkOverloads.Location = new System.Drawing.Point(8, 87);
            this._chkOverloads.Margin = new System.Windows.Forms.Padding(0);
            this._chkOverloads.Name = "_chkOverloads";
            this._chkOverloads.Size = new System.Drawing.Size(185, 21);
            this._chkOverloads.TabIndex = 34;
            this._chkOverloads.Text = "Support method overloads";
            this._chkOverloads.UseVisualStyleBackColor = true;
            // 
            // _chkDebugging
            // 
            this._chkDebugging.AutoSize = true;
            this._chkDebugging.Location = new System.Drawing.Point(8, 108);
            this._chkDebugging.Margin = new System.Windows.Forms.Padding(0);
            this._chkDebugging.Name = "_chkDebugging";
            this._chkDebugging.Size = new System.Drawing.Size(142, 21);
            this._chkDebugging.TabIndex = 35;
            this._chkDebugging.Text = "Support debugging";
            this._chkDebugging.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 106);
            this.label17.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(121, 17);
            this.label17.TabIndex = 37;
            this.label17.Text = "Migration operator";
            // 
            // _lblTag
            // 
            this._lblTag.AutoSize = true;
            this._lblTag.Location = new System.Drawing.Point(8, 139);
            this._lblTag.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this._lblTag.Name = "_lblTag";
            this._lblTag.Size = new System.Drawing.Size(82, 17);
            this._lblTag.TabIndex = 39;
            this._lblTag.Text = "Custom data";
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(434, 495);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(8);
            this.groupBox2.MaximumSize = new System.Drawing.Size(410, 0);
            this.groupBox2.MinimumSize = new System.Drawing.Size(410, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(410, 184);
            this.groupBox2.TabIndex = 41;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "I/O";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this._txtTitle, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label10, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this._txtInput, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this._txtOutput, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(404, 158);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 108);
            this.label8.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 17);
            this.label8.TabIndex = 26;
            this.label8.Text = "Title";
            // 
            // _txtTitle
            // 
            this._txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this._txtTitle.Location = new System.Drawing.Point(8, 125);
            this._txtTitle.Margin = new System.Windows.Forms.Padding(0);
            this._txtTitle.Name = "_txtTitle";
            this._txtTitle.Size = new System.Drawing.Size(388, 25);
            this._txtTitle.TabIndex = 25;
            // 
            // groupBox3
            // 
            this.groupBox3.AutoSize = true;
            this.groupBox3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox3.Controls.Add(this.tableLayoutPanel7);
            this.groupBox3.ForeColor = System.Drawing.Color.Blue;
            this.groupBox3.Location = new System.Drawing.Point(860, 158);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(8);
            this.groupBox3.MaximumSize = new System.Drawing.Size(410, 0);
            this.groupBox3.MinimumSize = new System.Drawing.Size(410, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Size = new System.Drawing.Size(410, 204);
            this.groupBox3.TabIndex = 42;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Advanced (not recommended)";
            this.groupBox3.Visible = false;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.AutoSize = true;
            this.tableLayoutPanel7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this._chkAbandonPEOs, 0, 6);
            this.tableLayoutPanel7.Controls.Add(this._chkErrors, 0, 5);
            this.tableLayoutPanel7.Controls.Add(this._chkDebugging, 0, 4);
            this.tableLayoutPanel7.Controls.Add(this._chkOverloads, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this._lblTestCases, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this._numTestCases, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this._chkCaching, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.Padding = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel7.RowCount = 7;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(404, 178);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // _chkAbandonPEOs
            // 
            this._chkAbandonPEOs.AutoSize = true;
            this._chkAbandonPEOs.Location = new System.Drawing.Point(8, 150);
            this._chkAbandonPEOs.Margin = new System.Windows.Forms.Padding(0);
            this._chkAbandonPEOs.Name = "_chkAbandonPEOs";
            this._chkAbandonPEOs.Size = new System.Drawing.Size(214, 20);
            this._chkAbandonPEOs.TabIndex = 37;
            this._chkAbandonPEOs.Text = "Abandon per-evaluation objects";
            this._chkAbandonPEOs.UseVisualStyleBackColor = true;
            // 
            // _chkErrors
            // 
            this._chkErrors.AutoSize = true;
            this._chkErrors.Location = new System.Drawing.Point(8, 129);
            this._chkErrors.Margin = new System.Windows.Forms.Padding(0);
            this._chkErrors.Name = "_chkErrors";
            this._chkErrors.Size = new System.Drawing.Size(159, 21);
            this._chkErrors.TabIndex = 36;
            this._chkErrors.Text = "Check for cache errors";
            this._chkErrors.UseVisualStyleBackColor = true;
            // 
            // _lblTestCases
            // 
            this._lblTestCases.AutoSize = true;
            this._lblTestCases.Location = new System.Drawing.Point(8, 37);
            this._lblTestCases.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this._lblTestCases.Name = "_lblTestCases";
            this._lblTestCases.Size = new System.Drawing.Size(133, 17);
            this._lblTestCases.TabIndex = 29;
            this._lblTestCases.Text = "Number of test cases";
            this._lblTestCases.Visible = false;
            // 
            // _numTestCases
            // 
            this._numTestCases.Dock = System.Windows.Forms.DockStyle.Top;
            this._numTestCases.Location = new System.Drawing.Point(8, 54);
            this._numTestCases.Margin = new System.Windows.Forms.Padding(0, 0, 0, 8);
            this._numTestCases.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this._numTestCases.Name = "_numTestCases";
            this._numTestCases.Size = new System.Drawing.Size(388, 25);
            this._numTestCases.TabIndex = 30;
            this._numTestCases.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.AutoSize = true;
            this.groupBox4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox4.Controls.Add(this.tableLayoutPanel1);
            this.groupBox4.ForeColor = System.Drawing.Color.Blue;
            this.groupBox4.Location = new System.Drawing.Point(8, 8);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(8);
            this.groupBox4.MaximumSize = new System.Drawing.Size(410, 0);
            this.groupBox4.MinimumSize = new System.Drawing.Size(410, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Size = new System.Drawing.Size(410, 373);
            this.groupBox4.TabIndex = 42;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "World";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this._numMethodSet, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown1, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this._btnAddCustomData, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this._chkSubsetMethods, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this._lstCustomData, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this._btnAddFunction, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this._btnFitness, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._lblTag, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this._numRandomSeed, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this._lstFunctions, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 9);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(404, 347);
            this.tableLayoutPanel1.TabIndex = 36;
            // 
            // _numMethodSet
            // 
            this._numMethodSet.Location = new System.Drawing.Point(8, 314);
            this._numMethodSet.Margin = new System.Windows.Forms.Padding(0);
            this._numMethodSet.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this._numMethodSet.Name = "_numMethodSet";
            this._numMethodSet.Size = new System.Drawing.Size(388, 25);
            this._numMethodSet.TabIndex = 45;
            this._numMethodSet.Visible = false;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(8, 156);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(0);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(388, 25);
            this.numericUpDown1.TabIndex = 44;
            // 
            // _btnAddCustomData
            // 
            this._btnAddCustomData.AutoSize = true;
            this._btnAddCustomData.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._btnAddCustomData.Dock = System.Windows.Forms.DockStyle.Top;
            this._btnAddCustomData.Image = global::EvoveUi.Properties.Resources.arrows;
            this._btnAddCustomData.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._btnAddCustomData.Location = new System.Drawing.Point(8, 208);
            this._btnAddCustomData.Margin = new System.Windows.Forms.Padding(0);
            this._btnAddCustomData.MinimumSize = new System.Drawing.Size(0, 27);
            this._btnAddCustomData.Name = "_btnAddCustomData";
            this._btnAddCustomData.Size = new System.Drawing.Size(388, 27);
            this._btnAddCustomData.TabIndex = 42;
            this._btnAddCustomData.Text = "Add...";
            this._btnAddCustomData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnAddCustomData.UseVisualStyleBackColor = true;
            this._btnAddCustomData.Click += new System.EventHandler(this._btnAddCustomData_Click);
            // 
            // _chkSubsetMethods
            // 
            this._chkSubsetMethods.AutoSize = true;
            this._chkSubsetMethods.Location = new System.Drawing.Point(8, 293);
            this._chkSubsetMethods.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this._chkSubsetMethods.Name = "_chkSubsetMethods";
            this._chkSubsetMethods.Size = new System.Drawing.Size(162, 21);
            this._chkSubsetMethods.TabIndex = 34;
            this._chkSubsetMethods.Text = "Specific method subset";
            this._chkSubsetMethods.UseVisualStyleBackColor = true;
            this._chkSubsetMethods.CheckedChanged += new System.EventHandler(this._chkSubsetMethods_CheckedChanged);
            // 
            // _lstCustomData
            // 
            this._lstCustomData.Dock = System.Windows.Forms.DockStyle.Top;
            this._lstCustomData.FormattingEnabled = true;
            this._lstCustomData.IntegralHeight = false;
            this._lstCustomData.ItemHeight = 17;
            this._lstCustomData.Location = new System.Drawing.Point(8, 181);
            this._lstCustomData.Margin = new System.Windows.Forms.Padding(0);
            this._lstCustomData.MinimumSize = new System.Drawing.Size(4, 27);
            this._lstCustomData.Name = "_lstCustomData";
            this._lstCustomData.Size = new System.Drawing.Size(388, 27);
            this._lstCustomData.TabIndex = 41;
            this._lstCustomData.SelectedIndexChanged += new System.EventHandler(this._lstCustomData_SelectedIndexChanged);
            this._lstCustomData.DoubleClick += new System.EventHandler(this._lstCustomData_DoubleClick);
            this._lstCustomData.KeyDown += new System.Windows.Forms.KeyEventHandler(this._lstCustomData_KeyDown);
            // 
            // _btnAddFunction
            // 
            this._btnAddFunction.AutoSize = true;
            this._btnAddFunction.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._btnAddFunction.Dock = System.Windows.Forms.DockStyle.Top;
            this._btnAddFunction.Image = global::EvoveUi.Properties.Resources.arrows;
            this._btnAddFunction.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._btnAddFunction.Location = new System.Drawing.Point(8, 104);
            this._btnAddFunction.Margin = new System.Windows.Forms.Padding(0);
            this._btnAddFunction.Name = "_btnAddFunction";
            this._btnAddFunction.Size = new System.Drawing.Size(388, 27);
            this._btnAddFunction.TabIndex = 37;
            this._btnAddFunction.Text = "Add...";
            this._btnAddFunction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnAddFunction.UseVisualStyleBackColor = true;
            this._btnAddFunction.Click += new System.EventHandler(this._btnAddFunction_Click);
            // 
            // _btnFitness
            // 
            this._btnFitness.AutoSize = true;
            this._btnFitness.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._btnFitness.Dock = System.Windows.Forms.DockStyle.Top;
            this._btnFitness.Image = global::EvoveUi.Properties.Resources.arrows;
            this._btnFitness.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._btnFitness.Location = new System.Drawing.Point(8, 25);
            this._btnFitness.Margin = new System.Windows.Forms.Padding(0);
            this._btnFitness.Name = "_btnFitness";
            this._btnFitness.Size = new System.Drawing.Size(388, 27);
            this._btnFitness.TabIndex = 37;
            this._btnFitness.Text = "(none)";
            this._btnFitness.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this._btnFitness, "Add items to the list\r\nUse the delete key to remove them\r\nand Ctrl+Up/Down to rea" +
        "rrange.");
            this._btnFitness.UseVisualStyleBackColor = true;
            this._btnFitness.Click += new System.EventHandler(this.button5_Click);
            // 
            // _numRandomSeed
            // 
            this._numRandomSeed.Location = new System.Drawing.Point(8, 260);
            this._numRandomSeed.Margin = new System.Windows.Forms.Padding(0);
            this._numRandomSeed.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this._numRandomSeed.Name = "_numRandomSeed";
            this._numRandomSeed.Size = new System.Drawing.Size(388, 25);
            this._numRandomSeed.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 60);
            this.label7.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 17);
            this.label7.TabIndex = 31;
            this.label7.Text = "Function set";
            // 
            // _lstFunctions
            // 
            this._lstFunctions.Dock = System.Windows.Forms.DockStyle.Top;
            this._lstFunctions.FormattingEnabled = true;
            this._lstFunctions.IntegralHeight = false;
            this._lstFunctions.ItemHeight = 17;
            this._lstFunctions.Location = new System.Drawing.Point(8, 77);
            this._lstFunctions.Margin = new System.Windows.Forms.Padding(0);
            this._lstFunctions.MinimumSize = new System.Drawing.Size(4, 27);
            this._lstFunctions.Name = "_lstFunctions";
            this._lstFunctions.Size = new System.Drawing.Size(388, 27);
            this._lstFunctions.TabIndex = 12;
            this._lstFunctions.KeyDown += new System.Windows.Forms.KeyEventHandler(this._lstTemplates_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 243);
            this.label5.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Random seed";
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.Controls.Add(this.tableLayoutPanel3);
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Location = new System.Drawing.Point(8, 597);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(8);
            this.groupBox1.MaximumSize = new System.Drawing.Size(410, 0);
            this.groupBox1.MinimumSize = new System.Drawing.Size(410, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(410, 113);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stopping";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.button4, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this._lstStopping, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(404, 87);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // button4
            // 
            this.button4.AutoSize = true;
            this.button4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.Image = global::EvoveUi.Properties.Resources.arrows;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.Location = new System.Drawing.Point(8, 52);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(388, 27);
            this.button4.TabIndex = 37;
            this.button4.Text = "Add...";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.button4, "Add items to the list\r\nUse the delete key to remove them\r\nand Ctrl+Up/Down to rea" +
        "rrange.");
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.AutoSize = true;
            this.groupBox5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox5.Controls.Add(this.tableLayoutPanel4);
            this.groupBox5.ForeColor = System.Drawing.Color.Blue;
            this.groupBox5.Location = new System.Drawing.Point(434, 366);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(8);
            this.groupBox5.MaximumSize = new System.Drawing.Size(410, 0);
            this.groupBox5.MinimumSize = new System.Drawing.Size(410, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox5.Size = new System.Drawing.Size(410, 113);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Excision";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.AutoSize = true;
            this.tableLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.button3, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this._lstTermination, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.Padding = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(404, 87);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.AutoSize = true;
            this.button3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.Image = global::EvoveUi.Properties.Resources.arrows;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(8, 52);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(388, 27);
            this.button3.TabIndex = 37;
            this.button3.Text = "Add...";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.button3, "Add items to the list\r\nUse the delete key to remove them\r\nand Ctrl+Up/Down to rea" +
        "rrange.");
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.AutoSize = true;
            this.groupBox6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox6.Controls.Add(this.tableLayoutPanel5);
            this.groupBox6.ForeColor = System.Drawing.Color.Blue;
            this.groupBox6.Location = new System.Drawing.Point(860, 8);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(8);
            this.groupBox6.MaximumSize = new System.Drawing.Size(410, 0);
            this.groupBox6.MinimumSize = new System.Drawing.Size(410, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Size = new System.Drawing.Size(410, 134);
            this.groupBox6.TabIndex = 42;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Threading (not recommended)";
            this.groupBox6.Visible = false;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.AutoSize = true;
            this.tableLayoutPanel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.label14, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this._numEThreads, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label15, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this._numBThreads, 0, 3);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.Padding = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(404, 108);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.AutoSize = true;
            this.groupBox7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox7.Controls.Add(this.tableLayoutPanel6);
            this.groupBox7.ForeColor = System.Drawing.Color.Blue;
            this.groupBox7.Location = new System.Drawing.Point(434, 8);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(8);
            this.groupBox7.MaximumSize = new System.Drawing.Size(410, 0);
            this.groupBox7.MinimumSize = new System.Drawing.Size(410, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox7.Size = new System.Drawing.Size(410, 342);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Breeding";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.AutoSize = true;
            this.tableLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this._numElites, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this._lblBreeding, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this._lstSelection, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this._lstBreeding, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this._lblMutation, 0, 6);
            this.tableLayoutPanel6.Controls.Add(this._lstMutation, 0, 7);
            this.tableLayoutPanel6.Controls.Add(this._lblCrossover, 0, 8);
            this.tableLayoutPanel6.Controls.Add(this._lstCrossover, 0, 9);
            this.tableLayoutPanel6.Controls.Add(this.label2, 0, 10);
            this.tableLayoutPanel6.Controls.Add(this._numMaxTree, 0, 11);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.Padding = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel6.RowCount = 12;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(404, 316);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // _lstSelection
            // 
            this._lstSelection.AutoSize = true;
            this._lstSelection.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._lstSelection.Dock = System.Windows.Forms.DockStyle.Top;
            this._lstSelection.Image = global::EvoveUi.Properties.Resources.arrows;
            this._lstSelection.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._lstSelection.Location = new System.Drawing.Point(8, 75);
            this._lstSelection.Margin = new System.Windows.Forms.Padding(0);
            this._lstSelection.Name = "_lstSelection";
            this._lstSelection.Size = new System.Drawing.Size(388, 27);
            this._lstSelection.TabIndex = 13;
            this._lstSelection.Text = "(none)";
            this._lstSelection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._lstSelection.UseVisualStyleBackColor = true;
            this._lstSelection.Click += new System.EventHandler(this._lstSelection_Click);
            // 
            // _lstBreeding
            // 
            this._lstBreeding.AutoSize = true;
            this._lstBreeding.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._lstBreeding.Dock = System.Windows.Forms.DockStyle.Top;
            this._lstBreeding.Image = global::EvoveUi.Properties.Resources.arrows;
            this._lstBreeding.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._lstBreeding.Location = new System.Drawing.Point(8, 127);
            this._lstBreeding.Margin = new System.Windows.Forms.Padding(0);
            this._lstBreeding.Name = "_lstBreeding";
            this._lstBreeding.Size = new System.Drawing.Size(388, 27);
            this._lstBreeding.TabIndex = 25;
            this._lstBreeding.Text = "(none)";
            this._lstBreeding.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._lstBreeding.UseVisualStyleBackColor = true;
            this._lstBreeding.Click += new System.EventHandler(this._lstBreeding_Click);
            // 
            // _lstMutation
            // 
            this._lstMutation.AutoSize = true;
            this._lstMutation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._lstMutation.Dock = System.Windows.Forms.DockStyle.Top;
            this._lstMutation.Image = global::EvoveUi.Properties.Resources.arrows;
            this._lstMutation.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._lstMutation.Location = new System.Drawing.Point(8, 179);
            this._lstMutation.Margin = new System.Windows.Forms.Padding(0);
            this._lstMutation.Name = "_lstMutation";
            this._lstMutation.Size = new System.Drawing.Size(388, 27);
            this._lstMutation.TabIndex = 17;
            this._lstMutation.Text = "(none)";
            this._lstMutation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._lstMutation.UseVisualStyleBackColor = true;
            this._lstMutation.Click += new System.EventHandler(this._lstMutation_Click);
            // 
            // _lstCrossover
            // 
            this._lstCrossover.AutoSize = true;
            this._lstCrossover.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._lstCrossover.Dock = System.Windows.Forms.DockStyle.Top;
            this._lstCrossover.Image = global::EvoveUi.Properties.Resources.arrows;
            this._lstCrossover.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._lstCrossover.Location = new System.Drawing.Point(8, 231);
            this._lstCrossover.Margin = new System.Windows.Forms.Padding(0);
            this._lstCrossover.Name = "_lstCrossover";
            this._lstCrossover.Size = new System.Drawing.Size(388, 27);
            this._lstCrossover.TabIndex = 15;
            this._lstCrossover.Text = "(none)";
            this._lstCrossover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._lstCrossover.UseVisualStyleBackColor = true;
            this._lstCrossover.Click += new System.EventHandler(this._lstCrossover_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.AutoSize = true;
            this.groupBox9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox9.Controls.Add(this.tableLayoutPanel8);
            this.groupBox9.ForeColor = System.Drawing.Color.Blue;
            this.groupBox9.Location = new System.Drawing.Point(8, 397);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(8);
            this.groupBox9.MaximumSize = new System.Drawing.Size(410, 0);
            this.groupBox9.MinimumSize = new System.Drawing.Size(410, 0);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox9.Size = new System.Drawing.Size(410, 184);
            this.groupBox9.TabIndex = 44;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Population";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.AutoSize = true;
            this.tableLayoutPanel8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.label17, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this._numDemes, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.label11, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this._lstMigration, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this._numIndividuals, 0, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.Padding = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel8.RowCount = 6;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(404, 158);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // _lstMigration
            // 
            this._lstMigration.AutoSize = true;
            this._lstMigration.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._lstMigration.Dock = System.Windows.Forms.DockStyle.Top;
            this._lstMigration.Image = global::EvoveUi.Properties.Resources.arrows;
            this._lstMigration.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._lstMigration.Location = new System.Drawing.Point(8, 123);
            this._lstMigration.Margin = new System.Windows.Forms.Padding(0);
            this._lstMigration.Name = "_lstMigration";
            this._lstMigration.Size = new System.Drawing.Size(388, 27);
            this._lstMigration.TabIndex = 36;
            this._lstMigration.Text = "(none)";
            this._lstMigration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._lstMigration.UseVisualStyleBackColor = true;
            this._lstMigration.Click += new System.EventHandler(this._lstMigration_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Individuals per island";
            // 
            // _btnOk
            // 
            this._btnOk.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._btnOk.Location = new System.Drawing.Point(1011, 8);
            this._btnOk.Margin = new System.Windows.Forms.Padding(8);
            this._btnOk.Name = "_btnOk";
            this._btnOk.Size = new System.Drawing.Size(128, 41);
            this._btnOk.TabIndex = 45;
            this._btnOk.Text = "OK";
            this._btnOk.UseVisualStyleBackColor = true;
            this._btnOk.Click += new System.EventHandler(this.button1_Click);
            // 
            // _btnCancel
            // 
            this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btnCancel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._btnCancel.Location = new System.Drawing.Point(1147, 8);
            this._btnCancel.Margin = new System.Windows.Forms.Padding(0, 8, 8, 8);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(131, 41);
            this._btnCancel.TabIndex = 46;
            this._btnCancel.Text = "Cancel";
            this._btnCancel.UseVisualStyleBackColor = true;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 50000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.groupBox4);
            this.flowLayoutPanel1.Controls.Add(this.groupBox9);
            this.flowLayoutPanel1.Controls.Add(this.groupBox1);
            this.flowLayoutPanel1.Controls.Add(this.groupBox7);
            this.flowLayoutPanel1.Controls.Add(this.groupBox5);
            this.flowLayoutPanel1.Controls.Add(this.groupBox2);
            this.flowLayoutPanel1.Controls.Add(this.groupBox6);
            this.flowLayoutPanel1.Controls.Add(this.groupBox3);
            this.flowLayoutPanel1.Controls.Add(this.groupBox8);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1286, 729);
            this.flowLayoutPanel1.TabIndex = 47;
            // 
            // groupBox8
            // 
            this.groupBox8.AutoSize = true;
            this.groupBox8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox8.Controls.Add(this.tableLayoutPanel9);
            this.groupBox8.ForeColor = System.Drawing.Color.Blue;
            this.groupBox8.Location = new System.Drawing.Point(860, 378);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(8);
            this.groupBox8.MaximumSize = new System.Drawing.Size(410, 0);
            this.groupBox8.MinimumSize = new System.Drawing.Size(410, 0);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox8.Size = new System.Drawing.Size(410, 69);
            this.groupBox8.TabIndex = 46;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Advanced";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.AutoSize = true;
            this.tableLayoutPanel9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.button5, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.Padding = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(404, 43);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // button5
            // 
            this.button5.AutoSize = true;
            this.button5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.Image = global::EvoveUi.Properties.Resources.arrows;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.Location = new System.Drawing.Point(8, 8);
            this.button5.Margin = new System.Windows.Forms.Padding(0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(388, 27);
            this.button5.TabIndex = 14;
            this.button5.Text = "Show advanced settings    ";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1286, 24);
            this.menuStrip1.TabIndex = 48;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveConfigurationToolStripMenuItem,
            this.loadConfigurationToolStripMenuItem,
            this.toolStripMenuItem2,
            this.loadAnAssemblyToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // saveConfigurationToolStripMenuItem
            // 
            this.saveConfigurationToolStripMenuItem.Name = "saveConfigurationToolStripMenuItem";
            this.saveConfigurationToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.saveConfigurationToolStripMenuItem.Text = "&Save configuration...";
            this.saveConfigurationToolStripMenuItem.Click += new System.EventHandler(this.saveConfigurationToolStripMenuItem_Click);
            // 
            // loadConfigurationToolStripMenuItem
            // 
            this.loadConfigurationToolStripMenuItem.Name = "loadConfigurationToolStripMenuItem";
            this.loadConfigurationToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.loadConfigurationToolStripMenuItem.Text = "&Load configuration...";
            this.loadConfigurationToolStripMenuItem.Click += new System.EventHandler(this.loadConfigurationToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(181, 6);
            // 
            // loadAnAssemblyToolStripMenuItem
            // 
            this.loadAnAssemblyToolStripMenuItem.Image = global::EvoveUi.Properties.Resources.Assembly_6212;
            this.loadAnAssemblyToolStripMenuItem.Name = "loadAnAssemblyToolStripMenuItem";
            this.loadAnAssemblyToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.loadAnAssemblyToolStripMenuItem.Text = "&Load an assembly...";
            this.loadAnAssemblyToolStripMenuItem.Click += new System.EventHandler(this.loadAnAssemblyToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(181, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.AutoSize = true;
            this.tableLayoutPanel10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.Controls.Add(this._btnCancel, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this._btnOk, 1, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 753);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(1286, 57);
            this.tableLayoutPanel10.TabIndex = 49;
            // 
            // FrmParameterEditor
            // 
            this.AcceptButton = this._btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btnCancel;
            this.ClientSize = new System.Drawing.Size(1286, 810);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tableLayoutPanel10);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmParameterEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Parameter Editor";
            ((System.ComponentModel.ISupportInitialize)(this._numIndividuals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numMaxTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numElites)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numDemes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numEThreads)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numBThreads)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._numTestCases)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._numMethodSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._numRandomSeed)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NumericUpDown _numIndividuals;
        private System.Windows.Forms.NumericUpDown _numMaxTree;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox _lstTermination;
        private System.Windows.Forms.NumericUpDown _numElites;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox _lstStopping;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button _lstSelection;
        private System.Windows.Forms.Label _lblBreeding;
        private System.Windows.Forms.Label _lblCrossover;
        private System.Windows.Forms.Button _lstCrossover;
        private System.Windows.Forms.Label _lblMutation;
        private System.Windows.Forms.Button _lstMutation;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox _txtInput;
        private System.Windows.Forms.NumericUpDown _numDemes;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox _txtOutput;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button _lstBreeding;
        private System.Windows.Forms.NumericUpDown _numEThreads;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown _numBThreads;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox _chkCaching;
        private System.Windows.Forms.CheckBox _chkOverloads;
        private System.Windows.Forms.CheckBox _chkDebugging;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button _lstMigration;
        private System.Windows.Forms.Label _lblTag;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button _btnOk;
        private System.Windows.Forms.Button _btnCancel;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label _lblTestCases;
        private System.Windows.Forms.NumericUpDown _numTestCases;
        private System.Windows.Forms.CheckBox _chkErrors;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button _btnFitness;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button _btnAddFunction;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox _lstFunctions;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadAnAssemblyToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.Button _btnAddCustomData;
        private System.Windows.Forms.ListBox _lstCustomData;
        private System.Windows.Forms.CheckBox _chkAbandonPEOs;
        private System.Windows.Forms.NumericUpDown _numRandomSeed;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown _numMethodSet;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.CheckBox _chkSubsetMethods;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox _txtTitle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
    }
}