﻿namespace EvoveUi
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Individuals can be examined when the session is paused");
            MCharting.Selection selection1 = new MCharting.Selection();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this._lstCurrent = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this._lstPrevious = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listView3 = new System.Windows.Forms.ListView();
            this._tabTabControl = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._btnEditParameters = new System.Windows.Forms.Button();
            this._btnStart = new System.Windows.Forms.Button();
            this._btnStep = new System.Windows.Forms.Button();
            this._btnPlay = new System.Windows.Forms.Button();
            this._btnPause = new System.Windows.Forms.Button();
            this._btnBreakpoint = new System.Windows.Forms.Button();
            this._btnStop = new System.Windows.Forms.Button();
            this._chkUnattendedMode = new System.Windows.Forms.CheckBox();
            this.toolStrip15 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel25 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel26 = new System.Windows.Forms.ToolStripLabel();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this._lblPopulation = new System.Windows.Forms.ToolStripLabel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this._lstPreviousIndividuals = new System.Windows.Forms.ListView();
            this.toolStrip9 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel11 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel12 = new System.Windows.Forms.ToolStripLabel();
            this._lblPreviousPopulation2 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel6 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this._lblPreviousPopulation = new System.Windows.Forms.ToolStripLabel();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this._txtError = new System.Windows.Forms.TextBox();
            this._lstProblemIndividuals = new System.Windows.Forms.ListView();
            this.toolStrip14 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel24 = new System.Windows.Forms.ToolStripLabel();
            this._btnIgnoreError = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip5 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel7 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this._lblErrorTitle = new System.Windows.Forms.ToolStripLabel();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.toolStrip6 = new System.Windows.Forms.ToolStrip();
            this._lblLog = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel10 = new System.Windows.Forms.ToolStripLabel();
            this._tabFunctions = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.toolStrip8 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel9 = new System.Windows.Forms.ToolStripLabel();
            this._chkProviders = new System.Windows.Forms.ToolStripButton();
            this._chkReceivers = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip7 = new System.Windows.Forms.ToolStrip();
            this._lblSyntax = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this._txtSyntax = new System.Windows.Forms.ToolStripLabel();
            this._lstSyntaxDetails = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this._tabIndividual = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this._tvwIndividual = new System.Windows.Forms.TreeView();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this._btnEnableCaching = new System.Windows.Forms.Button();
            this._lstNodeDetails = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator27 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel18 = new System.Windows.Forms.ToolStripLabel();
            this._btnCollapse = new System.Windows.Forms.ToolStripButton();
            this._btnExpand = new System.Windows.Forms.ToolStripButton();
            this._chkSimpleView = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this._btnDetails = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel8 = new System.Windows.Forms.ToolStripLabel();
            this._btnRun = new System.Windows.Forms.ToolStripButton();
            this._btnCrossover1 = new System.Windows.Forms.ToolStripButton();
            this._btnCrossover2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this._btnMutate = new System.Windows.Forms.ToolStripButton();
            this._btnClearTreeCache = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this._btnShowSyntax = new System.Windows.Forms.ToolStripButton();
            this._btnSwap1 = new System.Windows.Forms.ToolStripButton();
            this._btnSwap2 = new System.Windows.Forms.ToolStripButton();
            this._btnInsert = new System.Windows.Forms.ToolStripButton();
            this._btnReorder = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this._btnClearNodeCache = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this._btnMother = new System.Windows.Forms.ToolStripButton();
            this._btnFather = new System.Windows.Forms.ToolStripButton();
            this._btnSister = new System.Windows.Forms.ToolStripButton();
            this._btnBrother = new System.Windows.Forms.ToolStripButton();
            this._btnHistory = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this._btnCancel = new System.Windows.Forms.ToolStripButton();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel13 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this._lblIndividual = new System.Windows.Forms.ToolStripLabel();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._lblNotEvaluated = new System.Windows.Forms.Label();
            this._lblEvaluated = new System.Windows.Forms.Label();
            this._lblEvaluated2 = new System.Windows.Forms.Label();
            this._lblDeferred = new System.Windows.Forms.Label();
            this._lblFlagged = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this._lblDataType = new System.Windows.Forms.Label();
            this._lblProvider = new System.Windows.Forms.Label();
            this._lblReceiver = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.toolStrip16 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel27 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator28 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel28 = new System.Windows.Forms.ToolStripLabel();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.toolStrip13 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator25 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel22 = new System.Windows.Forms.ToolStripLabel();
            this._btnRefresh = new System.Windows.Forms.ToolStripButton();
            this._btnClearLog = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel23 = new System.Windows.Forms.ToolStripLabel();
            this._btnDebug = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator26 = new System.Windows.Forms.ToolStripSeparator();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.toolStrip10 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel14 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel15 = new System.Windows.Forms.ToolStripLabel();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this._plot = new MCharting.MChart();
            this.toolStrip12 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel19 = new System.Windows.Forms.ToolStripLabel();
            this._btnRefreshPlot = new System.Windows.Forms.ToolStripButton();
            this._chkAutoPlot = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel20 = new System.Windows.Forms.ToolStripLabel();
            this._chkFitness = new System.Windows.Forms.ToolStripButton();
            this._chkTree = new System.Windows.Forms.ToolStripButton();
            this._chkExecution = new System.Windows.Forms.ToolStripButton();
            this._chkRound = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator30 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this._chkYFitness = new System.Windows.Forms.ToolStripButton();
            this._chkYSize = new System.Windows.Forms.ToolStripButton();
            this._chkYTime = new System.Windows.Forms.ToolStripButton();
            this._chkYRound = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator32 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel21 = new System.Windows.Forms.ToolStripLabel();
            this._chkElite = new System.Windows.Forms.ToolStripButton();
            this._chkHighest = new System.Windows.Forms.ToolStripButton();
            this._chkLowest = new System.Windows.Forms.ToolStripButton();
            this._chkMean = new System.Windows.Forms.ToolStripButton();
            this._chkAll = new System.Windows.Forms.ToolStripButton();
            this._chkTruncate = new System.Windows.Forms.ToolStripButton();
            this.toolStrip11 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel16 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel17 = new System.Windows.Forms.ToolStripLabel();
            this._pgLock = new System.Windows.Forms.TabPage();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this._btnEditParameters2 = new System.Windows.Forms.ToolStripButton();
            this._btnStart2 = new System.Windows.Forms.ToolStripButton();
            this._btnStep2 = new System.Windows.Forms.ToolStripButton();
            this._btnPlay2 = new System.Windows.Forms.ToolStripButton();
            this._btnPause2 = new System.Windows.Forms.ToolStripButton();
            this._btnStop2 = new System.Windows.Forms.ToolStripButton();
            this._btnBreakpoint2 = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._lblIDebugging = new System.Windows.Forms.ToolStripStatusLabel();
            this._lblLastLog = new System.Windows.Forms.ToolStripStatusLabel();
            this._lblIAutomatic = new System.Windows.Forms.ToolStripStatusLabel();
            this._lblIError = new System.Windows.Forms.ToolStripStatusLabel();
            this._lblIStopping = new System.Windows.Forms.ToolStripStatusLabel();
            this._lblIPaused = new System.Windows.Forms.ToolStripStatusLabel();
            this._lblIBusy = new System.Windows.Forms.ToolStripStatusLabel();
            this._lblIStepThrough = new System.Windows.Forms.ToolStripStatusLabel();
            this._lblIStopped = new System.Windows.Forms.ToolStripStatusLabel();
            this._lblINoSession = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._btnStart3 = new System.Windows.Forms.ToolStripMenuItem();
            this._btnStep3 = new System.Windows.Forms.ToolStripMenuItem();
            this._btnPlay3 = new System.Windows.Forms.ToolStripMenuItem();
            this._btnPause3 = new System.Windows.Forms.ToolStripMenuItem();
            this._btnStop3 = new System.Windows.Forms.ToolStripMenuItem();
            this._btnBreakpoint3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this._btnEditParameters3 = new System.Windows.Forms.ToolStripMenuItem();
            this._chkAttendedMode3 = new System.Windows.Forms.ToolStripMenuItem();
            this.label28 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._tabTabControl.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.toolStrip15.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.toolStrip4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.toolStrip9.SuspendLayout();
            this._lblPreviousPopulation2.SuspendLayout();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.toolStrip14.SuspendLayout();
            this.toolStrip5.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.toolStrip6.SuspendLayout();
            this._tabFunctions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.toolStrip8.SuspendLayout();
            this.toolStrip7.SuspendLayout();
            this._tabIndividual.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.toolStrip16.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.toolStrip13.SuspendLayout();
            this.toolStrip10.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.toolStrip12.SuspendLayout();
            this.toolStrip11.SuspendLayout();
            this._pgLock.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // _lstCurrent
            // 
            this._lstCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lstCurrent.LargeImageList = this.imageList1;
            this._lstCurrent.Location = new System.Drawing.Point(3, 28);
            this._lstCurrent.Name = "_lstCurrent";
            this._lstCurrent.Size = new System.Drawing.Size(1056, 579);
            this._lstCurrent.SmallImageList = this.imageList1;
            this._lstCurrent.TabIndex = 0;
            this._lstCurrent.UseCompatibleStateImageBehavior = false;
            this._lstCurrent.View = System.Windows.Forms.View.Details;
            this._lstCurrent.Visible = false;
            this._lstCurrent.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Pending");
            this.imageList1.Images.SetKeyName(1, "Complete");
            this.imageList1.Images.SetKeyName(2, "Best");
            // 
            // _lstPrevious
            // 
            this._lstPrevious.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6});
            this._lstPrevious.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lstPrevious.Location = new System.Drawing.Point(0, 0);
            this._lstPrevious.Name = "_lstPrevious";
            this._lstPrevious.Size = new System.Drawing.Size(1056, 289);
            this._lstPrevious.TabIndex = 0;
            this._lstPrevious.UseCompatibleStateImageBehavior = false;
            this._lstPrevious.View = System.Windows.Forms.View.Details;
            this._lstPrevious.ItemActivate += new System.EventHandler(this._lstPrevious_ItemActivate);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Round";
            this.columnHeader5.Width = 66;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Occasion";
            this.columnHeader6.Width = 366;
            // 
            // listView3
            // 
            this.listView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView3.Location = new System.Drawing.Point(3, 28);
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(1056, 579);
            this.listView3.TabIndex = 0;
            this.listView3.UseCompatibleStateImageBehavior = false;
            this.listView3.View = System.Windows.Forms.View.Details;
            // 
            // _tabTabControl
            // 
            this._tabTabControl.Controls.Add(this.tabPage6);
            this._tabTabControl.Controls.Add(this.tabPage1);
            this._tabTabControl.Controls.Add(this.tabPage2);
            this._tabTabControl.Controls.Add(this.tabPage8);
            this._tabTabControl.Controls.Add(this.tabPage3);
            this._tabTabControl.Controls.Add(this._tabFunctions);
            this._tabTabControl.Controls.Add(this._tabIndividual);
            this._tabTabControl.Controls.Add(this.tabPage7);
            this._tabTabControl.Controls.Add(this.tabPage4);
            this._tabTabControl.Controls.Add(this.tabPage5);
            this._tabTabControl.Controls.Add(this._pgLock);
            this._tabTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabTabControl.Location = new System.Drawing.Point(0, 49);
            this._tabTabControl.Name = "_tabTabControl";
            this._tabTabControl.SelectedIndex = 0;
            this._tabTabControl.Size = new System.Drawing.Size(1070, 636);
            this._tabTabControl.TabIndex = 6;
            this._tabTabControl.SelectedIndexChanged += new System.EventHandler(this._tabTabControl_SelectedIndexChanged);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.flowLayoutPanel1);
            this.tabPage6.Controls.Add(this.toolStrip15);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1062, 610);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Controls";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanel1.Controls.Add(this._btnEditParameters);
            this.flowLayoutPanel1.Controls.Add(this._btnStart);
            this.flowLayoutPanel1.Controls.Add(this._btnStep);
            this.flowLayoutPanel1.Controls.Add(this._btnPlay);
            this.flowLayoutPanel1.Controls.Add(this._btnPause);
            this.flowLayoutPanel1.Controls.Add(this._btnBreakpoint);
            this.flowLayoutPanel1.Controls.Add(this._btnStop);
            this.flowLayoutPanel1.Controls.Add(this._chkUnattendedMode);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 28);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(8);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1056, 579);
            this.flowLayoutPanel1.TabIndex = 0;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // _btnEditParameters
            // 
            this._btnEditParameters.Image = global::EvoveUi.Properties.Resources.EditorZone_6025;
            this._btnEditParameters.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnEditParameters.Location = new System.Drawing.Point(16, 16);
            this._btnEditParameters.Margin = new System.Windows.Forms.Padding(8, 8, 8, 0);
            this._btnEditParameters.Name = "_btnEditParameters";
            this._btnEditParameters.Padding = new System.Windows.Forms.Padding(8);
            this._btnEditParameters.Size = new System.Drawing.Size(301, 45);
            this._btnEditParameters.TabIndex = 6;
            this._btnEditParameters.Text = "  Edit parameters";
            this._btnEditParameters.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnEditParameters.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this._btnEditParameters.UseVisualStyleBackColor = true;
            this._btnEditParameters.Click += new System.EventHandler(this._btnEditParameters_Click);
            // 
            // _btnStart
            // 
            this._btnStart.Image = global::EvoveUi.Properties.Resources.NewBuildDefinition_8952;
            this._btnStart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnStart.Location = new System.Drawing.Point(16, 69);
            this._btnStart.Margin = new System.Windows.Forms.Padding(8, 8, 8, 0);
            this._btnStart.Name = "_btnStart";
            this._btnStart.Padding = new System.Windows.Forms.Padding(8);
            this._btnStart.Size = new System.Drawing.Size(301, 45);
            this._btnStart.TabIndex = 0;
            this._btnStart.Text = "  Create";
            this._btnStart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnStart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this._btnStart.UseVisualStyleBackColor = true;
            this._btnStart.Click += new System.EventHandler(this._btnStart_Click);
            // 
            // _btnStep
            // 
            this._btnStep.Enabled = false;
            this._btnStep.Image = global::EvoveUi.Properties.Resources.Step_16xLG;
            this._btnStep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnStep.Location = new System.Drawing.Point(16, 122);
            this._btnStep.Margin = new System.Windows.Forms.Padding(8, 8, 8, 0);
            this._btnStep.Name = "_btnStep";
            this._btnStep.Padding = new System.Windows.Forms.Padding(8);
            this._btnStep.Size = new System.Drawing.Size(301, 45);
            this._btnStep.TabIndex = 1;
            this._btnStep.Text = "  Step";
            this._btnStep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnStep.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this._btnStep.UseVisualStyleBackColor = true;
            this._btnStep.Click += new System.EventHandler(this._btnStep_Click);
            // 
            // _btnPlay
            // 
            this._btnPlay.Enabled = false;
            this._btnPlay.Image = global::EvoveUi.Properties.Resources.PlayHS;
            this._btnPlay.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnPlay.Location = new System.Drawing.Point(16, 175);
            this._btnPlay.Margin = new System.Windows.Forms.Padding(8, 8, 8, 0);
            this._btnPlay.Name = "_btnPlay";
            this._btnPlay.Padding = new System.Windows.Forms.Padding(8);
            this._btnPlay.Size = new System.Drawing.Size(301, 45);
            this._btnPlay.TabIndex = 2;
            this._btnPlay.Text = "  Run";
            this._btnPlay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnPlay.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this._btnPlay.UseVisualStyleBackColor = true;
            this._btnPlay.Click += new System.EventHandler(this._btnPlay_Click);
            // 
            // _btnPause
            // 
            this._btnPause.Enabled = false;
            this._btnPause.Image = global::EvoveUi.Properties.Resources.PauseHS;
            this._btnPause.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnPause.Location = new System.Drawing.Point(16, 228);
            this._btnPause.Margin = new System.Windows.Forms.Padding(8, 8, 8, 0);
            this._btnPause.Name = "_btnPause";
            this._btnPause.Padding = new System.Windows.Forms.Padding(8);
            this._btnPause.Size = new System.Drawing.Size(301, 45);
            this._btnPause.TabIndex = 3;
            this._btnPause.Text = "  Pause";
            this._btnPause.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnPause.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this._btnPause.UseVisualStyleBackColor = true;
            this._btnPause.Click += new System.EventHandler(this._btnPause_Click);
            // 
            // _btnBreakpoint
            // 
            this._btnBreakpoint.Enabled = false;
            this._btnBreakpoint.Image = global::EvoveUi.Properties.Resources.BreakpointEnabled_6584_16x;
            this._btnBreakpoint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnBreakpoint.Location = new System.Drawing.Point(16, 281);
            this._btnBreakpoint.Margin = new System.Windows.Forms.Padding(8, 8, 8, 0);
            this._btnBreakpoint.Name = "_btnBreakpoint";
            this._btnBreakpoint.Padding = new System.Windows.Forms.Padding(8);
            this._btnBreakpoint.Size = new System.Drawing.Size(301, 45);
            this._btnBreakpoint.TabIndex = 3;
            this._btnBreakpoint.Text = "  Add breakpoint...";
            this._btnBreakpoint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnBreakpoint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this._btnBreakpoint.UseVisualStyleBackColor = true;
            this._btnBreakpoint.Click += new System.EventHandler(this._btnBreakpoint_Click);
            // 
            // _btnStop
            // 
            this._btnStop.Enabled = false;
            this._btnStop.Image = global::EvoveUi.Properties.Resources.StopHS;
            this._btnStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnStop.Location = new System.Drawing.Point(16, 334);
            this._btnStop.Margin = new System.Windows.Forms.Padding(8, 8, 8, 32);
            this._btnStop.Name = "_btnStop";
            this._btnStop.Padding = new System.Windows.Forms.Padding(8);
            this._btnStop.Size = new System.Drawing.Size(301, 45);
            this._btnStop.TabIndex = 4;
            this._btnStop.Text = "  Stop";
            this._btnStop.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnStop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this._btnStop.UseVisualStyleBackColor = true;
            this._btnStop.Click += new System.EventHandler(this._btnStop_Click);
            // 
            // _chkUnattendedMode
            // 
            this._chkUnattendedMode.AutoSize = true;
            this._chkUnattendedMode.Location = new System.Drawing.Point(32, 419);
            this._chkUnattendedMode.Margin = new System.Windows.Forms.Padding(24, 8, 8, 0);
            this._chkUnattendedMode.Name = "_chkUnattendedMode";
            this._chkUnattendedMode.Size = new System.Drawing.Size(154, 25);
            this._chkUnattendedMode.TabIndex = 10;
            this._chkUnattendedMode.Text = "Unattended mode";
            this._chkUnattendedMode.UseVisualStyleBackColor = true;
            this._chkUnattendedMode.CheckedChanged += new System.EventHandler(this._chkAutoClose_CheckedChanged);
            // 
            // toolStrip15
            // 
            this.toolStrip15.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel25,
            this.toolStripSeparator23,
            this.toolStripLabel26});
            this.toolStrip15.Location = new System.Drawing.Point(3, 3);
            this.toolStrip15.Name = "toolStrip15";
            this.toolStrip15.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip15.Size = new System.Drawing.Size(1056, 25);
            this.toolStrip15.TabIndex = 2;
            this.toolStrip15.Text = "toolStrip15";
            // 
            // toolStripLabel25
            // 
            this.toolStripLabel25.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel25.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel25.Name = "toolStripLabel25";
            this.toolStripLabel25.Size = new System.Drawing.Size(69, 22);
            this.toolStripLabel25.Text = "CONTROLS";
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel26
            // 
            this.toolStripLabel26.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel26.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel26.Name = "toolStripLabel26";
            this.toolStripLabel26.Size = new System.Drawing.Size(196, 22);
            this.toolStripLabel26.Text = "Control your simulation from here";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._lstCurrent);
            this.tabPage1.Controls.Add(this.toolStrip4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1062, 610);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Current";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // toolStrip4
            // 
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel4,
            this.toolStripSeparator6,
            this._lblPopulation});
            this.toolStrip4.Location = new System.Drawing.Point(3, 3);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip4.Size = new System.Drawing.Size(1056, 25);
            this.toolStrip4.TabIndex = 1;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(139, 22);
            this.toolStripLabel4.Text = "CURRENT GENERATION";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // _lblPopulation
            // 
            this._lblPopulation.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblPopulation.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._lblPopulation.Name = "_lblPopulation";
            this._lblPopulation.Size = new System.Drawing.Size(338, 22);
            this._lblPopulation.Text = "The current generation is shown when the session is paused";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer4);
            this.tabPage2.Controls.Add(this._lblPreviousPopulation2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1062, 610);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Previous";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(3, 28);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this._lstPrevious);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this._lstPreviousIndividuals);
            this.splitContainer4.Panel2.Controls.Add(this.toolStrip9);
            this.splitContainer4.Size = new System.Drawing.Size(1056, 579);
            this.splitContainer4.SplitterDistance = 289;
            this.splitContainer4.TabIndex = 3;
            // 
            // _lstPreviousIndividuals
            // 
            this._lstPreviousIndividuals.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lstPreviousIndividuals.LargeImageList = this.imageList1;
            this._lstPreviousIndividuals.Location = new System.Drawing.Point(0, 25);
            this._lstPreviousIndividuals.Name = "_lstPreviousIndividuals";
            this._lstPreviousIndividuals.Size = new System.Drawing.Size(1056, 261);
            this._lstPreviousIndividuals.SmallImageList = this.imageList1;
            this._lstPreviousIndividuals.TabIndex = 1;
            this._lstPreviousIndividuals.UseCompatibleStateImageBehavior = false;
            this._lstPreviousIndividuals.View = System.Windows.Forms.View.Details;
            this._lstPreviousIndividuals.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
            // 
            // toolStrip9
            // 
            this.toolStrip9.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel11,
            this.toolStripSeparator15,
            this.toolStripLabel12});
            this.toolStrip9.Location = new System.Drawing.Point(0, 0);
            this.toolStrip9.Name = "toolStrip9";
            this.toolStrip9.Size = new System.Drawing.Size(1056, 25);
            this.toolStrip9.TabIndex = 3;
            this.toolStrip9.Text = "toolStrip5";
            // 
            // toolStripLabel11
            // 
            this.toolStripLabel11.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel11.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel11.Name = "toolStripLabel11";
            this.toolStripLabel11.Size = new System.Drawing.Size(143, 22);
            this.toolStripLabel11.Text = "PREVIOUS GENERATION";
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel12
            // 
            this.toolStripLabel12.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel12.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel12.Name = "toolStripLabel12";
            this.toolStripLabel12.Size = new System.Drawing.Size(269, 22);
            this.toolStripLabel12.Text = "The previous generations can be explored here";
            // 
            // _lblPreviousPopulation2
            // 
            this._lblPreviousPopulation2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel6,
            this.toolStripSeparator7,
            this._lblPreviousPopulation});
            this._lblPreviousPopulation2.Location = new System.Drawing.Point(3, 3);
            this._lblPreviousPopulation2.Name = "_lblPreviousPopulation2";
            this._lblPreviousPopulation2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this._lblPreviousPopulation2.Size = new System.Drawing.Size(1056, 25);
            this._lblPreviousPopulation2.TabIndex = 2;
            this._lblPreviousPopulation2.Text = "toolStrip5";
            // 
            // toolStripLabel6
            // 
            this.toolStripLabel6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel6.Name = "toolStripLabel6";
            this.toolStripLabel6.Size = new System.Drawing.Size(150, 22);
            this.toolStripLabel6.Text = "PREVIOUS GENERATIONS";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // _lblPreviousPopulation
            // 
            this._lblPreviousPopulation.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblPreviousPopulation.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._lblPreviousPopulation.Name = "_lblPreviousPopulation";
            this._lblPreviousPopulation.Size = new System.Drawing.Size(245, 22);
            this._lblPreviousPopulation.Text = "Previous generations can be explored here";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.splitContainer3);
            this.tabPage8.Controls.Add(this.toolStrip14);
            this.tabPage8.Controls.Add(this.toolStrip5);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(1062, 610);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Errors";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(103, 28);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this._txtError);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this._lstProblemIndividuals);
            this.splitContainer3.Size = new System.Drawing.Size(956, 579);
            this.splitContainer3.SplitterDistance = 353;
            this.splitContainer3.TabIndex = 4;
            // 
            // _txtError
            // 
            this._txtError.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtError.Location = new System.Drawing.Point(0, 0);
            this._txtError.Multiline = true;
            this._txtError.Name = "_txtError";
            this._txtError.ReadOnly = true;
            this._txtError.Size = new System.Drawing.Size(956, 353);
            this._txtError.TabIndex = 0;
            // 
            // _lstProblemIndividuals
            // 
            this._lstProblemIndividuals.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lstProblemIndividuals.LargeImageList = this.imageList1;
            this._lstProblemIndividuals.Location = new System.Drawing.Point(0, 0);
            this._lstProblemIndividuals.Name = "_lstProblemIndividuals";
            this._lstProblemIndividuals.Size = new System.Drawing.Size(956, 222);
            this._lstProblemIndividuals.SmallImageList = this.imageList1;
            this._lstProblemIndividuals.TabIndex = 2;
            this._lstProblemIndividuals.UseCompatibleStateImageBehavior = false;
            this._lstProblemIndividuals.View = System.Windows.Forms.View.Details;
            this._lstProblemIndividuals.Visible = false;
            this._lstProblemIndividuals.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
            // 
            // toolStrip14
            // 
            this.toolStrip14.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip14.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip14.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator13,
            this.toolStripLabel24,
            this._btnIgnoreError,
            this.toolStripSeparator20});
            this.toolStrip14.Location = new System.Drawing.Point(3, 28);
            this.toolStrip14.Name = "toolStrip14";
            this.toolStrip14.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip14.Size = new System.Drawing.Size(100, 579);
            this.toolStrip14.TabIndex = 5;
            this.toolStrip14.Text = "toolStrip14";
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(97, 6);
            // 
            // toolStripLabel24
            // 
            this.toolStripLabel24.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel24.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel24.Name = "toolStripLabel24";
            this.toolStripLabel24.Size = new System.Drawing.Size(97, 15);
            this.toolStripLabel24.Text = "ACTIONS";
            // 
            // _btnIgnoreError
            // 
            this._btnIgnoreError.Enabled = false;
            this._btnIgnoreError.Image = global::EvoveUi.Properties.Resources.BrushTool_207;
            this._btnIgnoreError.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnIgnoreError.Name = "_btnIgnoreError";
            this._btnIgnoreError.Size = new System.Drawing.Size(97, 20);
            this._btnIgnoreError.Text = "Acknowledge";
            this._btnIgnoreError.Click += new System.EventHandler(this._btnIgnoreError_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(97, 6);
            // 
            // toolStrip5
            // 
            this.toolStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel7,
            this.toolStripSeparator12,
            this._lblErrorTitle});
            this.toolStrip5.Location = new System.Drawing.Point(3, 3);
            this.toolStrip5.Name = "toolStrip5";
            this.toolStrip5.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip5.Size = new System.Drawing.Size(1056, 25);
            this.toolStrip5.TabIndex = 3;
            this.toolStrip5.Text = "toolStrip5";
            // 
            // toolStripLabel7
            // 
            this.toolStripLabel7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel7.Name = "toolStripLabel7";
            this.toolStripLabel7.Size = new System.Drawing.Size(53, 22);
            this.toolStripLabel7.Text = "ERRORS";
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 25);
            // 
            // _lblErrorTitle
            // 
            this._lblErrorTitle.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblErrorTitle.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._lblErrorTitle.Name = "_lblErrorTitle";
            this._lblErrorTitle.Size = new System.Drawing.Size(244, 22);
            this._lblErrorTitle.Text = "Errors affecting individuals are shown here";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.listView3);
            this.tabPage3.Controls.Add(this.toolStrip6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1062, 610);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Log";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // toolStrip6
            // 
            this.toolStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._lblLog,
            this.toolStripSeparator14,
            this.toolStripLabel10});
            this.toolStrip6.Location = new System.Drawing.Point(3, 3);
            this.toolStrip6.Name = "toolStrip6";
            this.toolStrip6.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip6.Size = new System.Drawing.Size(1056, 25);
            this.toolStrip6.TabIndex = 2;
            this.toolStrip6.Text = "toolStrip6";
            // 
            // _lblLog
            // 
            this._lblLog.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblLog.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._lblLog.Name = "_lblLog";
            this._lblLog.Size = new System.Drawing.Size(58, 22);
            this._lblLog.Text = "HISTORY";
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel10
            // 
            this.toolStripLabel10.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel10.Name = "toolStripLabel10";
            this.toolStripLabel10.Size = new System.Drawing.Size(210, 22);
            this.toolStripLabel10.Text = "The population history is shown here";
            // 
            // _tabFunctions
            // 
            this._tabFunctions.Controls.Add(this.splitContainer2);
            this._tabFunctions.Location = new System.Drawing.Point(4, 22);
            this._tabFunctions.Name = "_tabFunctions";
            this._tabFunctions.Padding = new System.Windows.Forms.Padding(3);
            this._tabFunctions.Size = new System.Drawing.Size(1062, 610);
            this._tabFunctions.TabIndex = 3;
            this._tabFunctions.Text = "Functions";
            this._tabFunctions.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.treeView1);
            this.splitContainer2.Panel1.Controls.Add(this.toolStrip8);
            this.splitContainer2.Panel1.Controls.Add(this.toolStrip7);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this._lstSyntaxDetails);
            this.splitContainer2.Size = new System.Drawing.Size(1056, 604);
            this.splitContainer2.SplitterDistance = 778;
            this.splitContainer2.TabIndex = 1;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(77, 25);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(701, 579);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // toolStrip8
            // 
            this.toolStrip8.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip8.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator10,
            this.toolStripLabel9,
            this._chkProviders,
            this._chkReceivers,
            this.toolStripSeparator9});
            this.toolStrip8.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.toolStrip8.Location = new System.Drawing.Point(0, 25);
            this.toolStrip8.Name = "toolStrip8";
            this.toolStrip8.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip8.Size = new System.Drawing.Size(77, 579);
            this.toolStrip8.TabIndex = 3;
            this.toolStrip8.Text = "toolStrip8";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(74, 6);
            // 
            // toolStripLabel9
            // 
            this.toolStripLabel9.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel9.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel9.Name = "toolStripLabel9";
            this.toolStripLabel9.Size = new System.Drawing.Size(74, 15);
            this.toolStripLabel9.Text = "VIEW";
            // 
            // _chkProviders
            // 
            this._chkProviders.Checked = true;
            this._chkProviders.CheckOnClick = true;
            this._chkProviders.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkProviders.Image = global::EvoveUi.Properties.Resources.Function_8941;
            this._chkProviders.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkProviders.Name = "_chkProviders";
            this._chkProviders.Size = new System.Drawing.Size(74, 20);
            this._chkProviders.Text = "Providers";
            this._chkProviders.CheckedChanged += new System.EventHandler(this._chkProviders_CheckedChanged);
            // 
            // _chkReceivers
            // 
            this._chkReceivers.CheckOnClick = true;
            this._chkReceivers.Image = global::EvoveUi.Properties.Resources.Function_8941;
            this._chkReceivers.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkReceivers.Name = "_chkReceivers";
            this._chkReceivers.Size = new System.Drawing.Size(74, 20);
            this._chkReceivers.Text = "Receivers";
            this._chkReceivers.CheckedChanged += new System.EventHandler(this._chkReceivers_CheckedChanged);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(74, 6);
            // 
            // toolStrip7
            // 
            this.toolStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._lblSyntax,
            this.toolStripSeparator11,
            this._txtSyntax});
            this.toolStrip7.Location = new System.Drawing.Point(0, 0);
            this.toolStrip7.Name = "toolStrip7";
            this.toolStrip7.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip7.Size = new System.Drawing.Size(778, 25);
            this.toolStrip7.TabIndex = 2;
            this.toolStrip7.Text = "toolStrip7";
            // 
            // _lblSyntax
            // 
            this._lblSyntax.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblSyntax.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._lblSyntax.Name = "_lblSyntax";
            this._lblSyntax.Size = new System.Drawing.Size(74, 22);
            this._lblSyntax.Text = "FUNCTIONS";
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 25);
            // 
            // _txtSyntax
            // 
            this._txtSyntax.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._txtSyntax.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._txtSyntax.Name = "_txtSyntax";
            this._txtSyntax.Size = new System.Drawing.Size(321, 22);
            this._txtSyntax.Text = "The syntax can be viewed once the population is created";
            // 
            // _lstSyntaxDetails
            // 
            this._lstSyntaxDetails.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this._lstSyntaxDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lstSyntaxDetails.Location = new System.Drawing.Point(0, 0);
            this._lstSyntaxDetails.Name = "_lstSyntaxDetails";
            this._lstSyntaxDetails.Size = new System.Drawing.Size(274, 604);
            this._lstSyntaxDetails.TabIndex = 3;
            this._lstSyntaxDetails.UseCompatibleStateImageBehavior = false;
            this._lstSyntaxDetails.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Name";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Value";
            // 
            // _tabIndividual
            // 
            this._tabIndividual.Controls.Add(this.splitContainer1);
            this._tabIndividual.Controls.Add(this.toolStrip1);
            this._tabIndividual.Controls.Add(this.toolStrip2);
            this._tabIndividual.Location = new System.Drawing.Point(4, 22);
            this._tabIndividual.Name = "_tabIndividual";
            this._tabIndividual.Padding = new System.Windows.Forms.Padding(3);
            this._tabIndividual.Size = new System.Drawing.Size(1062, 610);
            this._tabIndividual.TabIndex = 4;
            this._tabIndividual.Text = "Individual";
            this._tabIndividual.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(96, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this._tvwIndividual);
            this.splitContainer1.Panel1.Controls.Add(this._btnEnableCaching);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this._lstNodeDetails);
            this.splitContainer1.Size = new System.Drawing.Size(963, 579);
            this.splitContainer1.SplitterDistance = 685;
            this.splitContainer1.TabIndex = 4;
            // 
            // _tvwIndividual
            // 
            this._tvwIndividual.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tvwIndividual.ImageIndex = 0;
            this._tvwIndividual.ImageList = this.imageList2;
            this._tvwIndividual.Location = new System.Drawing.Point(0, 0);
            this._tvwIndividual.Name = "_tvwIndividual";
            treeNode1.Name = "Node0";
            treeNode1.Text = "Individuals can be examined when the session is paused";
            this._tvwIndividual.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this._tvwIndividual.SelectedImageIndex = 0;
            this._tvwIndividual.Size = new System.Drawing.Size(685, 548);
            this._tvwIndividual.TabIndex = 1;
            this._tvwIndividual.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView2_AfterSelect);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "DELEGATE");
            this.imageList2.Images.SetKeyName(1, "EVALUATED");
            this.imageList2.Images.SetKeyName(2, "EMPTY");
            this.imageList2.Images.SetKeyName(3, "FLAGGED");
            // 
            // _btnEnableCaching
            // 
            this._btnEnableCaching.AutoSize = true;
            this._btnEnableCaching.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._btnEnableCaching.BackColor = System.Drawing.Color.White;
            this._btnEnableCaching.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._btnEnableCaching.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._btnEnableCaching.ForeColor = System.Drawing.Color.Red;
            this._btnEnableCaching.Image = global::EvoveUi.Properties.Resources.DataGenerator_10021;
            this._btnEnableCaching.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnEnableCaching.Location = new System.Drawing.Point(0, 548);
            this._btnEnableCaching.Name = "_btnEnableCaching";
            this._btnEnableCaching.Padding = new System.Windows.Forms.Padding(4);
            this._btnEnableCaching.Size = new System.Drawing.Size(685, 31);
            this._btnEnableCaching.TabIndex = 6;
            this._btnEnableCaching.Text = "Evaluated values are not visible because evaluation caching is not enabled by the" +
    " parameter set. Click here to enable.\r\n";
            this._btnEnableCaching.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnEnableCaching.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this._btnEnableCaching.UseVisualStyleBackColor = false;
            this._btnEnableCaching.Visible = false;
            this._btnEnableCaching.Click += new System.EventHandler(this._btnEnableCaching_Click_1);
            // 
            // _lstNodeDetails
            // 
            this._lstNodeDetails.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this._lstNodeDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lstNodeDetails.Location = new System.Drawing.Point(0, 0);
            this._lstNodeDetails.Name = "_lstNodeDetails";
            this._lstNodeDetails.Size = new System.Drawing.Size(274, 579);
            this._lstNodeDetails.TabIndex = 2;
            this._lstNodeDetails.UseCompatibleStateImageBehavior = false;
            this._lstNodeDetails.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Value";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator27,
            this.toolStripLabel18,
            this._btnCollapse,
            this._btnExpand,
            this._chkSimpleView,
            this.toolStripSeparator5,
            this.toolStripLabel3,
            this.toolStripComboBox1,
            this._btnDetails,
            this.toolStripSeparator1,
            this.toolStripLabel8,
            this._btnRun,
            this._btnCrossover1,
            this._btnCrossover2,
            this.toolStripButton1,
            this._btnMutate,
            this._btnClearTreeCache,
            this.toolStripSeparator2,
            this.toolStripLabel2,
            this._btnShowSyntax,
            this._btnSwap1,
            this._btnSwap2,
            this._btnInsert,
            this._btnReorder,
            this.toolStripButton2,
            this._btnClearNodeCache,
            this.toolStripSeparator4,
            this.toolStripLabel1,
            this._btnMother,
            this._btnFather,
            this._btnSister,
            this._btnBrother,
            this._btnHistory,
            this.toolStripSeparator3,
            this._btnCancel});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(3, 28);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(93, 579);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator27
            // 
            this.toolStripSeparator27.Name = "toolStripSeparator27";
            this.toolStripSeparator27.Size = new System.Drawing.Size(90, 6);
            // 
            // toolStripLabel18
            // 
            this.toolStripLabel18.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel18.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel18.Name = "toolStripLabel18";
            this.toolStripLabel18.Size = new System.Drawing.Size(90, 15);
            this.toolStripLabel18.Text = "VIEW";
            // 
            // _btnCollapse
            // 
            this._btnCollapse.Image = global::EvoveUi.Properties.Resources.Collapse;
            this._btnCollapse.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnCollapse.Name = "_btnCollapse";
            this._btnCollapse.Size = new System.Drawing.Size(90, 20);
            this._btnCollapse.Text = "Collapse all";
            this._btnCollapse.Click += new System.EventHandler(this._btnCollapse_Click);
            // 
            // _btnExpand
            // 
            this._btnExpand.Image = global::EvoveUi.Properties.Resources.Expand;
            this._btnExpand.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnExpand.Name = "_btnExpand";
            this._btnExpand.Size = new System.Drawing.Size(90, 20);
            this._btnExpand.Text = "Expand all";
            this._btnExpand.Click += new System.EventHandler(this._btnExpand_Click);
            // 
            // _chkSimpleView
            // 
            this._chkSimpleView.Checked = true;
            this._chkSimpleView.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkSimpleView.Image = global::EvoveUi.Properties.Resources.eye_16xLG;
            this._chkSimpleView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkSimpleView.Name = "_chkSimpleView";
            this._chkSimpleView.Size = new System.Drawing.Size(90, 20);
            this._chkSimpleView.Text = "Simple view";
            this._chkSimpleView.Click += new System.EventHandler(this._chkSimpleView_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(90, 6);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(90, 15);
            this.toolStripLabel3.Text = "VIEW CASE";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(74, 23);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            this.toolStripComboBox1.Click += new System.EventHandler(this.toolStripComboBox1_Click);
            // 
            // _btnDetails
            // 
            this._btnDetails.Image = ((System.Drawing.Image)(resources.GetObject("_btnDetails.Image")));
            this._btnDetails.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnDetails.Name = "_btnDetails";
            this._btnDetails.Size = new System.Drawing.Size(90, 20);
            this._btnDetails.Text = "Details";
            this._btnDetails.Click += new System.EventHandler(this._btnDetails_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(90, 6);
            // 
            // toolStripLabel8
            // 
            this.toolStripLabel8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel8.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel8.Name = "toolStripLabel8";
            this.toolStripLabel8.Size = new System.Drawing.Size(90, 15);
            this.toolStripLabel8.Text = "TREE OPS";
            // 
            // _btnRun
            // 
            this._btnRun.Image = global::EvoveUi.Properties.Resources.PlayHS;
            this._btnRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnRun.Name = "_btnRun";
            this._btnRun.Size = new System.Drawing.Size(90, 20);
            this._btnRun.Text = "Evaluate";
            this._btnRun.ToolTipText = "Evaluates the entire tree";
            this._btnRun.Click += new System.EventHandler(this._btnRun_Click);
            // 
            // _btnCrossover1
            // 
            this._btnCrossover1.Image = global::EvoveUi.Properties.Resources.Team_16xLG;
            this._btnCrossover1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnCrossover1.Name = "_btnCrossover1";
            this._btnCrossover1.Size = new System.Drawing.Size(90, 20);
            this._btnCrossover1.Text = "Crossover";
            this._btnCrossover1.ToolTipText = "Performs a crossover operation on the tree (selects first tree)";
            this._btnCrossover1.Click += new System.EventHandler(this._btnCrossover1_Click);
            // 
            // _btnCrossover2
            // 
            this._btnCrossover2.Enabled = false;
            this._btnCrossover2.Image = global::EvoveUi.Properties.Resources.Team_16xLG;
            this._btnCrossover2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnCrossover2.Name = "_btnCrossover2";
            this._btnCrossover2.Size = new System.Drawing.Size(90, 20);
            this._btnCrossover2.Text = "Crossover";
            this._btnCrossover2.ToolTipText = "Performs a crossover operation on the tree (selects second tree and applies)";
            this._btnCrossover2.Click += new System.EventHandler(this._btnRunIndividual_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::EvoveUi.Properties.Resources.user_16xLG;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(90, 20);
            this.toolStripButton1.Text = "FullMutate";
            this.toolStripButton1.ToolTipText = "Performs a mutation operation on the tree";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // _btnMutate
            // 
            this._btnMutate.Image = global::EvoveUi.Properties.Resources.user_16xLG;
            this._btnMutate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnMutate.Name = "_btnMutate";
            this._btnMutate.Size = new System.Drawing.Size(90, 20);
            this._btnMutate.Text = "Alter";
            this._btnMutate.ToolTipText = "Performs a mutation operation on the tree";
            this._btnMutate.Click += new System.EventHandler(this._btnMutate_Click);
            // 
            // _btnClearTreeCache
            // 
            this._btnClearTreeCache.Image = global::EvoveUi.Properties.Resources.CylinderPreview;
            this._btnClearTreeCache.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnClearTreeCache.Name = "_btnClearTreeCache";
            this._btnClearTreeCache.Size = new System.Drawing.Size(90, 20);
            this._btnClearTreeCache.Text = "Clear";
            this._btnClearTreeCache.ToolTipText = "Clears the cached values from the tree, permitting reevaluation";
            this._btnClearTreeCache.Click += new System.EventHandler(this._btnClearTreeCache_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(90, 6);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(90, 15);
            this.toolStripLabel2.Text = "NODE OPS";
            // 
            // _btnShowSyntax
            // 
            this._btnShowSyntax.Image = global::EvoveUi.Properties.Resources.GoToDefinition_5575;
            this._btnShowSyntax.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnShowSyntax.Name = "_btnShowSyntax";
            this._btnShowSyntax.Size = new System.Drawing.Size(90, 20);
            this._btnShowSyntax.Text = "Show syntax";
            this._btnShowSyntax.ToolTipText = "Finds the syntax for the selected node in the syntax browser";
            this._btnShowSyntax.Click += new System.EventHandler(this._btnShowSyntax_Click);
            // 
            // _btnSwap1
            // 
            this._btnSwap1.Image = global::EvoveUi.Properties.Resources.Team_16xLG;
            this._btnSwap1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnSwap1.Name = "_btnSwap1";
            this._btnSwap1.Size = new System.Drawing.Size(90, 20);
            this._btnSwap1.Text = "Swap";
            this._btnSwap1.ToolTipText = "Swaps two node in two different trees (selects the first node)";
            this._btnSwap1.Click += new System.EventHandler(this._btnSwap1_Click);
            // 
            // _btnSwap2
            // 
            this._btnSwap2.Enabled = false;
            this._btnSwap2.Image = global::EvoveUi.Properties.Resources.Team_16xLG;
            this._btnSwap2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnSwap2.Name = "_btnSwap2";
            this._btnSwap2.Size = new System.Drawing.Size(90, 20);
            this._btnSwap2.Text = "Swap";
            this._btnSwap2.ToolTipText = "Swaps two node in two different trees (selects the second node and applies)";
            this._btnSwap2.Click += new System.EventHandler(this._btnSwap2_Click);
            // 
            // _btnInsert
            // 
            this._btnInsert.Image = global::EvoveUi.Properties.Resources.user_16xLG;
            this._btnInsert.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnInsert.Name = "_btnInsert";
            this._btnInsert.Size = new System.Drawing.Size(90, 20);
            this._btnInsert.Text = "Insert";
            this._btnInsert.ToolTipText = "Inserts a new node between the selected node and its parent";
            this._btnInsert.Click += new System.EventHandler(this._btnInsert_Click);
            // 
            // _btnReorder
            // 
            this._btnReorder.Image = global::EvoveUi.Properties.Resources.user_16xLG;
            this._btnReorder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnReorder.Name = "_btnReorder";
            this._btnReorder.Size = new System.Drawing.Size(90, 20);
            this._btnReorder.Text = "Reorder";
            this._btnReorder.ToolTipText = "Reorders this node in its parents parameter set";
            this._btnReorder.Click += new System.EventHandler(this._btnReorder_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::EvoveUi.Properties.Resources.user_16xLG;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(90, 20);
            this.toolStripButton2.Text = "Replace";
            this.toolStripButton2.ToolTipText = "Reorders this node in its parents parameter set";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click_1);
            // 
            // _btnClearNodeCache
            // 
            this._btnClearNodeCache.Image = global::EvoveUi.Properties.Resources.CylinderPreview;
            this._btnClearNodeCache.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnClearNodeCache.Name = "_btnClearNodeCache";
            this._btnClearNodeCache.Size = new System.Drawing.Size(90, 20);
            this._btnClearNodeCache.Text = "Clear";
            this._btnClearNodeCache.ToolTipText = "Clears the cached value from this node, permitting reevaluation";
            this._btnClearNodeCache.Click += new System.EventHandler(this._btnClearNodeCache_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(90, 6);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(90, 15);
            this.toolStripLabel1.Text = "VIEW FAMILY";
            // 
            // _btnMother
            // 
            this._btnMother.Enabled = false;
            this._btnMother.Image = ((System.Drawing.Image)(resources.GetObject("_btnMother.Image")));
            this._btnMother.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnMother.Name = "_btnMother";
            this._btnMother.Size = new System.Drawing.Size(90, 20);
            this._btnMother.Text = "Mother";
            this._btnMother.Click += new System.EventHandler(this._btnMother_Click);
            // 
            // _btnFather
            // 
            this._btnFather.Enabled = false;
            this._btnFather.Image = ((System.Drawing.Image)(resources.GetObject("_btnFather.Image")));
            this._btnFather.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnFather.Name = "_btnFather";
            this._btnFather.Size = new System.Drawing.Size(60, 20);
            this._btnFather.Text = "Father";
            this._btnFather.Click += new System.EventHandler(this._btnFather_Click);
            // 
            // _btnSister
            // 
            this._btnSister.Enabled = false;
            this._btnSister.Image = ((System.Drawing.Image)(resources.GetObject("_btnSister.Image")));
            this._btnSister.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnSister.Name = "_btnSister";
            this._btnSister.Size = new System.Drawing.Size(55, 20);
            this._btnSister.Text = "Sister";
            this._btnSister.Click += new System.EventHandler(this._btnSister_Click);
            // 
            // _btnBrother
            // 
            this._btnBrother.Enabled = false;
            this._btnBrother.Image = ((System.Drawing.Image)(resources.GetObject("_btnBrother.Image")));
            this._btnBrother.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnBrother.Name = "_btnBrother";
            this._btnBrother.Size = new System.Drawing.Size(66, 20);
            this._btnBrother.Text = "Brother";
            this._btnBrother.Click += new System.EventHandler(this._btnBrother_Click);
            // 
            // _btnHistory
            // 
            this._btnHistory.Image = ((System.Drawing.Image)(resources.GetObject("_btnHistory.Image")));
            this._btnHistory.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnHistory.Name = "_btnHistory";
            this._btnHistory.Size = new System.Drawing.Size(73, 20);
            this._btnHistory.Text = "Timeline";
            this._btnHistory.Click += new System.EventHandler(this._btnHistory_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(90, 6);
            // 
            // _btnCancel
            // 
            this._btnCancel.BackColor = System.Drawing.Color.Red;
            this._btnCancel.ForeColor = System.Drawing.Color.White;
            this._btnCancel.Image = global::EvoveUi.Properties.Resources.delete_12x12;
            this._btnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(63, 20);
            this._btnCancel.Text = "Cancel";
            this._btnCancel.ToolTipText = "Cancels the selected operation";
            this._btnCancel.Visible = false;
            this._btnCancel.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel13,
            this.toolStripSeparator8,
            this._lblIndividual});
            this.toolStrip2.Location = new System.Drawing.Point(3, 3);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(1056, 25);
            this.toolStrip2.TabIndex = 3;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripLabel13
            // 
            this.toolStripLabel13.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel13.Name = "toolStripLabel13";
            this.toolStripLabel13.Size = new System.Drawing.Size(77, 22);
            this.toolStripLabel13.Text = "INDIVIDUAL";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // _lblIndividual
            // 
            this._lblIndividual.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblIndividual.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._lblIndividual.Name = "_lblIndividual";
            this._lblIndividual.Size = new System.Drawing.Size(16, 22);
            this._lblIndividual.Text = "...";
            // 
            // tabPage7
            // 
            this.tabPage7.AutoScroll = true;
            this.tabPage7.Controls.Add(this.flowLayoutPanel2);
            this.tabPage7.Controls.Add(this.toolStrip16);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(1062, 610);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Help";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel2.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel2.Controls.Add(this.label19);
            this.flowLayoutPanel2.Controls.Add(this.label20);
            this.flowLayoutPanel2.Controls.Add(this.label21);
            this.flowLayoutPanel2.Controls.Add(this.label23);
            this.flowLayoutPanel2.Controls.Add(this.label22);
            this.flowLayoutPanel2.Controls.Add(this.label24);
            this.flowLayoutPanel2.Controls.Add(this.label25);
            this.flowLayoutPanel2.Controls.Add(this.label7);
            this.flowLayoutPanel2.Controls.Add(this._lblNotEvaluated);
            this.flowLayoutPanel2.Controls.Add(this._lblEvaluated);
            this.flowLayoutPanel2.Controls.Add(this._lblEvaluated2);
            this.flowLayoutPanel2.Controls.Add(this._lblDeferred);
            this.flowLayoutPanel2.Controls.Add(this._lblFlagged);
            this.flowLayoutPanel2.Controls.Add(this.label8);
            this.flowLayoutPanel2.Controls.Add(this._lblDataType);
            this.flowLayoutPanel2.Controls.Add(this._lblProvider);
            this.flowLayoutPanel2.Controls.Add(this._lblReceiver);
            this.flowLayoutPanel2.Controls.Add(this.tableLayoutPanel1);
            this.flowLayoutPanel2.Controls.Add(this.label2);
            this.flowLayoutPanel2.Controls.Add(this.label3);
            this.flowLayoutPanel2.Controls.Add(this.label4);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 32);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Padding = new System.Windows.Forms.Padding(8);
            this.flowLayoutPanel2.Size = new System.Drawing.Size(739, 825);
            this.flowLayoutPanel2.TabIndex = 0;
            this.flowLayoutPanel2.WrapContents = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Gray;
            this.label19.Location = new System.Drawing.Point(12, 12);
            this.label19.Margin = new System.Windows.Forms.Padding(4);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(94, 21);
            this.label19.TabIndex = 17;
            this.label19.Text = "NOTATION";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Image = global::EvoveUi.Properties.Resources.method_16xLG;
            this.label20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label20.Location = new System.Drawing.Point(32, 41);
            this.label20.Margin = new System.Windows.Forms.Padding(24, 4, 4, 4);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(105, 21);
            this.label20.TabIndex = 3;
            this.label20.Text = "     Population";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Image = global::EvoveUi.Properties.Resources.OverloadBehavior_10445;
            this.label21.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label21.Location = new System.Drawing.Point(56, 70);
            this.label21.Margin = new System.Windows.Forms.Padding(48, 4, 4, 4);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(88, 21);
            this.label21.TabIndex = 3;
            this.label21.Text = "     Deme(s)";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Image = ((System.Drawing.Image)(resources.GetObject("label23.Image")));
            this.label23.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label23.Location = new System.Drawing.Point(80, 99);
            this.label23.Margin = new System.Windows.Forms.Padding(72, 4, 4, 4);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(115, 21);
            this.label23.TabIndex = 3;
            this.label23.Text = "     Individual(s)";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Image = global::EvoveUi.Properties.Resources.method_16xLG;
            this.label22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label22.Location = new System.Drawing.Point(104, 128);
            this.label22.Margin = new System.Windows.Forms.Padding(96, 4, 4, 4);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 21);
            this.label22.TabIndex = 18;
            this.label22.Text = "     Tree";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Image = ((System.Drawing.Image)(resources.GetObject("label24.Image")));
            this.label24.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label24.Location = new System.Drawing.Point(138, 157);
            this.label24.Margin = new System.Windows.Forms.Padding(130, 4, 4, 4);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(92, 21);
            this.label24.TabIndex = 18;
            this.label24.Text = "     Nodes(s)";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Image = global::EvoveUi.Properties.Resources.method_16xLG;
            this.label25.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label25.Location = new System.Drawing.Point(162, 186);
            this.label25.Margin = new System.Windows.Forms.Padding(154, 4, 4, 4);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(90, 21);
            this.label25.TabIndex = 18;
            this.label25.Text = "     Function";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gray;
            this.label7.Location = new System.Drawing.Point(12, 235);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 24, 4, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(169, 21);
            this.label7.TabIndex = 5;
            this.label7.Text = "LEGEND FOR NODES:";
            // 
            // _lblNotEvaluated
            // 
            this._lblNotEvaluated.AutoSize = true;
            this._lblNotEvaluated.ForeColor = System.Drawing.Color.Gray;
            this._lblNotEvaluated.Location = new System.Drawing.Point(32, 264);
            this._lblNotEvaluated.Margin = new System.Windows.Forms.Padding(24, 4, 4, 4);
            this._lblNotEvaluated.Name = "_lblNotEvaluated";
            this._lblNotEvaluated.Size = new System.Drawing.Size(334, 21);
            this._lblNotEvaluated.TabIndex = 0;
            this._lblNotEvaluated.Text = "Not evaluated: This node has not been touched";
            // 
            // _lblEvaluated
            // 
            this._lblEvaluated.AutoSize = true;
            this._lblEvaluated.ForeColor = System.Drawing.Color.Green;
            this._lblEvaluated.Location = new System.Drawing.Point(32, 293);
            this._lblEvaluated.Margin = new System.Windows.Forms.Padding(24, 4, 4, 4);
            this._lblEvaluated.Name = "_lblEvaluated";
            this._lblEvaluated.Size = new System.Drawing.Size(339, 21);
            this._lblEvaluated.TabIndex = 1;
            this._lblEvaluated.Text = "Evaluated: Outcome assumed to be unchanging";
            // 
            // _lblEvaluated2
            // 
            this._lblEvaluated2.AutoSize = true;
            this._lblEvaluated2.ForeColor = System.Drawing.Color.Teal;
            this._lblEvaluated2.Location = new System.Drawing.Point(32, 322);
            this._lblEvaluated2.Margin = new System.Windows.Forms.Padding(24, 4, 4, 4);
            this._lblEvaluated2.Name = "_lblEvaluated2";
            this._lblEvaluated2.Size = new System.Drawing.Size(416, 21);
            this._lblEvaluated2.TabIndex = 2;
            this._lblEvaluated2.Text = "Evaluated: Outcome assumed to be dependent on test case";
            // 
            // _lblDeferred
            // 
            this._lblDeferred.AutoSize = true;
            this._lblDeferred.ForeColor = System.Drawing.Color.Black;
            this._lblDeferred.Location = new System.Drawing.Point(32, 351);
            this._lblDeferred.Margin = new System.Windows.Forms.Padding(24, 4, 4, 4);
            this._lblDeferred.Name = "_lblDeferred";
            this._lblDeferred.Size = new System.Drawing.Size(451, 21);
            this._lblDeferred.TabIndex = 3;
            this._lblDeferred.Text = "Delegated: Function pointer provided but value never requested";
            // 
            // _lblFlagged
            // 
            this._lblFlagged.AutoSize = true;
            this._lblFlagged.ForeColor = System.Drawing.Color.Red;
            this._lblFlagged.Location = new System.Drawing.Point(32, 380);
            this._lblFlagged.Margin = new System.Windows.Forms.Padding(24, 4, 4, 4);
            this._lblFlagged.Name = "_lblFlagged";
            this._lblFlagged.Size = new System.Drawing.Size(453, 21);
            this._lblFlagged.TabIndex = 4;
            this._lblFlagged.Text = "Flagged: Changes to the tree mean this node needs reevaluating";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Gray;
            this.label8.Location = new System.Drawing.Point(12, 429);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 24, 4, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(205, 21);
            this.label8.TabIndex = 6;
            this.label8.Text = "LEGEND FOR FUNCTIONS:";
            // 
            // _lblDataType
            // 
            this._lblDataType.AutoSize = true;
            this._lblDataType.ForeColor = System.Drawing.Color.Blue;
            this._lblDataType.Location = new System.Drawing.Point(32, 458);
            this._lblDataType.Margin = new System.Windows.Forms.Padding(24, 4, 4, 4);
            this._lblDataType.Name = "_lblDataType";
            this._lblDataType.Size = new System.Drawing.Size(173, 21);
            this._lblDataType.TabIndex = 7;
            this._lblDataType.Text = "Type: This is a data type";
            // 
            // _lblProvider
            // 
            this._lblProvider.AutoSize = true;
            this._lblProvider.ForeColor = System.Drawing.Color.Black;
            this._lblProvider.Location = new System.Drawing.Point(32, 487);
            this._lblProvider.Margin = new System.Windows.Forms.Padding(24, 4, 4, 4);
            this._lblProvider.Name = "_lblProvider";
            this._lblProvider.Size = new System.Drawing.Size(402, 21);
            this._lblProvider.TabIndex = 8;
            this._lblProvider.Text = "Provider: This is a function that returns the specified type";
            // 
            // _lblReceiver
            // 
            this._lblReceiver.AutoSize = true;
            this._lblReceiver.ForeColor = System.Drawing.Color.Gray;
            this._lblReceiver.Location = new System.Drawing.Point(32, 516);
            this._lblReceiver.Margin = new System.Windows.Forms.Padding(24, 4, 4, 4);
            this._lblReceiver.Name = "_lblReceiver";
            this._lblReceiver.Size = new System.Drawing.Size(573, 21);
            this._lblReceiver.TabIndex = 9;
            this._lblReceiver.Text = "Receiver: This is a function that accepts the specified type as one of its parame" +
    "ters";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label11, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label14, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label15, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label16, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label17, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label18, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label26, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label27, 2, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 561);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0, 20, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(719, 145);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(20, 91);
            this.label10.Margin = new System.Windows.Forms.Padding(20, 4, 4, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(167, 21);
            this.label10.TabIndex = 15;
            this.label10.Text = "When population ends";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(20, 62);
            this.label9.Margin = new System.Windows.Forms.Padding(20, 4, 4, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 21);
            this.label9.TabIndex = 14;
            this.label9.Text = "Population starts in";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(195, 33);
            this.label6.Margin = new System.Windows.Forms.Padding(4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(164, 21);
            this.label6.TabIndex = 13;
            this.label6.Text = "When \"start\" is clicked";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(395, 4);
            this.label11.Margin = new System.Windows.Forms.Padding(4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 21);
            this.label11.TabIndex = 16;
            this.label11.Text = "Unattended";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(195, 4);
            this.label5.Margin = new System.Windows.Forms.Padding(4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 21);
            this.label5.TabIndex = 12;
            this.label5.Text = "Attended";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(20, 33);
            this.label12.Margin = new System.Windows.Forms.Padding(20, 4, 4, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(142, 21);
            this.label12.TabIndex = 14;
            this.label12.Text = "Population created";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Gray;
            this.label13.Location = new System.Drawing.Point(4, 4);
            this.label13.Margin = new System.Windows.Forms.Padding(4);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 21);
            this.label13.TabIndex = 12;
            this.label13.Text = "MODES";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(395, 33);
            this.label14.Margin = new System.Windows.Forms.Padding(4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(175, 21);
            this.label14.TabIndex = 13;
            this.label14.Text = "When window is shown";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(195, 62);
            this.label15.Margin = new System.Windows.Forms.Padding(4);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(95, 21);
            this.label15.TabIndex = 13;
            this.label15.Text = "Pause mode";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(395, 62);
            this.label16.Margin = new System.Windows.Forms.Padding(4);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 21);
            this.label16.TabIndex = 13;
            this.label16.Text = "Play mode";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(195, 91);
            this.label17.Margin = new System.Windows.Forms.Padding(4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 21);
            this.label17.TabIndex = 13;
            this.label17.Text = "No action";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(395, 91);
            this.label18.Margin = new System.Windows.Forms.Padding(4);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(320, 21);
            this.label18.TabIndex = 13;
            this.label18.Text = "Window closes (i.e. next test or program exit)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(20, 120);
            this.label1.Margin = new System.Windows.Forms.Padding(20, 4, 4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 21);
            this.label1.TabIndex = 15;
            this.label1.Text = "When error occurs";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(195, 120);
            this.label26.Margin = new System.Windows.Forms.Padding(4);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(192, 21);
            this.label26.TabIndex = 13;
            this.label26.Text = "Enters step-through mode";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(395, 120);
            this.label27.Margin = new System.Windows.Forms.Padding(4);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(77, 21);
            this.label27.TabIndex = 13;
            this.label27.Text = "No action";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(12, 734);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 24, 4, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 21);
            this.label2.TabIndex = 10;
            this.label2.Text = "COPYRIGHT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(32, 763);
            this.label3.Margin = new System.Windows.Forms.Padding(24, 4, 4, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(233, 21);
            this.label3.TabIndex = 9;
            this.label3.Text = "Text to be completed at runtime.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(32, 792);
            this.label4.Margin = new System.Windows.Forms.Padding(24, 4, 4, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(182, 21);
            this.label4.TabIndex = 11;
            this.label4.Text = "Icon designed by Freepik";
            // 
            // toolStrip16
            // 
            this.toolStrip16.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel27,
            this.toolStripSeparator28,
            this.toolStripLabel28});
            this.toolStrip16.Location = new System.Drawing.Point(3, 3);
            this.toolStrip16.Name = "toolStrip16";
            this.toolStrip16.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip16.Size = new System.Drawing.Size(1039, 25);
            this.toolStrip16.TabIndex = 3;
            this.toolStrip16.Text = "toolStrip16";
            // 
            // toolStripLabel27
            // 
            this.toolStripLabel27.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel27.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel27.Name = "toolStripLabel27";
            this.toolStripLabel27.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel27.Text = "HELP";
            // 
            // toolStripSeparator28
            // 
            this.toolStripSeparator28.Name = "toolStripSeparator28";
            this.toolStripSeparator28.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel28
            // 
            this.toolStripLabel28.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel28.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel28.Name = "toolStripLabel28";
            this.toolStripLabel28.Size = new System.Drawing.Size(154, 22);
            this.toolStripLabel28.Text = "View legends and notation";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.toolStrip13);
            this.tabPage4.Controls.Add(this.textBox1);
            this.tabPage4.Controls.Add(this.toolStrip10);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1062, 610);
            this.tabPage4.TabIndex = 8;
            this.tabPage4.Text = "Debug";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // toolStrip13
            // 
            this.toolStrip13.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip13.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip13.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator25,
            this.toolStripLabel22,
            this._btnRefresh,
            this._btnClearLog,
            this.toolStripSeparator22,
            this.toolStripLabel23,
            this._btnDebug,
            this.toolStripSeparator26});
            this.toolStrip13.Location = new System.Drawing.Point(3, 28);
            this.toolStrip13.Name = "toolStrip13";
            this.toolStrip13.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip13.Size = new System.Drawing.Size(87, 579);
            this.toolStrip13.TabIndex = 5;
            this.toolStrip13.Text = "toolStrip13";
            // 
            // toolStripSeparator25
            // 
            this.toolStripSeparator25.Name = "toolStripSeparator25";
            this.toolStripSeparator25.Size = new System.Drawing.Size(84, 6);
            // 
            // toolStripLabel22
            // 
            this.toolStripLabel22.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel22.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel22.Name = "toolStripLabel22";
            this.toolStripLabel22.Size = new System.Drawing.Size(84, 15);
            this.toolStripLabel22.Text = "LIST";
            // 
            // _btnRefresh
            // 
            this._btnRefresh.Image = global::EvoveUi.Properties.Resources._112_RefreshArrow_Blue_16x16_72;
            this._btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnRefresh.Name = "_btnRefresh";
            this._btnRefresh.Size = new System.Drawing.Size(84, 20);
            this._btnRefresh.Text = "Refresh";
            this._btnRefresh.Click += new System.EventHandler(this._btnRefresh_Click);
            // 
            // _btnClearLog
            // 
            this._btnClearLog.Image = global::EvoveUi.Properties.Resources.delete_12x12;
            this._btnClearLog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnClearLog.Name = "_btnClearLog";
            this._btnClearLog.Size = new System.Drawing.Size(84, 20);
            this._btnClearLog.Text = "Clear log";
            this._btnClearLog.Click += new System.EventHandler(this._btnClearLog_Click);
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(84, 6);
            // 
            // toolStripLabel23
            // 
            this.toolStripLabel23.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel23.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel23.Name = "toolStripLabel23";
            this.toolStripLabel23.Size = new System.Drawing.Size(84, 15);
            this.toolStripLabel23.Text = "VIEW";
            // 
            // _btnDebug
            // 
            this._btnDebug.CheckOnClick = true;
            this._btnDebug.Image = global::EvoveUi.Properties.Resources.eye_16xLG;
            this._btnDebug.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnDebug.Name = "_btnDebug";
            this._btnDebug.Size = new System.Drawing.Size(84, 20);
            this._btnDebug.Text = "Debugging";
            this._btnDebug.CheckedChanged += new System.EventHandler(this.toolStripButton2_CheckedChanged);
            // 
            // toolStripSeparator26
            // 
            this.toolStripSeparator26.Name = "toolStripSeparator26";
            this.toolStripSeparator26.Size = new System.Drawing.Size(84, 6);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(3, 28);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(1056, 581);
            this.textBox1.TabIndex = 4;
            // 
            // toolStrip10
            // 
            this.toolStrip10.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel14,
            this.toolStripSeparator17,
            this.toolStripLabel15});
            this.toolStrip10.Location = new System.Drawing.Point(3, 3);
            this.toolStrip10.Name = "toolStrip10";
            this.toolStrip10.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip10.Size = new System.Drawing.Size(1056, 25);
            this.toolStrip10.TabIndex = 3;
            this.toolStrip10.Text = "toolStrip10";
            // 
            // toolStripLabel14
            // 
            this.toolStripLabel14.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel14.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel14.Name = "toolStripLabel14";
            this.toolStripLabel14.Size = new System.Drawing.Size(75, 22);
            this.toolStripLabel14.Text = "DEBUG LOG";
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel15
            // 
            this.toolStripLabel15.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel15.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel15.Name = "toolStripLabel15";
            this.toolStripLabel15.Size = new System.Drawing.Size(165, 22);
            this.toolStripLabel15.Text = "The debug log is shown here";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this._plot);
            this.tabPage5.Controls.Add(this.toolStrip12);
            this.tabPage5.Controls.Add(this.toolStrip11);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1062, 610);
            this.tabPage5.TabIndex = 9;
            this.tabPage5.Text = "Plot";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // _plot
            // 
            this._plot.Dock = System.Windows.Forms.DockStyle.Fill;
            this._plot.Location = new System.Drawing.Point(96, 28);
            this._plot.Name = "_plot";
            this._plot.SelectedItem = selection1;
            this._plot.Size = new System.Drawing.Size(963, 579);
            this._plot.TabIndex = 0;
            // 
            // toolStrip12
            // 
            this.toolStrip12.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip12.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip12.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator19,
            this.toolStripLabel19,
            this._btnRefreshPlot,
            this._chkAutoPlot,
            this.toolStripSeparator21,
            this.toolStripLabel20,
            this._chkFitness,
            this._chkTree,
            this._chkExecution,
            this._chkRound,
            this.toolStripSeparator30,
            this.toolStripLabel5,
            this._chkYFitness,
            this._chkYSize,
            this._chkYTime,
            this._chkYRound,
            this.toolStripSeparator32,
            this.toolStripSeparator24,
            this.toolStripLabel21,
            this._chkElite,
            this._chkHighest,
            this._chkLowest,
            this._chkMean,
            this._chkAll,
            this._chkTruncate});
            this.toolStrip12.Location = new System.Drawing.Point(3, 28);
            this.toolStrip12.Name = "toolStrip12";
            this.toolStrip12.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip12.Size = new System.Drawing.Size(93, 579);
            this.toolStrip12.TabIndex = 5;
            this.toolStrip12.Text = "toolStrip12";
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(90, 6);
            // 
            // toolStripLabel19
            // 
            this.toolStripLabel19.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel19.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel19.Name = "toolStripLabel19";
            this.toolStripLabel19.Size = new System.Drawing.Size(90, 15);
            this.toolStripLabel19.Text = "PLOT";
            // 
            // _btnRefreshPlot
            // 
            this._btnRefreshPlot.Image = global::EvoveUi.Properties.Resources._112_RefreshArrow_Blue_16x16_72;
            this._btnRefreshPlot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnRefreshPlot.Name = "_btnRefreshPlot";
            this._btnRefreshPlot.Size = new System.Drawing.Size(90, 20);
            this._btnRefreshPlot.Text = "Refresh";
            this._btnRefreshPlot.Click += new System.EventHandler(this._btnRefreshPlot_Click);
            // 
            // _chkAutoPlot
            // 
            this._chkAutoPlot.Image = ((System.Drawing.Image)(resources.GetObject("_chkAutoPlot.Image")));
            this._chkAutoPlot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkAutoPlot.Name = "_chkAutoPlot";
            this._chkAutoPlot.Size = new System.Drawing.Size(90, 20);
            this._chkAutoPlot.Text = "Auto refresh";
            this._chkAutoPlot.Click += new System.EventHandler(this._chkAutoPlot_Click);
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(90, 6);
            // 
            // toolStripLabel20
            // 
            this.toolStripLabel20.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel20.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel20.Name = "toolStripLabel20";
            this.toolStripLabel20.Size = new System.Drawing.Size(90, 15);
            this.toolStripLabel20.Text = "COLLECT";
            // 
            // _chkFitness
            // 
            this._chkFitness.Checked = true;
            this._chkFitness.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkFitness.Image = ((System.Drawing.Image)(resources.GetObject("_chkFitness.Image")));
            this._chkFitness.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkFitness.Name = "_chkFitness";
            this._chkFitness.Size = new System.Drawing.Size(90, 20);
            this._chkFitness.Text = "Fitness";
            this._chkFitness.Click += new System.EventHandler(this._chkFitness_Click);
            // 
            // _chkTree
            // 
            this._chkTree.Image = ((System.Drawing.Image)(resources.GetObject("_chkTree.Image")));
            this._chkTree.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkTree.Name = "_chkTree";
            this._chkTree.Size = new System.Drawing.Size(90, 20);
            this._chkTree.Text = "Size";
            this._chkTree.Click += new System.EventHandler(this._chkFitness_Click);
            // 
            // _chkExecution
            // 
            this._chkExecution.Image = ((System.Drawing.Image)(resources.GetObject("_chkExecution.Image")));
            this._chkExecution.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkExecution.Name = "_chkExecution";
            this._chkExecution.Size = new System.Drawing.Size(90, 20);
            this._chkExecution.Text = "Time";
            this._chkExecution.Click += new System.EventHandler(this._chkFitness_Click);
            // 
            // _chkRound
            // 
            this._chkRound.Image = ((System.Drawing.Image)(resources.GetObject("_chkRound.Image")));
            this._chkRound.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkRound.Name = "_chkRound";
            this._chkRound.Size = new System.Drawing.Size(90, 20);
            this._chkRound.Text = "Round";
            this._chkRound.Click += new System.EventHandler(this._chkFitness_Click);
            // 
            // toolStripSeparator30
            // 
            this.toolStripSeparator30.Name = "toolStripSeparator30";
            this.toolStripSeparator30.Size = new System.Drawing.Size(90, 6);
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(90, 15);
            this.toolStripLabel5.Text = "COLLECT";
            // 
            // _chkYFitness
            // 
            this._chkYFitness.Image = ((System.Drawing.Image)(resources.GetObject("_chkYFitness.Image")));
            this._chkYFitness.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkYFitness.Name = "_chkYFitness";
            this._chkYFitness.Size = new System.Drawing.Size(90, 20);
            this._chkYFitness.Text = "Fitness";
            this._chkYFitness.Click += new System.EventHandler(this._chkYFitness_Click);
            // 
            // _chkYSize
            // 
            this._chkYSize.Image = ((System.Drawing.Image)(resources.GetObject("_chkYSize.Image")));
            this._chkYSize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkYSize.Name = "_chkYSize";
            this._chkYSize.Size = new System.Drawing.Size(90, 20);
            this._chkYSize.Text = "Size";
            this._chkYSize.Click += new System.EventHandler(this._chkYFitness_Click);
            // 
            // _chkYTime
            // 
            this._chkYTime.Image = ((System.Drawing.Image)(resources.GetObject("_chkYTime.Image")));
            this._chkYTime.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkYTime.Name = "_chkYTime";
            this._chkYTime.Size = new System.Drawing.Size(90, 20);
            this._chkYTime.Text = "Time";
            this._chkYTime.Click += new System.EventHandler(this._chkYFitness_Click);
            // 
            // _chkYRound
            // 
            this._chkYRound.Checked = true;
            this._chkYRound.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkYRound.Image = ((System.Drawing.Image)(resources.GetObject("_chkYRound.Image")));
            this._chkYRound.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkYRound.Name = "_chkYRound";
            this._chkYRound.Size = new System.Drawing.Size(90, 20);
            this._chkYRound.Text = "Round";
            this._chkYRound.Click += new System.EventHandler(this._chkYFitness_Click);
            // 
            // toolStripSeparator32
            // 
            this.toolStripSeparator32.Name = "toolStripSeparator32";
            this.toolStripSeparator32.Size = new System.Drawing.Size(90, 6);
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(90, 6);
            // 
            // toolStripLabel21
            // 
            this.toolStripLabel21.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel21.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel21.Name = "toolStripLabel21";
            this.toolStripLabel21.Size = new System.Drawing.Size(90, 15);
            this.toolStripLabel21.Text = "VIEW";
            // 
            // _chkElite
            // 
            this._chkElite.Image = ((System.Drawing.Image)(resources.GetObject("_chkElite.Image")));
            this._chkElite.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkElite.Name = "_chkElite";
            this._chkElite.Size = new System.Drawing.Size(90, 20);
            this._chkElite.Text = "Elite";
            this._chkElite.Click += new System.EventHandler(this._chkElite_Click);
            // 
            // _chkHighest
            // 
            this._chkHighest.Checked = true;
            this._chkHighest.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkHighest.Image = ((System.Drawing.Image)(resources.GetObject("_chkHighest.Image")));
            this._chkHighest.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkHighest.Name = "_chkHighest";
            this._chkHighest.Size = new System.Drawing.Size(90, 20);
            this._chkHighest.Text = "Highest";
            this._chkHighest.Click += new System.EventHandler(this._chkElite_Click);
            // 
            // _chkLowest
            // 
            this._chkLowest.Checked = true;
            this._chkLowest.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkLowest.Image = ((System.Drawing.Image)(resources.GetObject("_chkLowest.Image")));
            this._chkLowest.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkLowest.Name = "_chkLowest";
            this._chkLowest.Size = new System.Drawing.Size(90, 20);
            this._chkLowest.Text = "Lowest";
            this._chkLowest.Click += new System.EventHandler(this._chkElite_Click);
            // 
            // _chkMean
            // 
            this._chkMean.Image = ((System.Drawing.Image)(resources.GetObject("_chkMean.Image")));
            this._chkMean.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkMean.Name = "_chkMean";
            this._chkMean.Size = new System.Drawing.Size(90, 20);
            this._chkMean.Text = "Mean";
            this._chkMean.Click += new System.EventHandler(this._chkElite_Click);
            // 
            // _chkAll
            // 
            this._chkAll.Checked = true;
            this._chkAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this._chkAll.Image = ((System.Drawing.Image)(resources.GetObject("_chkAll.Image")));
            this._chkAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkAll.Name = "_chkAll";
            this._chkAll.Size = new System.Drawing.Size(90, 20);
            this._chkAll.Text = "All";
            this._chkAll.Click += new System.EventHandler(this._chkElite_Click);
            // 
            // _chkTruncate
            // 
            this._chkTruncate.Image = global::EvoveUi.Properties.Resources.eye_16xLG;
            this._chkTruncate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._chkTruncate.Name = "_chkTruncate";
            this._chkTruncate.Size = new System.Drawing.Size(90, 20);
            this._chkTruncate.Text = "Truncate";
            this._chkTruncate.Click += new System.EventHandler(this._chkTruncate_Click);
            // 
            // toolStrip11
            // 
            this.toolStrip11.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel16,
            this.toolStripSeparator16,
            this.toolStripLabel17});
            this.toolStrip11.Location = new System.Drawing.Point(3, 3);
            this.toolStrip11.Name = "toolStrip11";
            this.toolStrip11.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip11.Size = new System.Drawing.Size(1056, 25);
            this.toolStrip11.TabIndex = 4;
            this.toolStrip11.Text = "toolStrip11";
            // 
            // toolStripLabel16
            // 
            this.toolStripLabel16.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel16.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel16.Name = "toolStripLabel16";
            this.toolStripLabel16.Size = new System.Drawing.Size(36, 22);
            this.toolStripLabel16.Text = "PLOT";
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel17
            // 
            this.toolStripLabel17.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel17.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolStripLabel17.Name = "toolStripLabel17";
            this.toolStripLabel17.Size = new System.Drawing.Size(213, 22);
            this.toolStripLabel17.Text = "Values over time can be viewed here";
            // 
            // _pgLock
            // 
            this._pgLock.Controls.Add(this.tableLayoutPanel2);
            this._pgLock.Location = new System.Drawing.Point(4, 22);
            this._pgLock.Name = "_pgLock";
            this._pgLock.Padding = new System.Windows.Forms.Padding(3);
            this._pgLock.Size = new System.Drawing.Size(1062, 610);
            this._pgLock.TabIndex = 10;
            this._pgLock.Text = "Safety catch";
            this._pgLock.UseVisualStyleBackColor = true;
            // 
            // toolStrip3
            // 
            this.toolStrip3.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._btnEditParameters2,
            this._btnStart2,
            this._btnStep2,
            this._btnPlay2,
            this._btnPause2,
            this._btnStop2,
            this._btnBreakpoint2});
            this.toolStrip3.Location = new System.Drawing.Point(0, 24);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip3.Size = new System.Drawing.Size(1070, 25);
            this.toolStrip3.TabIndex = 7;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // _btnEditParameters2
            // 
            this._btnEditParameters2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._btnEditParameters2.Image = global::EvoveUi.Properties.Resources.EditorZone_6025;
            this._btnEditParameters2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnEditParameters2.Name = "_btnEditParameters2";
            this._btnEditParameters2.Size = new System.Drawing.Size(23, 22);
            this._btnEditParameters2.Text = "Edit parameters";
            this._btnEditParameters2.Click += new System.EventHandler(this._btnEditParameters_Click);
            // 
            // _btnStart2
            // 
            this._btnStart2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._btnStart2.Image = global::EvoveUi.Properties.Resources.NewBuildDefinition_8952;
            this._btnStart2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnStart2.Name = "_btnStart2";
            this._btnStart2.Size = new System.Drawing.Size(23, 22);
            this._btnStart2.Text = "Start";
            this._btnStart2.Click += new System.EventHandler(this._btnStart_Click);
            // 
            // _btnStep2
            // 
            this._btnStep2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._btnStep2.Enabled = false;
            this._btnStep2.Image = global::EvoveUi.Properties.Resources.Step_16xLG;
            this._btnStep2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnStep2.Name = "_btnStep2";
            this._btnStep2.Size = new System.Drawing.Size(23, 22);
            this._btnStep2.Text = "Step";
            this._btnStep2.Click += new System.EventHandler(this._btnStep_Click);
            // 
            // _btnPlay2
            // 
            this._btnPlay2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._btnPlay2.Enabled = false;
            this._btnPlay2.Image = global::EvoveUi.Properties.Resources.PlayHS;
            this._btnPlay2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnPlay2.Name = "_btnPlay2";
            this._btnPlay2.Size = new System.Drawing.Size(23, 22);
            this._btnPlay2.Text = "Run";
            this._btnPlay2.Click += new System.EventHandler(this._btnPlay_Click);
            // 
            // _btnPause2
            // 
            this._btnPause2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._btnPause2.Enabled = false;
            this._btnPause2.Image = global::EvoveUi.Properties.Resources.PauseHS;
            this._btnPause2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnPause2.Name = "_btnPause2";
            this._btnPause2.Size = new System.Drawing.Size(23, 22);
            this._btnPause2.Text = "Pause";
            this._btnPause2.Click += new System.EventHandler(this._btnPause_Click);
            // 
            // _btnStop2
            // 
            this._btnStop2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._btnStop2.Enabled = false;
            this._btnStop2.Image = global::EvoveUi.Properties.Resources.StopHS;
            this._btnStop2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnStop2.Name = "_btnStop2";
            this._btnStop2.Size = new System.Drawing.Size(23, 22);
            this._btnStop2.Text = "Stop";
            this._btnStop2.Click += new System.EventHandler(this._btnStop_Click);
            // 
            // _btnBreakpoint2
            // 
            this._btnBreakpoint2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._btnBreakpoint2.Image = global::EvoveUi.Properties.Resources.BreakpointEnabled_6584_16x;
            this._btnBreakpoint2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._btnBreakpoint2.Name = "_btnBreakpoint2";
            this._btnBreakpoint2.Size = new System.Drawing.Size(23, 22);
            this._btnBreakpoint2.Text = "Breakpoints";
            this._btnBreakpoint2.Click += new System.EventHandler(this._btnBreakpoint_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._lblIDebugging,
            this._lblLastLog,
            this._lblIAutomatic,
            this._lblIError,
            this._lblIStopping,
            this._lblIPaused,
            this._lblIBusy,
            this._lblIStepThrough,
            this._lblIStopped,
            this._lblINoSession});
            this.statusStrip1.Location = new System.Drawing.Point(0, 685);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1070, 24);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _lblIDebugging
            // 
            this._lblIDebugging.BackColor = System.Drawing.Color.White;
            this._lblIDebugging.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this._lblIDebugging.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this._lblIDebugging.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblIDebugging.ForeColor = System.Drawing.Color.Red;
            this._lblIDebugging.Name = "_lblIDebugging";
            this._lblIDebugging.Size = new System.Drawing.Size(91, 19);
            this._lblIDebugging.Text = "DEBUG BUILD";
            // 
            // _lblLastLog
            // 
            this._lblLastLog.Name = "_lblLastLog";
            this._lblLastLog.Size = new System.Drawing.Size(230, 19);
            this._lblLastLog.Spring = true;
            this._lblLastLog.Text = "Idle";
            this._lblLastLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _lblIAutomatic
            // 
            this._lblIAutomatic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this._lblIAutomatic.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this._lblIAutomatic.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this._lblIAutomatic.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblIAutomatic.ForeColor = System.Drawing.Color.White;
            this._lblIAutomatic.Name = "_lblIAutomatic";
            this._lblIAutomatic.Size = new System.Drawing.Size(90, 19);
            this._lblIAutomatic.Text = "UNATTENDED";
            // 
            // _lblIError
            // 
            this._lblIError.BackColor = System.Drawing.Color.Red;
            this._lblIError.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this._lblIError.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this._lblIError.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblIError.ForeColor = System.Drawing.Color.White;
            this._lblIError.Name = "_lblIError";
            this._lblIError.Size = new System.Drawing.Size(50, 19);
            this._lblIError.Text = "ERROR";
            // 
            // _lblIStopping
            // 
            this._lblIStopping.BackColor = System.Drawing.Color.Red;
            this._lblIStopping.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this._lblIStopping.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this._lblIStopping.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblIStopping.ForeColor = System.Drawing.Color.White;
            this._lblIStopping.Name = "_lblIStopping";
            this._lblIStopping.Size = new System.Drawing.Size(70, 19);
            this._lblIStopping.Text = "STOPPING";
            // 
            // _lblIPaused
            // 
            this._lblIPaused.BackColor = System.Drawing.Color.CornflowerBlue;
            this._lblIPaused.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this._lblIPaused.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this._lblIPaused.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblIPaused.ForeColor = System.Drawing.Color.White;
            this._lblIPaused.Name = "_lblIPaused";
            this._lblIPaused.Size = new System.Drawing.Size(57, 19);
            this._lblIPaused.Text = "PAUSED";
            // 
            // _lblIBusy
            // 
            this._lblIBusy.BackColor = System.Drawing.Color.Blue;
            this._lblIBusy.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this._lblIBusy.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this._lblIBusy.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblIBusy.ForeColor = System.Drawing.Color.White;
            this._lblIBusy.Name = "_lblIBusy";
            this._lblIBusy.Size = new System.Drawing.Size(42, 19);
            this._lblIBusy.Text = "BUSY";
            // 
            // _lblIStepThrough
            // 
            this._lblIStepThrough.BackColor = System.Drawing.Color.Gray;
            this._lblIStepThrough.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this._lblIStepThrough.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this._lblIStepThrough.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblIStepThrough.ForeColor = System.Drawing.Color.White;
            this._lblIStepThrough.Name = "_lblIStepThrough";
            this._lblIStepThrough.Size = new System.Drawing.Size(103, 19);
            this._lblIStepThrough.Text = "STEP-THROUGH";
            // 
            // _lblIStopped
            // 
            this._lblIStopped.BackColor = System.Drawing.Color.Gray;
            this._lblIStopped.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this._lblIStopped.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this._lblIStopped.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblIStopped.ForeColor = System.Drawing.Color.White;
            this._lblIStopped.Name = "_lblIStopped";
            this._lblIStopped.Size = new System.Drawing.Size(154, 19);
            this._lblIStopped.Text = "THE SESSION HAS ENDED";
            // 
            // _lblINoSession
            // 
            this._lblINoSession.BackColor = System.Drawing.Color.Gray;
            this._lblINoSession.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this._lblINoSession.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this._lblINoSession.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblINoSession.ForeColor = System.Drawing.Color.White;
            this._lblINoSession.Name = "_lblINoSession";
            this._lblINoSession.Size = new System.Drawing.Size(168, 19);
            this._lblINoSession.Text = "WAITING FOR POPULATION";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.debugToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(1070, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._btnStart3,
            this._btnStep3,
            this._btnPlay3,
            this._btnPause3,
            this._btnStop3,
            this._btnBreakpoint3,
            this.toolStripMenuItem1,
            this._btnEditParameters3,
            this._chkAttendedMode3});
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.debugToolStripMenuItem.Text = "&Debug";
            // 
            // _btnStart3
            // 
            this._btnStart3.Image = global::EvoveUi.Properties.Resources.NewBuildDefinition_8952;
            this._btnStart3.Name = "_btnStart3";
            this._btnStart3.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._btnStart3.Size = new System.Drawing.Size(184, 22);
            this._btnStart3.Text = "&Create";
            this._btnStart3.Click += new System.EventHandler(this._btnStart_Click);
            // 
            // _btnStep3
            // 
            this._btnStep3.Image = global::EvoveUi.Properties.Resources.Step_16xLG;
            this._btnStep3.Name = "_btnStep3";
            this._btnStep3.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this._btnStep3.Size = new System.Drawing.Size(184, 22);
            this._btnStep3.Text = "&Step";
            this._btnStep3.Click += new System.EventHandler(this._btnStep_Click);
            // 
            // _btnPlay3
            // 
            this._btnPlay3.Image = global::EvoveUi.Properties.Resources.PlayHS;
            this._btnPlay3.Name = "_btnPlay3";
            this._btnPlay3.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this._btnPlay3.Size = new System.Drawing.Size(184, 22);
            this._btnPlay3.Text = "&Run";
            this._btnPlay3.Click += new System.EventHandler(this._btnPlay_Click);
            // 
            // _btnPause3
            // 
            this._btnPause3.Image = global::EvoveUi.Properties.Resources.PauseHS;
            this._btnPause3.Name = "_btnPause3";
            this._btnPause3.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this._btnPause3.Size = new System.Drawing.Size(184, 22);
            this._btnPause3.Text = "&Pause";
            this._btnPause3.Click += new System.EventHandler(this._btnPause_Click);
            // 
            // _btnStop3
            // 
            this._btnStop3.Image = global::EvoveUi.Properties.Resources.StopHS;
            this._btnStop3.Name = "_btnStop3";
            this._btnStop3.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this._btnStop3.Size = new System.Drawing.Size(184, 22);
            this._btnStop3.Text = "&Stop";
            this._btnStop3.Click += new System.EventHandler(this._btnStop_Click);
            // 
            // _btnBreakpoint3
            // 
            this._btnBreakpoint3.Image = global::EvoveUi.Properties.Resources.BreakpointEnabled_6584_16x;
            this._btnBreakpoint3.Name = "_btnBreakpoint3";
            this._btnBreakpoint3.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this._btnBreakpoint3.Size = new System.Drawing.Size(184, 22);
            this._btnBreakpoint3.Text = "Add &breakpoint...";
            this._btnBreakpoint3.Click += new System.EventHandler(this._btnBreakpoint_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(181, 6);
            // 
            // _btnEditParameters3
            // 
            this._btnEditParameters3.Image = global::EvoveUi.Properties.Resources.EditorZone_6025;
            this._btnEditParameters3.Name = "_btnEditParameters3";
            this._btnEditParameters3.Size = new System.Drawing.Size(184, 22);
            this._btnEditParameters3.Text = "Edit &parameters...";
            this._btnEditParameters3.Click += new System.EventHandler(this._btnEditParameters_Click);
            // 
            // _chkAttendedMode3
            // 
            this._chkAttendedMode3.Image = global::EvoveUi.Properties.Resources.Stopapplyingcodechanges_6549;
            this._chkAttendedMode3.Name = "_chkAttendedMode3";
            this._chkAttendedMode3.Size = new System.Drawing.Size(184, 22);
            this._chkAttendedMode3.Text = "&Attended mode";
            this._chkAttendedMode3.Click += new System.EventHandler(this.attendedModeToolStripMenuItem_Click);
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(179, 281);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(698, 42);
            this.label28.TabIndex = 0;
            this.label28.Text = "Do not power off or unplug this computer.";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label28, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1056, 604);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 709);
            this.Controls.Add(this._tabTabControl);
            this.Controls.Add(this.toolStrip3);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "(Text to be completed at runtime)";
            this._tabTabControl.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.toolStrip15.ResumeLayout(false);
            this.toolStrip15.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.toolStrip9.ResumeLayout(false);
            this.toolStrip9.PerformLayout();
            this._lblPreviousPopulation2.ResumeLayout(false);
            this._lblPreviousPopulation2.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.toolStrip14.ResumeLayout(false);
            this.toolStrip14.PerformLayout();
            this.toolStrip5.ResumeLayout(false);
            this.toolStrip5.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.toolStrip6.ResumeLayout(false);
            this.toolStrip6.PerformLayout();
            this._tabFunctions.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.toolStrip8.ResumeLayout(false);
            this.toolStrip8.PerformLayout();
            this.toolStrip7.ResumeLayout(false);
            this.toolStrip7.PerformLayout();
            this._tabIndividual.ResumeLayout(false);
            this._tabIndividual.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.toolStrip16.ResumeLayout(false);
            this.toolStrip16.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.toolStrip13.ResumeLayout(false);
            this.toolStrip13.PerformLayout();
            this.toolStrip10.ResumeLayout(false);
            this.toolStrip10.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.toolStrip12.ResumeLayout(false);
            this.toolStrip12.PerformLayout();
            this.toolStrip11.ResumeLayout(false);
            this.toolStrip11.PerformLayout();
            this._pgLock.ResumeLayout(false);
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ListView _lstCurrent;
        private System.Windows.Forms.ListView _lstPrevious;
        private System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabControl _tabTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage _tabFunctions;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.TabPage _tabIndividual;
        private System.Windows.Forms.TreeView _tvwIndividual;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button _btnStart;
        private System.Windows.Forms.Button _btnStep;
        private System.Windows.Forms.Button _btnPlay;
        private System.Windows.Forms.Button _btnPause;
        private System.Windows.Forms.Button _btnStop;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton _btnCrossover2;
        private System.Windows.Forms.ToolStripButton _btnRun;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton _btnSwap2;
        private System.Windows.Forms.ToolStripButton _btnMutate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton _btnInsert;
        private System.Windows.Forms.ToolStripButton _btnClearNodeCache;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton _btnCrossover1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton _btnSwap1;
        private System.Windows.Forms.ToolStripButton _btnBrother;
        private System.Windows.Forms.ToolStripButton _btnMother;
        private System.Windows.Forms.ToolStripButton _btnFather;
        private System.Windows.Forms.ToolStripButton _btnSister;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton _btnCancel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripLabel _lblIndividual;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton _btnStart2;
        private System.Windows.Forms.ToolStripButton _btnStep2;
        private System.Windows.Forms.ToolStripButton _btnPlay2;
        private System.Windows.Forms.ToolStripButton _btnPause2;
        private System.Windows.Forms.ToolStripButton _btnBreakpoint2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView _lstNodeDetails;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ToolStripButton _btnShowSyntax;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListView _lstSyntaxDetails;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ToolStripStatusLabel _lblLastLog;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripLabel _lblPopulation;
        private System.Windows.Forms.ToolStrip _lblPreviousPopulation2;
        private System.Windows.Forms.ToolStripLabel _lblPreviousPopulation;
        private System.Windows.Forms.ToolStrip toolStrip6;
        private System.Windows.Forms.ToolStripLabel _lblLog;
        private System.Windows.Forms.ToolStrip toolStrip7;
        private System.Windows.Forms.ToolStripLabel _lblSyntax;
        private System.Windows.Forms.ToolStripLabel toolStripLabel8;
        private System.Windows.Forms.ToolStripButton _btnHistory;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton _btnDetails;
        private System.Windows.Forms.ToolStripLabel toolStripLabel13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label _lblNotEvaluated;
        private System.Windows.Forms.Label _lblEvaluated;
        private System.Windows.Forms.Label _lblEvaluated2;
        private System.Windows.Forms.Label _lblDeferred;
        private System.Windows.Forms.Label _lblFlagged;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label _lblDataType;
        private System.Windows.Forms.Label _lblProvider;
        private System.Windows.Forms.Label _lblReceiver;
        private System.Windows.Forms.ToolStrip toolStrip8;
        private System.Windows.Forms.ToolStripLabel toolStripLabel9;
        private System.Windows.Forms.ToolStripButton _chkProviders;
        private System.Windows.Forms.ToolStripButton _chkReceivers;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.Button _btnEnableCaching;
        private System.Windows.Forms.ToolStripButton _btnReorder;
        private System.Windows.Forms.ToolStripButton _btnClearTreeCache;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripLabel _txtSyntax;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button _btnEditParameters;
        private System.Windows.Forms.ToolStripButton _btnEditParameters2;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.TextBox _txtError;
        private System.Windows.Forms.ListView _lstProblemIndividuals;
        private System.Windows.Forms.ToolStrip toolStrip5;
        private System.Windows.Forms.ToolStripLabel _lblErrorTitle;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.CheckBox _chkUnattendedMode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ToolStripLabel toolStripLabel7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripLabel toolStripLabel10;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.ListView _lstPreviousIndividuals;
        private System.Windows.Forms.ToolStrip toolStrip9;
        private System.Windows.Forms.ToolStripLabel toolStripLabel11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripLabel toolStripLabel12;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _btnStart3;
        private System.Windows.Forms.ToolStripMenuItem _btnStep3;
        private System.Windows.Forms.ToolStripMenuItem _btnPlay3;
        private System.Windows.Forms.ToolStripMenuItem _btnPause3;
        private System.Windows.Forms.ToolStripMenuItem _btnStop3;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem _chkAttendedMode3;
        private System.Windows.Forms.ToolStripMenuItem _btnEditParameters3;
        private System.Windows.Forms.ToolStripMenuItem _btnBreakpoint3;
        private System.Windows.Forms.ToolStripButton _btnStop2;
        private System.Windows.Forms.Button _btnBreakpoint;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ToolStrip toolStrip10;
        private System.Windows.Forms.ToolStripLabel toolStripLabel14;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripLabel toolStripLabel15;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TabPage tabPage5;
        private MCharting.MChart _plot;
        private System.Windows.Forms.ToolStrip toolStrip11;
        private System.Windows.Forms.ToolStripLabel toolStripLabel16;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripLabel toolStripLabel17;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStrip toolStrip15;
        private System.Windows.Forms.ToolStripLabel toolStripLabel25;
        private System.Windows.Forms.ToolStripLabel toolStripLabel26;
        private System.Windows.Forms.ToolStrip toolStrip14;
        private System.Windows.Forms.ToolStripLabel toolStripLabel24;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator27;
        private System.Windows.Forms.ToolStripLabel toolStripLabel18;
        private System.Windows.Forms.ToolStrip toolStrip16;
        private System.Windows.Forms.ToolStripLabel toolStripLabel27;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator28;
        private System.Windows.Forms.ToolStripLabel toolStripLabel28;
        private System.Windows.Forms.ToolStrip toolStrip13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator25;
        private System.Windows.Forms.ToolStripLabel toolStripLabel22;
        private System.Windows.Forms.ToolStripLabel toolStripLabel23;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator26;
        private System.Windows.Forms.ToolStrip toolStrip12;
        private System.Windows.Forms.ToolStripLabel toolStripLabel19;
        private System.Windows.Forms.ToolStripLabel toolStripLabel20;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripLabel toolStripLabel21;
        private System.Windows.Forms.ToolStripButton _chkTruncate;
        private System.Windows.Forms.ToolStripButton _chkAll;
        private System.Windows.Forms.ToolStripButton _chkMean;
        private System.Windows.Forms.ToolStripButton _chkLowest;
        private System.Windows.Forms.ToolStripButton _chkHighest;
        private System.Windows.Forms.ToolStripButton _chkElite;
        private System.Windows.Forms.ToolStripButton _chkExecution;
        private System.Windows.Forms.ToolStripButton _chkTree;
        private System.Windows.Forms.ToolStripButton _chkFitness;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.ToolStripButton _btnRefreshPlot;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripButton _btnDebug;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripButton _btnClearLog;
        private System.Windows.Forms.ToolStripButton _btnRefresh;
        private System.Windows.Forms.ToolStripButton _chkSimpleView;
        private System.Windows.Forms.ToolStripButton _btnCollapse;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripButton _btnIgnoreError;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripButton _btnExpand;
        private System.Windows.Forms.ToolStripStatusLabel _lblIAutomatic;
        private System.Windows.Forms.ToolStripStatusLabel _lblIError;
        private System.Windows.Forms.ToolStripStatusLabel _lblIStopping;
        private System.Windows.Forms.ToolStripStatusLabel _lblIStopped;
        private System.Windows.Forms.ToolStripStatusLabel _lblINoSession;
        private System.Windows.Forms.ToolStripStatusLabel _lblIStepThrough;
        private System.Windows.Forms.ToolStripStatusLabel _lblIPaused;
        private System.Windows.Forms.ToolStripStatusLabel _lblIBusy;
        private System.Windows.Forms.ToolStripStatusLabel _lblIDebugging;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton _chkAutoPlot;
        private System.Windows.Forms.ToolStripButton _chkRound;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator30;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripButton _chkYFitness;
        private System.Windows.Forms.ToolStripButton _chkYSize;
        private System.Windows.Forms.ToolStripButton _chkYTime;
        private System.Windows.Forms.ToolStripButton _chkYRound;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator32;
        private System.Windows.Forms.TabPage _pgLock;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label28;
    }
}

