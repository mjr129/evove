﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvoveUi
{
    public partial class FrmBreakpoint : Form
    {
        private int Result;

        public static int Show(Form owner, int current)
        {
            using (FrmBreakpoint frm = new FrmBreakpoint())
            {
                frm._numRound.Value = current;
                frm.ShowDialog();

                return frm.Result;
            }
        }

        public FrmBreakpoint()
        {  
            InitializeComponent();
            this.Icon = Evove.Properties.Resources.Icon;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Result = (int)_numRound.Value;
            DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Result = -1;
            DialogResult = DialogResult.OK;
        }
    }
}
