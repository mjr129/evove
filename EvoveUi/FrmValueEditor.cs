﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Evove;
using Evove.Attributes;
using Evove.Blocks;

namespace EvoveUi
{
    public partial class FrmValueEditor : Form
    {
        readonly Type[] valids = new Type[] { typeof( double ), typeof( int ) };
        private readonly Font _italicFont = new Font( "Consolas", 9, FontStyle.Italic );
        private readonly Font _smallFont = new Font( "Consolas", 9, FontStyle.Regular );
        private readonly ESet _set;

        List<Label> _labels = new List<Label>();
        List<NumericUpDown> _upDowns = new List<NumericUpDown>();
        private object _result;

        public enum ESet
        {
            FitnessFunction,
            GpExtension,
            Object
        }

        public static bool Show( IWin32Window owner, ESet set, IEnumerable<Type> which, out object result )
        {
            using (FrmValueEditor frm = new FrmValueEditor( which, set ))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    result = frm._result;
                    return true;
                }

                result = null;
                return false;
            }
        }

        private FrmValueEditor( IEnumerable<Type> which, ESet set )
        {
            InitializeComponent();
            this.Icon = Evove.Properties.Resources.Icon;
            _set = set;

            switch (_set)
            {
                case ESet.FitnessFunction:
                    listView1.Columns.Add( "Assembly" );
                    listView1.Columns.Add( "Parameters" );
                    listView1.Columns.Add( "Fitness function" );
                    listView1.Columns.Add( "Additional extensions" );
                    listView1.Columns.Add( "GP functions provided" );
                    break;

                case ESet.GpExtension:
                    listView1.Columns.Add( "Assembly" );
                    listView1.Columns.Add( "Parameters" );
                    listView1.Columns.Add( "Additional extensions" );
                    listView1.Columns.Add( "GP functions provided" );
                    break;

                case ESet.Object:
                    listView1.Columns.Add( "Assembly" );
                    listView1.Columns.Add( "Parameters" );
                    listView1.Columns.Add( "Functionality" );
                    break;
            }

            listView1.Items.Add( "(none)" );

            int count = 0;

            foreach (Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (!asm.GlobalAssemblyCache) // Ignore GACs (.NET framework)
                {
                    foreach (Type t in asm.GetTypes())
                    {
                        ListViewItem lvi;

                        switch (set)
                        {
                            case ESet.FitnessFunction:
                                var fitnessFunction = EvoveHelper.GetEvoveFitnessFunction(t);

                                if (fitnessFunction == null)
                                {
                                    continue;
                                }

                                lvi = new ListViewItem( t.NameOfType() );
                                lvi.UseItemStyleForSubItems = false;
                                lvi.ForeColor = Color.Blue;

                                lvi.SubItems.Add( t.Assembly.GetName().Name );   
                                lvi.SubItems[1].ForeColor = Color.Silver;

                                lvi.SubItems.Add( GetCtorArgs( t ) );
                                lvi.SubItems[2].Font = _italicFont;
                                lvi.SubItems[2].ForeColor = Color.Silver;

                                lvi.SubItems.Add( fitnessFunction.Name );
                                lvi.SubItems[3].Font = _smallFont;
                                lvi.SubItems[3].ForeColor = Color.Silver;

                                lvi.SubItems.Add( GetExtensions( t ) );
                                lvi.SubItems[4].Font = _italicFont;
                                lvi.SubItems[4].ForeColor = Color.Green;

                                lvi.SubItems.Add( GetMethods( null, t ) );
                                lvi.SubItems[5].Font = _smallFont;
                                lvi.SubItems[5].ForeColor = Color.Violet;
                                break;

                            case ESet.GpExtension:
                                if (!EvoveHelper.IsEvoveExtension( t ))
                                {
                                    continue;
                                }

                                lvi = new ListViewItem( t.NameOfType() );
                                lvi.UseItemStyleForSubItems = false;
                                lvi.ForeColor = Color.Green;

                                lvi.SubItems.Add( t.Assembly.GetName().Name );   
                                lvi.SubItems[1].ForeColor = Color.Silver;

                                lvi.SubItems.Add( GetCtorArgs( t ) );
                                lvi.SubItems[2].Font = _italicFont;
                                lvi.SubItems[2].ForeColor = Color.Silver;

                                lvi.SubItems.Add( GetExtensions( t ) );
                                lvi.SubItems[3].Font = _italicFont;
                                lvi.SubItems[3].ForeColor = Color.Green;

                                lvi.SubItems.Add( GetMethods( null, t ) );
                                lvi.SubItems[4].Font = _smallFont;
                                lvi.SubItems[4].ForeColor = Color.Violet;
                                break;

                            case ESet.Object:
                                if (t.IsAbstract || !which.Any(z=> z.IsAssignableFrom( t )))
                                {
                                    continue;
                                }

                                lvi = new ListViewItem( t.NameOfType() );
                                lvi.UseItemStyleForSubItems = false;
                                lvi.ForeColor = Color.Olive;

                                lvi.SubItems.Add( t.Assembly.GetName().Name );
                                lvi.SubItems[1].Font = _italicFont;
                                lvi.SubItems[1].ForeColor = Color.Silver;

                                lvi.SubItems.Add( GetCtorArgs( t ) );
                                lvi.SubItems[2].Font = _italicFont;
                                lvi.SubItems[2].ForeColor = Color.Silver;

                                lvi.SubItems.Add( GetMethods( which.First(z=> z.IsAssignableFrom( t )), t ) );
                                lvi.SubItems[3].Font = _smallFont;
                                lvi.SubItems[3].ForeColor = Color.Violet;
                                break;

                            default:
                                throw new InvalidOperationException( "Invalid switch." );
                        }

                        lvi.Tag = t;
                        listView1.Items.Add( lvi );
                        count++;
                    }
                }
            }

            listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            if (count == 0)
            {
                MessageBox.Show( this, "Couldn't find any compatible data.\r\n\r\nPlease check:\r\n* Any necessary assemblies have been loaded first (either in your code or through the File menu of the editor).\r\n* Any custom classes or functions have been marked with the correct attributes.\r\n* The current configuration expects the type of data you are trying to enter." );
            }
        }                 

        private string GetCtorArgs( Type t )
        {
            if (t.IsAbstract && t.IsSealed)
            {
                return "(static)";
            }

            var ctor = GetCurrentConstructor( t );

            if (ctor != null)
            {
                return string.Join( ", ", ctor.GetParameters().Select( z => z.Name ) );
            }
            else
            {
                return "N/A";
            }
        }

        private string GetExtensions( Type t )
        {
            return string.Join( ", ", t.GetCustomAttributes<EvoveExtendsAttribute>().Select( z => z.ToString() ) );
        }

        private static string GetMethods( Type which, Type t )
        {
            List<string> methods = new List<string>();
            InterfaceMapping map =( which == null|| !which.IsInterface)  ? new InterfaceMapping() : t.GetInterfaceMap( which );

            foreach (var method in t.GetMethods( SyntaxManager.BINDING_FLAGS ))
            {
                if (method.DeclaringType != typeof( object ))
                {
                    var attr = method.GetCustomAttribute<EvoveMethodAttribute>();
                    if (attr  != null && attr.Role == EMethod.FitnessFunction)
                {
                        if (map.InterfaceMethods != null && map.TargetMethods.Contains( method ))
                        {
                            methods.Add( method.Name + "*" );
                        }
                        else
                        {
                            methods.Add( method.Name );
                        }
                    }
                }
            }

            return string.Join( ", ", methods );
        }

        ConstructorInfo GetCurrentConstructor( Type t )
        {
            ConstructorInfo[] ci = t.GetConstructors();

            if (ci.Length == 0)
            {
                return null;
            }

            return ci[0];
        }

        private void listBox1_SelectedIndexChanged( object sender, EventArgs e )
        {
            ClearBoxes();

            if (listView1.SelectedIndices.Count == 0)
            {
                _btnOk.Enabled = false;
                return;
            }
            else
            {
                Type x = (Type)listView1.SelectedItems[0].Tag;

                if (x == null || (x.IsAbstract && x.IsSealed))
                {
                    InvalidSelection( true );
                    return;
                }

                ConstructorInfo ci = GetCurrentConstructor( x );

                if (ci == null)
                {
                    InvalidSelection();
                    return;
                }

                ParameterInfo[] pi = ci.GetParameters().Where( z => valids.Contains( z.ParameterType ) ).ToArray();
                int row = 0;
                tableLayoutPanel2.RowStyles.Clear();
                tableLayoutPanel2.RowCount = 0;

                foreach (ParameterInfo p in pi)
                {
                    Label label = new Label()
                    {
                        Text = p.Name,
                        Visible = true,
                        AutoSize = true,
                    };

                    _labels.Add( label );

                    NumericUpDown upDown = new NumericUpDown()
                    {
                        Dock = DockStyle.Top,
                        Visible = true,
                        AutoSize = false,
                        Minimum = decimal.MinValue,
                        Maximum = decimal.MaxValue,
                        Increment = 1.0m,
                        DecimalPlaces = 2,
                    };

                    _upDowns.Add( upDown );

                    tableLayoutPanel2.RowStyles.Add( new RowStyle( SizeType.AutoSize ) );
                    tableLayoutPanel2.Controls.Add( label, 0, row );
                    tableLayoutPanel2.Controls.Add( upDown, 1, row );

                    row++;
                }

                _btnOk.Enabled = true;
            }
        }

        private void ClearBoxes()
        {
            foreach (Label label in _labels)
            {
                tableLayoutPanel2.Controls.Remove( label );
                label.Dispose();
            }

            _labels.Clear();

            foreach (NumericUpDown upDown in _upDowns)
            {
                tableLayoutPanel2.Controls.Remove( upDown );
                upDown.Dispose();
            }

            _upDowns.Clear();
        }

        private void InvalidSelection( bool okenabled = false )
        {
            _labelWarning.Visible = !okenabled;
            _btnOk.Enabled = okenabled;
        }

        private void _btnOk_Click( object sender, EventArgs e )
        {
            Type selectedType = (Type)listView1.SelectedItems[0].Tag;

            if (selectedType == null)
            {
                _result = null;
                DialogResult = DialogResult.OK;
                return;
            }

            if (selectedType.IsSealed && selectedType.IsAbstract)
            {
                Debug.Assert( _set != ESet.Object );
                _result = new Factory<InstantiationArgs>( selectedType );
                DialogResult = DialogResult.OK;
                return;
            }

            ConstructorInfo ci = GetCurrentConstructor( selectedType );
            ParameterInfo[] pis = ci.GetParameters();

            List<object> args = new List<object>();

            int index = 0;

            for (int n = 0; n < pis.Length; n++)
            {
                ParameterInfo pi = pis[n];

                if (valids.Contains( pi.ParameterType ))
                {
                    args.Add( Convert.ChangeType( _upDowns[index].Value, pi.ParameterType ) );
                    index++;
                }
                else if (n != 0 || pi.ParameterType != typeof( InstantiationArgs ))
                {
                    args.Add( null );
                }
            }

            if (_set == ESet.FitnessFunction || _set == ESet.GpExtension)
            {
                _result = new Factory<InstantiationArgs>( selectedType, args.ToArray() );
            }
            else
            {
                try
                {
                    _result = ci.Invoke( args.ToArray() );
                }
                catch (Exception ex)
                {
                    while (ex.InnerException != null)
                    {
                        ex = ex.InnerException;
                    }

                    MessageBox.Show( this, ex.Message );
                    return;
                }
            }

            DialogResult = DialogResult.OK;
            return;
        }

        private void listView1_ItemActivate( object sender, EventArgs e )
        {
            _btnOk.PerformClick();
        }
    }
}
