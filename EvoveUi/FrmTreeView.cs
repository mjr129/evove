﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Evove;

namespace EvoveUi
{
    public partial class FrmTreeView : Form
    {
        private object _target;

        public static void Show(IWin32Window owner, object target)
        {
            using (FrmTreeView frm = new FrmTreeView(target))
            {
                frm.ShowDialog(owner);
            }
        }

        public FrmTreeView()
        {
            InitializeComponent();
            this.Icon = Evove.Properties.Resources.Icon;

            Text = EvoveHelper.PROGRAM_NAME + " - Object inspector";
        }

        public FrmTreeView(object target)
            : this()
        {
            this._target = target;

            this.treeView1.Nodes.Add(AddTreeNode("", target, EvoveHelper.NameOfType(target?.GetType())));
        }

        class TreeNodeData
        {
            public readonly object target;

            public TreeNodeData(object target)
            {
                this.target = target;
            }
        }

        object expandId = new object();

        private TreeNode AddTreeNode(string name, object target, string t)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = string.Empty;
            }
            else
            {
                name = " " + name;
            }

            if (target == null)
            {
                return new TreeNode(t + " " + name + " = (null)");
            }

            TreeNode node = new TreeNode(t + name + " = " + target.ToString());
            FieldInfo[] fieldInfo = target.GetType().GetFields();
            node.Tag = new TreeNodeData(target);

            if (fieldInfo.Length != 0)
            {
                TreeNode expander = new TreeNode();
                expander.Tag = expandId;
                node.Nodes.Add(expander);
            }

            return node;
        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            TreeNode n = e.Node;

            if (n.Nodes.Count != 1 || n.Nodes[0].Tag != expandId)
            {
                return;
            }

            n.Nodes.Clear();

            TreeNodeData tnd = (TreeNodeData)n.Tag;
            FieldInfo[] fields = tnd.target.GetType().GetFields();

            foreach (FieldInfo field in fields)
            {
                n.Nodes.Add(AddTreeNode(field.Name, field.GetValue(tnd.target), EvoveHelper.NameOfType(field.FieldType)));
            }

            if (typeof(IEnumerable).IsAssignableFrom(tnd.target.GetType()))
            {
                TreeNode cnode = new TreeNode("Collection");
                n.Nodes.Add(cnode);

                int i = -1;

                foreach (object x in (IEnumerable)tnd.target)
                {
                    cnode.Nodes.Add(AddTreeNode("[" + (++i) + "]", x, EvoveHelper.NameOfType(x?.GetType())));

                    if (i == 1000)
                    {
                        cnode.Nodes.Add("Stopped evaluating after " + (i + 1) + " items.");
                        break;
                    }
                }
            }
        }
    }
}
